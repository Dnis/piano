FROM ubuntu:focal

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y git g++ cmake

# openmp
RUN apt-get update && apt-get install -y libomp-dev

# armadillo
RUN mkdir /gits
WORKDIR "/gits"
RUN git clone --branch="10.6.x" https://gitlab.com/conradsnicta/armadillo-code.git
WORKDIR "/gits/armadillo-code"
RUN mkdir build
WORKDIR "/gits/armadillo-code/build"
RUN apt-get update && apt-get install -y libopenblas-dev liblapack-dev libarpack2-dev libsuperlu-dev
RUN cmake .. && make && make install

# ensmallen
WORKDIR /gits
RUN git clone https://github.com/mlpack/ensmallen.git
WORKDIR /gits/ensmallen
RUN mkdir build
WORKDIR /gits/ensmallen/build
RUN cmake .. && make install

# cereal
RUN apt-get update && apt-get install -y libcereal-dev

# mlpack
RUN apt-get update && apt-get install -y libboost-dev
WORKDIR /gits
RUN git clone https://github.com/mlpack/mlpack.git
WORKDIR /gits/mlpack
RUN mkdir build
WORKDIR /gits/mlpack/build
RUN cmake .. && make install

# cplex
RUN apt-get update && apt-get install -y default-jre
RUN mkdir /cplex
COPY ./required_files/cplex/cplex_installer.bin /cplex/cplex_installer.bin
COPY ./required_files/cplex/installer_properties /cplex/installer_properties
WORKDIR "/cplex"
RUN chmod u+x cplex_installer.bin
RUN ./cplex_installer.bin -f installer_properties

# python3
RUN apt-get update && apt-get install -y python3 python3-pip
RUN pip3 install --upgrade pip
RUN pip3 install numpy scikit-learn

# Eigen3
RUN apt-get update && apt-get install -y libeigen3-dev

ENV CTEST_OUTPUT_ON_FAILURE=1

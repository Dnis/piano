// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <piano.h>

int main() {
    // Assume we have some method generating a Subset object from some linear regression data. We then want to create
    // a Solution object from the generated subset. Examine also the files in public/solution/ for more information.

    // First we generate some linear regression data with 100 observations, 30 variables, and 10 true predictors
    piano::SyntheticDataGenerator generator(100, 30, 10);
    generator.setSeed(476);
    piano::RegressionData data = generator.draw();

    // We normalize the data. The first two parameters are the matrix X and the response y. The third parameter
    // allows for a ridge regularization. In this case we set it to 0. After that we have to determine if we want
    // to normalize the data (yes) and if we want to have an intercept (no).
    piano::DataStandardization standardization(data.X, data.y, 0, true, false);

    // Now some method generates the following subset with some objective value
    piano::Subset subset{10, 12, 20, 29};
    double objectiveValue = 42;

    /* For a Solution object we have to submit the following parameters:
     * subset
     * fitted coefficients
     * objective value
     * estimated prediction error
     * time (in sec) the computation took
     * time (in sec) the complete method took (including preprocessing, warmstart, etc.)

     We are building a solution object via a SolutionBuilder.
    */

    // First, let us create the SolutionBuilder and add the subset and the objective value.
    piano::SolutionBuilder builder(*(standardization.getX()), *(standardization.getY()), *(standardization.getGamma()));

    builder.objectiveValue(objectiveValue).subset(subset);

    // Next, we construct the coefficients and add them to the builder
    piano::sls::LeastSquares leastSquares(standardization.getX(), standardization.getY(), standardization.getGamma());
    auto coefficients = leastSquares.computeCoefs(subset);

    /* The second argument determines if we are submitting the coefficients with zeros or without, i.e., if we submit a
     a vector (0, ..., 0, x, y, 0, z, 0, ..., 0) or a vector (x, y, z). In our case the least squares fit returns
     a vector without the zeros and only the coefficients belonging to the indices of the given subset. */
    builder.coefficients(coefficients, false);

    // Now let's compute the prediction error. For that we require a training function with which we can do a cross
    // validation.
    piano::cv::SubsetTraining training{};
    training.gamma = standardization.getGamma();

    double estimatedPredictionError = piano::cv::kFoldCrossValidation(*(standardization.getX()),
                                                                      *standardization.getY(),
                                                                      10, subset,
                                                                      piano::cv::mse, training);

    builder.estimatedPredictionError(estimatedPredictionError);

    // Finally, let's provide some arbitrary runtimes.
    builder.computationsRuntimeInSeconds(10).completeRuntimeInSeconds(12);

    // Now, we build our Solution object.
    piano::Solution solution = builder.build();
}

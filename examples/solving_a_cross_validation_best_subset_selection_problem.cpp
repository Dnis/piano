// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <piano.h>

int main() {
    // First we generate some linear regression data with 1000 observations, 20 variables, and 4 true predictors
    piano::SyntheticDataGenerator generator(1000, 20, 4);
    generator.setSeed(42);
    piano::RegressionData data = generator.draw();

    // We normalize the data. The first two parameters are the matrix X and the response y. The third parameter
    // allows for a ridge regularization. In this case we set it to 0. After that we have to determine if we want
    // to normalize the data (yes) and if we want to have an intercept (no).
    piano::DataStandardization standardization(data.X, data.y, 0, true, false);

    // Next, we create different best subset selection solvers: MIP, MaxMin, and PADM. MIP finds a global optimal solution
    // while the other two are heuristics
    piano::cvbss::Mip mip(*(standardization.getX()), *(standardization.getY()), *(standardization.getGamma()));

    // Run the best subset selection solver
    // For the cross-validation best subset selection we do not need to set a cardinality.
    piano::Solution sol = mip.run();

    // Retransform the solution to the state before the standardization.
    piano::Solution rsol = standardization.recoverSolution(sol);

    // Compare the computed subset with the true subset
    std::cout << "Subset computed by cross-validation best subset selection:     ";
    std::cout << piano::toPrettyString(rsol.subset, data.X.n_cols) << "\n";
    std::cout << "True Subset:                                  ";
    std::cout << piano::toPrettyString(data.subset, data.X.n_cols) << "\n\n";

    // Compare the computed coefficients with the true coefficients
    auto estimated_coefs = rsol.fullCoefficients();
    std::printf("| Estimated | True      |\n");
    for (size_t i = 0; i < data.X.n_cols; ++i) {
        std::printf("| %9.4f | %9.4f |\n", estimated_coefs[i], data.beta[i]);
    }
}

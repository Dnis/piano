// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <cstdlib>
#include <piano.h>
#include <iostream>

int main() {
    // In this example we are going to compute coefficients bounds for the best subset selection. That is, an optimal
    // best subset selection solution beta should always satisfy -bound[i] <= beta[i] <= bound[i].

    // First we generate some linear regression data with 100 observations, 20 variables, and 10 true predictors
    piano::SyntheticDataGenerator generator(100, 20, 10);
    piano::RegressionData data = generator.draw();

    // We normalize the data. The first two parameters are the matrix X and the response y. The third parameter
    // allows for a ridge regularization. After that we have to determine if we want
    // to normalize the data (yes) and if we want to have an intercept (no). We set the ridge parameter to 0.5.
    piano::DataStandardization standardization(data.X, data.y, 0.5, true, false);

    // Set up the bounds. The last argument sets the maximum number of non-zero coefficients. If this is unknown, it
    // should be set to the number of variables.
    piano::sls::CoefficientBounds bounds(standardization.getX(), standardization.getY(), standardization.getGamma(),
                                         10);

    // We also compute a best subset selection solution to compare to the bounds
    piano::bss::Mip mip(*(standardization.getX()), *(standardization.getY()), *(standardization.getGamma()));
    piano::Solution sol = mip.run(10);

    // Compare the computed coefficients with the bounds
    auto coefs = sol.fullCoefficients();
    std::printf("| abs(Coefficient) |            Bound |\n");
    for (size_t i = 0; i < data.X.n_cols; ++i) {
        std::printf("| %16.4f | %16.4f |\n", std::abs(coefs[i]), bounds.bound(i));
    }

    return EXIT_SUCCESS;
}


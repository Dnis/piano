// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <cstdlib>
#include <piano.h>
#include <iostream>

int main() {
    // First we generate some linear regression data with 100 observations, 30 variables, and 10 true predictors
    piano::SyntheticDataGenerator generator(100, 30, 10);
    generator.setSeed(476);
    piano::RegressionData data = generator.draw();

    // We normalize the data. The first two parameters are the matrix X and the response y. The third parameter
    // allows for a ridge regularization. In this case we set it to 0. After that we have to determine if we want
    // to normalize the data (yes) and if we want to have an intercept (no).
    piano::DataStandardization standardization(data.X, data.y, 0, true, false);

    // Next, we create different best subset selection solvers: MIP, MaxMin, and PADM. MIP finds a global optimal solution
    // while the other two are heuristics
    piano::bss::Mip mip(*(standardization.getX()), *(standardization.getY()), *(standardization.getGamma()));
    piano::bss::MaxMin maxMin(*(standardization.getX()), *(standardization.getY()), *(standardization.getGamma()));
    piano::bss::Padm padm(*(standardization.getX()), *(standardization.getY()), *(standardization.getGamma()));

    // We mute the solvers to reduce visual noise
    mip.mute(true);
    maxMin.mute(true);
    padm.mute(true);

    // Run the best subset selection solver and let it find a subset of size 5
    piano::Solution exactSol = mip.run(10);
    piano::Solution maxMinSol = maxMin.run(10);
    piano::Solution padmSol = padm.run(10);

    // Retransform the solution to the state before the standardization.
    piano::Solution exactRSol = standardization.recoverSolution(exactSol);
    piano::Solution maxMinRSol = standardization.recoverSolution(maxMinSol);
    piano::Solution padmRSol = standardization.recoverSolution(padmSol);

    // Compare the computed subset with the true subset
    std::cout << "True Subset:                   ";
    std::cout << piano::toPrettyString(data.subset, data.X.n_cols) << "\n";
    std::cout << "Subset computed by MIP:        ";
    std::cout << piano::toPrettyString(exactRSol.subset, data.X.n_cols) << "\n";
    std::cout << "Subset computed by MaxMin:     ";
    std::cout << piano::toPrettyString(maxMinRSol.subset, data.X.n_cols) << "\n";
    std::cout << "Subset computed by PADM:       ";
    std::cout << piano::toPrettyString(padmRSol.subset, data.X.n_cols) << "\n";

    // Compare the computed coefficients with the true coefficients
    auto mipEstimatedCoefs = exactRSol.fullCoefficients();
    auto maxMinEstimatedCoefs = maxMinRSol.fullCoefficients();
    auto padmEstimatedCoefs = padmRSol.fullCoefficients();
    std::printf("| MIP       | PADM      | MaxMin    | True      |\n");
    for (size_t i = 0; i < data.X.n_cols; ++i) {
        std::printf("| %9.4f | %9.4f | %9.4f | %9.4f |\n", mipEstimatedCoefs[i], padmEstimatedCoefs[i],
                    maxMinEstimatedCoefs[i], data.beta[i]);
    }


    return EXIT_SUCCESS;
}


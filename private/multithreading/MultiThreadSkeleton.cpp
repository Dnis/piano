// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by kreber on 15.10.20.
//

#include "MultiThreadSkeleton.h"
#include <TypeConversion.h>
#include <omp.h>
#ifdef PIANO_CBLAS_CAN_BE_INCLUDED
#include <cblas.h>
#else
#ifdef PIANO_CBLAS_OPENBLAS_CAN_BE_INCLUDED
#include <cblas-openblas.h>
#endif
#endif

size_t piano::MultiThreadSkeleton::getNumberOfThreads() const {
    return numberOfThreads;
}

void piano::MultiThreadSkeleton::setNumberOfThreads(size_t num) {
    numberOfThreads = num;
    limitNumberOfThreads(numberOfThreads, numberOfThreads);
}

void piano::MultiThreadSkeleton::startThreadControl() {
    ompPreviousNumberOfThreads = omp_get_num_threads();
#ifdef PIANO_BLAS_SET_N_THREADS
    blasPreviousNumberOfThreads = openblas_get_num_threads();
#endif
    limitNumberOfThreads(numberOfThreads, numberOfThreads);
}

void piano::MultiThreadSkeleton::endThreadControl() const {
    limitNumberOfThreads(ompPreviousNumberOfThreads, blasPreviousNumberOfThreads);
}

void piano::MultiThreadSkeleton::limitNumberOfThreads(size_t ompThreads, [[maybe_unused]] size_t blasThreads) const {
    omp_set_num_threads(
            safeCastToInt(ompThreads, "ompThreads", "number of threads for omp"));
#ifdef PIANO_BLAS_SET_N_THREADS
    openblas_set_num_threads(
            safeCastToInt(blasThreads, "blasThreads", "number of threads for BLAS"));
#endif
}

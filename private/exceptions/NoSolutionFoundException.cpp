//
// Created by dennis on 08.06.21.
//

#include "NoSolutionFoundException.h"

#include <utility>

piano::NoSolutionFoundException::NoSolutionFoundException(const std::string& message) {
    this->message = "No solution found: " + message;
}

const char *piano::NoSolutionFoundException::what() const noexcept {
    std::string return_message = "No solution found: " + message;
    return message.c_str();
}

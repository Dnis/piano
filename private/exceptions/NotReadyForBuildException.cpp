//
// Created by dennis on 04.10.21.
//

#include "NotReadyForBuildException.h"

const char *piano::NotReadyForBuildException::what() const noexcept {
    return exception::what();
}

piano::NotReadyForBuildException::NotReadyForBuildException(const std::unordered_map<std::string, bool> &mustHaves) {
    message = "You cannot call build without first calling:\n";
    for (const auto &item: mustHaves) {
        if (!item.second) {
            message += item.first + "\n";
        }
    }
}

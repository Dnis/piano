//
// Created by carina on 20.10.21.
//

#include "DualLowerLevelSolver.h"
#include <omp.h>
#include <vector>
#include <Conversion.h>

piano::cvbss::DualLowerLevelSolver::DualLowerLevelSolver(const std::vector<arma::mat> &trainingMatrices,
                     const std::vector<arma::vec> &trainingResponses, const std::vector<double> &lambdas) :
                     trainingMatrices(trainingMatrices), trainingResponses(trainingResponses),
                     lambdas(lambdas), numberOfPartitions(trainingResponses.size()) {}

std::vector<arma::vec> piano::cvbss::DualLowerLevelSolver::solve(const Subset &subset) {

    std::vector<arma::vec> alphas(numberOfPartitions);


    {
#pragma omp parallel num_threads(numberOfThreads)
        {
#pragma omp for
            for (size_t i = 0; i < numberOfPartitions; ++i) {
                if (subset.size() > 0) {
                    alphas[i] = arma::solve(
                            lambdas[i] * trainingMatrices[i].cols(arma::conv_to<arma::uvec>::from(subset))
                            * trainingMatrices[i].cols(arma::conv_to<arma::uvec>::from(subset)).t()
                            + arma::eye(trainingMatrices[i].n_rows, trainingMatrices[i].n_rows), trainingResponses[i]);
                } else {
                    alphas[i] = arma::solve(
                            arma::eye(trainingMatrices[i].n_rows, trainingMatrices[i].n_rows), trainingResponses[i]);
                }
            }
        }
    }

    return alphas;
}
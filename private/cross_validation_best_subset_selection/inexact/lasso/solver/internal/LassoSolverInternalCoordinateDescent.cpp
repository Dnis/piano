// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 29.05.20.
//

#include "LassoSolverInternalCoordinateDescent.h"
#include <LinearAlgebra.h>
#include <Conversion.h>
#include <SquaredL2.h>
#include <CrossValidation.h>
#include <TimeMeasurement.h>
#include <GridSearch.h>
#include <LassoHelper.h>
#include <PartialSolutionBuilder.h>

piano::PartialSolution piano::cvbss::lasso::solver::internal::CoordinateDescent::run() {
    TimeMeasurement measurement1;
    TimeMeasurement measurement2;
    measurement1.start();
    activeSet.clear();
    for (size_t i = 0; i < p; ++i) {
        activeSet.push_back(i);
    }
    numberOfNonzeros = p;

    initGradientInformation();

    measurement2.start();
    while (completeCycle()) {}
    measurement2.end();

    double objectiveValue = piano::sls::squaredL2((*X) * beta - (*y)) + lambda / lambdaCorrection * norm(beta, 1);
    measurement1.end();

    PartialSolutionBuilder builder;
    builder.
            objectiveValue(objectiveValue).
            subset(::piano::stools::fromCoefs(beta)).
            provableOptimal(false).
            completeRuntimeInSeconds(measurement1.getDurationInSeconds()).
            computationsRuntimeInSeconds(measurement2.getDurationInSeconds());

    PartialSolution sol = builder.build();

    return sol;
}

double piano::cvbss::lasso::solver::internal::CoordinateDescent::softThresholdOperator(double z, double gamma) {
    return ((z > 0) && (gamma < std::abs(z))) ? z - gamma : (((z < 0) && (gamma < std::abs(z))) ? z + gamma : 0);
}

double piano::cvbss::lasso::solver::internal::CoordinateDescent::updateCoordinate(size_t j) {
    double covarianceUpdate = Xty[j] - activeSetCovarianceSums[j];
    return softThresholdOperator(1.0 / n * (covarianceUpdate + XColumnSquaredNorms[j] * beta[j]), 1.0 / n * lambda) /
           (1.0 / n * XColumnSquaredNorms[j]);
}

void
piano::cvbss::lasso::solver::internal::CoordinateDescent::updateGradientInformation(size_t i, double newCoordinate) {
    for (size_t j = 0; j < p; ++j) {
        double cov = getCovariance(i, j);
        activeSetCovarianceSums[j] += cov * (newCoordinate - beta[i]);
    }
}

void piano::cvbss::lasso::solver::internal::CoordinateDescent::setInitialBeta(const arma::vec &beta) {
    this->beta = beta;
}

void piano::cvbss::lasso::solver::internal::CoordinateDescent::updateDataForY() {
    Xty = X->t() * (*y);
    lambdaPathComputed = false;
}

void piano::cvbss::lasso::solver::internal::CoordinateDescent::updateDataForX() {
    updateDataForY();
    p = X->n_cols;
    covariances.clear();
    XColumnSquaredNorms = arma::vec(p);
    for (size_t i = 0; i < p; ++i) {
        XColumnSquaredNorms[i] = as_scalar(X->col(i).t() * X->col(i));
    }
    lambdaPathComputed = false;
}

void piano::cvbss::lasso::solver::internal::CoordinateDescent::initialize() {
    beta = arma::vec(X->n_cols, arma::fill::zeros);
    p = X->n_cols;
    n = X->n_rows;
    Xty = X->t() * (*y);
    numberOfNonzeros = p;
    activeSetCovarianceSums = arma::vec(p);

    XColumnSquaredNorms = arma::vec(p);
    for (size_t i = 0; i < p; ++i) {
        XColumnSquaredNorms[i] = as_scalar(X->col(i).t() * X->col(i));
    }
}

double piano::cvbss::lasso::solver::internal::CoordinateDescent::getCovariance(size_t i, size_t j) {
    size_t k, l;
    if (i < j) {
        k = i;
        l = j;
    } else {
        k = j;
        l = i;
    }
    auto it = covariances.find(std::make_pair(k, l));
    if (it == covariances.end()) {
        double cov = arma::as_scalar(X->col(k).t() * X->col(l));;
        covariances[std::make_pair(k, l)] = cov;
        return cov;
    }
    return it->second;
}

bool piano::cvbss::lasso::solver::internal::CoordinateDescent::isZero(double coord) const {
    return std::abs(coord) < tolerance;
}

void piano::cvbss::lasso::solver::internal::CoordinateDescent::initGradientInformation() {
    activeSetCovarianceSums = arma::vec(p, arma::fill::zeros);
    for (size_t k = 0; k < p; ++k) {
        if (!isZero(beta[k])) {
            for (size_t j = 0; j < p; ++j) {
                activeSetCovarianceSums[j] += getCovariance(j, k) * beta[k];
            }
        }
    }
}

bool piano::cvbss::lasso::solver::internal::CoordinateDescent::completeCycle() {
    activeSet.clear();
    for (size_t i = 0; i < p; ++i) {
        activeSet.push_back(i);
    }
    numberOfNonzeros = p;

    bool activeSetChanged = false;

    double infNormBeta = -1;
    double newCoordinate;
    while ((infNormBeta == -1) || (infNormBeta > tolerance)) {
        infNormBeta = 0;
        auto it = activeSet.begin();
        while (it != activeSet.end()) {
            newCoordinate = updateCoordinate(*it);
            if (isZero(newCoordinate)) newCoordinate = 0;
            updateGradientInformation(*it, newCoordinate);
            if (!activeSetChanged) {
                if ((newCoordinate == 0) && (!isZero(beta[*it]))) activeSetChanged = true;
                if ((std::abs(newCoordinate) > 0) && (isZero(beta[*it]))) activeSetChanged = true;
            }

            infNormBeta = std::max(infNormBeta, std::abs(beta[*it] - newCoordinate));
            beta[*it] = newCoordinate;

            if (newCoordinate == 0) {
                activeSet.erase(it++);
                numberOfNonzeros--;
            } else {
                ++it;
            }
        }
    }

    return activeSetChanged;
}

void piano::cvbss::lasso::solver::internal::CoordinateDescent::setLengthOfPath(size_t lengthOfPath) {
    LassoSolverSkeleton::lengthOfPath = lengthOfPath;
    lambdaPathComputed = false;
}

void piano::cvbss::lasso::solver::internal::CoordinateDescent::computeLambdaPath() {
    double lambdaMax = arma::norm(X->t() * (*y), "inf");
    arma::vec lambdas = 2.0 * y->size() * lambdaGrid(lengthOfPath, lambdaMax) * lambdaCorrection;

    beta = arma::zeros(p);

    lambdaPath.reset(new std::vector<double>(lengthOfPath));
    betaPath.reset(new std::vector<arma::vec>(lengthOfPath));
    subsetPath.reset(new std::vector<::piano::Subset>(lengthOfPath));
    nonzerosPath.reset(new std::vector<size_t>(lengthOfPath));

    for (size_t j = 0; j < lengthOfPath; ++j) {
        lambda = lambdas[j];
        run();
        (*lambdaPath)[j] = lambda / lambdaCorrection;
        (*betaPath)[j] = beta;
        (*subsetPath)[j] = piano::stools::fromCoefs(beta);
        (*nonzerosPath)[j] = (*subsetPath)[j].size();
    }
    lambdaPathComputed = true;
}

const std::vector<double> &piano::cvbss::lasso::solver::internal::CoordinateDescent::getLambdaPath() {
    if (!lambdaPathComputed) computeLambdaPath();
    return *lambdaPath;
}

const std::vector<arma::vec> &piano::cvbss::lasso::solver::internal::CoordinateDescent::getBetaPath() {
    if (!lambdaPathComputed) computeLambdaPath();
    return *betaPath;
}

const std::vector<::piano::Subset> &piano::cvbss::lasso::solver::internal::CoordinateDescent::getSubsetPath() {
    if (!lambdaPathComputed) computeLambdaPath();
    return *subsetPath;
}

const std::vector<size_t> &piano::cvbss::lasso::solver::internal::CoordinateDescent::getNonzerosPath() {
    if (!lambdaPathComputed) computeLambdaPath();
    return *nonzerosPath;
}

void piano::cvbss::lasso::solver::internal::CoordinateDescent::setLambda(double l) {
    lambda = l * lambdaCorrection;
}

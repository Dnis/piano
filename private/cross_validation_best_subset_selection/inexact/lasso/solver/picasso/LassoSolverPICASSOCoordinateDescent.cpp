// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 27.09.20.
//

#include "LassoSolverPICASSOCoordinateDescent.h"

#include <BinarySearch.h>
#include <memory>
#include <picasso/objective.hpp>
#include <picasso/solver_params.hpp>
#include <picasso/actgd.hpp>
#include <SquaredL2.h>
#include <Conversion.h>
#include <TimeMeasurement.h>
#include <Types.h>
#include <LassoHelper.h>
#include <TypeConversion.h>
#include <PartialSolutionBuilder.h>

piano::PartialSolution piano::cvbss::lasso::solver::picasso::CoordinateDescent::run() {
    TimeMeasurement measurement1;
    TimeMeasurement measurement2;
    measurement1.start();

    auto lambdas = arma::vec(1);
    lambdas[0] = lambda;

    assertCastable<int>(lambdas.size(), "lambdas.size()", "size of vector lambdas");
    param->set_lambdas(lambdas.memptr(), static_cast<int>(lambdas.size()));

    assertCastable<int>(X->n_rows, "X->n_rows", "number of observations");
    assertCastable<int>(X->n_cols, "X->n_cols", "number of variables");
    auto *objective = new ::picasso::GaussianNaiveUpdateObjective(X->memptr(),
                                                                  y->memptr(),
                                                                  static_cast<int>(X->n_rows),
                                                                  static_cast<int>(X->n_cols));
    auto *solver = new ::picasso::solver::ActGDSolver(objective, *param);
    measurement2.start();
    solver->solve();
    measurement2.end();

    const ::picasso::ModelParam &picassoSolution = solver->get_model_param(static_cast<int>(lambdas.size()) - 1);

    assertCastable<long>(p, "p", "number of variables");

    beta = arma::zeros(p);
    for (long j = 0; j < static_cast<long>(p); ++j) {
        beta[j] = picassoSolution.beta[j];
    }
    delete solver;


    double objectiveValue = ::piano::sls::squaredL2((*X) * beta - (*y)) + lambda / lambdaCorrection * norm(beta, 1);

    measurement1.end();

    PartialSolutionBuilder builder;
    builder.
            objectiveValue(objectiveValue).
            subset(::piano::stools::fromCoefs(beta)).
            provableOptimal(false).
            completeRuntimeInSeconds(measurement1.getDurationInSeconds()).
            computationsRuntimeInSeconds(measurement2.getDurationInSeconds());

    PartialSolution sol = builder.build();

    return sol;
}

void piano::cvbss::lasso::solver::picasso::CoordinateDescent::updateDataForX() {
    updateDataForY();
    p = X->n_cols;
    lambdaPathComputed = false;
    setupParams();
}

void piano::cvbss::lasso::solver::picasso::CoordinateDescent::updateDataForY() {
    lambdaPathComputed = false;
    setupParams();
}

void piano::cvbss::lasso::solver::picasso::CoordinateDescent::setTolerance(double tol) {
    CoordinateDescent::tolerance = tol;
}

void piano::cvbss::lasso::solver::picasso::CoordinateDescent::setupParams() {
    param = std::make_unique<::picasso::solver::PicassoSolverParams>();
    param->gamma = 0;
    param->reg_type = ::picasso::solver::L1;
    param->include_intercept = false;
    param->prec = tolerance;
    param->max_iter = maxIterations;
    param->num_relaxation_round = 3;
}

void piano::cvbss::lasso::solver::picasso::CoordinateDescent::setMaxIterations(size_t iter) {
    assertCastable<int>(iter, "iter", "maximum number of iterations");
    CoordinateDescent::maxIterations = static_cast<int>(iter);
    param->max_iter = static_cast<int>(iter);
}

void piano::cvbss::lasso::solver::picasso::CoordinateDescent::computeLambdaPath() {
    assertCastable<int>(y->size(), "y->size()", "size of y");
    double lambdaMax = 2.0 * static_cast<int>(y->size()) * arma::norm(X->t() * (*y), "inf") * lambdaCorrection;

    assertCastable<int>(lengthOfPath, "lenghtOfPath", "length of path");
    arma::vec lambdas = lambdaGrid(lengthOfPath, lambdaMax);
    param->set_lambdas(lambdas.memptr(), static_cast<int>(lengthOfPath));

    assertCastable<int>(X->n_rows, "X->n_rows", "number of observations");
    assertCastable<int>(X->n_cols, "X->n_cols", "number of variables");
    auto *objective = new ::picasso::GaussianNaiveUpdateObjective(X->memptr(),
                                                                  y->memptr(),
                                                                  static_cast<int>(X->n_rows),
                                                                  static_cast<int>(X->n_cols));
    auto *solver = new ::picasso::solver::ActGDSolver(objective, *param);

    solver->solve();

    lambdaPath = std::make_unique<std::vector<double>>(lengthOfPath);
    betaPath = std::make_unique<std::vector<arma::vec>>(lengthOfPath);
    subsetPath = std::make_unique<std::vector<::piano::Subset>>(lengthOfPath);
    nonzerosPath = std::make_unique<std::vector<size_t>>(lengthOfPath);

    // lengthOfPath already checked to be castable to int
    assertCastable<long>(p, "p", "number of variables");
    for (int i = 0; i < static_cast<int>(lengthOfPath); ++i) {
        const ::picasso::ModelParam &picassoSolution = solver->get_model_param(i);
        (*betaPath)[i] = arma::zeros(p);
        for (long j = 0; j < static_cast<long>(p); ++j) {
            (*betaPath)[i][j] = picassoSolution.beta[j];
        }
        (*lambdaPath)[i] = lambdas[i] / lambdaCorrection;
        (*subsetPath)[i] = piano::stools::fromCoefs((*betaPath)[i]);
        (*nonzerosPath)[i] = (*subsetPath)[i].size();
    }
    lambdaPathComputed = true;

    delete solver;
}

const std::vector<double> &piano::cvbss::lasso::solver::picasso::CoordinateDescent::getLambdaPath() {
    if (!lambdaPathComputed) computeLambdaPath();
    return *lambdaPath;
}

const std::vector<arma::vec> &piano::cvbss::lasso::solver::picasso::CoordinateDescent::getBetaPath() {
    if (!lambdaPathComputed) computeLambdaPath();
    return *betaPath;
}

void piano::cvbss::lasso::solver::picasso::CoordinateDescent::initialize() {
    beta = arma::vec(X->n_cols, arma::fill::zeros);
    p = X->n_cols;
    n = X->n_rows;

    assertCastable<int>(n, "n", "number of observations");
    lambdaCorrection = 1.0 / (2.0 * static_cast<int>(n));
    lambdaPathComputed = false;
    setupParams();
}

const std::vector<::piano::Subset> &piano::cvbss::lasso::solver::picasso::CoordinateDescent::getSubsetPath() {
    if (!lambdaPathComputed) computeLambdaPath();
    return *subsetPath;
}

void piano::cvbss::lasso::solver::picasso::CoordinateDescent::setLengthOfPath(size_t lengthOfPath) {
    LassoSolverSkeleton::lengthOfPath = lengthOfPath;
    lambdaPathComputed = false;
}

const std::vector<size_t> &
piano::cvbss::lasso::solver::picasso::CoordinateDescent::getNonzerosPath() {
    if (!lambdaPathComputed) computeLambdaPath();
    return *nonzerosPath;
}

void piano::cvbss::lasso::solver::picasso::CoordinateDescent::setLambda(double l) {
    lambda = l * lambdaCorrection;
}

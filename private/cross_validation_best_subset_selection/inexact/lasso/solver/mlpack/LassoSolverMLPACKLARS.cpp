// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 28.05.20.
//
#include <mlpack/core/cv/k_fold_cv.hpp>
#include <mlpack/methods/lars/lars.hpp>
#include <mlpack/core/cv/metrics/mse.hpp>
#include <mlpack/core/hpt/cv_function.hpp>
#include <mlpack/core/hpt/hpt.hpp>
#include <mlpack/core.hpp>
#include "LassoSolverMLPACKLARS.h"
#include <Types.h>
#include <SquaredL2.h>
#include <TimeMeasurement.h>
#include <KFoldCrossValidation.h>
#include <ErrorMetrics.h>
#include <omp.h>
#include <TrainingFunctions.h>
#include <Conversion.h>
#include <PartialSolutionBuilder.h>

using namespace mlpack::cv;
using namespace mlpack::regression;
using namespace mlpack::hpt;
using namespace arma;

using namespace arma;

piano::PartialSolution piano::cvbss::lasso::solver::mlpack::LARS::run() {
    TimeMeasurement measurement1;
    TimeMeasurement measurement2;
    measurement1.start();

    bool transposeData = true;
    bool useCholesky = false;

    measurement2.start();
    ::mlpack::regression::LARS lars(X->t(), y->t(), transposeData, useCholesky, lambda, 0);
    measurement2.end();

    beta = lars.Beta();

    double objectiveValue = piano::sls::squaredL2((*X) * beta - (*y)) + lambda / lambdaCorrection * norm(beta, 1);
    measurement1.end();

    PartialSolutionBuilder builder;
    builder.
            objectiveValue(objectiveValue).
            subset(::piano::stools::fromCoefs(beta)).
            completeRuntimeInSeconds(measurement1.getDurationInSeconds()).
            computationsRuntimeInSeconds(measurement2.getDurationInSeconds());

    PartialSolution sol = builder.build();

    return sol;
}

void piano::cvbss::lasso::solver::mlpack::LARS::computeLambdaPath() {
    ::mlpack::regression::LARS lars(X->t(), y->t(), true, false, lambda, 0);
    betaPath.reset(new std::vector<arma::vec>(lars.BetaPath()));
    lambdaPath.reset(new std::vector<double>(lars.LambdaPath()));
    subsetPath.reset(new std::vector<piano::Subset>(lars.LambdaPath().size()));
    nonzerosPath.reset(new std::vector<size_t>(lars.LambdaPath().size()));

    for (size_t j = 0; j < lars.LambdaPath().size(); ++j) {
        (*subsetPath)[j] = piano::stools::fromCoefs((*betaPath)[j]);
        (*nonzerosPath)[j] = (*subsetPath)[j].size();
    }
    lambdaPathComputed = true;
}

const std::vector<double> &piano::cvbss::lasso::solver::mlpack::LARS::getLambdaPath() {
    if (!lambdaPathComputed) computeLambdaPath();
    return *lambdaPath;
}

const std::vector<arma::vec> &piano::cvbss::lasso::solver::mlpack::LARS::getBetaPath() {
    if (!lambdaPathComputed) computeLambdaPath();
    return *betaPath;
}

const std::vector<::piano::Subset> &piano::cvbss::lasso::solver::mlpack::LARS::getSubsetPath() {
    if (!lambdaPathComputed) computeLambdaPath();
    return *subsetPath;
}

const std::vector<size_t> &piano::cvbss::lasso::solver::mlpack::LARS::getNonzerosPath() {
    if (!lambdaPathComputed) computeLambdaPath();
    return *nonzerosPath;
}

void piano::cvbss::lasso::solver::mlpack::LARS::initialize() {
    lambdaCorrection = 0.5;
}

void piano::cvbss::lasso::solver::mlpack::LARS::setLambda(double l) {
    lambda = lambdaCorrection * l;
}

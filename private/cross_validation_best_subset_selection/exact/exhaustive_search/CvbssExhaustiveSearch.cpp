// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

#include "CvbssExhaustiveSearch.h"
#include <CrossValidation.h>
#include <AllSubsets.h>
#include <LeastSquares.h>
#include <PartialSolutionBuilder.h>
#include <NotImplementedException.h>

using namespace piano::sls;
using namespace piano::cv;
using namespace piano::stools;

piano::cvbss::ExhaustiveSearch::ExhaustiveSearch(const arma::mat *X, const arma::vec *y, const arma::vec *gamma,
                                                 size_t numberOfPartitions) :
        piano::cvbss::Base<cv::SubsetTraining>(*X, *y, *gamma, numberOfPartitions) {}

piano::PartialSolution piano::cvbss::ExhaustiveSearch::computePartialSolution() {
    AllSubsets allSubsets(p);

    auto optSubset = kFoldCrossValidationGridSearch(numberOfThreads, originalX, originalY, numberOfPartitions,
                                                    allSubsets, mse,
                                                    getTraining());

    piano::sls::LeastSquares leastSquares(&originalX, &originalY, &originalGamma);
    auto leastSquaresSol = leastSquares.compute(optSubset);

    PartialSolutionBuilder builder;
    builder.subset(optSubset).
            objectiveValue(leastSquaresSol.objectiveValue).
            provableOptimal(true).
            computationsRuntimeInSeconds(-1).
            completeRuntimeInSeconds(-1);

    return builder.build();
}

piano::cvbss::ExhaustiveSearch::TrainingFunction piano::cvbss::ExhaustiveSearch::getTraining() const {
    TrainingFunction trainingFunction;
    trainingFunction.gamma = &originalGamma;
    return trainingFunction;
}

void piano::cvbss::ExhaustiveSearch::setTimelimit([[maybe_unused]] size_t timelimitInSeconds) {
    PIANO_TIMELIMIT_NOT_IMPLEMENTED(piano::cvbss::ExhaustiveSearch);
}

void piano::cvbss::ExhaustiveSearch::setIterationLimit([[maybe_unused]] size_t iterationLimit) {
    PIANO_ITERATIONLIMIT_NOT_IMPLEMENTED(piano::cvbss::ExhaustiveSearch);
}


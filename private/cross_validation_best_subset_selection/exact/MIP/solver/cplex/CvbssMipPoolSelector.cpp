// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 02.04.20.
//

#ifdef PIANO_USE_CPLEX

#include "cplex/CvbssMipPoolSelector.h"
#include <Types.h>
#include <CplexHelper.h>
#include <Conversion.h>

using namespace piano;
using namespace piano::stools;
using namespace piano::cplex;

size_t cvbss::pool_selector::cplex::Sparsest::select(const IloCplex &cplex, const IloBoolVarArray &z) {
    long bestSol = 0;
    Subset bestSubset = toSubset(extractVars(cplex, z, bestSol));

    for (long i = 0; i < cplex.getSolnPoolNsolns(); ++i) {
        Subset s1 = toSubset(extractVars(cplex, z, i));

        if (s1.size() < bestSubset.size()) {
            bestSol = i;
            bestSubset = toSubset(extractVars(cplex, z, i));
        }

        if (s1.size() == bestSubset.size()) {
            if (cplex.getObjValue(i) < cplex.getObjValue(bestSol)) {
                bestSol = i;
                bestSubset = toSubset(extractVars(cplex, z, i));
            }
        }
    }
    return bestSol;
}

size_t cvbss::pool_selector::cplex::BestObjective::select(const IloCplex &cplex, const IloBoolVarArray &z) {
    long bestSol = 0;
    Subset bestSubset = toSubset(extractVars(cplex, z, bestSol));

    for (long i = 0; i < cplex.getSolnPoolNsolns(); ++i) {
        if (cplex.getObjValue(i) < cplex.getObjValue(bestSol)) {
            bestSol = i;
            bestSubset = toSubset(extractVars(cplex, z, i));
        }
    }

    return bestSol;
}

#endif
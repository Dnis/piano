//
// Created by kreber on 03.08.21.
//

#include "Types.h"

std::string piano::toPrettyString(const piano::Subset &subset, size_t maxIndex) {
    std::string ret;
    for (size_t i = 0; i < maxIndex; ++i) {
        ret += '*';
    }
    for (const auto &index : subset) {
        ret[index] = '#';
    }
    return ret;
}

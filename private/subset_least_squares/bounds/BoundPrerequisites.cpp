// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/6/20.
//

#include "BoundPrerequisites.h"
#include <SquaredL2.h>
#include <Maxk.h>
#include <HandingOver.h>

using namespace arma;

piano::sls::BoundPrerequisites::BoundPrerequisites(const arma::mat *X, const arma::vec *y, double ridgeScalar, size_t k) :
        X(X),
        y(y),
        k(k),
        n(y->size()),
        p(X->n_cols) {
    handOver(*(this->X), *(this->y), ridgeScalar, &newXPtr, &newYPtr, &(this->ridgeScalar));
    Xty = X->t() * (*y);
}

void piano::sls::BoundPrerequisites::computeGramianMatrix() {
    mat I = eye(newXPtr->n_cols, newXPtr->n_cols);
    XXplusI = newXPtr->t() * (*newXPtr) + ridgeScalar * I;
    XXplusI_Inv = inv(XXplusI);
    gramianMatrixComputed = true;
}

const arma::mat *piano::sls::BoundPrerequisites::getXXplusI() {
    if (!gramianMatrixComputed) computeGramianMatrix();
    return &XXplusI;
}

const arma::mat *piano::sls::BoundPrerequisites::getXXplusIInv() {
    if (!gramianMatrixComputed) computeGramianMatrix();
    return &XXplusI_Inv;
}

const mat *piano::sls::BoundPrerequisites::getX() const {
    return newXPtr;
}

const vec *piano::sls::BoundPrerequisites::getY() const {
    return newYPtr;
}

double piano::sls::BoundPrerequisites::getRidgeScalar() const {
    return ridgeScalar;
}

size_t piano::sls::BoundPrerequisites::getN() const {
    return n;
}

size_t piano::sls::BoundPrerequisites::getP() const {
    return p;
}

const vec *piano::sls::BoundPrerequisites::getXty() const {
    return &Xty;
}

size_t piano::sls::BoundPrerequisites::getK() const {
    return k;
}

piano::sls::BoundPrerequisites::BoundPrerequisites(const arma::mat *X, const arma::vec *y, const arma::vec *gamma, size_t k) :
        X(X),
        y(y),
        gamma(gamma),
        k(k),
        n(y->size()),
        p(X->n_cols) {
    handOver(*(this->X), *(this->y), *(this->gamma), &XAlloc, &yAlloc, &newXPtr, &newYPtr, &ridgeScalar);
    Xty = X->t() * (*y);
}

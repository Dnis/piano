// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/18/20.
//

#include "DualCauchyBound.h"
#include <SquaredL2.h>
#include <Maxk.h>
#include <GridSearch.h>
#include <EigenvalueExtraction.h>

using namespace arma;

piano::sls::upper_bound::predicted_value::DualCauchyBound::DualCauchyBound(piano::sls::BoundPrerequisites *prerequisites)
        : prerequisites(prerequisites) {

}

struct BoundInput {
    const piano::sls::EigenvalueExtraction *eigenvalueExtraction;

    const mat *Xty;

    std::size_t p;
    std::size_t k;

    double percentExtraction;
};

double computeSinglePredictionBound(const BoundInput &input) {
    mat XTilde;
    vec yTilde;
    double r;
    input.eigenvalueExtraction->computeExtraction(input.percentExtraction, &XTilde, &yTilde, &r);
    double yyTilde = piano::sls::squaredL2(yTilde);

    std::vector<double> quadXty(input.p);
    for (std::size_t i = 0; i < input.p; ++i) {
        quadXty[i] = pow((*(input.Xty))[i], 2);
    }
    double greedyQuadXty = piano::sls::maxK(quadXty, input.k);

    // std::cout << "Bound: " << yyTilde - pow(yyTilde, 2) / (yyTilde + 1.0 / r * greedyQuadXty) << "\n";
    return yyTilde - pow(yyTilde, 2) / (yyTilde + 1.0 / r * greedyQuadXty);
}

void piano::sls::upper_bound::predicted_value::DualCauchyBound::computePredictionBound() {
    const size_t numGridPoints = 10;

    auto eigenvalueExtraction = piano::sls::EigenvalueExtraction(*(prerequisites->getX()), *(prerequisites->getY()),
                                                                 prerequisites->getRidgeScalar());

    std::vector<BoundInput> inputs(numGridPoints);

    for (size_t i = 0; i < numGridPoints; ++i) {
        inputs[i].percentExtraction = 0 + (i * 0.95 / (numGridPoints - 1));

        inputs[i].Xty = prerequisites->getXty();
        inputs[i].k = prerequisites->getK();
        inputs[i].p = prerequisites->getP();
        inputs[i].eigenvalueExtraction = &eigenvalueExtraction;
    }

    predictionBound = gridSearch(numberOfThreads, inputs, computeSinglePredictionBound);
    predictionBoundComputed = true;
}

double piano::sls::upper_bound::predicted_value::DualCauchyBound::bound() {
    if (!predictionBoundComputed) computePredictionBound();
    return predictionBound;
}

piano::sls::lower_bound::rss::DualCauchyBound::DualCauchyBound(piano::sls::BoundPrerequisites *prerequisites)
        : upper_bound::predicted_value::DualCauchyBound(prerequisites) {

}

double piano::sls::lower_bound::rss::DualCauchyBound::bound() {
    double yy = squaredL2(*(upper_bound::predicted_value::DualCauchyBound::prerequisites->getY()));
    return yy - upper_bound::predicted_value::DualCauchyBound::bound();
}

// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/18/20.
//

#include "TrivialBounds.h"
#include <SquaredL2.h>

piano::sls::upper_bound::predicted_value::TrivialBound::TrivialBound(piano::sls::BoundPrerequisites *prerequisites) {
    yy = squaredL2(*(prerequisites->getY()));
}

double piano::sls::upper_bound::predicted_value::TrivialBound::bound() const {
    return yy;
}

piano::sls::lower_bound::predicted_value::TrivialBound::TrivialBound([[maybe_unused]] piano::sls::BoundPrerequisites *prerequisites) {

}

double piano::sls::lower_bound::predicted_value::TrivialBound::bound() const {
    return 0;
}

piano::sls::upper_bound::rss::TrivialBound::TrivialBound([[maybe_unused]] piano::sls::BoundPrerequisites *prerequisites) {

}

double piano::sls::upper_bound::rss::TrivialBound::bound() const {
    return 0;
}

piano::sls::lower_bound::rss::TrivialBound::TrivialBound(piano::sls::BoundPrerequisites *prerequisites) {
    yy = squaredL2(*(prerequisites->getY()));
}

double piano::sls::lower_bound::rss::TrivialBound::bound() const {
    return yy;
}

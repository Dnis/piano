// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 31.08.20.
//

#include "../../../public/subset_least_squares/eigenvalue_extraction/EigenvalueExtraction.h"
#include <SquaredL2.h>

using namespace arma;

piano::sls::EigenvalueExtraction::EigenvalueExtraction(const arma::mat &X, const arma::vec &y, const arma::vec &gamma,
                                                       double percentExtraction) :
                                                       ridgeScalar(0),
                                                       percent(percentExtraction) {

    bool allZero = true;
    for (std::size_t i = 0; i < gamma.size(); ++i) {
        if (std::abs(gamma[i]) > 1e-7) {
            allZero = false;
            break;
        }
    }
    if (allZero) {
        this->X = &X;
        this->y = &y;
        n = X.n_rows;
        p = X.n_cols;
    } else {
        n = X.n_cols + X.n_rows;
        p = X.n_cols;
        XAlloc = std::make_unique<mat>(n, p);
        *XAlloc = join_cols(X, arma::sqrt(diagmat(gamma)));
        yAlloc = std::make_unique<vec>(n, fill::zeros);
        yAlloc->subvec(0, X.n_rows - 1) = y;
        this->X = XAlloc.get();
        this->y = yAlloc.get();
    }
    setNumberOfThreads(1);
}

piano::sls::EigenvalueExtraction::EigenvalueExtraction(const mat &X, const vec &y, double ridgeScalar,
                                                       double percentExtraction) : n(y.size()),
                                                                                   p(X.n_cols),
                                                                                   X(&X),
                                                                                   y(&y),
                                                                                   ridgeScalar(ridgeScalar),
                                                                                   percent(percentExtraction) {
    setNumberOfThreads(1);
}

const mat &piano::sls::EigenvalueExtraction::getXTilde() const {
    if (recomputeExtraction) computeExtraction();
    return XTilde;
}

const vec &piano::sls::EigenvalueExtraction::getYTilde() const {
    if (recomputeExtraction) computeExtraction();
    return yTilde;
}

double piano::sls::EigenvalueExtraction::getR() const {
    if (recomputeExtraction) computeExtraction();
    return r;
}

void piano::sls::EigenvalueExtraction::computeSVDValues() const {
    vec s;
    mat U;
    svd(U, s, V, *X);
    V = V.t();

    mat Sigma = zeros<mat>(n, p);
    for (std::size_t j = 0; j < p; ++j) {
        Sigma.at(j, j) = s[j];
    }
    mat Sigmat = Sigma.t();
    SigmatSigma = Sigma.t() * Sigma;

    minEigenvalue = (s % s).min();

    SigmatUty = Sigmat * U.t() * (*y);

    svdsComputed = true;
}

void piano::sls::EigenvalueExtraction::setPercentExtraction(double percent) {
    this->percent = percent;
    recomputeExtraction = true;
}

void piano::sls::EigenvalueExtraction::computeExtraction(double percent, arma::mat *XTilde, arma::vec *yTilde,
                                                         double *r) const {
    if (!svdsComputed) computeSVDValues();
    *r = percent * (minEigenvalue + ridgeScalar);

    if (((*r) != 0) || (percent < 1)) {
        mat SigmaTilde = sqrt(SigmatSigma + (ridgeScalar - (*r)) * eye(p, p));
        *XTilde = SigmaTilde * V;
        *yTilde = inv(SigmaTilde) * SigmatUty;
    } else {
        *XTilde = *X;
        *yTilde = *y;
    }
}

piano::sls::EigenvalueExtraction::EigenvalueExtraction(const mat &X, const vec &y, [[maybe_unused]] double ridgeScalar) :
        n(y.size()),
        p(X.n_cols),
        X(&X),
        y(&y),
        ridgeScalar(0) {
    setNumberOfThreads(1);
}

piano::sls::EigenvalueExtraction::EigenvalueExtraction(const mat &X, const vec &y, const vec &gamma) : n(y.size()),
                                                                                                       p(X.n_cols),
                                                                                                       ridgeScalar(0) {
    bool allZero = true;
    for (std::size_t i = 0; i < gamma.size(); ++i) {
        if (std::abs(gamma[i]) > 1e-7) allZero = false;
    }
    if (allZero) {
        this->X = &X;
        this->y = &y;
    } else {
        XAlloc = std::make_unique<mat>(n + p, p);
        *XAlloc = join_cols(X, diagmat(gamma));
        yAlloc = std::make_unique<vec>(n + p, fill::zeros);
        yAlloc->subvec(0, n - 1) = y;
        this->X = XAlloc.get();
        this->y = yAlloc.get();
    }
    setNumberOfThreads(1);
}

void piano::sls::EigenvalueExtraction::computeExtraction() const {
    if (!svdsComputed) computeSVDValues();
    r = percent * (minEigenvalue + ridgeScalar);

    if (r != 0) {
        mat SigmaTilde = sqrt(SigmatSigma + (ridgeScalar - r) * eye(p, p));
        XTilde = SigmaTilde * V;
        yTilde = inv(SigmaTilde) * SigmatUty;
    } else {
        XTilde = *X;
        yTilde = *y;
    }

    recomputeExtraction = false;
}

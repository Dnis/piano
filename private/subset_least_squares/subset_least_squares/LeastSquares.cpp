// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/4/20.
//

#include <SquaredL2.h>
#include "LeastSquares.h"
#include <TimeMeasurement.h>
#include <SolutionBuilder.h>

using namespace arma;
using namespace piano::sls;
using namespace piano;

LeastSquares::LeastSquares(const arma::mat *X, const arma::vec *y) :
        X(X),
        y(y),
        activationCosts(X->n_cols, fill::zeros),
        useArmaLeastSquaresSolver(true) {
    gammaImpl = std::make_unique<arma::vec>(X->n_cols, fill::zeros);
    gamma = gammaImpl.get();
}

LeastSquares::LeastSquares(const arma::mat *X, const arma::vec *y, double ridgeScalar) :
        X(X),
        y(y),
        activationCosts(X->n_cols, fill::zeros),
        useArmaLeastSquaresSolver(ridgeScalar == 0) {
    gammaImpl = std::make_unique<arma::vec>(X->n_cols);
    gammaImpl->fill(ridgeScalar);
    gamma = gammaImpl.get();
}

Solution LeastSquares::compute(piano::Subset &&subset) const {
    TimeMeasurement measurement1;
    TimeMeasurement measurement2;
    measurement1.start();
    measurement2.start();
    vec coefficients = computeCoefs(subset);
    measurement2.end();
    double rss;
    double rssWithRegularization;
    if (subset.empty()) {
        rss = squaredL2(*y);
        rssWithRegularization = rss + accumulateActivationCosts(subset);
    } else {
        rss = squaredL2(XS * coefficients - (*y));
        rssWithRegularization =
                rss + as_scalar(coefficients.t() * GS * coefficients) + accumulateActivationCosts(subset);
    }
    measurement1.end();
    SolutionBuilder builder(*X, *y, *gamma);
    builder.
            subset(subset).
            coefficients(coefficients, false).
            objectiveValue(rssWithRegularization).
            estimatedPredictionError(-1).
            provableOptimal(true).
            completeRuntimeInSeconds(measurement1.getDurationInSeconds()).
            computationsRuntimeInSeconds(measurement2.getDurationInSeconds());

    Solution sol = builder.build();
    return sol;
}

Solution LeastSquares::compute(const piano::Subset &subset) const {
    TimeMeasurement measurement1;
    TimeMeasurement measurement2;
    measurement1.start();
    measurement2.start();
    vec coefficients = computeCoefs(subset);
    measurement2.end();
    double rss;
    double rssWithRegularization;
    if (subset.empty()) {
        rss = squaredL2(*y);
        rssWithRegularization = rss + accumulateActivationCosts(subset);
    } else {
        rss = squaredL2(XS * coefficients - (*y));
        rssWithRegularization =
                rss + as_scalar(coefficients.t() * GS * coefficients) + accumulateActivationCosts(subset);
    }

    measurement1.end();
    SolutionBuilder builder(*X, *y, *gamma);
    builder.
            subset(subset).
            coefficients(coefficients, false).
            objectiveValue(rssWithRegularization).
            estimatedPredictionError(-1).
            provableOptimal(true).
            completeRuntimeInSeconds(measurement1.getDurationInSeconds()).
            computationsRuntimeInSeconds(measurement2.getDurationInSeconds());

    Solution sol = builder.build();
    return sol;
}

Solution LeastSquares::compute() const {
    TimeMeasurement measurement1;
    TimeMeasurement measurement2;
    measurement1.start();
    measurement2.start();
    vec coefficients = computeCoefs();
    measurement2.end();
    piano::Subset subset(X->n_cols);
    std::iota(subset.begin(), subset.end(), 0);
    double rss = squaredL2((*X) * coefficients - (*y));
    double rssWithRegularization =
            rss + as_scalar(coefficients.t() * G * coefficients) + accumulateActivationCosts(subset);

    measurement1.end();
    SolutionBuilder builder(*X, *y, *gamma);
    builder.
            subset(subset).
            coefficients(coefficients, false).
            objectiveValue(rssWithRegularization).
            estimatedPredictionError(-1).
            provableOptimal(true).
            completeRuntimeInSeconds(measurement1.getDurationInSeconds()).
            computationsRuntimeInSeconds(measurement2.getDurationInSeconds());
    Solution sol = builder.build();
    return sol;
}

double LeastSquares::eval(const arma::vec &coefs) const {
    double reg = 0;
    Subset subset{};
    subset.reserve(X->n_cols);
    for (size_t j = 0; j < gamma->size(); ++j) {
        reg += (*gamma)[j] * coefs[j] * coefs[j];
        if (std::abs(coefs[j]) > 1e-8) subset.push_back(j);
    }
    return squaredL2((*X) * coefs - (*y)) + reg + accumulateActivationCosts(subset);
}

LeastSquares::LeastSquares(const arma::mat *X, const arma::vec *y, const arma::vec *gamma) :
        X(X),
        y(y),
        gamma(gamma),
        activationCosts(X->n_cols, fill::zeros) {
    bool gammaAllZero = true;
    for (size_t j = 0; j < gamma->size(); ++j) {
        if (std::abs((*gamma)[j]) >= 1e-8) {
            gammaAllZero = false;
            break;
        }
    }
    if (gammaAllZero) useArmaLeastSquaresSolver = true;
}

arma::vec LeastSquares::computeCoefs(Subset &&subset) const {
    if (subset.empty()) {
        return zeros(subset.size());
    }
    computeGramians(subset);
    if (useArmaLeastSquaresSolver) {
        return solve(XS, *y);
    } else {
        return solve(XX + GS, XS.t() * (*y));
    }
}

arma::vec LeastSquares::computeCoefs(const Subset &subset) const {
    if (subset.empty()) {
        return zeros(subset.size());
    }
    computeGramians(subset);
    if (useArmaLeastSquaresSolver) {
        return solve(XS, *y);
    } else {
        return solve(XX + GS, XS.t() * (*y));
    }
}

arma::vec LeastSquares::computeCoefs() const {
    computeGramians();
    if (useArmaLeastSquaresSolver) {
        return solve(*X, *y);
    } else {
        return solve(XX + G, X->t() * (*y));
    }
}

void LeastSquares::computeGramians() const {
    XX = X->t() * (*X);
    G = diagmat(*gamma);
}

void LeastSquares::computeGramians(const Subset &subset) const {
    if (!subset.empty()) {
        uvec inds = conv_to<uvec>::from(subset);
        XS = X->cols(inds);
        XX = XS.t() * XS;
        G = diagmat(*gamma);
        GS = G.submat(inds, inds);
    }
}

void LeastSquares::setActivationCosts(const arma::vec &activationCosts) {
    this->activationCosts = activationCosts;
}

double LeastSquares::accumulateActivationCosts(const piano::Subset &subset) const {
    double ret = 0;
    for (const auto &index: subset) {
        ret += activationCosts[index];
    }
    return ret;
}

void LeastSquares::setActivationCosts(double scalarActivationCosts) {
    this->activationCosts = scalarActivationCosts * arma::ones(X->n_cols);
}

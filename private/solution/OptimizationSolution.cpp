// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by kreber on 16.10.20.
//

#include "OptimizationSolution.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

piano::OptimizationSolution::OptimizationSolution(size_t n, size_t p, bool fullCoefficients,
                                                  const arma::vec &coefficients,
                                                  const PartialSolution &partialSolution) :
        piano::PartialSolution(partialSolution),
        coefficients(fullCoefficients ? coefficients.elem(arma::conv_to<arma::uvec>::from(subset)) : coefficients),
        n(n),
        p(p) {

}

piano::OptimizationSolution::OptimizationSolution(size_t n, size_t p, bool fullCoefficients,
                                                  const arma::vec &coefficients, double objectiveValue,
                                                  const piano::Subset &subset, bool provableOptimal,
                                                  double computationsRuntimeInSeconds, double completeRuntimeInSeconds) :
        PartialSolution(objectiveValue, subset, provableOptimal, computationsRuntimeInSeconds, completeRuntimeInSeconds),
        coefficients(fullCoefficients ? coefficients.elem(arma::conv_to<arma::uvec>::from(subset)) : coefficients),
        n(n),
        p(p) {

}

piano::OptimizationSolution::OptimizationSolution(size_t n, size_t p, bool fullCoefficients,
                                                  const arma::vec &coefficients, double objectiveValue,
                                                  const piano::Subset &subset, bool provableOptimal,
                                                  double computationsRuntimeInSeconds, double completeRuntimeInSeconds,
                                                  double gap) :
        PartialSolution(objectiveValue, subset, provableOptimal, computationsRuntimeInSeconds, completeRuntimeInSeconds, gap),
        coefficients(fullCoefficients ? coefficients.elem(arma::conv_to<arma::uvec>::from(subset)) : coefficients),
        n(n),
        p(p) {

}

piano::OptimizationSolution::OptimizationSolution(size_t n, size_t p, bool fullCoefficients,
                                                  const arma::vec &coefficients, double objectiveValue,
                                                  const piano::Subset &subset, bool provableOptimal,
                                                  double computationsRuntimeInSeconds, double completeRuntimeInSeconds,
                                                  double gap, bool sanityCheck) :
        PartialSolution(objectiveValue, subset, provableOptimal, computationsRuntimeInSeconds, completeRuntimeInSeconds, gap,
                        sanityCheck),
        coefficients(fullCoefficients ? coefficients.elem(arma::conv_to<arma::uvec>::from(subset)) : coefficients),
        n(n),
        p(p) {

}

piano::OptimizationSolution::OptimizationSolution(size_t n, size_t p, bool fullCoefficients,
                                                  const arma::vec &coefficients, double objectiveValue,
                                                  const piano::Subset& subset, bool provableOptimal,
                                                  double computationsRuntimeInSeconds, double completeRuntimeInSeconds,
                                                  bool sanityCheck) :
        PartialSolution(objectiveValue, subset, provableOptimal, computationsRuntimeInSeconds, completeRuntimeInSeconds,
                        sanityCheck),
        coefficients(fullCoefficients ? coefficients.elem(arma::conv_to<arma::uvec>::from(subset)) : coefficients),
        n(n),
        p(p) {

}

arma::vec piano::OptimizationSolution::fullCoefficients() const {
    arma::vec ret = arma::zeros(p);
    size_t c = 0;
    for (const auto &it : subset) {
        ret[it] = coefficients[c++];
    }
    return ret;
}

#pragma GCC diagnostic pop
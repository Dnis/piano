//
// Created by dennis on 04.10.21.
//

#include "OptimizationSolutionBuilder.h"
#include <Conversion.h>

piano::OptimizationSolutionBuilder::OptimizationSolutionBuilder(size_t n, size_t p) {
    solution = std::make_unique<OptimizationSolution>();
    solution->n = n;
    solution->p = p;
}

piano::OptimizationSolutionBuilder &
piano::OptimizationSolutionBuilder::coefficients(const arma::vec &coefs, bool withZeros) {
    if (withZeros) {
        if (!mustHaves["subset"]) {
            solution->subset = stools::fromCoefs(coefs);
            mustHaves["subset"] = true;
        }
        solution->coefficients = coefs.elem(arma::conv_to<arma::uvec>::from(solution->subset));
    } else {
        solution->coefficients = coefs;
    }
    mustHaves["coefficients"] = true;
    return *this;
}

PIANO_OPTIMIZATION_SOLUTION_BUILDER_SETTER_IMPL(OptimizationSolutionBuilder)

piano::OptimizationSolution piano::OptimizationSolutionBuilder::build() const {
    assertMustHaves(mustHaves);
    return *solution;
}

piano::OptimizationSolutionBuilder &
piano::OptimizationSolutionBuilder::fromPartialSolution(const piano::PartialSolution &partialSolution) {
    solution->subset = partialSolution.subset;
    solution->subsetSize = partialSolution.subsetSize;
    solution->objectiveValue = partialSolution.objectiveValue;
    solution->provableOptimal = partialSolution.provableOptimal;
    solution->computationsRuntimeInSeconds = partialSolution.computationsRuntimeInSeconds;
    solution->completeRuntimeInSeconds = partialSolution.completeRuntimeInSeconds;
    solution->gap = partialSolution.gap;
    solution->sanity = partialSolution.sanity;
    solution->reachedTimelimit = partialSolution.reachedTimelimit;
    solution->reachedIterationCount = partialSolution.reachedIterationCount;

    mustHaves["subset"] = true;
    mustHaves["objectiveValue"] = true;
    mustHaves["computationsRuntimeInSeconds"] = true;
    mustHaves["completeRuntimeInSeconds"] = true;
    return *this;
}

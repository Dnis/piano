// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 11.09.20.
//

#include "PartialSolution.h"

#include <utility>

piano::PartialSolution::PartialSolution(double objectiveValue, Subset subset, bool provableOptimal,
                                        double computationsRuntimeInSeconds,
                                        double completeRuntimeInSeconds) :
        objectiveValue(objectiveValue),
        subset(std::move(subset)),
        subsetSize(this->subset.size()),
        provableOptimal(provableOptimal),
        computationsRuntimeInSeconds(computationsRuntimeInSeconds),
        completeRuntimeInSeconds(completeRuntimeInSeconds) {

}

piano::PartialSolution::PartialSolution(double objectiveValue, piano::Subset subset,
                                        bool provableOptimal, double computationsRuntimeInSeconds,
                                        double completeRuntimeInSeconds, double gap)  :
        objectiveValue(objectiveValue),
        subset(std::move(subset)),
        subsetSize(this->subset.size()),
        provableOptimal(provableOptimal),
        computationsRuntimeInSeconds(computationsRuntimeInSeconds),
        completeRuntimeInSeconds(completeRuntimeInSeconds),
        gap(gap) {

}

piano::PartialSolution::PartialSolution(double objectiveValue, piano::Subset subset,
                                        bool provableOptimal, double computationsRuntimeInSeconds,
                                        double completeRuntimeInSeconds, double gap, bool sanityCheck)  :
        objectiveValue(objectiveValue),
        subset(std::move(subset)),
        subsetSize(this->subset.size()),
        provableOptimal(provableOptimal),
        computationsRuntimeInSeconds(computationsRuntimeInSeconds),
        completeRuntimeInSeconds(completeRuntimeInSeconds),
        gap(gap),
        sanity(sanityCheck) {

}

piano::PartialSolution::PartialSolution(double objectiveValue, piano::Subset subset,
                                        bool provableOptimal, double computationsRuntimeInSeconds,
                                        double completeRuntimeInSeconds, bool sanityCheck)  :
        objectiveValue(objectiveValue),
        subset(std::move(subset)),
        subsetSize(this->subset.size()),
        provableOptimal(provableOptimal),
        computationsRuntimeInSeconds(computationsRuntimeInSeconds),
        completeRuntimeInSeconds(completeRuntimeInSeconds),
        sanity(sanityCheck) {

}

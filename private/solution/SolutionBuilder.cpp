//
// Created by dennis on 01.10.21.
//

#include "SolutionBuilder.h"
#include <Conversion.h>
#include <SquaredL2.h>
#include <NotReadyForBuildException.h>

piano::SolutionBuilder::SolutionBuilder(const arma::mat &X, const arma::vec &y, const arma::vec &gamma) :
        X(X),
        y(y),
        gamma(gamma) {
    solution = std::make_unique<Solution>();
    solution->n = this->X.n_rows;
    solution->p = this->X.n_cols;
    solution->gamma = this->gamma;
    solution->nullScore = sls::squaredL2(this->y);
}

piano::Solution piano::SolutionBuilder::build() const {
    assertMustHaves(mustHaves);
    solution->rss = sls::squaredL2(
            X.cols(arma::conv_to<arma::uvec>::from(solution->subset)) * solution->coefficients - y);
    return *solution;
}

PIANO_SOLUTION_BUILDER_SETTER_IMPL(SolutionBuilder)

piano::SolutionBuilder &piano::SolutionBuilder::coefficients(const arma::vec &coefs, bool withZeros) {
    if (withZeros) {
        if (!mustHaves["subset"]) {
            solution->subset = stools::fromCoefs(coefs);
            mustHaves["subset"] = true;
        }
        solution->coefficients = coefs.elem(arma::conv_to<arma::uvec>::from(solution->subset));
    } else {
        solution->coefficients = coefs;
    }
    mustHaves["coefficients"] = true;
    return *this;
}

piano::SolutionBuilder &piano::SolutionBuilder::fromPartialSolution(const piano::PartialSolution &partialSolution) {
    solution->subset = partialSolution.subset;
    solution->subsetSize = partialSolution.subsetSize;
    solution->objectiveValue = partialSolution.objectiveValue;
    solution->provableOptimal = partialSolution.provableOptimal;
    solution->computationsRuntimeInSeconds = partialSolution.computationsRuntimeInSeconds;
    solution->completeRuntimeInSeconds = partialSolution.completeRuntimeInSeconds;
    solution->gap = partialSolution.gap;
    solution->sanity = partialSolution.sanity;
    solution->reachedTimelimit = partialSolution.reachedTimelimit;
    solution->reachedIterationCount = partialSolution.reachedIterationCount;

    mustHaves["subset"] = true;
    mustHaves["objectiveValue"] = true;
    mustHaves["computationsRuntimeInSeconds"] = true;
    mustHaves["completeRuntimeInSeconds"] = true;
    return *this;
}

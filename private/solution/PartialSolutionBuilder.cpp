//
// Created by dennis on 04.10.21.
//

#include <NotReadyForBuildException.h>
#include "PartialSolutionBuilder.h"

piano::PartialSolutionBuilder::PartialSolutionBuilder() {
    solution = std::make_unique<PartialSolution>();
}

PIANO_PARTIAL_SOLUTION_BUILDER_SETTER_IMPL(PartialSolutionBuilder)

piano::PartialSolution piano::PartialSolutionBuilder::build() const {
    assertMustHaves(mustHaves);
    return *solution;
}

void piano::assertMustHaves(const std::unordered_map<std::string, bool> &mustHaves) {
    for (const auto &item: mustHaves) {
        if (!item.second) {
            throw NotReadyForBuildException(mustHaves);
        }
    }
}

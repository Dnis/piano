// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 25.03.20.
//

#include "Solution.h"

#include <utility>
#include <SquaredL2.h>
#include <TypeConversion.h>

using namespace arma;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

double piano::Solution::mse() const {
    assertCastable<int>(n, "n", "number of observations");
    return 1.0 / static_cast<int>(n) * rss;
}

arma::vec piano::Solution::fullCoefficients() const {
    arma::vec ret = arma::zeros(p);
    size_t c = 0;
    for (const auto &it : subset) {
        ret[it] = coefficients[c++];
    }
    return ret;
}

double piano::Solution::ridgeParameter() const {
    return gamma[p - 1];
}

piano::Solution::Solution(const arma::mat &X, const arma::vec &y, double ridgeParameter, const Subset &subset,
                          bool fullCoefs,
                          const arma::vec &coefs, double intercept, double objectiveValue,
                          double estimatedPredictionError,
                          bool provableOptimal, double completeRuntimeInSeconds, double computationsRuntimeInSeconds) :
        PartialSolution(objectiveValue, subset, provableOptimal, computationsRuntimeInSeconds,
                        completeRuntimeInSeconds),
        coefficients(fullCoefs ? coefs.elem(arma::conv_to<arma::uvec>::from(subset)) : coefs),
        n(X.n_rows),
        p(X.n_cols),
        estimatedPredictionError(estimatedPredictionError),
        gamma(p),
        intercept(intercept),
        nullScore(sls::squaredL2(y)) {
    gamma.fill(ridgeParameter);
    if (fullCoefs) rss = sls::squaredL2(X * coefs - y);
    else rss = sls::squaredL2(X.cols(conv_to<uvec>::from(subset)) * coefs - y);
}

piano::Solution::Solution(const arma::mat &X, const arma::vec &y, arma::vec gamma, const Subset &subset, bool fullCoefs,
                          const arma::vec &coefs, double intercept, double objectiveValue,
                          double estimatedPredictionError,
                          bool provableOptimal, double completeRuntimeInSeconds, double computationsRuntimeInSeconds) :
        PartialSolution(objectiveValue, subset, provableOptimal, computationsRuntimeInSeconds,
                        completeRuntimeInSeconds),
        coefficients(fullCoefs ? coefs.elem(arma::conv_to<arma::uvec>::from(subset)) : coefs),
        n(X.n_rows),
        p(X.n_cols),
        estimatedPredictionError(estimatedPredictionError),
        gamma(std::move(gamma)),
        intercept(intercept),
        nullScore(sls::squaredL2(y)) {
    if (fullCoefs) rss = sls::squaredL2(X * coefs - y);
    else rss = sls::squaredL2(X.cols(conv_to<uvec>::from(subset)) * coefficients - y);
}

arma::uvec piano::Solution::indicatorVector() const {
    uvec ret(p, fill::zeros);
    for (const auto &item : subset) {
        ret[item] = 1;
    }
    return ret;
}

piano::Solution::Solution(const mat &X, const vec &y, double ridgeParameter, const piano::Subset &subset,
                          bool fullCoefs, const vec &coefs, double intercept, double objectiveValue,
                          double estimatedPredictionError, bool provableOptimal, double completeRuntimeInSeconds,
                          double computationsRuntimeInSeconds, bool sanityCheck) :
        PartialSolution(objectiveValue, subset, subset.size(), provableOptimal, computationsRuntimeInSeconds,
                        completeRuntimeInSeconds, sanityCheck),
        coefficients(fullCoefs ? coefs.elem(arma::conv_to<arma::uvec>::from(subset)) : coefs),
        n(X.n_rows),
        p(X.n_cols),
        estimatedPredictionError(estimatedPredictionError),
        gamma(p),
        intercept(intercept),
        nullScore(sls::squaredL2(y)) {
    gamma.fill(ridgeParameter);
    if (fullCoefs) rss = sls::squaredL2(X * coefs - y);
    else rss = sls::squaredL2(X.cols(conv_to<uvec>::from(subset)) * coefs - y);
}

piano::Solution::Solution(const mat &X, const vec &y, arma::vec gamma, const piano::Subset &subset, bool fullCoefs,
                          const vec &coefs, double intercept, double objectiveValue, double estimatedPredictionError,
                          bool provableOptimal, double completeRuntimeInSeconds, double computationsRuntimeInSeconds,
                          bool sanityCheck) :
        PartialSolution(objectiveValue, subset, subset.size(), provableOptimal, computationsRuntimeInSeconds,
                        completeRuntimeInSeconds, sanityCheck),
        coefficients(fullCoefs ? coefs.elem(arma::conv_to<arma::uvec>::from(subset)) : coefs),
        n(X.n_rows),
        p(X.n_cols),
        estimatedPredictionError(estimatedPredictionError),
        gamma(std::move(gamma)),
        intercept(intercept),
        nullScore(sls::squaredL2(y)) {
    if (fullCoefs) rss = sls::squaredL2(X * coefs - y);
    else rss = sls::squaredL2(X.cols(conv_to<uvec>::from(subset)) * coefficients - y);
}

piano::Solution::Solution(const mat &X, const vec &y, arma::vec gamma, bool fullCoefs, const vec &coefs,
                          double intercept, double estimatedPredictionError,
                          const piano::PartialSolution &partialSolution) :
        PartialSolution(partialSolution),
        coefficients(fullCoefs ? coefs.elem(arma::conv_to<arma::uvec>::from(subset)) : coefs),
        n(X.n_rows),
        p(X.n_cols),
        estimatedPredictionError(estimatedPredictionError),
        gamma(std::move(gamma)),
        intercept(intercept),
        nullScore(sls::squaredL2(y)) {
    if (fullCoefs) rss = sls::squaredL2(X * coefs - y);
    else rss = sls::squaredL2(X.cols(conv_to<uvec>::from(subset)) * coefficients - y);
}

piano::Solution::Solution(const mat &X, const vec &y, double ridgeParameter, bool fullCoefs, const vec &coefs,
                          double intercept, double estimatedPredictionError,
                          const piano::PartialSolution &partialSolution) :
        PartialSolution(partialSolution),
        coefficients(fullCoefs ? coefs.elem(arma::conv_to<arma::uvec>::from(subset)) : coefs),
        n(X.n_rows),
        p(X.n_cols),
        estimatedPredictionError(estimatedPredictionError),
        gamma(p),
        intercept(intercept),
        nullScore(sls::squaredL2(y)) {
    gamma.fill(ridgeParameter);
    if (fullCoefs) rss = sls::squaredL2(X * coefs - y);
    else rss = sls::squaredL2(X.cols(conv_to<uvec>::from(subset)) * coefs - y);
}

#pragma GCC diagnostic pop

// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//
// Created by dennis on 30.09.21.
//

#include "LinearRegressionBase.h"
#include <HandingOver.h>

piano::LinearRegressionBase::LinearRegressionBase(const arma::mat &X, const arma::vec &y, const arma::vec &gamma,
                                                  bool extractRidgeParameter) :
        originalX(X),
        originalY(y),
        originalGamma(gamma),
        n(X.n_rows),
        p(X.n_cols) {
    handOver(X, y, gamma, &XAlloc, &yAlloc, &(this->X), &(this->y), &ridgeParameter,
             extractRidgeParameter ? percentExtraction : 0);
}

size_t piano::LinearRegressionBase::getN() const {
    return n;
}

size_t piano::LinearRegressionBase::getP() const {
    return p;
}

piano::OptimizationSolution
piano::LinearRegressionBase::buildOptimizationSolutionFromPartialSolution(
        const PartialSolution &partialSolution) const {
    piano::sls::LeastSquares leastSquares(&originalX, &originalY, &originalGamma);
    arma::vec coefs = leastSquares.computeCoefs(partialSolution.subset);

    OptimizationSolutionBuilder builder(originalX.n_rows, originalY.n_cols);
    builder.fromPartialSolution(partialSolution).
            coefficients(coefs, false);

    return builder.build();
}

void piano::LinearRegressionBase::linkIndicators(const std::vector<size_t> &indicatorIndices) {
    linkedIndicators.push_back(indicatorIndices);
}

void piano::LinearRegressionBase::clearIndicatorLinks() {
    linkedIndicators.clear();
}

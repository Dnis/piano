//
// Created by dennis on 07.11.21.
//

#include "Print.h"

void piano::PrintTable::addIntegerColumn(const std::string &label, size_t minSize) {
    names.push_back(label);
    size_t width = std::max(label.size(), minSize);
    widths.push_back(width);
    formatStrings.push_back("%" + std::to_string(width) + "zu");
}

void piano::PrintTable::addDoubleColumn(const std::string &label, size_t minSize, size_t decimalPlaces) {
    names.push_back(label);
    size_t width = std::max(label.size(), minSize);
    widths.push_back(width);
    formatStrings.push_back("%" + std::to_string(width) + "." + std::to_string(decimalPlaces) + "f");
}

void piano::PrintTable::printHeader() const {
    std::string out = "| ";
    auto widthIt = widths.begin();
    for (auto nameIt=names.begin(); nameIt!=names.end(); ++nameIt) {
        size_t numberOfSpaces = (*widthIt) - (*nameIt).size();
        for (size_t i=0; i<numberOfSpaces; ++i) {
            out += " ";
        }
        out += (*nameIt);
        out += " | ";
        ++widthIt;
    }
    out += "\n";
    std::printf("%s", out.c_str());
}

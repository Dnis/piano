//
// Created by dennis on 30.09.21.
//

#include "LinearRegressionBase.h"
#include <HandingOver.h>

piano::LinearRegressionBase::LinearRegressionBase(const arma::mat &X, const arma::vec &y, const arma::vec &gamma,
                                                  bool extractRidgeParameter) :
        originalX(X),
        originalY(y),
        originalGamma(gamma),
        n(X.n_rows),
        p (X.n_cols) {
    handOver(X, y, gamma, &XAlloc, &yAlloc, &(this->X), &(this->y), &ridgeParameter,
             extractRidgeParameter ? percentExtraction : 0);
}

// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 07.09.20.
//

#include "HandingOver.h"
#include <EigenvalueExtraction.h>

using namespace arma;

void piano::handOver(const arma::mat &X, const arma::vec &y, const arma::vec &gamma, std::unique_ptr<arma::mat> *XAlloc,
                     std::unique_ptr<arma::vec> *yAlloc, const arma::mat **newXPtr, const arma::vec **newYPtr,
                     double *newRidgeScalarPtr) {
    handOver(X, y, gamma, XAlloc, yAlloc, newXPtr, newYPtr, newRidgeScalarPtr, 0, 1);
}

void piano::handOver(const arma::mat &X, const arma::vec &y, const arma::vec &gamma, std::unique_ptr<arma::mat> *XAlloc,
                     std::unique_ptr<arma::vec> *yAlloc, const arma::mat **newXPtr, const arma::vec **newYPtr,
                     double *newRidgeScalarPtr, double percentExtraction, size_t numberOfThreads) {
    if (percentExtraction > 0) {
        auto eigenvalueExtraction = piano::sls::EigenvalueExtraction(X, y, gamma, percentExtraction);
        eigenvalueExtraction.setNumberOfThreads(numberOfThreads);
        eigenvalueExtraction.computeExtraction();
        *newRidgeScalarPtr = eigenvalueExtraction.getR();
        *XAlloc = std::make_unique<arma::mat>(std::move(eigenvalueExtraction.XTilde));
        *yAlloc = std::make_unique<arma::vec>(std::move(eigenvalueExtraction.yTilde));
        *newXPtr = XAlloc->get();
        *newYPtr = yAlloc->get();
    } else {
        bool allSame = true;
        for (size_t i = 1; i < gamma.size(); ++i) {
            if (std::abs(gamma[i] - gamma[0]) > 1e-7) {
                allSame = false;
                break;
            }
        }
        if (allSame) {
            handOver(X, y, gamma[0], newXPtr, newYPtr, newRidgeScalarPtr);
        } else {
            *newRidgeScalarPtr = 0;

            *XAlloc = std::make_unique<arma::mat>(arma::join_cols(X, arma::sqrt(arma::diagmat(gamma))));
            *yAlloc = std::make_unique<arma::vec>((*XAlloc)->n_rows);
            (*yAlloc)->fill(0);
            (*yAlloc)->subvec(0, y.size() - 1) = y;
            *newXPtr = XAlloc->get();
            *newYPtr = yAlloc->get();
        }
    }
}

void piano::handOver(const arma::mat &X, const arma::vec &y, double ridgeScalar, std::unique_ptr<arma::mat> *XAlloc,
                     std::unique_ptr<arma::vec> *yAlloc, const arma::mat **newXPtr, const arma::vec **newYPtr,
                     double *newRidgeScalarPtr, double percentExtraction, size_t numberOfThreads) {
    if (percentExtraction > 0) {
        auto eigenvalueExtraction = piano::sls::EigenvalueExtraction(X, y, ridgeScalar, percentExtraction);
        eigenvalueExtraction.setNumberOfThreads(numberOfThreads);
        *newRidgeScalarPtr = eigenvalueExtraction.getR();
        *XAlloc = std::make_unique<arma::mat>(std::move(eigenvalueExtraction.XTilde));
        *yAlloc = std::make_unique<arma::vec>(std::move(eigenvalueExtraction.yTilde));
        *newXPtr = XAlloc->get();
        *newYPtr = yAlloc->get();
    } else {
        *newRidgeScalarPtr = ridgeScalar;
        *newXPtr = &X;
        *newYPtr = &y;
    }
}

void piano::handOver(const arma::mat &X, const arma::vec &y, double ridgeScalar, const arma::mat **newXPtr,
                     const arma::vec **newYPtr, double *newRidgeScalarPtr) {
    *newXPtr = &X;
    *newYPtr = &y;
    *newRidgeScalarPtr = ridgeScalar;
}

void piano::handOver(const arma::mat &X, const arma::vec &y, const arma::vec &gamma, std::unique_ptr<arma::mat> *XAlloc,
                     std::unique_ptr<arma::vec> *yAlloc, const arma::mat **newXPtr, const arma::vec **newYPtr) {
    bool isGammaZero = true;
    for (const auto &item : gamma) {
        if (std::abs(item) > 1e-8) isGammaZero = false;
    }
    if (isGammaZero) {
        *newXPtr = &X;
        *newYPtr = &y;
    } else {
        *XAlloc = std::make_unique<arma::mat>(arma::join_cols(X, arma::sqrt(arma::diagmat(gamma))));
        *yAlloc = std::make_unique<arma::vec>((*XAlloc)->n_rows);
        (*yAlloc)->fill(0);
        (*yAlloc)->subvec(0, y.size() - 1) = y;
        *newXPtr = XAlloc->get();
        *newYPtr = yAlloc->get();
    }
}

double piano::handOverAndNormaliseY(const mat &X, const vec &y, const vec &gamma, std::unique_ptr<arma::mat> *XAlloc,
                                    std::unique_ptr<arma::vec> *yAlloc, const arma::mat **newXPtr,
                                    const arma::vec **newYPtr) {
    double normY = norm(y, 2);
    if (std::abs(normY - 1.0) < 1e-8) {
        handOver(X, y, gamma, XAlloc, yAlloc, newXPtr, newYPtr);
    } else {
        bool isGammaZero = true;
        for (const auto &item : gamma) {
            if (std::abs(item) > 1e-8) isGammaZero = false;
        }
        if (isGammaZero) {
            *newXPtr = &X;
            *yAlloc = std::make_unique<arma::vec>(y / normY);
            *newYPtr = yAlloc->get();
        } else {
            *XAlloc = std::make_unique<arma::mat>(arma::join_cols(X, arma::sqrt(arma::diagmat(gamma))));
            *yAlloc = std::make_unique<arma::vec>((*XAlloc)->n_rows);
            (*yAlloc)->fill(0);
            (*yAlloc)->subvec(0, y.size() - 1) = y / normY;
            *newXPtr = XAlloc->get();
            *newYPtr = yAlloc->get();
        }
    }
    return 1.0 / normY;
}

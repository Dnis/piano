// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 26.03.20.
//

#include "DataStandardization.h"

using namespace arma;

piano::DataStandardization::DataStandardization(const arma::mat &X, const arma::vec &y, double ridgeParameter,
                                                bool normalize, bool intercept) :
        originalP(X.n_cols),
        n(X.n_rows),
        intercept(intercept),
        normalize(normalize) {
    if (intercept) {
        p = originalP + 1;
    } else {
        p = originalP;
    }

    mat U(X);
    vec u(y);

    if (normalize) {
        this->normalizeCoefs = std::make_unique<vec>(originalP);

        for (size_t i = 0; i < U.n_cols; ++i) {
            vec c = U.col(i);
            (*normalizeCoefs)[i] = 1.0 / norm(c, 2);
            U.col(i) = c * (*normalizeCoefs)[i];
        }

        normalizeCoefY = 1.0 / norm(y, 2);
        u = u * normalizeCoefY;
    }

    this->gamma = std::make_unique<vec>(p);

    if (intercept) {
        this->X = std::make_unique<mat>(n, p);
        this->y = std::make_unique<vec>(n);

        this->X->col(0).ones();
        this->X->submat(0, 1, n - 1, p - 1) = U;

        this->y->subvec(0, n - 1) = u;

        (*gamma)[0] = 0;
        for (size_t i = 1; i < p; ++i) {
            (*gamma)[i] = ridgeParameter;
        }
    } else {
        this->X = std::make_unique<mat>(std::move(U));
        this->y = std::make_unique<vec>(std::move(u));

        for (size_t i = 0; i < p; ++i) {
            (*gamma)[i] = ridgeParameter;
        }
    }
}

double piano::DataStandardization::getIntercept(const arma::vec &coefs) const {
    return intercept ? coefs[0] : 0;
}

arma::vec piano::DataStandardization::recoverCoefficients(const arma::vec &coefs) const {
    vec ret;
    if (normalize) {
        if (intercept) {
            ret = coefs.subvec(1, coefs.size() - 1) % (*normalizeCoefs) / normalizeCoefY;
        } else {
            ret = coefs % (*normalizeCoefs) / normalizeCoefY;
        }
    } else {
        if (intercept) {
            ret = coefs.subvec(1, coefs.size() - 1);
        } else {
            ret = coefs;
        }
    }
    return ret;
}

piano::Solution piano::DataStandardization::recoverSolution(const piano::Solution &sol) const {
    piano::Solution ret;
    vec fullCoefs = sol.fullCoefficients();
    if (intercept) {
        ret.intercept = recoverIntercept(fullCoefs);
        ret.p = sol.p - 1;

        size_t c = 0;
        for (size_t i = 0; i < sol.subsetSize; ++i) {
            if (sol.subset[i] != 0) {
                c++;
            }
        }
        Subset newSubset(c);

        c = 0;
        for (size_t i = 0; i < sol.subsetSize; ++i) {
            if (sol.subset[i] != 0) {
                newSubset[c++] = sol.subset[i] - 1;
            }
        }
        ret.subsetSize = newSubset.size();
        ret.subset = newSubset;

        ret.coefficients = recoverCoefficients(fullCoefs).elem(conv_to<uvec>::from(newSubset));

        ret.gamma = sol.gamma.subvec(1, sol.p-1);
    } else {
        ret.intercept = 0;
        ret.p = sol.p;

        ret.subsetSize = sol.subsetSize;
        ret.subset = sol.subset;

        ret.coefficients = recoverCoefficients(fullCoefs).elem(conv_to<uvec>::from(sol.subset));
        ret.gamma = sol.gamma;
    }
    ret.provableOptimal = sol.provableOptimal;
    ret.estimatedPredictionError = sol.estimatedPredictionError;

    ret.objectiveValue = sol.objectiveValue;
    ret.rss = sol.rss;

    ret.n = sol.n;

    ret.computationsRuntimeInSeconds = sol.computationsRuntimeInSeconds;
    ret.completeRuntimeInSeconds = sol.completeRuntimeInSeconds;
    ret.gap = sol.gap;
    if (normalize) ret.nullScore = sol.nullScore / normalizeCoefY;
    else ret.nullScore = sol.nullScore;
    ret.reachedTimelimit = sol.reachedTimelimit;
    ret.reachedIterationCount = sol.reachedIterationCount;
    ret.sanity = sol.sanity;

    return ret;
}

size_t piano::DataStandardization::getN() const {
    return n;
}

size_t piano::DataStandardization::getP() const {
    return p;
}

bool piano::DataStandardization::isInterceptApplied() const {
    return intercept;
}

const arma::mat *piano::DataStandardization::getX() const {
    return X.get();
}

bool piano::DataStandardization::isNormalizeApplied() const {
    return normalize;
}

const arma::vec *piano::DataStandardization::getY() const {
    return y.get();
}

const arma::vec *piano::DataStandardization::getGamma() const {
    return gamma.get();
}

double piano::DataStandardization::recoverIntercept(const vec &coefs) const {
    return normalize ? getIntercept(coefs) / normalizeCoefY : getIntercept(coefs);
}
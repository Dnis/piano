//
// Created by dennis on 15.04.20.
//

#include "TimeMeasurement.h"

using namespace std::chrono;

void piano::TimeMeasurement::start() {
    s = high_resolution_clock::now();
}

void piano::TimeMeasurement::end() {
    e = high_resolution_clock::now();
    duration = e - s;
}

double piano::TimeMeasurement::getDurationInSeconds() const {
    auto ret = std::chrono::duration_cast<std::chrono::duration<double>>(duration);
    return ret.count();
}

double piano::TimeMeasurement::getMidPointInSeconds() const {
    auto midpoint = high_resolution_clock::now();
    std::chrono::high_resolution_clock::duration d = midpoint - s;
    auto ret = std::chrono::duration_cast<std::chrono::duration<double>>(d);
    return ret.count();
}

double piano::TimeMeasurement::getDurationInMilliseconds() const {
    auto ret = std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(duration);
    return ret.count();
}

double piano::TimeMeasurement::getMidPointInMilliseconds() const {
    auto midpoint = high_resolution_clock::now();
    std::chrono::high_resolution_clock::duration d = midpoint - s;
    auto ret = std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(d);
    return ret.count();
}

//
// Created by dennis on 16.07.21.
//

#include "RhoCovariance.h"
#include <cmath>

piano::RhoCovariance::RhoCovariance(size_t p) : p(p) {

}

piano::RhoCovariance::matrix_type piano::RhoCovariance::generate() const {
    matrix_type covariance_matrix(p, p);
    for (long j = 0; (size_t) j < p; ++j) {
        for (long k = 0; (size_t) k < p; ++k) {
            covariance_matrix(j, k) = std::pow(rho, std::abs(j - k));
        }
    }
    return covariance_matrix;
}

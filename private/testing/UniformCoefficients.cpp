//
// Created by dennis on 16.07.21.
//

#include "UniformCoefficients.h"

piano::UniformCoefficients::UniformCoefficients(size_t p, size_t k) : p(p), k(k) {

}

piano::DrawnCoefficients piano::UniformCoefficients::draw() const {
    if (p > INT_MAX) {
        throw std::overflow_error("p is larger than INT_MAX. ");
    }
    arma::vec truncated_beta = (upper_bound - lower_bound) * arma::randu(k) + lower_bound;
    arma::uvec indices = arma::randperm(p, k);
    arma::vec beta = arma::zeros(p);
    for (size_t j = 0; j < k; ++j) {
        beta[indices[j]] = truncated_beta[j];
    }

    DrawnCoefficients drawnCoefficients;
    drawnCoefficients.beta = beta;
    drawnCoefficients.subset = arma::conv_to<Subset>::from(indices);

    return drawnCoefficients;
}

void piano::UniformCoefficients::setBounds(double lower, double upper) {
    lower_bound = lower;
    upper_bound = upper;
}

void piano::UniformCoefficients::setSeed(size_t seed) {
    arma::arma_rng::set_seed(seed);
}

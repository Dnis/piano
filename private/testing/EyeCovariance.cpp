//
// Created by dennis on 16.07.21.
//

#include "EyeCovariance.h"

piano::EyeCovariance::EyeCovariance(size_t p) : p(p) {
}

piano::EyeCovariance::matrix_type piano::EyeCovariance::generate() const {
    return arma::eye(p, p);
}

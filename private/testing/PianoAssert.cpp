//
// Created by dennis on 20.10.21.
//

#include "PianoAssert.h"
#include <algorithm>
#include <iostream>

bool piano::isSameSubset(Subset expected, Subset actual) {
    if (expected.size() != actual.size()) {
        std::cout << "Size of actual subset:\n\n    " << actual.size() << "\n\nSize of expected subset:\n\n    "
                  << expected.size() << "\n\n";
        return false;
    }

    std::sort(expected.begin(), expected.end());
    std::sort(actual.begin(), actual.end());
    for (size_t i = 0; i < expected.size(); ++i) {
        if (expected[i] != actual[i]) {
            std::cout << "Index in actual subset:\n\n    " << actual[i] << "\n\nIndex in expected subset:\n\n    "
                      << expected[i] << "\n\n";
            return false;
        }
    }
    return true;
}

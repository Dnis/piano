// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//
// Created by dennis on 04.10.21.
//

#include "TestData.h"
#include <unordered_map>

piano::RegressionData piano::testdata::small(size_t i) {
    std::unordered_map<size_t, size_t> seeds = {
            {0, 233423},
            {1, 12423}
    };

    SyntheticDataGenerator generator(2000, 10, 5);
    generator.setSeed(seeds[i]);
    generator.snr = 6;
    generator.coefficients.lower_bound = 1.0;
    generator.coefficients.upper_bound = 1.0;
    return generator.draw();
}

piano::RegressionData piano::testdata::medium(size_t i) {
    std::unordered_map<size_t, size_t> seeds = {
            {0, 231},
            {1, 1413423}
    };

    SyntheticDataGenerator generator(2000, 100, 10);
    generator.setSeed(seeds[i]);
    generator.snr = 10;
    generator.coefficients.lower_bound = 1.0;
    generator.coefficients.upper_bound = 1.0;
    return generator.draw();
}

piano::RegressionData piano::testdata::large(size_t i) {
    std::unordered_map<size_t, size_t> seeds = {
            {0, 43564},
            {1, 98732}
    };

    SyntheticDataGenerator generator(2000, 500, 25);
    generator.setSeed(seeds[i]);
    generator.snr = 5;
    generator.coefficients.lower_bound = 1.0;
    generator.coefficients.upper_bound = 1.0;
    return generator.draw();
}

piano::RegressionData piano::testdata::huge(size_t i) {
    std::unordered_map<size_t, size_t> seeds = {
            {0, 312441},
            {1, 1323}
    };

    SyntheticDataGenerator generator(2000, 1000, 50);
    generator.setSeed(seeds[i]);
    generator.snr = 10;
    generator.coefficients.lower_bound = 1.0;
    generator.coefficients.upper_bound = 1.0;
    return generator.draw();
}

piano::RegressionData piano::testdata::humongous(size_t i) {
    std::unordered_map<size_t, size_t> seeds = {
            {0, 324},
            {1, 74832}
    };

    SyntheticDataGenerator generator(3000, 3000, 1500);
    generator.setSeed(seeds[i]);
    generator.snr = 10;
    generator.coefficients.lower_bound = 1.0;
    generator.coefficients.upper_bound = 1.0;
    return generator.draw();
}

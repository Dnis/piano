// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 02.04.20.
//

#include "AllSubsetsOfSizeK.h"

piano::Subset piano::stools::getSubsetOfSizeK(size_t i, size_t maxIndex, size_t k) {
    // TODO: handle signed unsigned
    std::vector<size_t> picker(k);
    long j = i;
    size_t maxPickerNumber = maxIndex - k + 1;
    size_t ceiling = maxPickerNumber;
    for (long l = k-1; l >= 0; --l) {
        long sub = 0;
        for (std::size_t n = 0; n < ceiling; ++n) {
            sub += helper::nChoosek(l + n , l);
        }
        for (long m = ceiling - 1; m >= 0; --m) {
            sub -= helper::nChoosek(l + m , l);
            if (j >= sub) {
                j -= sub;
                picker[l] = m;
                ceiling = m + 1;
                break;
            }
        }
    }
    Subset subset(k);
    for (size_t n = 0; n < k; ++n) {
        subset[n] = picker[n] + n;
    }
    return subset;
}


std::size_t piano::stools::helper::nChoosek(size_t n, size_t k) {
    if (k > n) return 0;
    if (k * 2 > n) k = n-k;
    if (k == 0) return 1;

    size_t result = n;
    for( size_t i = 2; i <= k; ++i ) {
        result *= (n-i+1);
        result /= i;
    }
    return result;
}

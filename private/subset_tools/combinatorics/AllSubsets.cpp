// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 01.04.20.
//

#include "AllSubsets.h"
#include <bitset>
#include <LinearAlgebra.h>
#include <Conversion.h>

using namespace arma;

piano::Subset piano::stools::getSubset(size_t i, size_t maxIndex) {
    std::bitset<CHAR_BIT*sizeof(size_t)> bs(i);
    uvec v(maxIndex, fill::zeros);
    for (size_t j = 0; j < maxIndex; ++j) {
        v[j] = bs[j];
    }
    return toSubset(v);
}

size_t piano::stools::AllSubsets::size() const {
    return static_cast<size_t>(pow(2, maxIndex));
}

piano::Subset piano::stools::AllSubsets::operator[](size_t i) const {
    return getSubset(i, maxIndex);
}

piano::stools::RandomAccessIterator<piano::stools::AllSubsets, piano::Subset>
piano::stools::AllSubsets::const_iterator() const {
    return RandomAccessIterator<AllSubsets, Subset>(this);
}

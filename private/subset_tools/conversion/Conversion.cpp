// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 01.04.20.
//

#include "Conversion.h"

piano::Subset piano::stools::toSubset(const arma::uvec &indicatorVector) {
    Subset subset;
    for (size_t i = 0; i < indicatorVector.size(); ++i) {
        if (indicatorVector[i] == 1) subset.push_back(i);
    }
    return subset;
}

piano::Subset piano::stools::toSubset(arma::uvec &&indicatorVector) {
    Subset subset;
    for (size_t i = 0; i < indicatorVector.size(); ++i) {
        if (indicatorVector[i] == 1) subset.push_back(i);
    }
    return subset;
}

arma::uvec piano::stools::toIndicatorVector(const piano::Subset &subset, size_t size) {
    arma::uvec ret(size, arma::fill::zeros);
    for (const auto &item : subset) {
        ret[item] = 1;
    }
    return ret;
}

arma::uvec piano::stools::toIndicatorVector(piano::Subset &&subset, size_t size) {
    arma::uvec ret(size, arma::fill::zeros);
    for (const auto &item : subset) {
        ret[item] = 1;
    }
    return ret;
}

piano::Subset piano::stools::fromCoefs(const arma::vec &coefs) {
    size_t nonzeros = 0;
    for (const auto &item : coefs) {
        if (std::abs(item) > 1e-8) ++nonzeros;
    }
    Subset subset(nonzeros);
    size_t c = 0;
    for (size_t j = 0; j < coefs.size(); ++j) {
        if (std::abs(coefs[j]) > 1e-8) {
            subset[c++] = j;
        }
    }

    return subset;
}

piano::Subset piano::stools::fromCoefs(arma::vec &&coefs) {
    size_t nonzeros = 0;
    for (const auto &item : coefs) {
        if (std::abs(item) > 1e-8) ++nonzeros;
    }
    Subset subset(nonzeros);
    size_t c = 0;
    for (size_t j = 0; j < coefs.size(); ++j) {
        if (std::abs(coefs[j]) > 1e-8) {
            subset[c++] = j;
        }
    }

    return subset;
}

size_t piano::stools::numberOfNonZeros(const arma::vec &coefs) {
    return numberOfNonZeros(coefs, 1e-7);
}

size_t piano::stools::numberOfNonZeros(const arma::vec &coefs, double tolerance) {
    size_t nonzeros = 0;
    for (const auto &item : coefs) {
        if (std::abs(item) > tolerance) ++nonzeros;
    }
    return nonzeros;
}

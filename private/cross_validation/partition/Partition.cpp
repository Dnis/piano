// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

#include "Partition.h"

using namespace arma;

piano::cv::Partition::Partition(size_t n, size_t numberOfPartitions) : n(n), numberOfPartitions(numberOfPartitions) {

}

arma::uvec piano::cv::Partition::getPartition(size_t i) const {
    size_t partSize;
    if (i == numberOfPartitions-1) {
        partSize = n - (numberOfPartitions - 1) * (n / numberOfPartitions);
    } else {
        partSize = n / numberOfPartitions;
    }
    uvec ret(partSize);
    size_t startIndex = i * (n / numberOfPartitions);
    for (size_t j = 0; j < partSize; ++j) {
        ret[j] = startIndex + j;
    }
    return ret;
}

arma::uvec piano::cv::Partition::getComplementPartition(size_t i) const {
    size_t partSize;
    if (i == numberOfPartitions-1) {
        partSize = (numberOfPartitions - 1) * (n / (numberOfPartitions));
    } else {
        partSize = n - (n / numberOfPartitions);
    }
    uvec ret(partSize);
    size_t startIndex = i * (n / numberOfPartitions);
    for (size_t j = 0; j < partSize; ++j) {
        if (j < startIndex) ret[j] = j;
        else ret[j] = (n - partSize) + j;
    }
    return ret;
}

arma::mat piano::cv::Partition::getPartitionMatrix(size_t i, const mat &obj) const {
    uvec inds = getPartition(i);
    mat ret = obj.rows(inds);
    return ret;
}

arma::mat piano::cv::Partition::getComplementPartitionMatrix(size_t i, const arma::mat &obj) const {
    uvec inds = getComplementPartition(i);
    mat ret = obj.rows(inds);
    return ret;
}

arma::vec piano::cv::Partition::getPartitionVector(size_t i, const arma::vec &obj) const {
    uvec inds = getPartition(i);
    vec ret = obj.elem(inds);
    return ret;
}

arma::vec piano::cv::Partition::getComplementPartitionVector(size_t i, const arma::vec &obj) const {
    uvec inds = getComplementPartition(i);
    mat ret = obj.elem(inds);
    return ret;
}

// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

#include "PadmStartParameterSearchMLPACKLasso.h"
#include <mlpack/methods/lars/lars.hpp>
#include <Conversion.h>

double piano::bss::padm::penalization::search::mlpack::LARSSearch::search(std::size_t k) {
    if (!isInit) {
        // mlpack::regression::LARS lars(X->t(), y->t(), true, true, 0, 0);
        ::mlpack::regression::LARS lars(true);
        double maxCorr = lars.Train(X->t(), y->t());
        if (maxCorr < 0) throw std::runtime_error("Unknown error: max. correlation is below zero.");
        lambdaPath = lars.LambdaPath();
        betaPath = lars.BetaPath();
        isInit = true;
    }


    double adjustedLambda = 0.1;
    size_t nonzeros;
    if ((lambdaPath.size() == 1) && (std::abs(lambdaPath[0]) < 1e-8))
        throw std::runtime_error(
                "LARS was not able to compute a meaningful solution. The lambda path has size 1 with only the zero element.");
    for (size_t i = 0; i < lambdaPath.size(); ++i) {
        if (lambdaPath[i] < adjustedLambda) adjustedLambda = lambdaPath[i];
        nonzeros = piano::stools::numberOfNonZeros(betaPath[i], 1e-8);
        if (nonzeros >= k) {
            adjustedLambda = lambdaPath[i];
            break;
        }
    }

    if (!mute)
        std::printf("|  MLPACK parameter search  | %14.10f | -------------- | >=%*zu | -------------- |\n",
                    2 * adjustedLambda, 6, nonzeros);

    return 2 * adjustedLambda;
}

void piano::bss::padm::penalization::search::mlpack::LARSSearch::warmUp() {
    ::mlpack::regression::LARS lars(X->t(), y->t(), true, false, 0, 0);
    lambdaPath = lars.LambdaPath();
    betaPath = lars.BetaPath();
    isInit = true;
}

// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

#include "PadmStartParameterSearchPICASSOLasso.h"
#include <BinarySearch.h>

piano::bss::padm::penalization::search::picasso::CoordinateDescentSearch::CoordinateDescentSearch(const arma::mat *X,
                                                                                                 const arma::vec *y) :
        X(X),
        y(y) {
    lasso = std::make_unique<::piano::cvbss::Lasso<::piano::cvbss::lasso::solver::picasso::CoordinateDescent>>(
            *(this->X), *(this->y));
}

void piano::bss::padm::penalization::search::picasso::CoordinateDescentSearch::warmUp() {
    lasso->computeLambdaPath();
    isInit = true;
}

double piano::bss::padm::penalization::search::picasso::CoordinateDescentSearch::search(std::size_t k) {
    double lambda;
    size_t ind = lasso->getNonzerosPath().size() - 1;

    if (!isInit) warmUp();
    if (lasso->getNonzerosPath()[ind] < k) {
        std::printf("|  PICASSO parameter search | %14.10f | -------------- | = %*zu | -------------- |\n",
                    lasso->getLambdaPath()[ind], 6, lasso->getNonzerosPath()[ind]);
        lambda = manualSearch(k, lasso->getLambdaPath()[ind]);
    } else {
        ind = discreteBinarySearch<size_t>(lasso->getNonzerosPath(), BinarySearch::Identity<size_t>{}, k);
        lambda = lasso->getLambdaPath()[ind];
        if (!mute)
            std::printf("|  PICASSO parameter search | %14.10f | -------------- | >=%*zu | -------------- |\n",
                        lambda, 6, lasso->getNonzerosPath()[ind]);
    }

    return lambda;
}

double piano::bss::padm::penalization::search::picasso::CoordinateDescentSearch::manualSearch(size_t k,
                                                                                             double startValue) const {
    double l = startValue / 2.0;
    auto sol = lasso->setLambda(l).computePartialSolution();
    if (!mute)
        std::printf("|  PICASSO parameter search | %14.10f | -------------- | = %*zu | -------------- |\n",
                    l, 6, sol.subsetSize);
    while (sol.subsetSize < k) {
        l = l / 2.0;
        sol = lasso->setLambda(l).computePartialSolution();
        if (!mute)
            std::printf("|  PICASSO parameter search | %14.10f | -------------- | = %*zu | -------------- |\n",
                        l, 6, sol.subsetSize);
    }
    return l;
}


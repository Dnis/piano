// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 15.05.20.
//

#include "DoublingPenalty.h"

piano::bss::padm::penalty_series::DoublingPenalty::DoublingPenalty([[maybe_unused]] const arma::mat *X, [[maybe_unused]] const arma::vec *y,
                                                                  double startPenalization, [[maybe_unused]] size_t k)
        : penalization(startPenalization) {

}

double piano::bss::padm::penalty_series::DoublingPenalty::getPenalization([[maybe_unused]] const arma::vec &beta1, [[maybe_unused]] const arma::vec &beta2,
                                                                         size_t iteration, [[maybe_unused]] size_t nonzeros) {
    return penalization*pow(2, iteration);
}

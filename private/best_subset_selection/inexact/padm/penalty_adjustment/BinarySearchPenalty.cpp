// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 15.05.20.
//

#include "BinarySearchPenalty.h"

piano::bss::padm::penalty_series::BinarySearchPenalty::BinarySearchPenalty([[maybe_unused]] const arma::mat *X,
                                                                          [[maybe_unused]] const arma::vec *y,
                                                                          double startPenalization,
                                                                          size_t k) :
        k(k),
        penalization(startPenalization) {

}

double piano::bss::padm::penalty_series::BinarySearchPenalty::getPenalization([[maybe_unused]] const arma::vec &beta1, [[maybe_unused]] const arma::vec &beta2,
                                                                             size_t iteration, size_t nonzeros) {
    if (iteration == 0) return penalization;
    else {
        if (nonzeros < k) {
            maxPenalization = penalization;
        }
        if (nonzeros > k) {
            minPenalization = penalization;
        }
        if (maxPenalization == -1) {
            penalization = 2*penalization;
            return penalization;
        }
        if (minPenalization == -1) {
            penalization = 0.5*penalization;
            return penalization;
        }
        penalization = 0.5*(minPenalization + maxPenalization);
        return penalization;
    }
}

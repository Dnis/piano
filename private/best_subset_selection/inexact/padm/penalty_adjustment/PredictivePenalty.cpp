// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 15.05.20.
//

#include "PredictivePenalty.h"
#include <exception>
#include <TypeConversion.h>

piano::bss::padm::penalty_series::PredictivePenalty::PredictivePenalty([[maybe_unused]] const arma::mat *X, [[maybe_unused]] const arma::vec *y,
                                                                      double startPenalization, size_t k) :
        k(static_cast<int>(k)),
        penalization(startPenalization),
        prevPenalization(startPenalization) {
    assertCastable<int>(k, "k", "maximum number of regressors");
}

double
piano::bss::padm::penalty_series::PredictivePenalty::getPenalization([[maybe_unused]] const arma::vec &beta1, [[maybe_unused]] const arma::vec &beta2,
                                                                    size_t iteration, size_t nonzeros) {
    assertCastable<int>(prevNonzeros, "prevNonzeros", "number of non-zeros in prev. iteration");
    assertCastable<int>(nonzeros, "nonzeros", "number of non-zeros");
    if (iteration == 0) {
        prevNonzeros = nonzeros;
        if (static_cast<int>(nonzeros) < k) {
            prevPenalization = penalization;
            penalization = 0.5 * penalization;
        }
        if (static_cast<int>(nonzeros) > k) {
            prevPenalization = penalization;
            penalization = 2 * penalization;
        }
    } else {
        double slope =
                (static_cast<int>(prevNonzeros) - static_cast<int>(nonzeros)) / (prevPenalization - penalization);
        double intercept = static_cast<int>(nonzeros) - slope * penalization;
        prevPenalization = penalization;
        prevNonzeros = nonzeros;

        if (std::abs(slope) > 1e-8) {
            penalization = (k - intercept) / slope;
            if (penalization <= 0) {
                if (static_cast<int>(nonzeros) < k) {
                    penalization = 0.5 * prevPenalization;
                }
                if (static_cast<int>(nonzeros) > k) {
                    penalization = 2 * prevPenalization;
                }
            }
            // if (nonzeros > k) penalization = 5.0 / 10.0 * prevPenalization + 5.0 / 10.0 * penalization;
        } else {
            if (static_cast<int>(nonzeros) < k) {
                penalization = 0.5 * penalization;
            }
            if (static_cast<int>(nonzeros) > k) {
                penalization = 2 * penalization;
            }
        }
    }
    return penalization;
}

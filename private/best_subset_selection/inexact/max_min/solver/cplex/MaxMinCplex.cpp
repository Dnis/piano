// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

#ifdef PIANO_USE_CPLEX

#include "cplex/MaxMinCplex.h"
#include <CplexHelper.h>
#include <ilconcert/ilomodel.h>
#include <ilconcert/iloenv.h>
#include <SquaredL2.h>
#include <TimeMeasurement.h>
#include <LeastSquares.h>
#include <Conversion.h>
#include <CrossValidation.h>
#include <Maxk.h>
#include <PartialSolution.h>
#include <PartialSolutionBuilder.h>
#include <NoSolutionFoundException.h>

using namespace arma;

piano::bss::max_min::solver::Cplex::Cplex() :
        env(nullptr, delIloEnv),
        model(nullptr, delIloModel),
        cplex(nullptr, delIloCplex) {
}

void piano::bss::max_min::solver::Cplex::setupVariables() {
    alpha = std::make_unique<IloNumVarArray>(*env, n, -IloInfinity, IloInfinity);
    u = std::make_unique<IloNumVarArray>(*env, p, 0, IloInfinity);
    t = std::make_unique<IloNumVar>(*env, -IloInfinity, IloInfinity);
    d = std::make_unique<IloNumVarArray>(*env, p, -IloInfinity, IloInfinity);
    dBound = std::make_unique<IloNumVar>(*env, 0, IloInfinity);
}

void piano::bss::max_min::solver::Cplex::setupObjectiveFunction(std::size_t k) {
    IloExpr obj(*env);
    for (std::size_t i = 0; i < n; i++) {
        obj += -0.5 * (*alpha)[i] * (*alpha)[i];
    }
    obj += piano::cplex::dot(*env, *y, *alpha);
    for (std::size_t i = 0; i < p; i++) {
        obj -= (*u)[i];
    }
    obj -= ((double) k) * (*t);

    model->add(IloMaximize(*env, numericalScalar * obj));
    obj.end();
}

void piano::bss::max_min::solver::Cplex::setupCplex() {
    cplex.reset(new IloCplex(*model));
    if (timelimit != 0) cplex->setParam(IloCplex::Param::TimeLimit, timelimit);
    if (numberOfThreads != 0) cplex->setParam(IloCplex::Param::Threads, numberOfThreads);
    if (mute) {
        cplex->setOut(env->getNullStream());
        cplex->setParam(IloCplex::Param::MIP::Display, 0);
        cplex->setParam(IloCplex::Param::Barrier::Display, 0);
    } else {
        cplex->setParam(IloCplex::Param::MIP::Display, 3);
    }
}

void piano::bss::max_min::solver::Cplex::buildModel(std::size_t k) {
    env.reset(new IloEnv);
    model.reset(new IloModel(*env));

    setupVariables();
    setupConstraints();
    setupObjectiveFunction(k);

    setupCplex();
}

void piano::bss::max_min::solver::Cplex::setupConstraints() {
    for (size_t i = 0; i < p; ++i) {
        IloExpr exprD(*env);
        exprD += piano::cplex::dot(*env, X->col(i), *alpha, n);
        model->add((*d)[i] == exprD);
    }

    mat I = eye(p, p);

    IloExpr exprDBound(*env);
    exprDBound += piano::cplex::quadraticExpr(*env, I, *d);
    model->add(exprDBound <= (*dBound));

    for (std::size_t i = 0; i < p; i++) {
        IloExpr expr(*env);
        expr += *dBound;
        expr -= (2.0 / ridgeScalar) * (*u)[i];
        expr -= (2.0 / ridgeScalar) * (*t);
        model->add(expr <= 0);
        expr.end();
    }
}

piano::PartialSolution piano::bss::max_min::solver::Cplex::solve(size_t k) {
    TimeMeasurement measurement;
    bool solved = false;
    measurement.start();
    try {
        solved = static_cast<bool>(cplex->solve());
    } catch (const IloAlgorithm::Exception &e) {
        std::cout << e.getMessage() << "\n";
    }
    measurement.end();

    if (!solved) {
        IloAlgorithm::Status solStatus = cplex->getStatus();
        env->out() << std::endl << "Solution status: " << solStatus << std::endl;
        throw NoSolutionFoundException("MaxMin was not able to find a solution within the time limit.");
    } else {
        IloAlgorithm::Status solStatus = cplex->getStatus();
        if (!mute) env->out() << std::endl << "Solution status: " << solStatus << std::endl;

        arma::vec a = piano::cplex::extractVars(*cplex, *alpha);
        std::vector<double> selectionValues(p);
        for (std::size_t i = 0; i < p; ++i) {
            selectionValues[i] = pow(dot(X->col(i), a), 2.0);
        }

        Subset optSubset = piano::sls::maxKInds(selectionValues, k);

        PartialSolutionBuilder builder;
        builder.
                objectiveValue(cplex->getObjValue()).
                subset(optSubset).
                provableOptimal(false).
                computationsRuntimeInSeconds(measurement.getDurationInSeconds()).
                completeRuntimeInSeconds(-1).
                gap(cplex->getMIPRelativeGap());

        PartialSolution sol = builder.build();

        if (cplex->getStatus() != IloAlgorithm::Status::Optimal) sol.reachedTimelimit = true;

        return sol;
    }
}

void piano::bss::max_min::solver::Cplex::setNumericalScalar(double numericalScalar) {
    Cplex::numericalScalar = numericalScalar;
}

void piano::bss::max_min::solver::Cplex::setData(const arma::mat *X, const arma::vec *y, double ridgeScalar) {
    this->X = X;
    this->y = y;
    this->ridgeScalar = ridgeScalar;
    n = X->n_rows;
    p = X->n_cols;
    numericalScalar = 1.0 / piano::sls::squaredL2(*y);
}

#endif
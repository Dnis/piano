// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

#include "BssExhaustiveSearch.h"

#include <AllSubsetsOfSizeK.h>
#include <CrossValidation.h>
#include <GridSearch.h>
#include <PartialSolutionBuilder.h>

using namespace piano::stools;
using namespace piano::cv;

piano::bss::ExhaustiveSearch::ExhaustiveSearch(const arma::mat *X, const arma::vec *y, const arma::vec *gamma) :
        Base<cv::SubsetTraining>(*X, *y, *gamma) {
}

piano::bss::ExhaustiveSearch::TrainingFunction piano::bss::ExhaustiveSearch::getTraining() const {
    TrainingFunction trainingFunction;
    trainingFunction.gamma = &originalGamma;
    return trainingFunction;
}

void piano::bss::ExhaustiveSearch::setTimelimit([[maybe_unused]] size_t timelimitInSeconds) {
    PIANO_TIMELIMIT_NOT_IMPLEMENTED(piano::bss::ExhaustiveSearch);
}

void piano::bss::ExhaustiveSearch::setIterationLimit([[maybe_unused]] size_t iterationLimit) {
    PIANO_ITERATIONLIMIT_NOT_IMPLEMENTED(piano::bss::ExhaustiveSearch);
}

piano::PartialSolution piano::bss::ExhaustiveSearch::computePartialSolution(size_t k) {
    AllSubsetsOfSizeK allSubsets(p, k);

    struct Objective {
        size_t p;
        const arma::mat *X;
        const arma::vec *y;
        const arma::vec *gamma;

        double operator()(const Subset &subset) const {
            sls::LeastSquares leastSquares(X, y, gamma);
            return leastSquares.compute(subset).objectiveValue;
        }
    } objective{};
    objective.X = &originalX;
    objective.y = &originalY;
    objective.gamma = &originalGamma;
    objective.p = p;

    auto optSubset = argGridSearch(numberOfThreads, allSubsets, objective);

    sls::LeastSquares leastSquares(&originalX, &originalY, &originalGamma);
    auto leastSquaresSol = leastSquares.compute(optSubset);

    PartialSolutionBuilder builder;
    builder.subset(optSubset).
            objectiveValue(leastSquaresSol.objectiveValue).
            provableOptimal(true).
            computationsRuntimeInSeconds(-1).
            completeRuntimeInSeconds(-1);

    // TODO: Times
    return builder.build();;
}

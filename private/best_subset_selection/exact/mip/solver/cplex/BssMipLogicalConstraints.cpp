//
// Created by dennis on 04.08.21.
//

#include "cplex/BssMipLogicalConstraints.h"
#include <TypeConversion.h>

#ifdef PIANO_USE_CPLEX

piano::bss::mip::indicator_constraints::cplex::LogicalConstraints::LogicalConstraints(const arma::mat *X,
                                                                                      const arma::vec *y,
                                                                                      const arma::vec *gamma) :
        X(X),
        y(y),
        gamma(gamma) {

}

void piano::bss::mip::indicator_constraints::cplex::LogicalConstraints::buildConstraints(IloEnv &env, IloModel &model,
                                                                                         const IloNumVarArray &beta,
                                                                                         const IloBoolVarArray &z,
                                                                                         size_t k) const {
    size_t p = X->n_cols;

    assertCastable<long>(k, "k", "maximum number of regressors");
    assertCastable<long>(p, "p", "number of variables");
    for (long i = 0; i < static_cast<long>(p); i++) {
        model.add(IloIfThen(env, z[i] == 0, beta[i] == 0));

        IloExpr expr(env);
        for (long j = 0; j < static_cast<long>(p); ++j) {
            expr += z[j];
        }

        model.add(expr <= static_cast<long>(k));
        expr.end();
    }
}

#endif
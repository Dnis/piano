//
// Created by dennis on 09.08.21.
//

#ifdef PIANO_USE_CPLEX

#include "cplex/RbssLogicalConstraints.h"

piano::rbss::mip::indicator_constraints::cplex::LogicalConstraints::LogicalConstraints(const arma::mat *X,
                                                                                       [[maybe_unused]] const arma::vec *y,
                                                                                       [[maybe_unused]] const arma::vec *gamma)
        : p(X->n_cols) {}

void piano::rbss::mip::indicator_constraints::cplex::LogicalConstraints::buildConstraints(IloEnv &env, IloModel &model,
                                                                                          const IloNumVarArray &beta,
                                                                                          const IloBoolVarArray &z)
const {
    for (size_t i = 0; i < p; i++) {
        model.add(IloIfThen(env, z[i] == 0, beta[i] == 0));
    }
}

#endif
//
// Created by kreber on 03.08.21.
//

#include "RbssExhaustiveSearch.h"
#include <AllSubsets.h>
#include <ErrorMetrics.h>
#include <PartialSolutionBuilder.h>

using namespace piano::stools;
using namespace piano::cv;

piano::rbss::ExhaustiveSearch::ExhaustiveSearch(const arma::mat *X, const arma::vec *y, const arma::vec *gamma) :
        Base<cv::SubsetTraining>(*X, *y, *gamma) {

}

piano::rbss::ExhaustiveSearch::TrainingFunction piano::rbss::ExhaustiveSearch::getTraining() const {
    TrainingFunction trainingFunction;
    trainingFunction.gamma = &originalGamma;
    return trainingFunction;
}

piano::PartialSolution piano::rbss::ExhaustiveSearch::computePartialSolution(const arma::vec &activationCosts) {
    AllSubsets allSubsets(p);

    struct Objective {
        const arma::mat* X;
        const arma::vec* y;
        const arma::vec* gamma;
        const arma::vec* activationCosts;
        size_t p;

        double operator()(const Subset &subset) const {
            sls::LeastSquares leastSquares(X, y, gamma);
            leastSquares.setActivationCosts(*activationCosts);
            return leastSquares.compute(subset).objectiveValue;
        }
    } objective{};

    objective.p = p;
    objective.X = &originalX;
    objective.y = &originalY;
    objective.gamma = &originalGamma;
    objective.activationCosts = &activationCosts;

    auto optSubset = argGridSearch(numberOfThreads, allSubsets, objective);

    sls::LeastSquares leastSquares(&originalX, &originalY, &originalGamma);
    leastSquares.setActivationCosts(activationCosts);
    auto leastSquaresSol = leastSquares.compute(optSubset);

    PartialSolutionBuilder builder;
    builder.subset(optSubset).
            objectiveValue(leastSquaresSol.objectiveValue).
            provableOptimal(true).
            computationsRuntimeInSeconds(-1).
            completeRuntimeInSeconds(-1);

    // TODO: Times
    return builder.build();
}

piano::PartialSolution piano::rbss::ExhaustiveSearch::computePartialSolution(double activationCost) {
    return computePartialSolution(activationCost * arma::ones(p));
}

get_filename_component(Piano_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(CMakeFindDependencyMacro)

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}" ${CMAKE_MODULE_PATH})

find_dependency(CPLEX REQUIRED)
find_dependency(MLPACK)
find_dependency(Armadillo REQUIRED)
add_library(Armadillo IMPORTED INTERFACE)
set_property(TARGET Armadillo PROPERTY
        INTERFACE_INCLUDE_DIRECTORIES ${ARMADILLO_INCLUDE_DIRS})
set_property(TARGET Armadillo PROPERTY
        INTERFACE_LINK_LIBRARIES ${ARMADILLO_LIBRARIES})
find_dependency(OpenMP REQUIRED)
if(NOT TARGET OpenMP::OpenMP_CXX)
    find_package(Threads REQUIRED)
    add_library(OpenMP::OpenMP_CXX IMPORTED INTERFACE)
    set_property(TARGET OpenMP::OpenMP_CXX
            PROPERTY INTERFACE_COMPILE_OPTIONS ${OpenMP_CXX_FLAGS})
    # Only works if the same flag is passed to the linker; use CMake 3.9+ otherwise (Intel, AppleClang)
    set_property(TARGET OpenMP::OpenMP_CXX
            PROPERTY INTERFACE_LINK_LIBRARIES ${OpenMP_CXX_FLAGS} Threads::Threads)
endif()
find_dependency(BLAS REQUIRED)

include("${Piano_CMAKE_DIR}/PianoTargets.cmake")

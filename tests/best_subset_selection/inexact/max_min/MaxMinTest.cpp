// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 31.08.20.
//

#include <MaxMin.h>
#include <gtest/gtest.h>
#include <DataStandardization.h>
#include <BssExhaustiveSearch.h>
#include <BssPadm.h>
#include <config.h>

using namespace arma;
using namespace piano::bss;
using namespace piano;

#ifdef PIANO_USE_CPLEX

TEST_F(Small1Test, RunsSuccessfully1) {
    bool success = true;
    Solution sol;
    Solution solRecovered;
    try {
        MaxMin<> maxMin(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
        sol = maxMin.run(5);
        solRecovered = dataStandardization->recoverSolution(sol);
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

TEST_F(Medium1Test, RunsSuccessfully2) {
    DataStandardization transformation(X, y, 2.0, true, false);
    bool success = true;
    Solution sol;
    Solution solRecovered;
    try {
        MaxMin<> maxMin(*(transformation.getX()), *(transformation.getY()), *(transformation.getGamma()));
        maxMin.mute(true);
        sol = maxMin.run(5);
        solRecovered = transformation.recoverSolution(sol);
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

TEST_F(Small1Test, YieldsOptimalSolution1) {
    double ridge = 1.5;
    DataStandardization transformation(X, y, ridge, true, false);

    MaxMin<> maxMin(*(transformation.getX()), *(transformation.getY()), *(transformation.getGamma()));
    auto sol1 = maxMin.run(5);

    Solution sol2;
    ExhaustiveSearch exhaustiveSearch((transformation.getX()), (transformation.getY()), (transformation.getGamma()));
    sol2 = exhaustiveSearch.run(5);
    EXPECT_DOUBLE_EQ(sol1.estimatedPredictionError, sol2.estimatedPredictionError);
    std::sort(sol1.subset.begin(), sol1.subset.end());
    std::sort(sol2.subset.begin(), sol2.subset.end());
    ASSERT_EQ(sol1.subsetSize, sol2.subsetSize);
    for (size_t i = 0; i < sol1.subsetSize; ++i) {
        EXPECT_EQ(sol1.subset[i], sol2.subset[i]);
    }
}

TEST_F(Small2Test, YieldsOptimalSolution2) {
    double ridge = 1.0;
    DataStandardization transformation(X, y, ridge, true, false);

    MaxMin<> maxMin(*(transformation.getX()), *(transformation.getY()), *(transformation.getGamma()));
    auto sol1 = maxMin.run(5);

    Solution sol2;
    ExhaustiveSearch exhaustiveSearch((transformation.getX()), (transformation.getY()), (transformation.getGamma()));
    sol2 = exhaustiveSearch.run(5);
    EXPECT_DOUBLE_EQ(sol1.estimatedPredictionError, sol2.estimatedPredictionError);
    std::sort(sol1.subset.begin(), sol1.subset.end());
    std::sort(sol2.subset.begin(), sol2.subset.end());
    ASSERT_EQ(sol1.subsetSize, sol2.subsetSize);
    for (size_t i = 0; i < sol1.subsetSize; ++i) {
        EXPECT_EQ(sol1.subset[i], sol2.subset[i]);
    }
}

TEST_F(Small2Test, MaxMinAndADMHaveSameCoefficients1) {
    double ridge = 1.0;
    DataStandardization transformation(X, y, ridge, true, false);

    MaxMin<> maxMin(*(transformation.getX()), *(transformation.getY()), *(transformation.getGamma()));
    auto sol1 = maxMin.run(5);

    Solution sol2;
    Padm<> padm(*(transformation.getX()), *(transformation.getY()), *(transformation.getGamma()));
    padm.setStartParameterCorrection(0.1);
    sol2 = padm.run(5);

    EXPECT_DOUBLE_EQ(sol1.estimatedPredictionError, sol2.estimatedPredictionError);

    ASSERT_EQ(sol1.p, sol2.p);
    auto coefs1 = sol1.fullCoefficients();
    auto coefs2 = sol2.fullCoefficients();
    for (size_t i = 0; i < sol1.p; ++i) {
        EXPECT_DOUBLE_EQ(coefs1[i], coefs2[i]);
    }

    std::sort(sol1.subset.begin(), sol1.subset.end());
    std::sort(sol2.subset.begin(), sol2.subset.end());
    ASSERT_EQ(sol1.subsetSize, sol2.subsetSize);
    for (size_t i = 0; i < sol1.subsetSize; ++i) {
        EXPECT_EQ(sol1.subset[i], sol2.subset[i]);
    }
}

TEST_F(Small1Test, NulScoreIsGreaterThanZero1) {
    DataStandardization transformation(X, y, 0, true, false);

    MaxMin<> maxMin(*(transformation.getX()), *(transformation.getY()), *(transformation.getGamma()));
    auto sol = maxMin.run(5);

    EXPECT_GT(sol.nullScore, 0);

    auto solRecovered = transformation.recoverSolution(sol);
    EXPECT_GT(solRecovered.nullScore, solRecovered.rss);
}

TEST_F(Medium1Test, NulScoreIsGreaterThanZero2) {
    DataStandardization transformation(X, y, 0, true, false);

    MaxMin<> maxMin(*(transformation.getX()), *(transformation.getY()), *(transformation.getGamma()));
    auto sol = maxMin.run(5);

    EXPECT_GT(sol.nullScore, sol.rss);

    auto solRecovered = transformation.recoverSolution(sol);
    EXPECT_GT(solRecovered.nullScore, solRecovered.rss);
}

TEST_F(Small2Test, NulScoreIsGreaterThanZero3) {
    DataStandardization transformation(X, y, 0, true, false);

    MaxMin<> maxMin(*(transformation.getX()), *(transformation.getY()), *(transformation.getGamma()));
    auto sol = maxMin.run(5);

    EXPECT_GT(sol.nullScore, sol.rss);

    auto solRecovered = transformation.recoverSolution(sol);
    EXPECT_GT(solRecovered.nullScore, solRecovered.rss);
}

#endif

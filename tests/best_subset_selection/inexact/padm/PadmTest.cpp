// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 29.04.20.
//

#include <BssPadm.h>
#include <gtest/gtest.h>
#include <DataStandardization.h>
#include <BssExhaustiveSearch.h>
#include <config.h>

using namespace arma;
using namespace piano::bss;
using namespace piano;

// TODO: Split into multiple files

template<class StartParameterSearch, class PenaltySeries>
using InternalL1 = piano::bss::padm::variant::PadmSkeleton<StartParameterSearch, PenaltySeries, piano::bss::padm::variant::L1Optimizer<piano::cvbss::Lasso<piano::cvbss::lasso::solver::internal::CoordinateDescent>>>;
using InternalPadm = Padm<
        InternalL1<
                padm::StartParameterSearch<padm::penalization::search::internal::CoordinateDescentSearch>,
                padm::penalty_series::DoublingPenalty
        >>;

TEST_F(Small1Test, InternalRunsSuccessfully1) {
    bool success = true;
    Solution sol;
    Solution solRecovered;
    try {
        InternalPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
        padm.setStartPenalization(0.03);
        sol = padm.run(5);
        EXPECT_TRUE(sol.sanity);
        solRecovered = dataStandardization->recoverSolution(sol);
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

#ifdef PIANO_USE_PICASSO
template<class StartParameterSearch, class PenaltySeries>
using PICASSOL1 = piano::bss::padm::variant::PadmSkeleton<StartParameterSearch, PenaltySeries, piano::bss::padm::variant::L1Optimizer<piano::cvbss::Lasso<piano::cvbss::lasso::solver::picasso::CoordinateDescent>>>;
using PicassoPadm = Padm<
        PICASSOL1<padm::StartParameterSearch<padm::penalization::search::picasso::CoordinateDescentSearch>,
                padm::penalty_series::DoublingPenalty
        >
>;

TEST_F(Small1Test, PICASSORunsSuccessfully1) {
    bool success = true;
    Solution sol;
    Solution solRecovered;
    try {
        PicassoPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
        padm.setStartPenalization(0.03);
        sol = padm.run(5);
        EXPECT_TRUE(sol.sanity);
        solRecovered = dataStandardization->recoverSolution(sol);
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

TEST_F(Large1Test, PICASSORunsSuccessfully2) {
    bool success = true;
    Solution sol;
    Solution solRecovered;
    try {
        PicassoPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
        padm.setStartPenalization(0.03);
        sol = padm.run(5);
        EXPECT_TRUE(sol.sanity);
        solRecovered = dataStandardization->recoverSolution(sol);
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

TEST_F(Medium1Test, PICASSORunsSuccessfully3) {
    bool success = true;
    Solution sol;
    Solution solRecovered;
    try {
        PicassoPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
        padm.setStartPenalization(0.03);
        sol = padm.run(5);
        EXPECT_TRUE(sol.sanity);
        solRecovered = dataStandardization->recoverSolution(sol);
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

#endif

#ifdef PIANO_USE_MLPACK
template<class StartParameterSearch, class PenaltySeries>
using MLPACKL1 = piano::bss::padm::variant::PadmSkeleton<StartParameterSearch, PenaltySeries, piano::bss::padm::variant::L1Optimizer<piano::cvbss::Lasso<piano::cvbss::lasso::solver::mlpack::LARS>>>;
using MlpackPadm = Padm<
        MLPACKL1<
                padm::StartParameterSearch<padm::penalization::search::mlpack::LARSSearch>, padm::penalty_series::DoublingPenalty
        >
>;

TEST_F(Small1Test, MLPACKRunsSuccessfully1) {
    bool success = true;
    Solution sol;
    Solution solRecovered;
    try {
        MlpackPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
        padm.setStartPenalization(0.03);
        sol = padm.run(5);
        EXPECT_TRUE(sol.sanity);
        solRecovered = dataStandardization->recoverSolution(sol);
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

TEST_F(Large1Test, MLPACKRunsSuccessfully2) {
    bool success = true;
    Solution sol;
    Solution solRecovered;
    try {
        MlpackPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
        padm.setStartPenalization(0.03);
        sol = padm.run(5);
        EXPECT_TRUE(sol.sanity);
        solRecovered = dataStandardization->recoverSolution(sol);
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

TEST_F(Medium1Test, MLPACKRunsSuccessfully3) {
    bool success = true;
    Solution sol;
    Solution solRecovered;
    try {
        MlpackPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
        padm.setStartPenalization(0.03);
        sol = padm.run(5);
        EXPECT_TRUE(sol.sanity);
        solRecovered = dataStandardization->recoverSolution(sol);
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

TEST_F(Small1Test, MLPACKRunsSuccessfullyWithInternalSearch) {
    bool success = true;
    Solution sol;
    Solution solRecovered;
    try {
        Padm<
                MLPACKL1<
                        padm::StartParameterSearch<padm::penalization::search::internal::CoordinateDescentSearch>, padm::penalty_series::DoublingPenalty
                >
        > padm(
                *(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
        sol = padm.run(5);
        EXPECT_TRUE(sol.sanity);
        solRecovered = dataStandardization->recoverSolution(sol);
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

#endif

TEST_F(Small1Test, MuteWorks) {
    bool success = true;
    Solution sol;
    Solution solRecovered;
    try {
        Padm<> padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
        padm.mute(true);
        sol = padm.run(5);
        solRecovered = dataStandardization->recoverSolution(sol);
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

TEST_F(Small1RidgeTest, InternalYieldsOptimalSolution1) {
    Solution sol1;
    InternalPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    padm.setStartParameterCorrection(0.1);
    sol1 = padm.run(5);
    ASSERT_TRUE(sol1.sanity);

    Solution sol2;
    ExhaustiveSearch exhaustiveSearch((dataStandardization->getX()), (dataStandardization->getY()), (dataStandardization->getGamma()));
    sol2 = exhaustiveSearch.run(5);

    std::sort(sol1.subset.begin(), sol1.subset.end());
    std::sort(sol2.subset.begin(), sol2.subset.end());

    ASSERT_EQ(sol1.subsetSize, sol2.subsetSize);
    for (size_t i = 0; i < sol1.subsetSize; ++i) {
        EXPECT_EQ(sol1.subset[i], sol2.subset[i]);
    }

    EXPECT_DOUBLE_EQ(sol1.estimatedPredictionError, sol2.estimatedPredictionError);
}

#ifdef PIANO_USE_PICASSO
TEST_F(Small1RidgeTest, PICASSOYieldsOptimalSolution1) {
    Solution sol1;
    PicassoPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    padm.setStartParameterCorrection(0.1);
    sol1 = padm.run(5);
    ASSERT_TRUE(sol1.sanity);

    Solution sol2;
    ExhaustiveSearch exhaustiveSearch((dataStandardization->getX()), (dataStandardization->getY()), (dataStandardization->getGamma()));
    sol2 = exhaustiveSearch.run(5);

    std::sort(sol1.subset.begin(), sol1.subset.end());
    std::sort(sol2.subset.begin(), sol2.subset.end());

    ASSERT_EQ(sol1.subsetSize, sol2.subsetSize);
    for (size_t i = 0; i < sol1.subsetSize; ++i) {
        EXPECT_EQ(sol1.subset[i], sol2.subset[i]);
    }

    EXPECT_DOUBLE_EQ(sol1.estimatedPredictionError, sol2.estimatedPredictionError);
}

#endif

#ifdef PIANO_USE_MLPACK
TEST_F(Small1RidgeTest, MLPACKYieldsOptimalSolution1) {
    Solution sol1;
    MlpackPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    padm.setStartParameterCorrection(0.1);
    sol1 = padm.run(5);
    ASSERT_TRUE(sol1.sanity);

    Solution sol2;
    ExhaustiveSearch exhaustiveSearch((dataStandardization->getX()), (dataStandardization->getY()), (dataStandardization->getGamma()));
    sol2 = exhaustiveSearch.run(5);

    std::sort(sol1.subset.begin(), sol1.subset.end());
    std::sort(sol2.subset.begin(), sol2.subset.end());

    ASSERT_EQ(sol1.subsetSize, sol2.subsetSize);
    for (size_t i = 0; i < sol1.subsetSize; ++i) {
        EXPECT_EQ(sol1.subset[i], sol2.subset[i]);
    }

    EXPECT_DOUBLE_EQ(sol1.estimatedPredictionError, sol2.estimatedPredictionError);
}

#endif

TEST_F(Small1Test, InternalCorrectSparsity1) {
    Solution sol;
    InternalPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    padm.setStartPenalization(0.03);
    sol = padm.run(5);

    EXPECT_EQ(dataStandardization->recoverSolution(sol).subset.size(), 5);
}

#ifdef PIANO_USE_PICASSO
TEST_F(Small1Test, PICASSOCorrectSparsity1) {
    Solution sol;
    PicassoPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    padm.setStartPenalization(0.03);
    sol = padm.run(5);

    EXPECT_EQ(dataStandardization->recoverSolution(sol).subset.size(), 5);
}

TEST_F(Large1Test, PICASSOCorrectSparsity2) {
    Solution sol;
    PicassoPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    padm.setStartPenalization(0.03);
    sol = padm.run(5);

    EXPECT_EQ(dataStandardization->recoverSolution(sol).subset.size(), 5);
}

#endif

#ifdef PIANO_USE_MLPACK
TEST_F(Small1Test, MLPACKCorrectSparsity1) {
    Solution sol;
    MlpackPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    padm.setStartPenalization(0.03);
    sol = padm.run(5);

    EXPECT_EQ(dataStandardization->recoverSolution(sol).subset.size(), 5);
}

TEST_F(Large1Test, MLPACKCorrectSparsity2) {
    Solution sol;
    MlpackPadm padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    padm.setStartPenalization(0.03);
    sol = padm.run(5);

    EXPECT_EQ(dataStandardization->recoverSolution(sol).subset.size(), 5);
}

#endif

TEST_F(Large1Test, PenalizationCorrectionWorks) {
    Solution sol;
    Padm<> padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    padm.setNumberOfThreads(1);
    padm.setStartParameterCorrection(0.5);
    sol = padm.run(5);

    EXPECT_EQ(dataStandardization->recoverSolution(sol).subset.size(), 5);
}

TEST_F(Small1Test, HPTRunsSuccessfully1) {
    bool success = true;
    Solution sol;
    Solution solRecovered;
    Padm<> padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    auto padmHpt = padm.getHyperParameterTuner();
    padmHpt.setNumberOfThreads(4);
    sol = padmHpt.run();
    solRecovered = dataStandardization->recoverSolution(sol);
    EXPECT_TRUE(success);
}

TEST_F(Medium2Test, HPTRunsSuccessfully2) {
    bool success = true;
    Solution sol;
    Solution solRecovered;
    try {
        Padm<> padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
        auto padmHpt = padm.getHyperParameterTuner();
        padmHpt.setNumberOfThreads(4);
        sol = padmHpt.run();
        solRecovered = dataStandardization->recoverSolution(sol);
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

TEST_F(Small1Test, NullScoreIsGreaterThanRSS1) {
    Padm<> padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    auto sol = padm.run(5);

    EXPECT_GT(sol.nullScore, 0);

    auto solRecovered = dataStandardization->recoverSolution(sol);
    EXPECT_GT(solRecovered.nullScore, 0);
}

TEST_F(Large1Test, NullScoreIsGreaterThanRSS2) {
    Padm<> padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    padm.mute(false);
    auto sol = padm.run(5);

    EXPECT_GT(sol.nullScore, sol.rss);

    auto solRecovered = dataStandardization->recoverSolution(sol);
    EXPECT_GT(solRecovered.nullScore, solRecovered.rss);
}

TEST_F(Medium1Test, NullScoreIsGreaterThanRSS3) {
    Padm<> padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    auto sol = padm.run(5);

    EXPECT_GT(sol.nullScore, sol.rss);

    auto solRecovered = dataStandardization->recoverSolution(sol);
    EXPECT_GT(solRecovered.nullScore, solRecovered.rss);
}

TEST_F(Medium1Test, SanityCheckWorks) {
    Padm<> padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    padm.setStartParameterCorrection(10);
    auto sol = padm.run(5);

    EXPECT_FALSE(sol.sanity);

    auto solRecovered = dataStandardization->recoverSolution(sol);

    EXPECT_FALSE(solRecovered.sanity);
}

// Only allow PICASSO because others are too slow
#ifdef PIANO_USE_PICASSO
TEST_F(Humongous1Test, TimelimitWorks) {
    Padm<> padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    padm.setStartParameterCorrection(0.01);
    padm.setTimelimit(1);
    auto sol = padm.run(5);

    EXPECT_TRUE(sol.reachedTimelimit);

    auto solRecovered = dataStandardization->recoverSolution(sol);

    EXPECT_TRUE(solRecovered.reachedTimelimit);
}

TEST_F(Humongous1Test, DoesProduceSolutionWithKNonzeros1) {
    Padm<> padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    padm.setStartParameterCorrection(0.8);
    padm.setTimelimit(0);
    padm.setNumberOfThreads(8);
    auto sol = padm.run(1500);

    EXPECT_TRUE(sol.subsetSize == 1500);
}

#endif

TEST_F(Large1Test, DoesProduceSolutionWithKNonzeros2) {
    Padm<> padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    padm.setStartParameterCorrection(0.8);
    padm.setTimelimit(0);
    padm.setNumberOfThreads(8);
    auto sol = padm.run(499);

    EXPECT_TRUE(sol.subsetSize == 499);
}

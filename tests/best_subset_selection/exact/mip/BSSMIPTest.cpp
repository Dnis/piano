// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 02.05.20.
//

#include <BssMip.h>
#include <gtest/gtest.h>
#include <BssExhaustiveSearch.h>
#include <DataStandardization.h>
#include "cplex/BssMipCplex.h"
#include <PianoAssert.h>
#include <config.h>

using namespace arma;
using namespace piano::bss;

#ifdef PIANO_USE_CPLEX

TEST_F(Small1Test, BSSAndExhaustiveSearchReturnSameSolution1) {
    ExhaustiveSearch exhaustiveSearch((dataStandardization->getX()), (dataStandardization->getY()),
                                      (dataStandardization->getGamma()));
    exhaustiveSearch.setNumberOfThreads(4);
    auto exhaustiveSol = dataStandardization->recoverSolution(exhaustiveSearch.run(5));

    Mip<> bss(
            *(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    bss.mute(false);
    auto bssSol = dataStandardization->recoverSolution(bss.run(5));

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, bssSol.estimatedPredictionError);
}

TEST_F(Small1RidgeTest, BSSAndExhaustiveSearchReturnSameSolutionWithRidge) {
    ExhaustiveSearch exhaustiveSearch((dataStandardization->getX()), (dataStandardization->getY()),
                                      (dataStandardization->getGamma()));
    exhaustiveSearch.setNumberOfThreads(4);
    auto exhaustiveSol = dataStandardization->recoverSolution(exhaustiveSearch.run(5));

    Mip<> bss(
            *(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    bss.mute(false);
    auto bssSol = dataStandardization->recoverSolution(bss.run(5));

    EXPECT_TRUE(piano::isSameSubset(exhaustiveSol.subset, bssSol.subset));
    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, bssSol.estimatedPredictionError);
}

TEST_F(Small1RidgeInterceptTest, BSSAndExhaustiveSearchReturnSameSolutionWithRidgeAndIntercept) {
    ExhaustiveSearch exhaustiveSearch((dataStandardization->getX()), (dataStandardization->getY()),
                                      (dataStandardization->getGamma()));
    exhaustiveSearch.setNumberOfThreads(4);
    auto exhaustiveSol = dataStandardization->recoverSolution(exhaustiveSearch.run(5));

    Mip<> bss(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    bss.mute(false);
    auto bssSol = dataStandardization->recoverSolution(bss.run(5));

    EXPECT_TRUE(piano::isSameSubset(exhaustiveSol.subset, bssSol.subset));
    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, bssSol.estimatedPredictionError);
}

TEST_F(Small2Test, BSSAndExhaustiveSearchReturnSameSolution2) {
    ExhaustiveSearch exhaustiveSearch((dataStandardization->getX()), (dataStandardization->getY()),
                                      (dataStandardization->getGamma()));
    exhaustiveSearch.setNumberOfThreads(4);
    auto exhaustiveSol = dataStandardization->recoverSolution(exhaustiveSearch.run(5));

    Mip<Padm<>, piano::bss::mip::solver::Cplex<piano::bss::mip::indicator_constraints::cplex::BigMConstraints<>>> bss(
            *(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    bss.mute(false);
    auto bssSol = dataStandardization->recoverSolution(bss.run(5));

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, bssSol.estimatedPredictionError);
}

TEST_F(Small2Test, GapIsZero) {
    Mip<> bss(
            *(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    bss.mute(false);
    auto bssSol = dataStandardization->recoverSolution(bss.run(5));

    EXPECT_DOUBLE_EQ(0, bssSol.gap);
}

TEST_F(Small1RidgeTest, sameSolutionAsADM1) {
    Mip<> bss(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    bss.mute(false);
    auto sol1 = bss.run(5);

    Padm<> padm(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    auto sol2 = padm.run(5);

    auto recSol1 = dataStandardization->recoverSolution(sol1);
    auto recSol2 = dataStandardization->recoverSolution(sol2);

    ASSERT_EQ(sol1.subsetSize, sol2.subsetSize);
    std::sort(sol1.subset.begin(), sol1.subset.end());
    std::sort(sol2.subset.begin(), sol2.subset.end());
    for (size_t i = 0; i < sol1.subsetSize; ++i) {
        EXPECT_EQ(sol1.subset[i], sol2.subset[i]);
    }

    EXPECT_DOUBLE_EQ(sol1.rss, sol2.rss);

    ASSERT_EQ(recSol1.subsetSize, recSol2.subsetSize);
    std::sort(recSol1.subset.begin(), recSol1.subset.end());
    std::sort(recSol2.subset.begin(), recSol2.subset.end());
    for (size_t i = 0; i < recSol1.subsetSize; ++i) {
        EXPECT_EQ(recSol1.subset[i], recSol2.subset[i]);
    }

    EXPECT_DOUBLE_EQ(recSol1.rss, recSol2.rss);
}

#endif
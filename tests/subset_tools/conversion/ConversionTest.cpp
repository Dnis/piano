// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 01.04.20.
//

#include <gtest/gtest.h>

#include <Conversion.h>

using namespace arma;
using namespace piano::stools;
using namespace piano;

TEST(ConversionTest, ToSubsetTest) {
    uvec v{0, 1, 0, 0};
    Subset subset1 = toSubset(v);
    ASSERT_EQ(subset1.size(), 1);
    EXPECT_EQ(subset1[0], 1);

    uvec u{1, 1, 0, 1, 0, 1};
    Subset corrSubset{0, 1, 3, 5};
    Subset subset2 = toSubset(u);
    ASSERT_EQ(subset2.size(), 4);
    for (size_t i = 0; i < 4; ++i) {
        EXPECT_EQ(corrSubset[i], subset2[i]);
    }
}

TEST(ConversionTest, ToEmptySubset) {
    Subset subset = toSubset(uvec{0, 0, 0, 0});
    EXPECT_EQ(subset.empty(), true);
}

TEST(ConversionTest, ToSubsetRValueTest) {
    Subset subset1 = toSubset(uvec{0, 1, 0, 0});
    ASSERT_EQ(subset1.size(), 1);
    EXPECT_EQ(subset1[0], 1);

    Subset corrSubset{0, 1, 3, 5};
    Subset subset2 = toSubset(uvec{{1, 1, 0, 1, 0, 1}});
    ASSERT_EQ(subset2.size(), 4);
    for (size_t i = 0; i < 4; ++i) {
        EXPECT_EQ(corrSubset[i], subset2[i]);
    }
}

TEST(ConversionTest, toIndicatorVectorTest) {
    Subset subset{0, 2, 4};
    uvec corrVec{1, 0, 1, 0, 1, 0};
    uvec vec = toIndicatorVector(subset, 6);
    ASSERT_EQ(vec.size(), 6);
    for (size_t i = 0; i < 6; ++i) {
        EXPECT_EQ(corrVec[i], vec[i]);
    }
}

TEST(ConversionTest, toIndicatorVectorRValueTest) {
    uvec corrVec{1, 0, 1, 0, 1, 0};
    uvec vec = toIndicatorVector(Subset{0, 2, 4}, 6);
    ASSERT_EQ(vec.size(), 6);
    for (size_t i = 0; i < 6; ++i) {
        EXPECT_EQ(corrVec[i], vec[i]);
    }
}

TEST(ConversionTest, fromCoefsTest) {
    vec coefs{0.1, 2, 0, 1, 0};
    auto subset = fromCoefs(coefs);
    Subset corrSubset{0, 1, 3};
    ASSERT_EQ(subset.size(), 3);
    for (size_t i = 0; i < 3; ++i) {
        EXPECT_EQ(corrSubset[i], subset[i]);
    }
}

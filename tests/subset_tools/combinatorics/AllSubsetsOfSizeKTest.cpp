// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 02.04.20.
//

#include <gtest/gtest.h>
#include "../../../private/subset_tools/combinatorics/AllSubsetsOfSizeK.cpp"

using namespace piano;
using namespace piano::stools;

TEST(IteratotGetSubsetsOfSizeKTest, GetSubsetOfSizeKWorksCorrectly1) {
    Subset subset = getSubsetOfSizeK(0, 3, 2);
    ASSERT_EQ(subset.size(), 2);
    EXPECT_EQ(subset[0], 0);
    EXPECT_EQ(subset[1], 1);
}

TEST(IteratotGetSubsetsOfSizeKTest, GetSubsetOfSizeKWorksCorrectly2) {
    Subset subset = getSubsetOfSizeK(1, 3, 2);
    ASSERT_EQ(subset.size(), 2);
    EXPECT_EQ(subset[0], 0);
    EXPECT_EQ(subset[1], 2);
}

TEST(IteratotGetSubsetsOfSizeKTest, GetSubsetOfSizeKWorksCorrectly3) {
    Subset subset = getSubsetOfSizeK(2, 3, 2);
    ASSERT_EQ(subset.size(), 2);
    EXPECT_EQ(subset[0], 1);
    EXPECT_EQ(subset[1], 2);
}

TEST(IteratotGetSubsetsOfSizeKTest, GetSubsetOfSizeKWorksCorrectly4) {
    Subset subset = getSubsetOfSizeK(2, 5, 3);
    ASSERT_EQ(subset.size(), 3);
    EXPECT_EQ(subset[0], 0);
    EXPECT_EQ(subset[1], 2);
    EXPECT_EQ(subset[2], 3);
}

TEST(IteratotGetSubsetsOfSizeKTest, GetSubsetOfSizeKWorksCorrectly5) {
    Subset subset = getSubsetOfSizeK(3, 5, 3);
    ASSERT_EQ(subset.size(), 3);
    EXPECT_EQ(subset[0], 1);
    EXPECT_EQ(subset[1], 2);
    EXPECT_EQ(subset[2], 3);
}


TEST(IteratorGetSubsetsOfSizeKTest, RandomAccessWorks)  {
    std::vector<Subset> v(10);
    v[0] = Subset{0, 1, 2};
    v[1] = Subset{0, 1, 3};
    v[2] = Subset{0, 2, 3};
    v[3] = Subset{1, 2, 3};
    v[4] = Subset{0, 1, 4};
    v[5] = Subset{0, 2, 4};
    v[6] = Subset{1, 2, 4};
    v[7] = Subset{0, 3, 4};
    v[8] = Subset{1, 3, 4};
    v[9] = Subset{2, 3, 4};

    size_t c = 0;
    AllSubsetsOfSizeK allSubsets(5, 3);
    ASSERT_EQ(allSubsets.size(), 10);
    for (size_t i = 0; i< allSubsets.size(); i++) {
        Subset subset = allSubsets[i];
        if (subset.empty()) std::cout << "{}\n";
        else {
            std::cout << "{";
            for (size_t j = 0; j < subset.size() - 1; ++j) {
                std::cout << subset[j] << ", ";
            }

            std::cout << subset[subset.size() - 1] << "}\n";
        }
        ASSERT_EQ(subset.size(), v[c].size());
        for (size_t i = 0; i < subset.size(); ++i) {
            EXPECT_EQ(subset[i], v[c][i]);
        }
        ++c;
    }
    EXPECT_EQ(c, 10);
}

TEST(IteratorGetSubsetsOfSizeKTest, IteratorWorks)  {
    std::vector<Subset> v(10);
    v[0] = Subset{0, 1, 2};
    v[1] = Subset{0, 1, 3};
    v[2] = Subset{0, 2, 3};
    v[3] = Subset{1, 2, 3};
    v[4] = Subset{0, 1, 4};
    v[5] = Subset{0, 2, 4};
    v[6] = Subset{1, 2, 4};
    v[7] = Subset{0, 3, 4};
    v[8] = Subset{1, 3, 4};
    v[9] = Subset{2, 3, 4};

    size_t c = 0;
    AllSubsetsOfSizeK allSubsets(5, 3);
    for (const auto &subset : allSubsets.const_iterator()) {
        if (subset.empty()) std::cout << "{}\n";
        else {
            std::cout << "{";
            for (size_t j = 0; j < subset.size() - 1; ++j) {
                std::cout << subset[j] << ", ";
            }

            std::cout << subset[subset.size() - 1] << "}\n";
        }
        ASSERT_EQ(subset.size(), v[c].size());
        for (size_t i = 0; i < subset.size(); ++i) {
            EXPECT_EQ(subset[i], v[c][i]);
        }
        ++c;
    }
    EXPECT_EQ(c, 10);
}

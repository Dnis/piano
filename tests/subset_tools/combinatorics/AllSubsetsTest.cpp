// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 01.04.20.
//

#include <gtest/gtest.h>

#include <AllSubsets.h>

using namespace piano;
using namespace piano::stools;

TEST(IteratotAllSubsetsTest, GetSubsetWorksCorrectly1) {
    Subset subset = getSubset(1, 3);
    ASSERT_EQ(subset.size(), 1);
    EXPECT_EQ(subset[0], 0);
}

TEST(IteratotAllSubsetsTest, GetSubsetWorksCorrectl1y2) {
    Subset subset = getSubset(5, 3);
    ASSERT_EQ(subset.size(), 2);
    EXPECT_EQ(subset[0], 0);
    EXPECT_EQ(subset[1], 2);
}

TEST(IteratorAllSubsetsTest, IteratorWorks)  {
    std::vector<Subset> v(8);
    v[0] = Subset{};
    v[1] = Subset{0};
    v[2] = Subset{1};
    v[3] = Subset{0, 1};
    v[4] = Subset{2};
    v[5] = Subset{0, 2};
    v[6] = Subset{1, 2};
    v[7] = Subset{0, 1, 2};

    size_t c = 0;
    AllSubsets allSubsets(3);
    for (const auto &subset : allSubsets.const_iterator()) {
        if (subset.empty()) std::cout << "{}\n";
        else {
            std::cout << "{";
            for (size_t j = 0; j < subset.size() - 1; ++j) {
                std::cout << subset[j] << ", ";
            }

            std::cout << subset[subset.size() - 1] << "}\n";
        }
        ASSERT_EQ(subset.size(), v[c].size());
        for (size_t i = 0; i < subset.size(); ++i) {
            EXPECT_EQ(subset[i], v[c][i]);
        }
        ++c;
    }
    EXPECT_EQ(c, 8);
}
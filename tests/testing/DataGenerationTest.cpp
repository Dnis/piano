// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

#include <gtest/gtest.h>
#include <SyntheticData.h>
#include <RhoCovariance.h>
#include <cmath>
#include <iostream>

using namespace piano;
using namespace arma;

TEST(DataGenerationTest, DefaultGeneratorProducesCorrectDimensions) {
    SyntheticDataGenerator generator(100, 10, 5);
    auto data = generator.draw();
    EXPECT_EQ(data.X.n_cols, 10);
    EXPECT_EQ(data.X.n_rows, 100);
    EXPECT_EQ(data.y.size(), 100);
}

TEST(DataGenerationTest, GeneratorWithEyeCovarianceProducesCorrectDimensions) {
    SyntheticDataGenerator<EyeCovariance> generator(100, 10, 5);
    auto data = generator.draw();
    EXPECT_EQ(data.X.n_cols, 10);
    EXPECT_EQ(data.X.n_rows, 100);
    EXPECT_EQ(data.y.size(), 100);
}

TEST(DataGenerationTest, GeneratorWithRhoCovarianceProducesCorrectDimensions) {
    SyntheticDataGenerator<RhoCovariance> generator(100, 10, 5);
    auto data = generator.draw();
    EXPECT_EQ(data.X.n_cols, 10);
    EXPECT_EQ(data.X.n_rows, 100);
    EXPECT_EQ(data.y.size(), 100);
}

TEST(DataGenerationTest, GeneratorWithEyeCovarianceProducesCorrectCovariance) {
    SyntheticDataGenerator<EyeCovariance> generator(1e5, 10, 5);
    auto data = generator.draw();
    mat covariance_matrix = cov(data.X);
    for (size_t i = 0; i < 10; ++i) {
        for (size_t j = 0; j < 10; ++j) {
            if (i == j) {
                EXPECT_NEAR(covariance_matrix(i, j), 1, 0.02);
            } else {
                EXPECT_NEAR(covariance_matrix(i, j), 0, 0.02);
            }
        }
    }
}

TEST(DataGenerationTest, GeneratorWithRhoCovarianceProducesCorrectCovariance) {
    SyntheticDataGenerator<RhoCovariance> generator(1e5, 10, 5);
    generator.covariance.rho = 0.3;
    auto data = generator.draw();
    mat covariance_matrix = cov(data.X);
    for (size_t i = 0; i < 10; ++i) {
        for (size_t j = 0; j < 10; ++j) {
            EXPECT_NEAR(covariance_matrix(i, j), std::pow(0.3, std::abs((int) i - (int) j)), 0.02);
        }
    }
}

TEST(DataGenerationTest, GeneratorProducesCorrectCoefficients) {
    SyntheticDataGenerator generator(100, 10, 5);
    generator.coefficients.lower_bound = 5.0;
    generator.coefficients.upper_bound = 25.0;
    auto data = generator.draw();
    EXPECT_EQ(data.beta.size(), 10);
    EXPECT_EQ(data.subset.size(), 5);
    for (const auto &index: data.subset) {
        EXPECT_LE(data.beta[index], 25);
        EXPECT_GE(data.beta[index], 5);
    }
}

TEST(DataGenerationTest, ErrorVarianceIsConsistentWithSNR) {
    std::vector<size_t> seeds{213, 4324, 5435, 42};
    SyntheticDataGenerator generator(4000, 10, 5);
    generator.snr = 1;
    for (const auto &seed: seeds) {
        generator.setSeed(seed);
        auto data = generator.draw();
        double expectedVariance = arma::var(data.noise);
        double actualVariance = arma::as_scalar(data.beta.t() * data.beta / generator.snr);
        std::cout << "Relative error: " << std::abs(expectedVariance - actualVariance) / expectedVariance << "\n";
        EXPECT_TRUE(std::abs(expectedVariance - actualVariance) / expectedVariance < 5e-2);
    }
}
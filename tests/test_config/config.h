//
// Created by Dennis Kreber on 21.05.22.
//

#ifndef PIANO_CONFIG_H
#define PIANO_CONFIG_H

#include <gtest/gtest.h>
#include <LinearAlgebra.h>
#include <memory>
#include "Types.h"
#include "DataStandardization.h"

class TestDataBase : public ::testing::Test {
protected:
    inline static arma::mat X;
    inline static arma::vec y;
    inline static arma::vec gamma;
    inline static piano::Subset subset;
    inline static std::unique_ptr<piano::DataStandardization> dataStandardization;
};

class Small1Test : public TestDataBase {
protected:
    static void SetUpTestSuite();
};

class Small1RidgeTest : public TestDataBase {
protected:
    static void SetUpTestSuite();
};

class Small1InterceptTest : public TestDataBase {
protected:
    static void SetUpTestSuite();
};

class Small1RidgeInterceptTest : public TestDataBase {
protected:
    static void SetUpTestSuite();
};

class Small2Test : public TestDataBase {
protected:
    static void SetUpTestSuite();
};

class Medium1Test : public TestDataBase {
protected:
    static void SetUpTestSuite();
};

class Medium2Test : public TestDataBase {
protected:
    static void SetUpTestSuite();
};

class Large1Test : public TestDataBase {
protected:
    static void SetUpTestSuite();
};

class Large2Test : public TestDataBase {
protected:
    static void SetUpTestSuite();
};

class Huge1Test : public TestDataBase {
protected:
    static void SetUpTestSuite();
};

class Huge2Test : public TestDataBase {
protected:
    static void SetUpTestSuite();
};

class Humongous1Test : public TestDataBase {
protected:
    static void SetUpTestSuite();
};

class Humongous2Test : public TestDataBase {
protected:
    static void SetUpTestSuite();
};




#endif //PIANO_CONFIG_H

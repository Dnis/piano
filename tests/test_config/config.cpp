//
// Created by Dennis Kreber on 21.05.22.
//

#include "config.h"
#include <TestData.h>

void Small1Test::SetUpTestSuite() {
    auto regressionData = piano::testdata::small(0);
    X = regressionData.X;
    y = regressionData.y;
    gamma = arma::zeros(X.n_cols);
    subset = regressionData.subset;
    dataStandardization = std::make_unique<piano::DataStandardization>(X, y, 0, true, false);
}

void Small1RidgeTest::SetUpTestSuite() {
    auto regressionData = piano::testdata::small(0);
    X = regressionData.X;
    y = regressionData.y;
    gamma = arma::ones(X.n_cols);
    subset = regressionData.subset;
    dataStandardization = std::make_unique<piano::DataStandardization>(X, y, 1, true, false);
}

void Small1InterceptTest::SetUpTestSuite() {
    auto regressionData = piano::testdata::small(0);
    X = regressionData.X;
    y = regressionData.y;
    gamma = arma::zeros(X.n_cols);
    subset = regressionData.subset;
    dataStandardization = std::make_unique<piano::DataStandardization>(X, y, 0, true, true);
}

void Small1RidgeInterceptTest::SetUpTestSuite() {
    auto regressionData = piano::testdata::small(0);
    X = regressionData.X;
    y = regressionData.y;
    gamma = arma::ones(X.n_cols);
    subset = regressionData.subset;
    dataStandardization = std::make_unique<piano::DataStandardization>(X, y, 1, true, true);
}

void Small2Test::SetUpTestSuite() {
    auto regressionData = piano::testdata::small(1);
    X = regressionData.X;
    y = regressionData.y;
    gamma = arma::zeros(X.n_cols);
    subset = regressionData.subset;
    dataStandardization = std::make_unique<piano::DataStandardization>(X, y, 0, true, false);
}

void Medium1Test::SetUpTestSuite() {
    auto regressionData = piano::testdata::medium(0);
    X = regressionData.X;
    y = regressionData.y;
    gamma = arma::zeros(X.n_cols);
    subset = regressionData.subset;
    dataStandardization = std::make_unique<piano::DataStandardization>(X, y, 0, true, false);
}

void Medium2Test::SetUpTestSuite() {
    auto regressionData = piano::testdata::medium(1);
    X = regressionData.X;
    y = regressionData.y;
    gamma = arma::zeros(X.n_cols);
    subset = regressionData.subset;
    dataStandardization = std::make_unique<piano::DataStandardization>(X, y, 0, true, false);
}

void Large1Test::SetUpTestSuite() {
    auto regressionData = piano::testdata::large(0);
    X = regressionData.X;
    y = regressionData.y;
    gamma = arma::zeros(X.n_cols);
    subset = regressionData.subset;
    dataStandardization = std::make_unique<piano::DataStandardization>(X, y, 0, true, false);
}

void Large2Test::SetUpTestSuite() {
    auto regressionData = piano::testdata::large(1);
    X = regressionData.X;
    y = regressionData.y;
    gamma = arma::zeros(X.n_cols);
    subset = regressionData.subset;
    dataStandardization = std::make_unique<piano::DataStandardization>(X, y, 0, true, false);
}

void Huge1Test::SetUpTestSuite() {
    auto regressionData = piano::testdata::huge(0);
    X = regressionData.X;
    y = regressionData.y;
    gamma = arma::zeros(X.n_cols);
    subset = regressionData.subset;
    dataStandardization = std::make_unique<piano::DataStandardization>(X, y, 0, true, false);
}

void Huge2Test::SetUpTestSuite() {
    auto regressionData = piano::testdata::huge(1);
    X = regressionData.X;
    y = regressionData.y;
    gamma = arma::zeros(X.n_cols);
    subset = regressionData.subset;
    dataStandardization = std::make_unique<piano::DataStandardization>(X, y, 0, true, false);
}

void Humongous1Test::SetUpTestSuite() {
    auto regressionData = piano::testdata::humongous(0);
    X = regressionData.X;
    y = regressionData.y;
    gamma = arma::zeros(X.n_cols);
    subset = regressionData.subset;
    dataStandardization = std::make_unique<piano::DataStandardization>(X, y, 0, true, false);
}

void Humongous2Test::SetUpTestSuite() {
    auto regressionData = piano::testdata::humongous(1);
    X = regressionData.X;
    y = regressionData.y;
    gamma = arma::zeros(X.n_cols);
    subset = regressionData.subset;
    dataStandardization = std::make_unique<piano::DataStandardization>(X, y, 0, true, false);
}

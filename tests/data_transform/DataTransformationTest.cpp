// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 26.03.20.
//

#include <DataStandardization.h>
#include <gtest/gtest.h>
#include <LinearAlgebra.h>
#include <LeastSquares.h>

using namespace piano;
using namespace arma;

class DataTransformTest1 : public ::testing::Test {

protected:
    mat X{
            {2, 0, 0},
            {0, 2, 0},
            {0, 0, 2}
    };
    vec y{2, 3, 4};
};

class DataTransformTest2 : public ::testing::Test {

protected:

    mat X{
            {2, 0},
            {0, 2},
            {0, 0}
    };
    vec y{2, 3, 4};
};

TEST_F(DataTransformTest1, IsCorrectlyNormalized) {
    DataStandardization transformation(X, y, 0, true, false);
    const mat &nX = *(transformation.getX());
    const vec &ny = *(transformation.getY());

    ASSERT_EQ(nX.n_cols, 3);
    ASSERT_EQ(nX.n_rows, 3);
    ASSERT_EQ(ny.size(), 3);
    for (size_t i = 0; i < 3; ++i) {
        for (size_t j = 0; j < 3; ++j) {
            EXPECT_DOUBLE_EQ(X.at(i, j) / 2.0, nX.at(i, j));
        }
    }

    for (size_t k = 0; k < 3; ++k) {
        EXPECT_DOUBLE_EQ(ny[k], y[k] / sqrt(29));
    }
}

TEST_F(DataTransformTest1, CoefficientsAreCorrectlyRecovered1) {
    DataStandardization transformation(X, y, 0, true, false);
    const mat &nX = *(transformation.getX());
    const vec &ny = *(transformation.getY());
    nX.print();

    sls::LeastSquares leastSquares(&X, &y);
    sls::LeastSquares leastSquaresNormalized(&nX, &ny);

    Solution sol = leastSquares.compute();
    Solution solNormalized = leastSquaresNormalized.compute();
    vec coefs = sol.fullCoefficients();
    vec coefsDenormalized = transformation.recoverCoefficients(solNormalized.fullCoefficients());

    ASSERT_EQ(nX.n_cols, 3);
    for (size_t i = 0; i < 3; ++i) {
        EXPECT_DOUBLE_EQ(coefs[i], coefsDenormalized[i]);
    }
}

TEST_F(DataTransformTest1, CoefficientsAreCorrectlyRecovered2) {
    DataStandardization transformation(X, y, 3, true, false);
    const mat &nX = *(transformation.getX());
    const vec &ny = *(transformation.getY());

    sls::LeastSquares leastSquares(&X, &y, 3);
    sls::LeastSquares leastSquaresNormalized(&nX, &ny, 3.0 / 4.0);

    Solution sol = leastSquares.compute();
    Solution solNormalized = leastSquaresNormalized.compute();
    vec coefs = sol.fullCoefficients();
    vec coefsDenormalized = transformation.recoverCoefficients(solNormalized.fullCoefficients());

    ASSERT_EQ(nX.n_cols, 3);
    for (size_t i = 0; i < 3; ++i) {
        EXPECT_DOUBLE_EQ(coefs[i], coefsDenormalized[i]);
    }
}

TEST_F(DataTransformTest2, GammaIsCorrectlyComputed) {
    DataStandardization transformation(X, y, 1, false, true);
    ASSERT_EQ(transformation.getGamma()->size(), 3);
    EXPECT_DOUBLE_EQ(transformation.getGamma()->at(0), 0);
    EXPECT_DOUBLE_EQ(transformation.getGamma()->at(1), 1);
    EXPECT_DOUBLE_EQ(transformation.getGamma()->at(2), 1);
}

TEST_F(DataTransformTest2, CoefficientsAreCorrectlyRecoveredWithIntercept1) {
    DataStandardization transformation(X, y, 0, false, true);
    DataStandardization transformationNormalized(X, y, 0, true, true);
    const mat &X = *(transformation.getX());
    const vec &y = *(transformation.getY());
    const mat &nX = *(transformationNormalized.getX());
    const vec &ny = *(transformationNormalized.getY());

    sls::LeastSquares leastSquares(&X, &y);
    sls::LeastSquares leastSquaresNormalized(&nX, &ny);

    Solution sol = leastSquares.compute();
    Solution solNormalized = leastSquaresNormalized.compute();
    vec coefs = transformation.recoverCoefficients(sol.fullCoefficients());
    vec coefsDenormalized = transformationNormalized.recoverCoefficients(solNormalized.fullCoefficients());

    ASSERT_EQ(coefs.size(), 2);
    ASSERT_EQ(coefsDenormalized.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_DOUBLE_EQ(coefs[i], coefsDenormalized[i]);
    }

    double intercept = transformation.getIntercept(sol.fullCoefficients());
    double interceptNormalized = transformationNormalized.getIntercept(sol.fullCoefficients());
    EXPECT_DOUBLE_EQ(intercept, interceptNormalized);
}

TEST_F(DataTransformTest1, SolutionisCorrectlyRecovered1) {
    DataStandardization transformation(X, y, 0, false, false);
    DataStandardization transformationNormalized(X, y, 0, true, false);
    const mat &X = *(transformation.getX());
    const vec &y = *(transformation.getY());
    const mat &nX = *(transformationNormalized.getX());
    const vec &ny = *(transformationNormalized.getY());

    sls::LeastSquares leastSquares(&X, &y);
    sls::LeastSquares leastSquaresNormalized(&nX, &ny);

    Solution sol = leastSquares.compute();
    Solution solNormalized = leastSquaresNormalized.compute();
    Solution recovered = transformationNormalized.recoverSolution(solNormalized);

    ASSERT_EQ(sol.coefficients.size(), 3);
    ASSERT_EQ(recovered.coefficients.size(), 3);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_DOUBLE_EQ(sol.coefficients[i], recovered.coefficients[i]);
    }

    EXPECT_GT(recovered.nullScore, recovered.rss);
}

TEST_F(DataTransformTest2, SolutionisCorrectlyRecoveredWithIntercept1) {
    DataStandardization transformation(X, y, 0.0, false, true);
    const mat &X = *(transformation.getX());
    const vec &y = *(transformation.getY());

    sls::LeastSquares leastSquares(&X, &y);

    Solution sol = leastSquares.compute();
    Solution recovered = transformation.recoverSolution(sol);

    ASSERT_EQ(recovered.coefficients.size(), 2);
    EXPECT_DOUBLE_EQ(recovered.coefficients[0], -1);
    EXPECT_DOUBLE_EQ(recovered.coefficients[1], -0.5);
    EXPECT_DOUBLE_EQ(recovered.intercept, 4);

    EXPECT_GT(recovered.nullScore, recovered.rss);
}
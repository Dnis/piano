// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/4/20.
//

#include <gtest/gtest.h>
#include <Solution.h>
#include <LinearAlgebra.h>
#include <SolutionBuilder.h>
#include <TestData.h>
#include <Types.h>
#include <NotReadyForBuildException.h>
#include <PartialSolutionBuilder.h>
#include <OptimizationSolutionBuilder.h>
#include <SquaredL2.h>

using namespace piano;

TEST(SolutionTest, mssIsComputedCorrectly) {
    Solution sol;
    sol.rss = 10.0;
    sol.n = 10;

    EXPECT_DOUBLE_EQ(1, sol.mse());
}

TEST(SolutionTest, fullCoefsAreCorrectlyHandled) {
    Solution sol;
    sol.coefficients = {1.0, 2.0, 3.0};
    sol.subset = {0, 2, 4};
    sol.p = 6;

    arma::vec trueCoefs = {1.0, 0, 2.0, 0, 3.0, 0};
    arma::vec fullCoefs = sol.fullCoefficients();
    for (size_t i = 0; i < sol.p; ++i) {
        EXPECT_DOUBLE_EQ(trueCoefs[i], fullCoefs[i]);
    }
}

TEST(SolutionTest, indicatorVectorIsCorrectlyDetermined) {
    Solution sol;
    sol.subset = {0, 2, 4};
    sol.p = 6;
    arma::uvec v1{1, 0, 1, 0, 1, 0};
    arma::uvec v2 = sol.indicatorVector();
    for (size_t i = 0; i < sol.p; ++i) {
        EXPECT_EQ(v1[i], v2[i]);
    }
}

TEST(SolutionTest, solutionBuilderWorks) {
    auto data = piano::testdata::small();
    SolutionBuilder solutionBuilder(data.X, data.y, arma::zeros(data.X.n_cols));
    solutionBuilder.
            subset(Subset{0, 2, 3, 4, 5}).
            objectiveValue(0).
            coefficients(arma::vec{1, 2, 3, 4, 5}, false).
            estimatedPredictionError(0).
            completeRuntimeInSeconds(0).
            computationsRuntimeInSeconds(0);
    Solution sol = solutionBuilder.build();
    arma::vec expected{1, 2, 3, 4, 5};
    for (size_t j = 0; j < expected.size(); ++j) {
        EXPECT_DOUBLE_EQ(expected[j], sol.coefficients[j]);
    }

    double expectedRSS = piano::sls::squaredL2(data.X * arma::vec{1, 0, 2, 3, 4, 5, 0, 0, 0, 0} - data.y);
    EXPECT_DOUBLE_EQ(expectedRSS, sol.rss);
}

TEST(SolutionTest, solutionBuilderThrowsErrorWhenNotReady) {
    auto data = piano::testdata::small();
    SolutionBuilder solutionBuilder(data.X, data.y, arma::zeros(data.X.n_cols));
    EXPECT_THROW(auto sol = solutionBuilder.build(), NotReadyForBuildException);
}

TEST(SolutionTest, solutionBuilderWorksOnlyWhenOnlyProvidedWithCoefficients) {
    auto data = piano::testdata::small();
    SolutionBuilder solutionBuilder(data.X, data.y, arma::zeros(data.X.n_cols));
    solutionBuilder.
            objectiveValue(0).
            coefficients(arma::vec{1, 0, 2, 3, 4, 5, 0, 0, 0, 0}, true).
            estimatedPredictionError(0).
            completeRuntimeInSeconds(0).
            computationsRuntimeInSeconds(0);
    Solution sol = solutionBuilder.build();
    arma::vec expected{1, 2, 3, 4, 5};
    for (size_t j = 0; j < expected.size(); ++j) {
        EXPECT_DOUBLE_EQ(expected[j], sol.coefficients[j]);
    }

    double expectedRSS = piano::sls::squaredL2(data.X * arma::vec{1, 0, 2, 3, 4, 5, 0, 0, 0, 0} - data.y);
    EXPECT_DOUBLE_EQ(expectedRSS, sol.rss);
}

TEST(SolutionTest, solutionBuilderWorksWhenFirstProvidedWithCoefficientsAndThenWithASubset) {
    auto data = piano::testdata::small();
    SolutionBuilder solutionBuilder(data.X, data.y, arma::zeros(data.X.n_cols));
    solutionBuilder.
            objectiveValue(0).
            coefficients(arma::vec{1, 2, 3, 4, 5}, false).
            subset(Subset{0, 2, 3, 4, 5}).
            estimatedPredictionError(0).
            completeRuntimeInSeconds(0).
            computationsRuntimeInSeconds(0);
    Solution sol = solutionBuilder.build();
    arma::vec expected{1, 2, 3, 4, 5};
    for (size_t j = 0; j < expected.size(); ++j) {
        EXPECT_DOUBLE_EQ(expected[j], sol.coefficients[j]);
    }

    double expectedRSS = piano::sls::squaredL2(data.X * arma::vec{1, 0, 2, 3, 4, 5, 0, 0, 0, 0} - data.y);
    EXPECT_DOUBLE_EQ(expectedRSS, sol.rss);
}

TEST(PartialSolutionTest, partialSolutionBuilderWorksOnlyWithCoefficients) {
    PartialSolutionBuilder builder;
    builder.
            objectiveValue(0).
            subset(Subset{0, 2, 5}).
            completeRuntimeInSeconds(0).
            computationsRuntimeInSeconds(0);
    PartialSolution sol = builder.build();
    Subset expected{0, 2, 5};
    for (size_t j = 0; j < expected.size(); ++j) {
        EXPECT_DOUBLE_EQ(expected[j], sol.subset[j]);
    }
}

TEST(PartialSolutionTest, partialSolutionBuilderThrowsErrorWhenNotReady) {
    PartialSolutionBuilder builder;
    EXPECT_THROW(builder.build(), NotReadyForBuildException);
}

TEST(OptimizationSolutionTest, optimizationSolutionBuilderWorks) {
    auto data = piano::testdata::small();
    OptimizationSolutionBuilder builder(data.X.n_rows, data.X.n_cols);
    builder.
            subset(Subset{0, 2, 3, 4, 5}).
            objectiveValue(0).
            coefficients(arma::vec{1, 2, 3, 4, 5}, false).
            completeRuntimeInSeconds(0).
            computationsRuntimeInSeconds(0);
    OptimizationSolution sol = builder.build();
    arma::vec expected{1, 2, 3, 4, 5};
    for (size_t j = 0; j < expected.size(); ++j) {
        EXPECT_DOUBLE_EQ(expected[j], sol.coefficients[j]);
    }
}

TEST(OptimizationSolutionTest, optimizationSolutionBuilderThrowsErrorWhenNotReady) {
    OptimizationSolutionBuilder builder(2000, 10);
    EXPECT_THROW(builder.build(), NotReadyForBuildException);
}


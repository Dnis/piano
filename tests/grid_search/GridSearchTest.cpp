// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 10.03.20.
//

#include <gtest/gtest.h>
#include <GridSearch.h>
#include <numeric>

class GridSearch_CorrectResult1 : public ::testing::Test {

protected:
    static double f(double x) { return x*x; }
};

class GridSearch_CorrectResult2 : public ::testing::Test {

protected:
    static double f(double x) { return -x; }
};

class GridSearch_CorrectResult3 : public ::testing::Test {

protected:
    static double f(double x) { return -x; }
};


TEST_F(GridSearch_CorrectResult1, CorrectResult1) {
    std::vector<double> v{-2, 0, 1};

    struct Comp {
        bool operator()(double lhs, double rhs) {
            return lhs < rhs;
        }
    };

    auto res = gridSearch(4, v, f, Comp{});
    EXPECT_DOUBLE_EQ(res, 0);
}

TEST_F(GridSearch_CorrectResult2, CorrectResult2) {
    long len = 100000;
    std::vector<double> v(len);
    std::iota(v.begin(), v.end(), 0);

    struct Comp {
        bool operator()(double lhs, double rhs) {
            return lhs < rhs;
        }
    };

    auto res = gridSearch(4, v, f, Comp{});
    EXPECT_DOUBLE_EQ(res, -len+1);
}

TEST_F(GridSearch_CorrectResult3, CorrectResult3) {
    long len = 100000;
    std::vector<double> v(len);
    std::iota(v.begin(), v.end(), 0);

    struct Comp {
        bool operator()(double lhs, double rhs) {
            return lhs < rhs;
        }
    };

    auto res = gridSearch(4, v, f);
    EXPECT_DOUBLE_EQ(res, -len+1);
}

TEST_F(GridSearch_CorrectResult1, CorrectResult4) {
    long len = 100000;
    std::vector<double> v(len);
    std::iota(v.begin(), v.end(), 10);

    struct Comp {
        bool operator()(double lhs, double rhs) {
            return lhs < rhs;
        }
    };

    auto res = argGridSearch(4, v, f, Comp{});
    EXPECT_DOUBLE_EQ(res, 10);
}
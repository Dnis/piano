// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 24.03.20.
//

#include <gtest/gtest.h>
#include <Partition.h>

using namespace arma;
using namespace piano;

TEST(PartitionTest, CorrectPartition) {
    cv::Partition partition(10, 3);
    uvec part = partition.getPartition(0);
    ASSERT_EQ(part.size(), 3);
    EXPECT_DOUBLE_EQ(part[0], 0);
    EXPECT_DOUBLE_EQ(part[1], 1);
    EXPECT_DOUBLE_EQ(part[2], 2);

    part = partition.getPartition(1);
    ASSERT_EQ(part.size(), 3);
    EXPECT_DOUBLE_EQ(part[0], 3);
    EXPECT_DOUBLE_EQ(part[1], 4);
    EXPECT_DOUBLE_EQ(part[2], 5);

    part = partition.getPartition(2);
    ASSERT_EQ(part.size(), 4);
    EXPECT_DOUBLE_EQ(part[0], 6);
    EXPECT_DOUBLE_EQ(part[1], 7);
    EXPECT_DOUBLE_EQ(part[2], 8);
    EXPECT_DOUBLE_EQ(part[3], 9);
}

TEST(PartitionTest, CorrectComplementPartition) {
    cv::Partition partition(10, 3);
    uvec part = partition.getComplementPartition(0);
    ASSERT_EQ(part.size(), 7);
    EXPECT_DOUBLE_EQ(part[0], 3);
    EXPECT_DOUBLE_EQ(part[1], 4);
    EXPECT_DOUBLE_EQ(part[2], 5);
    EXPECT_DOUBLE_EQ(part[3], 6);
    EXPECT_DOUBLE_EQ(part[4], 7);
    EXPECT_DOUBLE_EQ(part[5], 8);
    EXPECT_DOUBLE_EQ(part[6], 9);

    part = partition.getComplementPartition(1);
    ASSERT_EQ(part.size(), 7);
    EXPECT_DOUBLE_EQ(part[0], 0);
    EXPECT_DOUBLE_EQ(part[1], 1);
    EXPECT_DOUBLE_EQ(part[2], 2);
    EXPECT_DOUBLE_EQ(part[3], 6);
    EXPECT_DOUBLE_EQ(part[4], 7);
    EXPECT_DOUBLE_EQ(part[5], 8);
    EXPECT_DOUBLE_EQ(part[6], 9);

    part = partition.getComplementPartition(2);
    ASSERT_EQ(part.size(), 6);
    EXPECT_DOUBLE_EQ(part[0], 0);
    EXPECT_DOUBLE_EQ(part[1], 1);
    EXPECT_DOUBLE_EQ(part[2], 2);
    EXPECT_DOUBLE_EQ(part[3], 3);
    EXPECT_DOUBLE_EQ(part[4], 4);
    EXPECT_DOUBLE_EQ(part[5], 5);
}

class PartitionTest2 : public ::testing::Test {

protected:


protected:
    virtual void SetUp() {

    }

    vec v{1, 2, 3};
    vec v1{1};
    vec v2{2, 3};

    mat X{
            {1, 2},
            {3, 4},
            {5, 6}
    };
    mat X1{
            {1, 2}
    };
    mat X2{
            {3, 4},
            {5, 6}
    };
};

TEST_F(PartitionTest2, VectorTest) {
    cv::Partition partition(3, 2);
    vec testV = partition.getPartitionMatrix(0, v);
    ASSERT_EQ(testV.size(), v1.size());
    EXPECT_DOUBLE_EQ(testV[0], v1[0]);

    testV = partition.getPartitionMatrix(1, v);
    ASSERT_EQ(testV.size(), v2.size());
    for (size_t i = 0; i < v2.size(); ++i) {
        EXPECT_DOUBLE_EQ(v2[i], testV[i]);
    }
}

TEST_F(PartitionTest2, VectorComplementTest) {
    cv::Partition partition(3, 2);
    vec testV = partition.getComplementPartitionVector(1, v);
    ASSERT_EQ(testV.size(), v1.size());
    EXPECT_DOUBLE_EQ(testV[0], v1[0]);

    testV = partition.getComplementPartitionVector(0, v);
    ASSERT_EQ(testV.size(), v2.size());
    for (size_t i = 0; i < v2.size(); ++i) {
        EXPECT_DOUBLE_EQ(v2[i], testV[i]);
    }
}

TEST_F(PartitionTest2, MatrixTest) {
    cv::Partition partition(3, 2);
    mat testX = partition.getPartitionMatrix(0, X);
    ASSERT_EQ(testX.n_rows, X1.n_rows);
    for (size_t j = 0; j < X1.n_rows; ++j) {
        for (size_t i = 0; i < X1.n_cols; ++i) {
            EXPECT_DOUBLE_EQ(testX.at(j, i), X1.at(j, i));
        }
    }


    testX = partition.getPartitionMatrix(1, X);
    ASSERT_EQ(testX.n_rows, X2.n_rows);
    for (size_t j = 0; j < X2.n_rows; ++j) {
        for (size_t i = 0; i < X2.n_cols; ++i) {
            EXPECT_DOUBLE_EQ(testX.at(j, i), X2.at(j, i));
        }
    }
}

TEST_F(PartitionTest2, MatrixComplementTest) {
    cv::Partition partition(3, 2);
    mat testX = partition.getComplementPartitionMatrix(1, X);
    ASSERT_EQ(testX.n_rows, X1.n_rows);
    for (size_t j = 0; j < X1.n_rows; ++j) {
        for (size_t i = 0; i < X1.n_cols; ++i) {
            EXPECT_DOUBLE_EQ(testX.at(j, i), X1.at(j, i));
        }
    }


    testX = partition.getComplementPartitionMatrix(0, X);
    ASSERT_EQ(testX.n_rows, X2.n_rows);
    for (size_t j = 0; j < X2.n_rows; ++j) {
        for (size_t i = 0; i < X2.n_cols; ++i) {
            EXPECT_DOUBLE_EQ(testX.at(j, i), X2.at(j, i));
        }
    }
}
#  Copyright 2021  Dennis Kreber
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#

import numpy as np

from sklearn.linear_model import RidgeCV, Ridge
from sklearn.model_selection import KFold

if __name__ == "__main__":
    p = 200
    n = 2000
    np.random.seed(42)
    X = np.random.rand(n, p)
    for i in range(p):
        norm = np.linalg.norm(X[:, i], 2)
        X[:, i] = X[:, i] / norm
    y = np.ones(n, dtype=np.double)
    y = y / np.linalg.norm(y, 2)
    np.savetxt("X1.testdata", X, delimiter=" ", newline="\n")
    np.savetxt("y1.testdata", y, delimiter=" ", newline="\n")

    clf = RidgeCV(alphas=[1, 2, 3], fit_intercept=False, cv=5, scoring="neg_mean_squared_error").fit(X, y)

    k_fold = KFold(5)
    s = 0
    for k, (train, test) in enumerate(k_fold.split(X, y)):
        rid = Ridge(clf.alpha_, fit_intercept=False).fit(X[train], y[train])
        s += 1.0 / len(test) * np.linalg.norm(rid.predict(X[test]) - y[test], 2) ** 2

    np.savetxt("cv_value1.testdata", np.array([s]))

    clf = RidgeCV(alphas=list(np.arange(0, 10, 0.1)), fit_intercept=False, cv=5, scoring="neg_mean_squared_error").fit(X, y)

    k_fold = KFold(5)
    s = 0
    for k, (train, test) in enumerate(k_fold.split(X, y)):
        rid = Ridge(clf.alpha_, fit_intercept=False).fit(X[train], y[train])
        s += 1.0 / len(test) * np.linalg.norm(rid.predict(X[test]) - y[test], 2) ** 2

    np.savetxt("cv_value2.testdata", np.array([s]))

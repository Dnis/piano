// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 25.03.20.
//

#include <gtest/gtest.h>
#include <CrossValidation.h>
#include <SquaredL2.h>

using namespace arma;
using namespace piano;
using namespace piano::cv;
using namespace piano::sls;

class KFoldCrossValidationTest1 : public ::testing::Test {

protected:
    virtual void SetUp() {
        std::string file_path = __FILE__;
        std::string dir_path;
        const size_t last_slash_idx = file_path.rfind('/');
        dir_path = file_path.substr(0, last_slash_idx);

        this->X.load(dir_path + "/X1.testdata", raw_ascii);

        this->y.load(dir_path + "/y1.testdata", raw_ascii);

        std::fstream myfile(dir_path + "/cv_value1.testdata", std::ios_base::in);
        myfile >> this->cvValue;
    }

    mat X;
    mat y;
    double cvValue;
};

TEST_F(KFoldCrossValidationTest1, CorrectResult1) {
    std::vector<double> grid{1, 2, 3};

    double optRidgeParam = piano::cv::kFoldCrossValidationGridSearch(4, X, y, 5, grid,
                                                                                 mse,
                                                                                 RidgeTraining{});
    EXPECT_NEAR(cvValue, kFoldCrossValidation(X, y, 5, optRidgeParam, mse, RidgeTraining{}), 1e-5);
}

class KFoldCrossValidationTest2 : public ::testing::Test {

protected:
    virtual void SetUp() {
        std::string file_path = __FILE__;
        std::string dir_path;
        const size_t last_slash_idx = file_path.rfind('/');
        dir_path = file_path.substr(0, last_slash_idx);

        this->X.load(dir_path + "/X1.testdata", raw_ascii);

        this->y.load(dir_path + "/y1.testdata", raw_ascii);

        std::fstream myfile(dir_path + "/cv_value2.testdata", std::ios_base::in);
        myfile >> this->cvValue;
    }

    mat X;
    mat y;
    double cvValue;
};

TEST_F(KFoldCrossValidationTest2, CorrectResult2) {
    std::vector<double> grid(100);
    for (size_t i = 0; i < 100; ++i) {
        grid[i] = 0.1 * i;
    }

    double optRidgeParam = piano::cv::kFoldCrossValidationGridSearch(4, X, y, 5, grid,
                                                                                 mse,RidgeTraining{});
    EXPECT_NEAR(cvValue, kFoldCrossValidation(X, y, 5, optRidgeParam, mse, RidgeTraining{}), 1e-5);
}

TEST_F(KFoldCrossValidationTest2, TrainingOutputIsCorrect) {
    std::vector<double> grid(100);
    for (size_t i = 0; i < 100; ++i) {
        grid[i] = 0.1 * i;
    }

    std::vector<arma::vec> trainingOutputs;
    double optRidgeInput = piano::cv::kFoldCrossValidationGridSearch(4, X, y, 5, grid, mse, RidgeTraining{}, &trainingOutputs);

    double sum = 0;
    Partition partition(2000, 5);
    for (size_t j = 0; j < 5; ++j) {
        arma::mat vX = partition.getPartitionMatrix(j, X);
        arma::vec vy = partition.getPartitionMatrix(j, y);
        sum += 1.0 / vy.size() * squaredL2(vX*trainingOutputs[j] - vy);
    }

    EXPECT_NEAR(kFoldCrossValidation(X, y, 5, optRidgeInput, mse, RidgeTraining{}), sum, 1e-9);
}
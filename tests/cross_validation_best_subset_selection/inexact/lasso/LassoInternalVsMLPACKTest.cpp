// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by kreber on 30.09.20.
//

#include <gtest/gtest.h>
#include <DataStandardization.h>
#include <Lasso.h>
#include <config.h>

using namespace arma;
using namespace piano::cvbss;
using namespace piano;

#ifdef PIANO_USE_MLPACK
TEST_F(Small1Test, SameSolutionIsReturned) {
    DataStandardization transformation(X, y, 0.0, true, false);
    Lasso<lasso::solver::internal::CoordinateDescent> lasso1(*(transformation.getX()), *(transformation.getY()),
                                                             *(transformation.getGamma()));
    auto sol1 = lasso1.run();
    sol1.fullCoefficients().print();
    Lasso<lasso::solver::mlpack::LARS> lasso2(*(transformation.getX()), *(transformation.getY()),
                                              *(transformation.getGamma()));
    auto sol2 = lasso2.run();
    sol2.fullCoefficients().print();
    EXPECT_LE(std::abs(sol1.objectiveValue - sol2.objectiveValue), 1e-6);
    for (size_t i = 0; i < X.n_cols; ++i) {
        EXPECT_LE(std::abs(sol1.fullCoefficients()[i] - sol2.fullCoefficients()[i]), 1e-6);
    }
}

TEST_F(Small1Test, SameSolutionWhenYIsNotNormalized) {
    DataStandardization transformation(X, y, 0.0, true, false);
    Lasso<lasso::solver::internal::CoordinateDescent> lasso1(*(transformation.getX()), y,
                                                             *(transformation.getGamma()));
    auto sol1 = lasso1.setLambda(0.01).run();
    sol1.fullCoefficients().print();
    std::cout << "\n\n";
    Lasso<lasso::solver::mlpack::LARS> lasso2(*(transformation.getX()), y,
                                                            *(transformation.getGamma()));
    lasso2.setLambda(0.01);
    auto sol2 = lasso2.run();
    sol2.fullCoefficients().print();
    EXPECT_LE(std::abs(sol1.objectiveValue - sol2.objectiveValue), 1e-6);
    for (size_t i = 0; i < X.n_cols; ++i) {
        std::cout << i << ":\n" << sol1.fullCoefficients()[i] << " vs. " << sol2.fullCoefficients()[i] << "\n\n";
        EXPECT_LE(std::abs(sol1.fullCoefficients()[i] - sol2.fullCoefficients()[i]), 1e-5);
    }
}
#endif
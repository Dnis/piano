// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 15.04.20.
//

#include <gtest/gtest.h>
#include <DataStandardization.h>
#include <Lasso.h>
#include <BinarySearch.h>
#include <config.h>

using namespace arma;
using namespace piano::cvbss;
using namespace piano;

#ifdef PIANO_USE_MLPACK
TEST_F(Huge1Test, LARSNumThreads) {
    DataStandardization transformation(X, y, 0.0, true, false);
    bool success = true;
    Solution sol;
    try {
        Lasso<lasso::solver::mlpack::LARS> lasso(*(transformation.getX()), *(transformation.getY()),
                                                 *(transformation.getGamma()));
        lasso.setNumberOfThreads(1);
        sol = lasso.run();
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

#endif

TEST_F(Small1Test, CoordianteDescentHPTRunsSuccessfully) {
    DataStandardization transformation(X, y, 0.0, true, false);
    bool success = true;
    Solution sol;
    try {
        Lasso<lasso::solver::internal::CoordinateDescent> lasso(*(transformation.getX()), *(transformation.getY()),
                                                              *(transformation.getGamma()));
        auto lassoHPT = lasso.getHyperParameterTuner();
        lassoHPT.setNumberOfThreads(8);
        sol = lassoHPT.run();
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

#ifdef PIANO_USE_MLPACK
TEST_F(Small1Test, LARSHPTRunsSuccessfully) {
    DataStandardization transformation(X, y, 0.0, true, false);
    bool success = true;
    Solution sol;
    try {
        Lasso<lasso::solver::mlpack::LARS> lasso(*(transformation.getX()), *(transformation.getY()),
                                                 *(transformation.getGamma()));
        auto lassoHPT = lasso.getHyperParameterTuner();
        sol = lassoHPT.run();
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

#endif

#ifdef PIANO_USE_MLPACK
TEST_F(Huge1Test, LARSWorksCorrectly1) {
    DataStandardization transformation(X, y, 0.0, true, false);
    Solution sol;

    Lasso<lasso::solver::mlpack::LARS> lasso(*(transformation.getX()), *(transformation.getY()),
                                             *(transformation.getGamma()));
    lasso.setNumberOfThreads(1);
    sol = lasso.run();

    EXPECT_TRUE(sol.subsetSize > 0);
}
#endif

#ifdef PIANO_USE_PICASSO
TEST_F(Huge1Test, PICASSOWarmUpWorks1) {
    DataStandardization transformation(X, y, 0.0, true, false);
    bool success = true;
    Solution sol;
    try {
        Lasso<lasso::solver::picasso::CoordinateDescent> lasso(*(transformation.getX()), *(transformation.getY()),
                                                               *(transformation.getGamma()));
        lasso.computeLambdaPath();
        sol = lasso.run();
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}
#endif

#ifdef PIANO_USE_PICASSO
TEST_F(Small1Test, PICASSOLambdaPathYieldsCorrectSparsity) {
    DataStandardization transformation(X, y, 0.0, true, false);

    Solution sol;
    Lasso<lasso::solver::picasso::CoordinateDescent> lasso(*(transformation.getX()), *(transformation.getY()),
                                                               *(transformation.getGamma()));
    lasso.computeLambdaPath();

    size_t ind = discreteBinarySearch<size_t>(lasso.getNonzerosPath(), BinarySearch::Identity<size_t>{}, 2);

    lasso.setLambda(lasso.getLambdaPath()[ind]);
    sol = lasso.run();
    EXPECT_EQ(2, sol.subsetSize);
}
#endif

#ifdef PIANO_USE_PICASSO
TEST_F(Small1Test, SolutionsForLambdaPathYieldBetaPath1) {
    DataStandardization transformation(X, y, 0.0, true, false);

    Solution sol;
    Lasso<lasso::solver::picasso::CoordinateDescent> lasso(*(transformation.getX()), *(transformation.getY()),
                                                           *(transformation.getGamma()));
    auto lambdaPath = lasso.getLambdaPath();

    for (size_t j = 0; j < lambdaPath.size(); ++j) {
        lasso.computePartialSolution();
        arma::vec betaComputed = lasso.getBeta();
        arma::vec beta = lasso.getBeta();
        ASSERT_EQ(betaComputed.size(), beta.size());
        for (size_t k = 0; k < beta.size(); ++k) {
            EXPECT_LE(std::abs(beta[k] - betaComputed[k]), 1e-5);
        }
    }
}
#endif

#ifdef PIANO_USE_PICASSO
TEST_F(Small1Test, PICASSOFirstLambdaYieldsZeroNonzeros1) {
    DataStandardization transformation(X, y, 0.0, true, false);

    Solution sol;
    Lasso<lasso::solver::picasso::CoordinateDescent> lasso(*(transformation.getX()), *(transformation.getY()),
                                                           *(transformation.getGamma()));
    auto nonzeros = lasso.getNonzerosPath();

    EXPECT_EQ(nonzeros[0], 0);
}

TEST_F(Huge1Test, PICASSOFirstLambdaYieldsZeroNonzeros2) {
    DataStandardization transformation(X, y, 0.0, true, false);

    Solution sol;
    Lasso<lasso::solver::picasso::CoordinateDescent> lasso(*(transformation.getX()), *(transformation.getY()),
                                                           *(transformation.getGamma()));
    auto nonzeros = lasso.getNonzerosPath();

    EXPECT_EQ(nonzeros[0], 0);
}

TEST_F(Large1Test, PICASSOFirstLambdaYieldsZeroNonzeros3) {
    DataStandardization transformation(X, y, 0.0, true, false);

    Solution sol;
    Lasso<lasso::solver::picasso::CoordinateDescent> lasso(*(transformation.getX()), *(transformation.getY()),
                                                           *(transformation.getGamma()));
    auto nonzeros = lasso.getNonzerosPath();

    EXPECT_EQ(nonzeros[0], 0);
}
#endif

TEST_F(Small1Test, InternalFirstLambdaYieldsZeroNonzeros1) {
    DataStandardization transformation(X, y, 0.0, true, false);

    Solution sol;
    Lasso<lasso::solver::internal::CoordinateDescent> lasso(*(transformation.getX()), *(transformation.getY()),
                                                           *(transformation.getGamma()));
    auto nonzeros = lasso.getNonzerosPath();

    EXPECT_EQ(nonzeros[0], 0);
}
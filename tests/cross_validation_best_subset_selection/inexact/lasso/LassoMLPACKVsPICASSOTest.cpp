// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by kreber on 30.09.20.
//

#include <gtest/gtest.h>
#include <DataStandardization.h>
#include <Lasso.h>
#include <config.h>

using namespace arma;
using namespace piano::cvbss;
using namespace piano;

#ifdef PIANO_USE_MLPACK
#ifdef PIANO_USE_PICASSO
TEST_F(Small1Test, SameSolutionIsReturned1) {
    DataStandardization transformation(X, y, 0.0, true, false);
    Lasso<lasso::solver::mlpack::LARS> lasso1(*(transformation.getX()), *(transformation.getY()),
                                              *(transformation.getGamma()));
    auto sol1 = lasso1.setLambda(0.01).run();
    sol1.fullCoefficients().print();
    std::cout << "\n\n";
    Lasso<lasso::solver::picasso::CoordinateDescent> lasso2(*(transformation.getX()), *(transformation.getY()),
                                                            *(transformation.getGamma()));
    lasso2.setLambda(0.01);
    auto sol2 = lasso2.run();
    sol2.fullCoefficients().print();
    EXPECT_LE(std::abs(sol1.objectiveValue - sol2.objectiveValue), 1e-6);
    for (size_t i = 0; i < X.n_cols; ++i) {
        std::cout << i << ":\n" << sol1.fullCoefficients()[i] << " vs. " << sol2.fullCoefficients()[i] << "\n\n";
        EXPECT_LE(std::abs(sol1.fullCoefficients()[i] - sol2.fullCoefficients()[i]), 1e-4);
    }
}

TEST_F(Huge1Test, SameSolutionIsReturned2) {
    DataStandardization transformation(X, y, 0.0, true, false);
    Lasso<lasso::solver::mlpack::LARS> lasso1(*(transformation.getX()), *(transformation.getY()),
                                              *(transformation.getGamma()));
    auto sol1 = lasso1.setLambda(0.01).run();
    sol1.fullCoefficients().print();
    std::cout << "\n\n";
    Lasso<lasso::solver::picasso::CoordinateDescent> lasso2(*(transformation.getX()), *(transformation.getY()),
                                                            *(transformation.getGamma()));
    lasso2.setLambda(0.01);
    auto sol2 = lasso2.run();
    sol2.fullCoefficients().print();
    EXPECT_LE(std::abs(sol1.objectiveValue - sol2.objectiveValue), 1e-6);
    for (size_t i = 0; i < X.n_cols; ++i) {
        std::cout << i << ":\n" << sol1.fullCoefficients()[i] << " vs. " << sol2.fullCoefficients()[i] << "\n\n";
        EXPECT_LE(std::abs(sol1.fullCoefficients()[i] - sol2.fullCoefficients()[i]), 1e-4);
    }
}

TEST_F(Large1Test, SameSolutionIsReturned3) {
    DataStandardization transformation(X, y, 0.0, true, false);
    Lasso<lasso::solver::mlpack::LARS> lasso1(*(transformation.getX()), *(transformation.getY()),
                                              *(transformation.getGamma()));
    auto sol1 = lasso1.setLambda(0.1).run();
    sol1.fullCoefficients().print();
    std::cout << "\n\n";
    Lasso<lasso::solver::picasso::CoordinateDescent> lasso2(*(transformation.getX()), *(transformation.getY()),
                                                            *(transformation.getGamma()));
    lasso2.computeLambdaPath();
    lasso2.setLambda(0.1);
    auto sol2 = lasso2.run();
    sol2.fullCoefficients().print();
    EXPECT_LE(std::abs(sol1.objectiveValue - sol2.objectiveValue), 1e-6);
    for (size_t i = 0; i < X.n_cols; ++i) {
        std::cout << i << ":\n" << sol1.fullCoefficients()[i] << " vs. " << sol2.fullCoefficients()[i] << "\n\n";
        EXPECT_LE(std::abs(sol1.fullCoefficients()[i] - sol2.fullCoefficients()[i]), 1e-4);
    }
}

TEST_F(Small1Test, SameSolutionWhenYIsNotNormalized1) {
    DataStandardization transformation(X, y, 0.0, true, false);
    Lasso<lasso::solver::mlpack::LARS> lasso1(*(transformation.getX()), y,
                                              *(transformation.getGamma()));
    auto sol1 = lasso1.setLambda(10).run();
    sol1.fullCoefficients().print();
    std::cout << "\n\n";
    Lasso<lasso::solver::picasso::CoordinateDescent> lasso2(*(transformation.getX()), y,
                                                            *(transformation.getGamma()));
    lasso2.setLambda(10);
    auto sol2 = lasso2.run();
    sol2.fullCoefficients().print();
    EXPECT_LE(std::abs(sol1.objectiveValue - sol2.objectiveValue), 1e-6);
    for (size_t i = 0; i < X.n_cols; ++i) {
        std::cout << i << ":\n" << sol1.fullCoefficients()[i] << " vs. " << sol2.fullCoefficients()[i] << "\n\n";
        EXPECT_LE(std::abs(sol1.fullCoefficients()[i] - sol2.fullCoefficients()[i]), 1e-4);
    }
}

TEST_F(Huge1Test, SameSolutionWhenYIsNotNormalized2) {
    DataStandardization transformation(X, y, 0.0, true, false);
    Lasso<lasso::solver::mlpack::LARS> lasso1(*(transformation.getX()), y,
                                              *(transformation.getGamma()));
    auto sol1 = lasso1.setLambda(100).run();
    sol1.fullCoefficients().print();
    std::cout << "\n\n";
    Lasso<lasso::solver::picasso::CoordinateDescent> lasso2(*(transformation.getX()), y,
                                                            *(transformation.getGamma()));
    lasso2.setLambda(100);
    auto sol2 = lasso2.run();
    sol2.fullCoefficients().print();
    EXPECT_LE(std::abs(sol1.objectiveValue - sol2.objectiveValue), 1e-6);
    for (size_t i = 0; i < X.n_cols; ++i) {
        std::cout << i << ":\n" << sol1.fullCoefficients()[i] << " vs. " << sol2.fullCoefficients()[i] << "\n\n";
        EXPECT_LE(std::abs(sol1.fullCoefficients()[i] - sol2.fullCoefficients()[i]), 1e-4);
    }
}

TEST_F(Large1Test, SameSolutionWhenYIsNotNormalized3) {
    DataStandardization transformation(X, y, 0.0, true, false);
    Lasso<lasso::solver::mlpack::LARS> lasso1(*(transformation.getX()), y,
                                              *(transformation.getGamma()));
    auto sol1 = lasso1.setLambda(10.0).run();
    sol1.fullCoefficients().print();
    std::cout << "\n\n";
    Lasso<lasso::solver::picasso::CoordinateDescent> lasso2(*(transformation.getX()), y,
                                                            *(transformation.getGamma()));
    lasso2.setLambda(10.0);
    auto sol2 = lasso2.run();
    sol2.fullCoefficients().print();
    EXPECT_LE(std::abs(sol1.objectiveValue - sol2.objectiveValue), 1e-4);
    for (size_t i = 0; i < X.n_cols; ++i) {
        std::cout << i << ":\n" << sol1.fullCoefficients()[i] << " vs. " << sol2.fullCoefficients()[i] << "\n\n";
        EXPECT_LE(std::abs(sol1.fullCoefficients()[i] - sol2.fullCoefficients()[i]), 1e-4);
    }
}

TEST_F(Small1Test, SameSolutionsWhenYIsChanged1) {
    DataStandardization transformation(X, y, 0.0, true, false);
    Lasso<lasso::solver::mlpack::LARS> lasso1(*(transformation.getX()), *(transformation.getY()),
                                              *(transformation.getGamma()));
    auto sol1 = lasso1.setLambda(0.01).run();
    sol1.fullCoefficients().print();
    std::cout << "\n\n";
    Lasso<lasso::solver::picasso::CoordinateDescent> lasso2(*(transformation.getX()), *(transformation.getY()),
                                                            *(transformation.getGamma()));
    lasso2.setLambda(0.01);
    auto sol2 = lasso2.run();
    sol2.fullCoefficients().print();
    EXPECT_LE(std::abs(sol1.objectiveValue - sol2.objectiveValue), 1e-6);
    for (size_t i = 0; i < X.n_cols; ++i) {
        std::cout << i << ":\n" << sol1.fullCoefficients()[i] << " vs. " << sol2.fullCoefficients()[i] << "\n\n";
        EXPECT_LE(std::abs(sol1.fullCoefficients()[i] - sol2.fullCoefficients()[i]), 1e-4);
    }

    lasso1.setY(y);
    lasso1.setLambda(10);
    lasso2.setY(y);
    lasso2.setLambda(10);
    sol1 = lasso1.run();
    sol2 = lasso2.run();

    EXPECT_LE(std::abs(sol1.objectiveValue - sol2.objectiveValue), 1e-6);
    for (size_t i = 0; i < X.n_cols; ++i) {
        std::cout << i << ":\n" << sol1.fullCoefficients()[i] << " vs. " << sol2.fullCoefficients()[i] << "\n\n";
        EXPECT_LE(std::abs(sol1.fullCoefficients()[i] - sol2.fullCoefficients()[i]), 1e-4);
    }
}
#endif
#endif
// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by kreber on 30.09.20.
//

#include <gtest/gtest.h>
#include <DataStandardization.h>
#include <Lasso.h>
#include <config.h>

using namespace arma;
using namespace piano::cvbss;
using namespace piano;

TEST_F(Small1Test, InternalRunsSuccessfully) {
    DataStandardization transformation(X, y, 0.0, true, false);
    bool success = true;
    Solution sol;
    try {
        Lasso<lasso::solver::internal::CoordinateDescent> lasso(*(transformation.getX()), *(transformation.getY()),
                                                                *(transformation.getGamma()));
        sol = lasso.run();
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}

#ifdef PIANO_USE_MLPACK
TEST_F(Small1Test, MLPACKRunsSuccessfully1) {
    DataStandardization transformation(X, y, 0.0, true, false);
    bool success = true;
    Solution sol;
    try {
        Lasso<lasso::solver::mlpack::LARS> lasso(*(transformation.getX()), *(transformation.getY()),
                                                                *(transformation.getGamma()));
        sol = lasso.run();
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}
#endif

#ifdef PIANO_USE_MLPACK
TEST_F(Huge1Test, MLPACKRunsSuccessfully2) {
    DataStandardization transformation(X, y, 0.0, true, false);
    bool success = true;
    Solution sol;
    try {
        Lasso<lasso::solver::mlpack::LARS> lasso(*(transformation.getX()), *(transformation.getY()),
                                                 *(transformation.getGamma()));
        sol = lasso.run();
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}
#endif

#ifdef PIANO_USE_MLPACK
TEST_F(Large1Test, MLPACKRunsSuccessfully3) {
    DataStandardization transformation(X, y, 0.0, true, false);
    bool success = true;
    Solution sol;
    try {
        Lasso<lasso::solver::mlpack::LARS> lasso(*(transformation.getX()), *(transformation.getY()),
                                                 *(transformation.getGamma()));
        sol = lasso.run();
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}
#endif

#ifdef PIANO_USE_PICASSO
TEST_F(Small1Test, PICASSORunsSuccessfully1) {
    DataStandardization transformation(X, y, 0.0, true, false);
    bool success = true;
    Solution sol;
    try {
        Lasso<lasso::solver::picasso::CoordinateDescent> lasso(*(transformation.getX()), *(transformation.getY()),
                                                 *(transformation.getGamma()));
        sol = lasso.run();
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}
#endif

#ifdef PIANO_USE_PICASSO
TEST_F(Huge1Test, PICASSORunsSuccessfully2) {
    DataStandardization transformation(X, y, 0.0, true, false);
    bool success = true;
    Solution sol;
    try {
        Lasso<lasso::solver::picasso::CoordinateDescent> lasso(*(transformation.getX()), *(transformation.getY()),
                                                               *(transformation.getGamma()));
        sol = lasso.run();
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}
#endif

#ifdef PIANO_USE_PICASSO
TEST_F(Large1Test, PICASSORunsSuccessfully3) {
    DataStandardization transformation(X, y, 0.0, true, false);
    bool success = true;
    Solution sol;
    try {
        Lasso<lasso::solver::picasso::CoordinateDescent> lasso(*(transformation.getX()), *(transformation.getY()),
                                                               *(transformation.getGamma()));
        sol = lasso.setLambda(0.1).run();
    } catch (...) {
        success = false;
    }
    EXPECT_TRUE(success);
}
#endif
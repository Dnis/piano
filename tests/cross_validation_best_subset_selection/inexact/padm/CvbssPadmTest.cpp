//
// Created by dennis on 30.09.21.
//

#include <gtest/gtest.h>
#include <CvbssPadm.h>
#include <TestData.h>
#include <memory>
#include <config.h>

using namespace piano;

#ifdef PIANO_USE_CPLEX

TEST_F(Small1Test, CvbssPadmRunsWithoutError) {
    cvbss::Padm padm((*dataStandardization->getX()), (*dataStandardization->getY()), (*dataStandardization->getGamma()));
    auto sol = padm.run();
    auto recoveredSol = dataStandardization->recoverSolution(sol);
    std::cout << piano::toPrettyString(recoveredSol.subset, recoveredSol.p) << "\n";
    std::cout << piano::toPrettyString(subset, X.n_cols) << "\n";
}

TEST_F(Small1Test, CvbssPadmRunsWithoutErrorWhenTheUpperLevelIterationLimitIsUsed) {
    cvbss::Padm padm((*dataStandardization->getX()), (*dataStandardization->getY()), (*dataStandardization->getGamma()));
    padm.setIterationLimitForUpperLevelSolver(1000);
    auto sol = padm.run();
    auto recoveredSol = dataStandardization->recoverSolution(sol);
    std::cout << piano::toPrettyString(recoveredSol.subset, recoveredSol.p) << "\n";
    std::cout << piano::toPrettyString(subset, X.n_cols) << "\n";
}

#endif
// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 26.03.20.
//

#include <CvbssMip.h>
#include <CvbssExhaustiveSearch.h>
#include <gtest/gtest.h>
#include <DataStandardization.h>
#include <Lasso.h>
#include <config.h>

using namespace arma;
using namespace piano::cvbss;
using namespace piano::sls;

#ifdef PIANO_USE_CPLEX

template<class L, class U> using CB1 = upper_bound::coefficients::MinimumEigenvalueBound<L, U>;
template<class L, class U> using CB2 = upper_bound::coefficients::ConditionBound<L, U>;
template<class L, class U> using CB = upper_bound::coefficients::Min<CB1<L, U>, CB2<L, U>>;

TEST_F(Small1Test, CVBSSAndExhaustiveSearchReturnSameSolution1) {
    ExhaustiveSearch exhaustiveSearch(&X, &y, &gamma);
    exhaustiveSearch.setNumberOfThreads(4);
    auto exhaustiveSol = exhaustiveSearch.run();

    using PVLB = lower_bound::predicted_value::TrivialBound;
    using PVUB = upper_bound::predicted_value::DualCauchyBound;

    Mip<
            NoWarmstart,
            mip::solver::Cplex<
                    mip::indicator_constraints::cplex::LogicalConstraints<
                            CoefficientBounds<CB<PVLB, PVUB>>,
                            PVUB>,
                    pool_selector::cplex::BestObjective
            >
    > cvbss(X, y, gamma);
    cvbss.mute(false);
    auto cvbssSol = cvbss.run();

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, cvbssSol.estimatedPredictionError);
}

TEST_F(Small2Test, CVBSSAndExhaustiveSearchReturnSameSolution2) {
    ExhaustiveSearch exhaustiveSearch(dataStandardization->getX(), dataStandardization->getY(), dataStandardization->getGamma());
    exhaustiveSearch.setNumberOfThreads(4);
    auto exhaustiveSol = exhaustiveSearch.run();

    Mip<> cvbss(*(dataStandardization->getX()), *(dataStandardization->getY()), *(dataStandardization->getGamma()));
    cvbss.mute(false);
    auto cvbssSol = cvbss.run();

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, cvbssSol.estimatedPredictionError);
}

TEST_F(Small2Test, CVBSSAndExhaustiveSearchReturnSameSolution3) {
    piano::DataStandardization dataTransformation(X, y, 0, true, false);

    ExhaustiveSearch exhaustiveSearch(dataTransformation.getX(), dataTransformation.getY(), &gamma);
    exhaustiveSearch.setNumberOfThreads(4);
    auto exhaustiveSol = dataTransformation.recoverSolution(exhaustiveSearch.run());

    using PVLB = lower_bound::predicted_value::TrivialBound;
    using PVUB = upper_bound::predicted_value::DualCauchyBound;

    Mip<
            NoWarmstart,
            mip::solver::Cplex<
                    mip::indicator_constraints::cplex::LogicalConstraints<
                            CoefficientBounds<CB<PVLB, PVUB>>,
                            PVUB
                    >,
                    pool_selector::cplex::BestObjective>
    > cvbss(*(dataTransformation.getX()), *(dataTransformation.getY()), gamma);
    cvbss.mute(false);
    auto cvbssSol = dataTransformation.recoverSolution(cvbss.run());

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, cvbssSol.estimatedPredictionError);
}

TEST_F(Small2Test, CVBSSAndExhaustiveSearchReturnSameSolution4) {
    piano::DataStandardization dataTransformation(X, y, 1.0, true, false);

    ExhaustiveSearch exhaustiveSearch(dataTransformation.getX(), dataTransformation.getY(),
                                      dataTransformation.getGamma());
    exhaustiveSearch.setNumberOfThreads(4);
    auto exhaustiveSol = dataTransformation.recoverSolution(exhaustiveSearch.run());

    Mip<
            NoWarmstart,
            mip::solver::Cplex<
                    mip::indicator_constraints::cplex::LogicalConstraints<>,
                    pool_selector::cplex::BestObjective
            >
    > cvbss(*(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    cvbss.mute(false);
    auto cvbssSol = dataTransformation.recoverSolution(cvbss.run());

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, cvbssSol.estimatedPredictionError);
}

TEST_F(Small1RidgeTest, CVBSSAndExhaustiveSearchReturnSameSolution5) {
    piano::DataStandardization dataTransformation(X, y, 1.0, true, true);

    ExhaustiveSearch exhaustiveSearch(dataTransformation.getX(), dataTransformation.getY(),
                                      dataTransformation.getGamma());
    exhaustiveSearch.setNumberOfThreads(4);
    auto exhaustiveSol = dataTransformation.recoverSolution(exhaustiveSearch.run());

    Mip<
            NoWarmstart,
            mip::solver::Cplex<
                    mip::indicator_constraints::cplex::LogicalConstraints<>,
                    pool_selector::cplex::BestObjective
            >
    > cvbss(*(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    cvbss.mute(false);
    auto cvbssSol = dataTransformation.recoverSolution(cvbss.run());

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, cvbssSol.estimatedPredictionError);
}

TEST_F(Small2Test, CVBSSAndExhaustiveSearchReturnSameSolution6) {
    piano::DataStandardization dataTransformation(X, y, 0.0, true, false);

    ExhaustiveSearch exhaustiveSearch(dataTransformation.getX(), dataTransformation.getY(),
                                      dataTransformation.getGamma());
    exhaustiveSearch.setNumberOfThreads(4);
    auto exhaustiveSol = dataTransformation.recoverSolution(exhaustiveSearch.run());

    Mip<
            NoWarmstart,
            mip::solver::Cplex<
                    mip::indicator_constraints::cplex::BigMConstraints<>,
                    pool_selector::cplex::BestObjective
            >
    > cvbss(*(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    cvbss.mute(false);
    auto cvbssSol = dataTransformation.recoverSolution(cvbss.run());

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, cvbssSol.estimatedPredictionError);
}

TEST_F(Small2Test, CVBSSAndExhaustiveSearchReturnSameSolution7) {
    piano::DataStandardization dataTransformation(X, y, 0.0, true, false);

    ExhaustiveSearch exhaustiveSearch(dataTransformation.getX(), dataTransformation.getY(),
                                      dataTransformation.getGamma());
    exhaustiveSearch.setNumberOfThreads(4);
    auto exhaustiveSol = dataTransformation.recoverSolution(exhaustiveSearch.run());

    Lasso<> lasso(*(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    auto lassoHpt = lasso.getHyperParameterTuner();
    Mip<NoWarmstart,
            mip::solver::Cplex<
                    mip::indicator_constraints::cplex::BigMConstraints<>,
                    pool_selector::cplex::BestObjective
            >
    > cvbss(*(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    cvbss.addWarmstartSolution(lassoHpt.run());
    cvbss.mute(false);
    auto cvbssSol = dataTransformation.recoverSolution(cvbss.run());

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, cvbssSol.estimatedPredictionError);
}

TEST_F(Small2Test, MuteSolverWorks) {
    piano::DataStandardization dataTransformation(X, y, 0.0, true, false);

    ExhaustiveSearch exhaustiveSearch(dataTransformation.getX(), dataTransformation.getY(),
                                      dataTransformation.getGamma());
    exhaustiveSearch.setNumberOfThreads(4);
    auto exhaustiveSol = dataTransformation.recoverSolution(exhaustiveSearch.run());

    Lasso<> lasso(*(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    auto lassoHpt = lasso.getHyperParameterTuner();

    Mip<
            NoWarmstart,
            mip::solver::Cplex<
                    mip::indicator_constraints::cplex::BigMConstraints<>,
                    pool_selector::cplex::BestObjective
            >
    > cvbss(*(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    cvbss.addWarmstartSolution(lassoHpt.run());
    cvbss.mute(false);
    auto cvbssSol = dataTransformation.recoverSolution(cvbss.run());

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, cvbssSol.estimatedPredictionError);
}

#endif
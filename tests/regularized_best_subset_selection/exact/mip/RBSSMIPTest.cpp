// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

#include <RbssMip.h>
#include <gtest/gtest.h>
#include <RbssExhaustiveSearch.h>
#include <DataStandardization.h>
#include "cplex/RbssMipCplex.h"
#include <config.h>

using namespace arma;
using namespace piano::rbss;

#ifdef PIANO_USE_CPLEX

TEST_F(Small1Test, RBSSAndExhaustiveSearchReturnSameSolution1) {
    piano::DataStandardization dataTransformation(X, y, 0, true, false);

    ExhaustiveSearch exhaustiveSearch((dataTransformation.getX()), (dataTransformation.getY()),
                                      (dataTransformation.getGamma()));
    exhaustiveSearch.setNumberOfThreads(1);
    auto exhaustiveSol = dataTransformation.recoverSolution(exhaustiveSearch.run(arma::ones(10)));

    for (const auto &subsetElement: exhaustiveSol.subset) {
        std::cout << subsetElement << "\n";
    }

    Mip<> rbss(
            *(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    rbss.mute(false);
    auto rbssSol = dataTransformation.recoverSolution(rbss.run(arma::ones(10)));

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, rbssSol.estimatedPredictionError);
}

TEST_F(Small1Test, RBSSAndExhaustiveSearchReturnSameSolutionWithRidge) {
    piano::DataStandardization dataTransformation(X, y, 10.0, true, true);

    ExhaustiveSearch exhaustiveSearch((dataTransformation.getX()), (dataTransformation.getY()),
                                      (dataTransformation.getGamma()));
    exhaustiveSearch.setNumberOfThreads(1);
    auto exhaustiveSol = dataTransformation.recoverSolution(exhaustiveSearch.run(arma::ones(10)));

    for (const auto &subsetElement: exhaustiveSol.subset) {
        std::cout << subsetElement << "\n";
    }

    Mip<> rbss(
            *(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    rbss.mute(false);
    auto rbssSol = dataTransformation.recoverSolution(rbss.run(arma::ones(10)));

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, rbssSol.estimatedPredictionError);
}

TEST_F(Small1Test, RBSSAndExhaustiveSearchReturnSameSolutionWithRidgeAndIntercept) {
    piano::DataStandardization dataTransformation(X, y, 10.0, true, true);

    ExhaustiveSearch exhaustiveSearch((dataTransformation.getX()), (dataTransformation.getY()),
                                      (dataTransformation.getGamma()));
    exhaustiveSearch.setNumberOfThreads(1);
    auto exhaustiveSol = dataTransformation.recoverSolution(exhaustiveSearch.run(arma::ones(10)));

    for (const auto &subsetElement: exhaustiveSol.subset) {
        std::cout << subsetElement << "\n";
    }

    Mip<> rbss(
            *(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    rbss.mute(false);
    auto rbssSol = dataTransformation.recoverSolution(rbss.run(arma::ones(10)));

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, rbssSol.estimatedPredictionError);
}

TEST_F(Small1Test, RBSSAndExhaustiveSearchReturnSameSolution2) {
    piano::DataStandardization dataTransformation(X, y, 0, true, false);

    ExhaustiveSearch exhaustiveSearch((dataTransformation.getX()), (dataTransformation.getY()),
                                      (dataTransformation.getGamma()));
    exhaustiveSearch.setNumberOfThreads(1);
    auto exhaustiveSol = dataTransformation.recoverSolution(exhaustiveSearch.run(5));

    Mip<NoWarmstart, piano::rbss::mip::solver::Cplex<piano::rbss::mip::indicator_constraints::cplex::BigMConstraints<>>> rbss(
            *(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    rbss.mute(false);
    auto rbssSol = dataTransformation.recoverSolution(rbss.run(arma::ones(10)));

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, rbssSol.estimatedPredictionError);
}

TEST_F(Small2Test, RBSSAndExhaustiveSearchReturnSameSolution1) {
    piano::DataStandardization dataTransformation(X, y, 0, true, false);

    ExhaustiveSearch exhaustiveSearch((dataTransformation.getX()), (dataTransformation.getY()),
                                      (dataTransformation.getGamma()));
    exhaustiveSearch.setNumberOfThreads(1);
    auto exhaustiveSol = dataTransformation.recoverSolution(exhaustiveSearch.run(arma::ones(10)));

    Mip<> rbss(
            *(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    rbss.mute(false);
    auto rbssSol = dataTransformation.recoverSolution(rbss.run(arma::ones(10)));

    EXPECT_DOUBLE_EQ(exhaustiveSol.estimatedPredictionError, rbssSol.estimatedPredictionError);
}

TEST_F(Small2Test, GapIsZero) {
    piano::DataStandardization dataTransformation(X, y, 0, true, false);

    Mip<> rbss(
            *(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    rbss.mute(false);
    auto rbssSol = dataTransformation.recoverSolution(rbss.run(arma::ones(10)));

    EXPECT_DOUBLE_EQ(0, rbssSol.gap);
}

TEST_F(Small2Test, LinkedIndicatorsWork) {
    piano::DataStandardization dataTransformation(X, y, 0, true, false);

    Mip<> rbss(
            *(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    rbss.mute(false);
    rbss.linkIndicators({0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
    auto rbssSol = dataTransformation.recoverSolution(rbss.run({0, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01}));
    std::cout << "Subset: ";
    for (const auto &elem: rbssSol.subset) {
        std::cout << elem << " ";
    }
    std::cout << "\n";

    piano::Subset toBeIncluded{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    EXPECT_TRUE(std::includes(rbssSol.subset.begin(), rbssSol.subset.end(), toBeIncluded.begin(), toBeIncluded.end()));
}

#endif
// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <gtest/gtest.h>
#include <limits>
#include <TypeConversion.h>

using namespace piano;

TEST(TypeAssertTest, AssertionThrowsErrorForInt) {
    size_t i = std::numeric_limits<int>::max();
    i++;
    EXPECT_THROW(assertCastable<int>(i, "i", ""), std::overflow_error);
}

TEST(TypeAssertTest, AssertionThrowsNoErrorForInt) {
    size_t i = std::numeric_limits<int>::max();
    EXPECT_NO_THROW(assertCastable<int>(i, "i", ""));
}

TEST(TypeAssertTest, AssertionThrowsErrorForLong) {
    size_t i = std::numeric_limits<long>::max();
    i++;
    EXPECT_THROW(assertCastable<long>(i, "i", ""), std::overflow_error);
}

TEST(TypeAssertTest, AssertionThrowsNoErrorForLong) {
    size_t i = std::numeric_limits<long>::max();
    EXPECT_NO_THROW(assertCastable<long>(i, "i", ""));
}

TEST(CastTest, CastThrowsErrorForInt) {
    size_t i = std::numeric_limits<int>::max();
    i++;
    EXPECT_THROW(PIANO_SAFE_CAST_TO_INT(i, ""), std::overflow_error);
}

TEST(CastTest, CastThrowsErrorNoForInt) {
    size_t i = std::numeric_limits<int>::max();
    EXPECT_NO_THROW(PIANO_SAFE_CAST_TO_INT(i, ""));
}

TEST(CastTest, CorrectCastForInt) {
    size_t i = 42;
    EXPECT_EQ(PIANO_SAFE_CAST_TO_INT(i, ""), 42);
}

TEST(CastTest, CastThrowsErrorForLong) {
    size_t i = std::numeric_limits<long>::max();
    i++;
    EXPECT_THROW(PIANO_SAFE_CAST_TO_LONG(i, ""), std::overflow_error);
}

TEST(CastTest, CastThrowsErrorNoForLong) {
    size_t i = std::numeric_limits<long>::max();
    EXPECT_NO_THROW(PIANO_SAFE_CAST_TO_LONG(i, ""));
}

TEST(CastTest, CorrectCastForLong) {
    size_t i = 42;
    EXPECT_EQ(PIANO_SAFE_CAST_TO_LONG(i, ""), 42);
}
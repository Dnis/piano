// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/5/20.
//

#include <gtest/gtest.h>
#include <Solution.h>
#include <LinearAlgebra.h>
#include <LeastSquares.h>
#include <DataStandardization.h>
#include <HandingOver.h>
#include <config.h>

using namespace arma;
using namespace piano::sls;
using namespace piano;

TEST(LeastSquaresTest_ToyExample1, CorrectResultIsComputed) {
    mat X = {
            {1, 0, 0},
            {0, 1, 0},
            {0, 0, 1}
    };
    vec y = { 1, 2, 3 };
    LeastSquares leastSquares(&X, &y);
    auto sol = leastSquares.compute();

    EXPECT_DOUBLE_EQ(0, sol.objectiveValue);
    EXPECT_DOUBLE_EQ(0, sol.rss);
    EXPECT_DOUBLE_EQ(0, sol.mse());

    piano::Subset subset = { 0, 1, 2 };
    ASSERT_EQ(3, sol.subsetSize);
    for (size_t i = 0; i < sol.subsetSize; ++i) {
        EXPECT_EQ(sol.subset[i], subset[i]);
    }

    vec coefs = {1, 2, 3};
    ASSERT_EQ(3, sol.p);
    for (size_t j = 0; j < sol.p; ++j) {
        EXPECT_DOUBLE_EQ(sol.coefficients[j], coefs[j]);
    }
}

TEST(LeastSquaresTest_ToyExample2, CorrectResultIsComputed) {
    mat X = {
            {1, 0, 0},
            {0, 1, 0},
            {0, 0, 1}
    };
    vec y = { 1, 2, 3 };
    LeastSquares leastSquares(&X, &y, 2);
    auto sol = leastSquares.compute();

    EXPECT_DOUBLE_EQ(28.0 / 3.0, sol.objectiveValue);
    EXPECT_DOUBLE_EQ(56.0 / 9.0, sol.rss);
    EXPECT_DOUBLE_EQ(56.0 / 9.0 / 3.0, sol.mse());

    piano::Subset subset = { 0, 1, 2 };
    ASSERT_EQ(3, sol.subsetSize);
    for (size_t i = 0; i < sol.subsetSize; ++i) {
        EXPECT_EQ(sol.subset[i], subset[i]);
    }

    vec coefs = {1.0 / 3.0, 2.0 / 3.0, 1};
    ASSERT_EQ(3, sol.p);
    for (size_t j = 0; j < sol.p; ++j) {
        EXPECT_DOUBLE_EQ(sol.coefficients[j], coefs[j]);
    }
}

TEST(LeastSquaresTest_ToyExample3, CorrectResultIsComputed) {
    mat X = {
            {1, 0, 0},
            {0, 1, 0},
            {0, 0, 1}
    };
    vec y = { 1, 2, 3 };
    piano::Subset subset = { 0, 2 };
    LeastSquares leastSquares(&X, &y);
    auto sol = leastSquares.compute(subset);

    EXPECT_DOUBLE_EQ(4, sol.objectiveValue);
    EXPECT_DOUBLE_EQ(4, sol.rss);
    EXPECT_DOUBLE_EQ(4.0 / 3.0, sol.mse());

    ASSERT_EQ(2, sol.subsetSize);
    for (size_t i = 0; i < sol.subsetSize; ++i) {
        EXPECT_EQ(sol.subset[i], subset[i]);
    }

    vec coefs = {1, 3};

    for (size_t j = 0; j < sol.subsetSize; ++j) {
        EXPECT_DOUBLE_EQ(sol.coefficients[j], coefs[j]);
    }

    vec fullCoefs = {1, 0, 3};

    ASSERT_EQ(3, sol.p);
    for (size_t j = 0; j < sol.p; ++j) {
        EXPECT_DOUBLE_EQ(sol.fullCoefficients()[j], fullCoefs[j]);
    }
}

TEST(LeastSquaresTest_ToyExample4, CorrectResultIsComputed) {
    mat X = {
            {1, 0, 0},
            {0, 1, 0},
            {0, 0, 1}
    };
    vec y = { 1, 2, 3 };
    piano::Subset subset = { 0, 2 };
    LeastSquares leastSquares(&X, &y);
    auto sol = leastSquares.compute({ 0, 2 });

    EXPECT_DOUBLE_EQ(4, sol.objectiveValue);
    EXPECT_DOUBLE_EQ(4, sol.rss);
    EXPECT_DOUBLE_EQ(4.0 / 3.0, sol.mse());

    ASSERT_EQ(2, sol.subsetSize);
    for (size_t i = 0; i < sol.subsetSize; ++i) {
        EXPECT_EQ(sol.subset[i], subset[i]);
    }

    vec coefs = {1, 3};

    for (size_t j = 0; j < sol.subsetSize; ++j) {
        EXPECT_DOUBLE_EQ(sol.coefficients[j], coefs[j]);
    }

    vec fullCoefs = {1, 0, 3};

    ASSERT_EQ(3, sol.p);
    for (size_t j = 0; j < sol.p; ++j) {
        EXPECT_DOUBLE_EQ(sol.fullCoefficients()[j], fullCoefs[j]);
    }
}

TEST(LeastSquaresTest_ToyExample5, CorrectResultIsReturnedWithActivationCosts) {
    mat X = {
            {1, 0, 0},
            {0, 1, 0},
            {0, 0, 1}
    };
    vec y = { 1, 2, 3 };
    piano::Subset subset = { 0, 2 };
    LeastSquares leastSquares(&X, &y);
    leastSquares.setActivationCosts(1);
    auto sol = leastSquares.compute({ 0, 2 });

    EXPECT_DOUBLE_EQ(6, sol.objectiveValue);
    EXPECT_DOUBLE_EQ(4, sol.rss);
    EXPECT_DOUBLE_EQ(4.0 / 3.0, sol.mse());

    ASSERT_EQ(2, sol.subsetSize);
    for (size_t i = 0; i < sol.subsetSize; ++i) {
        EXPECT_EQ(sol.subset[i], subset[i]);
    }

    vec coefs = {1, 3};

    for (size_t j = 0; j < sol.subsetSize; ++j) {
        EXPECT_DOUBLE_EQ(sol.coefficients[j], coefs[j]);
    }

    vec fullCoefs = {1, 0, 3};

    ASSERT_EQ(3, sol.p);
    for (size_t j = 0; j < sol.p; ++j) {
        EXPECT_DOUBLE_EQ(sol.fullCoefficients()[j], fullCoefs[j]);
    }
}

TEST(LeastSquaresTest_ToyExample6, EmptySubsetTest) {
    mat X = {
            {1, 0, 0},
            {0, 1, 0},
            {0, 0, 1}
    };
    vec y = { 1, 2, 3 };
    piano::Subset subset{};
    LeastSquares leastSquares(&X, &y);
    auto sol = leastSquares.compute(subset);

    EXPECT_DOUBLE_EQ(14, sol.objectiveValue);
    EXPECT_DOUBLE_EQ(14, sol.rss);
    EXPECT_DOUBLE_EQ(14.0 / 3.0, sol.mse());

    EXPECT_EQ(0, sol.subsetSize);

    vec v = sol.fullCoefficients();

    for (size_t j = 0; j < v.size(); ++j) {
        EXPECT_DOUBLE_EQ(v[j], 0);
    }
}

TEST_F(Small1Test, ShuffledSubsetYieldSameSolution1) {
    LeastSquares leastSquares(&X, &y);
    Subset subset1{0, 1, 2, 3};
    Subset subset2{2, 0, 1, 3};

    auto coefs1 = leastSquares.computeCoefs(subset1);
    auto coefs2 = leastSquares.computeCoefs(subset2);

    EXPECT_NEAR(coefs1[0], coefs2[1], 1e-9);
    EXPECT_NEAR(coefs1[1], coefs2[2], 1e-9);
    EXPECT_NEAR(coefs1[2], coefs2[0], 1e-9);
    EXPECT_NEAR(coefs1[3], coefs2[3], 1e-9);
}

TEST_F(Small1Test, ShuffledSubsetYieldSameSolution2) {
    LeastSquares leastSquares(&X, &y);
    Subset subset1{0, 1, 2, 3};
    Subset subset2{2, 0, 1, 3};

    auto sol1 = leastSquares.compute(subset1);
    auto sol2 = leastSquares.compute(subset2);

    EXPECT_NEAR(sol1.coefficients[0], sol2.coefficients[1], 1e-9);
    EXPECT_NEAR(sol1.coefficients[1], sol2.coefficients[2], 1e-9);
    EXPECT_NEAR(sol1.coefficients[2], sol2.coefficients[0], 1e-9);
    EXPECT_NEAR(sol1.coefficients[3], sol2.coefficients[3], 1e-9);

    for (size_t i = 0; i < X.n_cols; ++i) {
        EXPECT_NEAR(sol1.fullCoefficients()[i], sol2.fullCoefficients()[i], 1e-9);
    }

    EXPECT_DOUBLE_EQ(sol1.rss, sol2.rss);
}

TEST_F(Large1Test, ShuffledSubsetYieldSameSolution2) {
    LeastSquares leastSquares(&X, &y);
    Subset subset1{0, 1, 2, 3};
    Subset subset2{2, 0, 1, 3};

    auto sol1 = leastSquares.compute(subset1);
    auto sol2 = leastSquares.compute(subset2);

    EXPECT_NEAR(sol1.coefficients[0], sol2.coefficients[1], 1e-9);
    EXPECT_NEAR(sol1.coefficients[1], sol2.coefficients[2], 1e-9);
    EXPECT_NEAR(sol1.coefficients[2], sol2.coefficients[0], 1e-9);
    EXPECT_NEAR(sol1.coefficients[3], sol2.coefficients[3], 1e-9);

    for (size_t i = 0; i < X.n_cols; ++i) {
        EXPECT_NEAR(sol1.fullCoefficients()[i], sol2.fullCoefficients()[i], 1e-9);
    }

    EXPECT_NEAR(sol1.rss, sol2.rss, 1e-9);
}

TEST_F(Medium1Test, ShuffledSubsetYieldSameSolution2) {
    LeastSquares leastSquares(&X, &y);
    Subset subset1{0, 1, 2, 3};
    Subset subset2{2, 0, 1, 3};

    auto sol1 = leastSquares.compute(subset1);
    auto sol2 = leastSquares.compute(subset2);

    EXPECT_NEAR(sol1.coefficients[0], sol2.coefficients[1], 1e-9);
    EXPECT_NEAR(sol1.coefficients[1], sol2.coefficients[2], 1e-9);
    EXPECT_NEAR(sol1.coefficients[2], sol2.coefficients[0], 1e-9);
    EXPECT_NEAR(sol1.coefficients[3], sol2.coefficients[3], 1e-9);

    for (size_t i = 0; i < X.n_cols; ++i) {
        EXPECT_NEAR(sol1.fullCoefficients()[i], sol2.fullCoefficients()[i], 1e-9);
    }

    EXPECT_NEAR(sol1.rss, sol2.rss, 1e-9);
}

TEST_F(Medium1Test, ShuffledSubsetYieldSameSolution3) {
    const arma::mat* newXPtr;
    const arma::vec* newYPtr;
    std::unique_ptr<arma::mat> XAlloc;
    std::unique_ptr<arma::vec> yAlloc;

    DataStandardization dataStandardization(X, y, 2, true, false);
    handOver(*(dataStandardization.getX()), *(dataStandardization.getY()), *(dataStandardization.getGamma()), &XAlloc, &yAlloc, &newXPtr, &newYPtr);
    LeastSquares leastSquares1(dataStandardization.getX(), dataStandardization.getY(), dataStandardization.getGamma());
    LeastSquares leastSquares2(newXPtr, newYPtr);

    Subset subset1{0, 1, 2, 3};
    Subset subset2{2, 0, 1, 3};

    auto sol1 = leastSquares1.compute(subset1);
    auto sol2 = leastSquares2.compute(subset2);

    EXPECT_NEAR(sol1.coefficients[0], sol2.coefficients[1], 1e-9);
    EXPECT_NEAR(sol1.coefficients[1], sol2.coefficients[2], 1e-9);
    EXPECT_NEAR(sol1.coefficients[2], sol2.coefficients[0], 1e-9);
    EXPECT_NEAR(sol1.coefficients[3], sol2.coefficients[3], 1e-9);

    for (size_t i = 0; i < X.n_cols; ++i) {
        EXPECT_NEAR(sol1.fullCoefficients()[i], sol2.fullCoefficients()[i], 1e-9);
    }
}
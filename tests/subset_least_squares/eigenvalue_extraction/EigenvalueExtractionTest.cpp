// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 08.09.20.
//

#include <gtest/gtest.h>
#include <EigenvalueExtraction.h>
#include <LeastSquares.h>
#include <config.h>


using namespace arma;
using namespace piano::sls;
using namespace piano;

TEST_F(Small1Test, YieldsSameCoefs1) {
    auto eigenvalueExtraction = EigenvalueExtraction(X, y, 0, 0.5);
    auto leastSquares1 = LeastSquares(&(eigenvalueExtraction.getXTilde()), &(eigenvalueExtraction.getYTilde()), eigenvalueExtraction.getR());
    auto coefs1 = leastSquares1.computeCoefs();

    auto leastSquares2 = LeastSquares(&X, &y);
    auto coefs2 = leastSquares2.computeCoefs();

    ASSERT_EQ(coefs1.size(), coefs2.size());
    for (size_t i = 0; i < coefs1.size(); ++i) {
        EXPECT_TRUE(std::abs(coefs1[i] - coefs2[i]) < 1e-6);
    }
}

TEST_F(Large1Test, YieldsSameCoefs2) {
    auto eigenvalueExtraction = EigenvalueExtraction(X, y, 0, 0.5);
    auto leastSquares1 = LeastSquares(&(eigenvalueExtraction.getXTilde()), &(eigenvalueExtraction.getYTilde()), eigenvalueExtraction.getR());
    auto coefs1 = leastSquares1.computeCoefs();

    auto leastSquares2 = LeastSquares(&X, &y);
    auto coefs2 = leastSquares2.computeCoefs();

    ASSERT_EQ(coefs1.size(), coefs2.size());
    for (size_t i = 0; i < coefs1.size(); ++i) {
        EXPECT_TRUE(std::abs(coefs1[i] - coefs2[i]) < 1e-6);
    }
}

TEST_F(Medium1Test, YieldsSameCoefs3) {
    auto eigenvalueExtraction = EigenvalueExtraction(X, y, 0, 0.5);
    auto leastSquares1 = LeastSquares(&(eigenvalueExtraction.getXTilde()), &(eigenvalueExtraction.getYTilde()), eigenvalueExtraction.getR());
    auto coefs1 = leastSquares1.computeCoefs();

    auto leastSquares2 = LeastSquares(&X, &y);
    auto coefs2 = leastSquares2.computeCoefs();

    ASSERT_EQ(coefs1.size(), coefs2.size());
    for (size_t i = 0; i < coefs1.size(); ++i) {
        EXPECT_TRUE(std::abs(coefs1[i] - coefs2[i]) < 1e-6);
    }
}

TEST_F(Medium1Test, YieldsSameCoefs4) {
    auto eigenvalueExtraction = EigenvalueExtraction(X, y, 0, 0.1);
    auto leastSquares1 = LeastSquares(&(eigenvalueExtraction.getXTilde()), &(eigenvalueExtraction.getYTilde()), eigenvalueExtraction.getR());
    auto coefs1 = leastSquares1.computeCoefs();

    auto leastSquares2 = LeastSquares(&X, &y);
    auto coefs2 = leastSquares2.computeCoefs();

    ASSERT_EQ(coefs1.size(), coefs2.size());
    for (size_t i = 0; i < coefs1.size(); ++i) {
        EXPECT_TRUE(std::abs(coefs1[i] - coefs2[i]) < 1e-6);
    }
}

TEST_F(Medium1Test, YieldsSameCoefs5) {
    auto eigenvalueExtraction = EigenvalueExtraction(X, y, 0, 0.9);
    auto leastSquares1 = LeastSquares(&(eigenvalueExtraction.getXTilde()), &(eigenvalueExtraction.getYTilde()), eigenvalueExtraction.getR());
    auto coefs1 = leastSquares1.computeCoefs();

    auto leastSquares2 = LeastSquares(&X, &y);
    auto coefs2 = leastSquares2.computeCoefs();

    ASSERT_EQ(coefs1.size(), coefs2.size());
    for (size_t i = 0; i < coefs1.size(); ++i) {
        EXPECT_TRUE(std::abs(coefs1[i] - coefs2[i]) < 1e-6);
    }
}

TEST_F(Medium1Test, YieldsSameCoefs6) {
    auto eigenvalueExtraction = EigenvalueExtraction(X, y, 1.0, 0.9);
    auto leastSquares1 = LeastSquares(&(eigenvalueExtraction.getXTilde()), &(eigenvalueExtraction.getYTilde()), eigenvalueExtraction.getR());
    auto coefs1 = leastSquares1.computeCoefs();

    auto leastSquares2 = LeastSquares(&X, &y, 1.0);
    auto coefs2 = leastSquares2.computeCoefs();

    ASSERT_EQ(coefs1.size(), coefs2.size());
    for (size_t i = 0; i < coefs1.size(); ++i) {
        EXPECT_TRUE(std::abs(coefs1[i] - coefs2[i]) < 1e-6);
    }
}

TEST_F(Medium1Test, YieldsSameCoefs7) {
    vec gamma(X.n_cols);
    gamma.fill(1.0);
    auto eigenvalueExtraction = EigenvalueExtraction(X, y, gamma, 0.9);
    auto leastSquares1 = LeastSquares(&(eigenvalueExtraction.getXTilde()), &(eigenvalueExtraction.getYTilde()), eigenvalueExtraction.getR());
    auto coefs1 = leastSquares1.computeCoefs();

    auto leastSquares2 = LeastSquares(&X, &y, &gamma);
    auto coefs2 = leastSquares2.computeCoefs();

    ASSERT_EQ(coefs1.size(), coefs2.size());
    for (size_t i = 0; i < coefs1.size(); ++i) {
        EXPECT_TRUE(std::abs(coefs1[i] - coefs2[i]) < 1e-6);
    }
}

TEST_F(Medium1Test, YieldsSameCoefsForSubset1) {
    Subset subset{0, 50, 33, 4, 86, 57};

    auto eigenvalueExtraction = EigenvalueExtraction(X, y, 0, 0.9);
    auto leastSquares1 = LeastSquares(&(eigenvalueExtraction.getXTilde()), &(eigenvalueExtraction.getYTilde()), eigenvalueExtraction.getR());
    auto coefs1 = leastSquares1.computeCoefs(subset);

    auto leastSquares2 = LeastSquares(&X, &y);
    auto coefs2 = leastSquares2.computeCoefs(subset);

    ASSERT_EQ(coefs1.size(), coefs2.size());
    for (size_t i = 0; i < coefs1.size(); ++i) {
        EXPECT_TRUE(std::abs(coefs1[i] - coefs2[i]) < 1e-6);
    }
}


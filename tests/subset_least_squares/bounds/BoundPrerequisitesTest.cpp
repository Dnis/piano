// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 14.04.20.
//

#include <gtest/gtest.h>
#include <BoundPrerequisites.h>

using namespace arma;
using namespace piano::sls;

TEST(BoundPrerequisites, IsConstructedCorrectly) {
    mat X{
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9},
            {10, 11, 12}
    };
    vec y{1, 1, 1, 1};
    vec gamma(3);
    gamma.fill(1.0);
    gamma[0] = 2;
    BoundPrerequisites prerequisites(&X, &y, &gamma, 3);
    mat cX = *(prerequisites.getX());
    vec cY = *(prerequisites.getY());

    ASSERT_EQ(cX.n_rows, 7);
    ASSERT_EQ(cX.n_cols, 3);
    ASSERT_EQ(cY.size(), 7);

    for (size_t i = 0; i < 4; ++i) {
        for (size_t j = 0; j < 3; ++j) {
            EXPECT_DOUBLE_EQ(cX.at(i, j), X.at(i, j));
        }
    }

    for (size_t k = 0; k < 3; ++k) {
        for (size_t i = 0; i < 3; ++i) {
            if (i == k) {
                EXPECT_DOUBLE_EQ(cX.at(4 + k, i), sqrt(gamma[i]));
            } else {
                EXPECT_DOUBLE_EQ(cX.at(4 + k, i), 0.0);
            }
        }
    }

    for (size_t l = 0; l < 4; ++l) {
        EXPECT_DOUBLE_EQ(cY[l], y[l]);
    }

    for (size_t m = 4; m < 7; ++m) {
        EXPECT_DOUBLE_EQ(cY[m], 0.0);
    }
}
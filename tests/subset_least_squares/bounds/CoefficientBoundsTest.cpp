// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/6/20.
//

#include <gtest/gtest.h>
#include <LinearAlgebra.h>
#include <iostream>
#include <fstream>
#include <CoefficientBounds.h>
#include <TrivialBounds.h>
#include <DualCauchyBound.h>

using namespace arma;
using namespace piano::sls;

class CoefficientBoundsTest1 : public ::testing::Test {

protected:
    virtual void SetUp() {
        std::string file_path = __FILE__;
        // std::replace(file_path.begin(), file_path.end(), '/', '\\');
        std::string dir_path;
        const size_t last_slash_idx = file_path.rfind('/');
        dir_path = file_path.substr(0, last_slash_idx);

        this->X.load(dir_path + "/X1.testdata", raw_ascii);

        this->y.load(dir_path + "/y1.testdata", raw_ascii);

        this->coefBounds.load(dir_path + "/coef_bounds1.testdata", raw_ascii);
    }

    mat X;
    vec y;
    vec coefBounds;
};

class CoefficientBoundsTest2 : public ::testing::Test {

protected:
    template<class L, class U> using Min = upper_bound::coefficients::Min<upper_bound::coefficients::MinimumEigenvalueBound<L, U>, upper_bound::coefficients::ConditionBound<L, U>>;

    virtual void SetUp() {
        std::string file_path = __FILE__;
        // std::replace(file_path.begin(), file_path.end(), '/', '\\');
        std::string dir_path;
        const size_t last_slash_idx = file_path.rfind('/');
        dir_path = file_path.substr(0, last_slash_idx);

        this->X.load(dir_path + "/X2.testdata", raw_ascii);

        this->y.load(dir_path + "/y2.testdata", raw_ascii);

        this->coefBounds.load(dir_path + "/coef_bounds2.testdata", raw_ascii);
    }

    mat X;
    vec y;
    vec coefBounds;
};

TEST_F(CoefficientBoundsTest1, CorrectResult1) {
    CoefficientBounds<
            upper_bound::coefficients::MinimumEigenvalueBound<
                    lower_bound::predicted_value::TrivialBound,
                    upper_bound::predicted_value::DualCauchyBound
            >
    > coefficientBounds(&X, &y, 1, 5);
    for (size_t i = 0; i < coefBounds.size(); ++i) {
        std::cout << "Bound:   " << coefficientBounds.bound(i) << " >= " << coefBounds[i] << "\n";
        EXPECT_TRUE(coefficientBounds.bound(i) >= coefBounds[i]);
    }
}

TEST_F(CoefficientBoundsTest1, CorrectResult2) {
    CoefficientBounds<upper_bound::coefficients::ConditionBound<>> coefficientBounds(&X, &y, 1, 5);
    for (size_t i = 0; i < coefBounds.size(); ++i) {
        std::cout << "Bound:   " << coefficientBounds.bound(i) << " >= " << coefBounds[i] << "\n";
        EXPECT_TRUE(coefficientBounds.bound(i) >= coefBounds[i]);
    }
}

TEST_F(CoefficientBoundsTest2, CorrectResult3) {
    CoefficientBounds<upper_bound::coefficients::MinimumEigenvalueBound<>> coefficientBounds(&X, &y, 2, 5);
    for (size_t i = 0; i < coefBounds.size(); ++i) {
        std::cout << "Bound:   " << coefficientBounds.bound(i) << " >= " << coefBounds[i] << "\n";
        EXPECT_TRUE(coefficientBounds.bound(i) >= coefBounds[i]);
    }
}

TEST_F(CoefficientBoundsTest2, CorrectResult4) {
    CoefficientBounds<upper_bound::coefficients::ConditionBound<>> coefficientBounds(&X, &y, 2, 5);
    for (size_t i = 0; i < coefBounds.size(); ++i) {
        std::cout << "Bound:   " << coefficientBounds.bound(i) << " >= " << coefBounds[i] << "\n";
        EXPECT_TRUE(coefficientBounds.bound(i) >= coefBounds[i]);
    }
}

TEST_F(CoefficientBoundsTest2, CorrectResult5) {
    CoefficientBounds<
            CoefficientBoundsTest2::Min<
                    lower_bound::predicted_value::TrivialBound,
                    upper_bound::predicted_value::DualCauchyBound
            >
    > coefficientBounds(&X, &y, 2, 5);
    for (size_t i = 0; i < coefBounds.size(); ++i) {
        std::cout << "Bound:   " << coefficientBounds.bound(i) << " >= " << coefBounds[i] << "\n";
        EXPECT_TRUE(coefficientBounds.bound(i) >= coefBounds[i]);
    }
}
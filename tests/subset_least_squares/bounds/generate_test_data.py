#  Copyright 2021  Dennis Kreber
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#

from itertools import combinations

from numpy.linalg import eigvals
from scipy.linalg import solve
import numpy as np


def predictedValue(X, y, ridgeScalar, subset):
    XS = X[:, subset]
    _, ssize = XS.shape
    beta = solve(XS.transpose().dot(XS) + ridgeScalar * np.eye(ssize, ssize), XS.transpose().dot(y))
    return beta.transpose().dot(XS.transpose()).dot(XS).dot(beta) + ridgeScalar * beta.dot(beta)

def coefficients(X, y, ridgeScalar, subset):
    XS = X[:, subset]
    _, ssize = XS.shape
    return solve(XS.transpose().dot(XS) + ridgeScalar * np.eye(ssize, ssize), XS.transpose().dot(y))


if __name__ == "__main__":
    # predicted value bound 1
    p = 30
    n = 200
    r = 1
    all_subsets = combinations(range(p), 5)
    np.random.seed(42)
    X = np.random.rand(n, p)
    for i in range(p):
        norm = np.linalg.norm(X[:, i], 2)
        X[:, i] = X[:, i] / norm
    y = np.ones(n, dtype=np.double)
    y = y / np.linalg.norm(y, 2)
    np.savetxt("X1.testdata", X, delimiter=" ", newline="\n")
    np.savetxt("y1.testdata", y, delimiter=" ", newline="\n")
    mx = 0
    for subset in all_subsets:
        val = predictedValue(X, y, r, subset)
        if val >= mx:
            mx = val
    np.savetxt("predicted_value_bound1.testdata", np.array([mx]))

    # predicted value bound 2
    p = 7
    n = 200
    r = 2
    all_subsets = combinations(range(p), 5)
    np.random.seed(42)
    X = np.random.rand(n, p)
    for i in range(p):
        norm = np.linalg.norm(X[:, i], 2)
        X[:, i] = X[:, i] / norm
    y = np.ones(n, dtype=np.double)
    y = y / np.linalg.norm(y, 2)
    np.savetxt("X2.testdata", X, delimiter=" ", newline="\n")
    np.savetxt("y2.testdata", y, delimiter=" ", newline="\n")
    mx = 0
    for subset in all_subsets:
        val = predictedValue(X, y, r, subset)
        if val >= mx:
            mx = val
    np.savetxt("predicted_value_bound2.testdata", np.array([mx]))

    # coefficient bounds 1
    p = 30
    n = 200
    r = 1
    all_subsets = combinations(range(p), 5)
    np.random.seed(42)
    X = np.random.rand(n, p)
    for i in range(p):
        norm = np.linalg.norm(X[:, i], 2)
        X[:, i] = X[:, i] / norm
    y = np.ones(n, dtype=np.double)
    y = y / np.linalg.norm(y, 2)
    mx_bounds = np.zeros(p)
    for subset in all_subsets:
        beta = coefficients(X, y, r, subset)
        for i, coef in enumerate(beta):
            if np.abs(coef) > mx_bounds[subset[i]]:
                mx_bounds[subset[i]] = np.abs(coef)
    np.savetxt("coef_bounds1.testdata", mx_bounds)

    # coefficient bounds 2
    p = 7
    n = 200
    r = 2
    all_subsets = combinations(range(p), 5)
    np.random.seed(42)
    X = np.random.rand(n, p)
    for i in range(p):
        norm = np.linalg.norm(X[:, i], 2)
        X[:, i] = X[:, i] / norm
    y = np.ones(n, dtype=np.double)
    y = y / np.linalg.norm(y, 2)
    mx_bounds = np.zeros(p)
    for subset in all_subsets:
        beta = coefficients(X, y, r, subset)
        for i, coef in enumerate(beta):
            if np.abs(coef) > mx_bounds[subset[i]]:
                mx_bounds[subset[i]] = np.abs(coef)
    np.savetxt("coef_bounds2.testdata", mx_bounds)

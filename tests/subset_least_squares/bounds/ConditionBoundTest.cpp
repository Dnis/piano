// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/14/20.
//

#include <gtest/gtest.h>
#include <LinearAlgebra.h>
#include <BoundPrerequisites.h>
#include <iostream>
#include <string>
#include <ConditionBound.h>
#include <TrivialBounds.h>
#include <DualCauchyBound.h>

using namespace arma;
using namespace piano::sls;

class ConditionBoundTest1 : public ::testing::Test {

protected:
    virtual void SetUp() {
        std::string file_path = __FILE__;
        // std::replace(file_path.begin(), file_path.end(), '/', '\\');
        std::string dir_path;
        const size_t last_slash_idx = file_path.rfind('/');
        dir_path = file_path.substr(0, last_slash_idx);

        this->X.load(dir_path + "/X1.testdata", raw_ascii);

        this->y.load(dir_path + "/y1.testdata", raw_ascii);

        this->coefBounds.load(dir_path + "/coef_bounds1.testdata", raw_ascii);
    }

    mat X;
    vec y;
    vec coefBounds;
};

class ConditionBoundTest2 : public ::testing::Test {

protected:
    virtual void SetUp() {
        std::string file_path = __FILE__;
        // std::replace(file_path.begin(), file_path.end(), '/', '\\');
        std::string dir_path;
        const size_t last_slash_idx = file_path.rfind('/');
        dir_path = file_path.substr(0, last_slash_idx);

        this->X.load(dir_path + "/X2.testdata", raw_ascii);

        this->y.load(dir_path + "/y2.testdata", raw_ascii);

        this->coefBounds.load(dir_path + "/coef_bounds2.testdata", raw_ascii);
    }

    mat X;
    vec y;
    vec coefBounds;
};

TEST_F(ConditionBoundTest1, CorrectResult1) {
    BoundPrerequisites boundPrerequisites(&X, &y, 1, 5);
    lower_bound::predicted_value::TrivialBound lowerPVBound(&boundPrerequisites);
    upper_bound::predicted_value::DualCauchyBound upperPVBound(&boundPrerequisites);
    upper_bound::coefficients::ConditionBound<lower_bound::predicted_value::TrivialBound, upper_bound::predicted_value::DualCauchyBound> conditionBound(&boundPrerequisites, &lowerPVBound, &upperPVBound);
    for (size_t i = 0; i < coefBounds.size(); ++i) {
        std::cout << "Bound:   " << conditionBound.bound(i) << " >= " << coefBounds[i] << "\n";
        EXPECT_TRUE(conditionBound.bound(i) >= coefBounds[i]);
    }
}

TEST_F(ConditionBoundTest1, CorrectResult2) {
    BoundPrerequisites boundPrerequisites(&X, &y, 1, 5);
    lower_bound::predicted_value::TrivialBound lowerPVBound(&boundPrerequisites);
    upper_bound::predicted_value::TrivialBound upperPVBound(&boundPrerequisites);
    upper_bound::coefficients::ConditionBound<lower_bound::predicted_value::TrivialBound, upper_bound::predicted_value::TrivialBound> conditionBound(&boundPrerequisites, &lowerPVBound, &upperPVBound);
    for (size_t i = 0; i < coefBounds.size(); ++i) {
        std::cout << "Bound:   " << conditionBound.bound(i) << " >= " << coefBounds[i] << "\n";
        EXPECT_TRUE(conditionBound.bound(i) >= coefBounds[i]);
    }
}

TEST_F(ConditionBoundTest2, CorrectResult3) {
    BoundPrerequisites boundPrerequisites(&X, &y, 2, 5);
    lower_bound::predicted_value::TrivialBound lowerPVBound(&boundPrerequisites);
    upper_bound::predicted_value::DualCauchyBound upperPVBound(&boundPrerequisites);
    upper_bound::coefficients::ConditionBound<lower_bound::predicted_value::TrivialBound, upper_bound::predicted_value::DualCauchyBound> conditionBound(&boundPrerequisites, &lowerPVBound, &upperPVBound);
    for (size_t i = 0; i < coefBounds.size(); ++i) {
        std::cout << "Bound:   " << conditionBound.bound(i) << " >= " << coefBounds[i] << "\n";
        EXPECT_TRUE(conditionBound.bound(i) >= coefBounds[i]);
    }
}

TEST_F(ConditionBoundTest2, CorrectResult4) {
    BoundPrerequisites boundPrerequisites(&X, &y, 2, 5);
    lower_bound::predicted_value::TrivialBound lowerPVBound(&boundPrerequisites);
    upper_bound::predicted_value::TrivialBound upperPVBound(&boundPrerequisites);
    upper_bound::coefficients::ConditionBound<lower_bound::predicted_value::TrivialBound, upper_bound::predicted_value::TrivialBound> conditionBound(&boundPrerequisites, &lowerPVBound, &upperPVBound);
    for (size_t i = 0; i < coefBounds.size(); ++i) {
        std::cout << "Bound:   " << conditionBound.bound(i) << " >= " << coefBounds[i] << "\n";
        EXPECT_TRUE(conditionBound.bound(i) >= coefBounds[i]);
    }
}
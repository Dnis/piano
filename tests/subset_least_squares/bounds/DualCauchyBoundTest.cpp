// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 24.03.20.
//

#include <gtest/gtest.h>
#include <LinearAlgebra.h>
#include <BoundPrerequisites.h>
#include <iostream>
#include <string>
#include <DualCauchyBound.h>

using namespace arma;
using namespace piano::sls;

class DualCauchyBoundTest1 : public ::testing::Test {

protected:
    virtual void SetUp() {
        this->X = {
                {1,  4, 3},
                {0,  0, 2},
                {-1, 5, 1}
        };

        this->y = {1, 1, 1};

        this->upperBound1 = 3;
        this->upperBound2 = 2.57142857143;
    }

    mat X;
    vec y;
    double upperBound1, upperBound2;
};

TEST_F(DualCauchyBoundTest1, CorrectResult1) {
    BoundPrerequisites boundPrerequisites(&X, &y, 0.0, 2);
    upper_bound::predicted_value::DualCauchyBound dualCauchyBound(&boundPrerequisites);
    EXPECT_TRUE(dualCauchyBound.bound() >= upperBound1);
}

TEST_F(DualCauchyBoundTest1, CorrectResult2) {
    BoundPrerequisites boundPrerequisites(&X, &y, 0.0, 1);
    upper_bound::predicted_value::DualCauchyBound dualCauchyBound(&boundPrerequisites);
    EXPECT_TRUE(dualCauchyBound.bound() >= upperBound2);
}

class DualCauchyBoundTest2 : public ::testing::Test {

protected:
    virtual void SetUp() {
        std::string file_path = __FILE__;
        // std::replace(file_path.begin(), file_path.end(), '/', '\\');
        std::string dir_path;
        const size_t last_slash_idx = file_path.rfind('/');
        dir_path = file_path.substr(0, last_slash_idx);

        this->X.load(dir_path + "/X1.testdata", raw_ascii);

        this->y.load(dir_path + "/y1.testdata", raw_ascii);

        std::fstream myfile(dir_path + "/predicted_value_bound1.testdata", std::ios_base::in);
        myfile >> this->upperBound;
    }

    mat X;
    vec y;
    double upperBound;
};

TEST_F(DualCauchyBoundTest2, CorrectResult3) {
    BoundPrerequisites boundPrerequisites(&X, &y, 1, 5);
    upper_bound::predicted_value::DualCauchyBound dualCauchyBound(&boundPrerequisites);
    EXPECT_TRUE(dualCauchyBound.bound() >= upperBound);
}

class DualCauchyBoundTest3 : public ::testing::Test {

protected:
    virtual void SetUp() {
        std::string file_path = __FILE__;
        // std::replace(file_path.begin(), file_path.end(), '/', '\\');
        std::string dir_path;
        const size_t last_slash_idx = file_path.rfind('/');
        dir_path = file_path.substr(0, last_slash_idx);

        this->X.load(dir_path + "/X2.testdata", raw_ascii);

        this->y.load(dir_path + "/y2.testdata", raw_ascii);

        std::fstream myfile(dir_path + "/predicted_value_bound2.testdata", std::ios_base::in);
        myfile >> this->upperBound;
    }

    mat X;
    vec y;
    double upperBound;
};

TEST_F(DualCauchyBoundTest3, CorrectResult4) {
    BoundPrerequisites boundPrerequisites(&X, &y, 2, 5);
    upper_bound::predicted_value::DualCauchyBound dualCauchyBound(&boundPrerequisites);
    EXPECT_TRUE(dualCauchyBound.bound() >= upperBound);
}
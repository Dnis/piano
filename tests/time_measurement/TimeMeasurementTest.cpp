// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 18.04.20.
//

#include <gtest/gtest.h>
#include <TimeMeasurement.h>
#include <chrono>
#include <thread>

using namespace piano;
using namespace std::this_thread;     // sleep_for, sleep_until
using namespace std::chrono_literals; // ns, us, ms, s, h, etc.

TEST(TimeMeasurementTest, IsMeasuredCorrectly1) {
    TimeMeasurement m;
    m.start();
    sleep_for(1300ms);
    m.end();
    auto d = m.getDurationInSeconds();
    EXPECT_GE(d, 1.1);
    EXPECT_LE(d, 1.4);
}

TEST(TimeMeasurementTest, IsMeasuredCorrectly2) {
    TimeMeasurement m1;
    TimeMeasurement m2;
    m2.start();
    sleep_for(100ms);
    m1.start();
    sleep_for(200ms);
    m1.end();
    sleep_for(100ms);
    m2.end();
    double d1 = m1.getDurationInSeconds();
    EXPECT_GE(d1, 0.2);
    EXPECT_LE(d1, 0.3);

    auto d2 = m2.getDurationInSeconds();
    EXPECT_GE(d2, 0.400);
    EXPECT_LE(d2, 0.600);
}
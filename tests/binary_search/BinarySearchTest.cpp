// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 24.09.20.
//

#include <gtest/gtest.h>
#include <BinarySearch.h>

using namespace piano;

struct TestFunc {
    double operator()(double x) {
        return x * x;
    }
};

TEST(BinarySearchTest, WorksCorrectly1) {
    TestFunc testFunc;
    auto sol = rationalBinarySearch(0, 10, testFunc, 1);
    EXPECT_TRUE(sol * sol >= 1);
    EXPECT_TRUE(sol * sol <= 1 + 1e-4);
}

TEST(BinarySearchTest, WorksCorrectly2) {
    auto sol = rationalBinarySearch(0, 10, TestFunc{}, 1);
    EXPECT_TRUE(sol * sol >= 1);
    EXPECT_TRUE(sol * sol <= 1 + 1e-4);
}

TEST(BinarySearchTest, WorksCorrectly3) {
    TestFunc testFunc;
    auto sol = rationalBinarySearch(0, 10, testFunc, 1, false);
    EXPECT_TRUE(sol * sol <= 1);
    EXPECT_TRUE(sol * sol >= 1 - 1e-4);
}

TEST(BinarySearchTest, WorksCorrectly4) {
    auto sol = rationalBinarySearch(0, 10, TestFunc{}, 1, false);
    EXPECT_TRUE(sol * sol <= 1);
    EXPECT_TRUE(sol * sol >= 1 - 1e-4);
}

TEST(BinarySearchTest, ErrorIsThrown1) {
    EXPECT_ANY_THROW({
                         rationalBinarySearch(2, 1, TestFunc{}, 1, false);
                     });
}

TEST(BinarySearchTest, GridTest1) {
    std::vector<double> grid{0.1, 0.2, 1.0, 4.0, 5.0};
    auto sol = discreteBinarySearch<double>(grid, TestFunc{}, 1.0);
    EXPECT_EQ(sol, 2);
}

TEST(BinarySearchTest, GridTest2) {
    std::vector<double> grid{0.1, 0.2, 1.1, 4.0, 5.0};
    auto sol = discreteBinarySearch<double>(grid, TestFunc{}, 1.0);
    EXPECT_EQ(sol, 2);
}

TEST(BinarySearchTest, GridTest3) {
    std::vector<double> grid{0.1, 0.2, 0.9, 4.0, 5.0};
    auto sol = discreteBinarySearch<double>(grid, TestFunc{}, 1.0, false);
    EXPECT_EQ(sol, 2);
}

TEST(BinarySearchTest, GridTest4) {
    std::vector<double> grid{4.3, 3.1, 2.0, 0.9, 0.1};
    auto sol = discreteBinarySearch<double>(grid, BinarySearch::Identity<double>{}, 1.0, false, false);
    EXPECT_EQ(sol, 3);
}


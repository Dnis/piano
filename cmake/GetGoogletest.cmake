include(FetchContent)

set(DEFAULT_GOOGLETEST_VERSION release-1.11.0)
IF (NOT USE_GOOGLETEST_VERSION)
    SET(USE_GOOGLETEST_VERSION "${DEFAULT_GOOGLETEST_VERSION}" CACHE STRING "Choose version for googletest." FORCE)
    # Set the possible values of build type for cmake-gui
    SET_PROPERTY(CACHE USE_GOOGLETEST_VERSION PROPERTY STRINGS
            "release-1.10.0" "release-1.11.0"
            )
ENDIF ()

FetchContent_Declare(
        PianoGoogletest
        GIT_REPOSITORY https://github.com/google/googletest.git
        GIT_TAG ${USE_GOOGLETEST_VERSION}
        SOURCE_DIR ${PROJECT_SOURCE_DIR}/extern/googletest
)

FetchContent_GetProperties(PianoGoogletest)

STRING(TOLOWER "PianoGoogletest" LOWER_CASE_NAME)
IF (NOT ${LOWER_CASE_NAME}_POPULATED)
    MESSAGE(STATUS "Downloading googletest ${USE_GOOGLETEST_VERSION}")
    FetchContent_Populate(PianoGoogletest)
ENDIF ()

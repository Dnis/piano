find_package(Armadillo QUIET)
if (NOT Armadillo_FOUND AND NOT DOWNLOAD_ARMADILLO)
    message(WARNING "Armadillo was not found and DOWNLOAD_ARMADILLO was set to OFF. Forcing DOWNLOAD_ARMADILLO to ON.")
    set(DOWNLOAD_ARMADILLO CACHE BOOL ON "Whether or not to download armadillo. If the option is set to off armadillo must be installed." ON)
endif()

if (DOWNLOAD_ARMADILLO)
    include(FetchContent)

    set(DEFAULT_ARMA_VERSION 10.8.x)
    IF (NOT USE_ARMA_VERSION)
        SET(USE_ARMA_VERSION "${DEFAULT_ARMA_VERSION}" CACHE STRING "Choose version for Armadillo." FORCE)
        # Set the possible values of build type for cmake-gui
        SET_PROPERTY(CACHE USE_ARMA_VERSION PROPERTY STRINGS
                "10.5.x" "10.6.x" "10.7.x" "10.8.x"
                )
    ENDIF ()

    FetchContent_Declare(
            PianoArmadillo
            GIT_REPOSITORY https://gitlab.com/conradsnicta/armadillo-code.git
            GIT_TAG ${USE_ARMA_VERSION}
            SOURCE_DIR ${PROJECT_SOURCE_DIR}/extern/armadillo-code
    )

    FetchContent_GetProperties(PianoArmadillo)

    STRING(TOLOWER "PianoArmadillo" LOWER_CASE_NAME)
    IF (NOT ${LOWER_CASE_NAME}_POPULATED)
        MESSAGE(STATUS "Downloading Armadillo ${USE_ARMA_VERSION}")
        FetchContent_Populate(PianoArmadillo)
    ENDIF ()

    set(CMAKE_POLICY_DEFAULT_CMP0048 NEW)
    add_subdirectory(${PROJECT_SOURCE_DIR}/extern/armadillo-code)
    if (TARGET armadillo)
        add_library(Armadillo ALIAS armadillo)
    else ()
        message(FATAL_ERROR "Armadillo target not found. Make sure that armadillo-code was downloaded in the directory 'extern'.")
    endif ()
else ()
    add_library(Armadillo IMPORTED INTERFACE)
    set_property(TARGET Armadillo PROPERTY
            INTERFACE_INCLUDE_DIRECTORIES ${ARMADILLO_INCLUDE_DIRS})
    set_property(TARGET Armadillo PROPERTY
            INTERFACE_LINK_LIBRARIES ${ARMADILLO_LIBRARIES})
    message("Armadillo found.")
    message("Armadillo Include dirs: " ${ARMADILLO_INCLUDE_DIRS})
    message("Armadillo Library: " ${ARMADILLO_LIBRARIES})
endif ()
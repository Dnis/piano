# Debug options
#######################################
if (NOT CMAKE_BUILD_TYPE)
    message(STATUS "Setting build type to 'Release' as none was specified.")
    set(CMAKE_BUILD_TYPE "Release" CACHE
            STRING "Choose the type of build." FORCE)
    # Set the possible values of build type for cmake-gui
    set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
            "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
endif ()

# Apple specific settings
################################################
if (APPLE)
    option(COMPILE_FOR_X86_64 "Choose if you want to compile for Intel or for M1 chip" OFF)
endif ()

# MLPACK
#################################################
option(USE_MLPACK "Whether or not to use mlpack. Mlpack must be installed and cannot be downloaded." OFF)
option(INCLUDE_MLPACK_BEFORE_ARMADILLO "If ON mlpack is included before armadillo as recommend by mlpack. If MLPACK is not used this option should be turned off. Has no effect if USE_MLPACK is set to OFF." ON)

# Armadillo
#################################################
option(DOWNLOAD_ARMADILLO "Whether or not to download armadillo. If the option is set to off armadillo must be installed." OFF)

# PICASSO
###################################################
option(DOWNLOAD_PICASSO "Whether or not to download PICASSO. If the the option is set to off PICASSO must be installed." ON)
option(USE_PICASSO "Whether or not to use PICASSO." ON)
option(USE_PICASSO_EIGEN "Whether or not to use Eigen provided by PICASSO or Eigen installed on the system" ON)

# CPLEX
##################################################
option(USE_CPLEX "Whether or not to use CPLEX." ON)
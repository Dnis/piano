# Find MLPACK
#
# It sets the following variables:
#   MLPACK_FOUND - set to true if the library is found
#   MLPACK_INCLUDE_DIRS - list of required include directories
#   MLPACK_LIBRARIES - list of libraries to be linked

include(FindPackageHandleStandardArgs)

find_package(PkgConfig)
pkg_check_modules(PC_MLPACK QUIET mlpack>=3.4.2)

find_path(MLPACK_INCLUDE_DIR
        NAMES mlpack/core.hpp mlpack/prereqs.hpp
        PATHS ${PC_MLPACK_INCLUDE_DIRS}
        )
message(STATUS "Location of MLPACK include directories: " ${MLPACK_INCLUDE_DIR})
find_library(MLPACK_LIBRARY
        NAMES mlpack
        PATHS ${PC_MLPACK_LIBRARY_DIRS}
        )
message(STATUS "Location of MLPACK library: " ${MLPACK_LIBRARY})
find_package_handle_standard_args(MLPACK
        REQUIRED_VARS MLPACK_LIBRARY MLPACK_INCLUDE_DIR
        VERSION_VAR MLPACK_VERSION_STRING
        )

if(MLPACK_FOUND)
    set(MLPACK_INCLUDE_DIRS ${MLPACK_INCLUDE_DIR})
    set(MLPACK_LIBRARIES ${MLPACK_LIBRARY})

    add_library(mlpack IMPORTED INTERFACE)
    set_property(TARGET mlpack PROPERTY
            INTERFACE_INCLUDE_DIRECTORIES ${MLPACK_INCLUDE_DIRS})
    set_property(TARGET mlpack PROPERTY
            INTERFACE_LINK_LIBRARIES ${MLPACK_LIBRARIES})

    find_package(OpenMP QUIET)
    if(${OpenMP_CXX_FOUND})
        if(NOT TARGET OpenMP::OpenMP_CXX)
            find_package(Threads REQUIRED)
            add_library(OpenMP::OpenMP_CXX IMPORTED INTERFACE)
            set_property(TARGET OpenMP::OpenMP_CXX
                    PROPERTY INTERFACE_COMPILE_OPTIONS ${OpenMP_CXX_FLAGS})
            # Only works if the same flag is passed to the linker; use CMake 3.9+ otherwise (Intel, AppleClang)
            set_property(TARGET OpenMP::OpenMP_CXX
                    PROPERTY INTERFACE_LINK_LIBRARIES ${OpenMP_CXX_FLAGS} Threads::Threads)
        endif()

        target_link_libraries(mlpack INTERFACE OpenMP::OpenMP_CXX)
    endif()
endif()

mark_as_advanced(
        MLPACK_INCLUDE_DIR
        MLPACK_LIBRARY
)
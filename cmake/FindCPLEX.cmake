# This module finds cplex.
#
# User can give CPLEX_ROOT_DIR as a hint stored in the cmake cache.
#
# It sets the following variables:
#  CPLEX_FOUND              - Set to false, or undefined, if cplex isn't found.
#  CPLEX_INCLUDE_DIRS       - include directory
#  CPLEX_LIBRARIES          - library files

include(FindPackageHandleStandardArgs)

if (UNIX AND NOT APPLE)
    set(CPLEX_ILOG_DIRS /opt/ibm/ILOG /opt/IBM/ILOG)
elseif (APPLE)
    set(CPLEX_ILOG_DIRS /Applications)
else()
    message(WARNING "Finding CPLEX only works on a Linux or MacOS system for now. Please provide the path to cplex manually.")
endif ()

if (NOT CPLEX_STUDIO_DIR)
    foreach (dir ${CPLEX_ILOG_DIRS})
        file(GLOB LOOKED_UP_CPLEX_STUDIO_DIRS "${dir}/CPLEX_Studio*")
        list(APPEND CPLEX_STUDIO_DIRS ${LOOKED_UP_CPLEX_STUDIO_DIRS})
    endforeach ()
    list(SORT CPLEX_STUDIO_DIRS)
    list(REVERSE CPLEX_STUDIO_DIRS)
    if (CPLEX_STUDIO_DIRS)
        list(GET CPLEX_STUDIO_DIRS 0 CPLEX_STUDIO_DIR_TEMP)
    endif ()

    if (NOT CPLEX_STUDIO_DIR_TEMP)
        set(CPLEX_STUDIO_DIR CPLEX_STUDIO_DIR-NOTFOUND)
    endif ()
    set(CPLEX_STUDIO_DIR ${CPLEX_STUDIO_DIR_TEMP} CACHE PATH
            "Path to the CPLEX Studio directory")
    message(STATUS "Location of CPLEX Studio directory: ${CPLEX_STUDIO_DIR}")
endif ()

find_path(CPLEX_INCLUDE_DIR
        ilcplex/cplex.h
        HINTS ${CPLEX_STUDIO_DIR}/cplex/include
        PATHS ENV C_INCLUDE_PATH
        ENV C_PLUS_INCLUDE_PATH
        ENV INCLUDE_PATH
        )
message("Location of CPLEX include directory: ${CPLEX_INCLUDE_DIR}")

find_path(CPLEX_CONCERT_INCLUDE_DIR
        ilconcert/iloenv.h
        HINTS ${CPLEX_STUDIO_DIR}/concert/include
        ${CPLEX_STUDIO_DIR}/include
        PATHS ENV C_INCLUDE_PATH
        ENV C_PLUS_INCLUDE_PATH
        ENV INCLUDE_PATH
        )
message("Location of CPLEX Concert include directory: ${CPLEX_CONCERT_INCLUDE_DIR}")

find_library(CPLEX_LIBRARY
        NAMES cplex
        HINTS ${CPLEX_STUDIO_DIR}/cplex/lib/x86-64_linux/static_pic
        ${CPLEX_STUDIO_DIR}/cplex/lib/x86-64_osx/static_pic
        PATHS ENV LIBRARY_PATH
        ENV LD_LIBRARY_PATH
        )
message("Location of CPLEX library: ${CPLEX_LIBRARY}")

find_library(CPLEX_ILOCPLEX_LIBRARY
        NAMES ilocplex
        HINTS ${CPLEX_STUDIO_DIR}/cplex/lib/x86-64_linux/static_pic
        ${CPLEX_STUDIO_DIR}/cplex/lib/x86-64_osx/static_pic
        PATHS ENV LIBRARY_PATH
        ENV LD_LIBRARY_PATH
        )
message(STATUS "Location of ILOCPLEX library: ${CPLEX_ILOCPLEX_LIBRARY}")

find_library(CPLEX_CONCERT_LIBRARY
        concert
        HINTS ${CPLEX_STUDIO_DIR}/concert/lib/x86-64_linux/static_pic
        ${CPLEX_STUDIO_DIR}/concert/lib/x86-64_osx/static_pic
        PATHS ENV LIBRARY_PATH
        ENV LD_LIBRARY_PATH
        )
message(STATUS "Location of CPLEX Concert library: ${CPLEX_ILOCPLEX_LIBRARY}")

find_package_handle_standard_args(CPLEX DEFAULT_MSG
        CPLEX_LIBRARY CPLEX_INCLUDE_DIR CPLEX_ILOCPLEX_LIBRARY CPLEX_CONCERT_LIBRARY CPLEX_CONCERT_INCLUDE_DIR)

if (CPLEX_FOUND)
    set(CPLEX_INCLUDE_DIRS ${CPLEX_INCLUDE_DIR} ${CPLEX_CONCERT_INCLUDE_DIR})
    set(CPLEX_LIBRARIES ${CPLEX_CONCERT_LIBRARY} ${CPLEX_ILOCPLEX_LIBRARY} ${CPLEX_LIBRARY})
    if (CMAKE_SYSTEM_NAME STREQUAL "Linux")
        set(CPLEX_LIBRARIES "${CPLEX_LIBRARIES};m;pthread;")
    endif (CMAKE_SYSTEM_NAME STREQUAL "Linux")
endif (CPLEX_FOUND)

if (CPLEX_FOUND AND NOT TARGET CPLEX::CPLEX)
    add_library(CPLEX::CPLEX INTERFACE IMPORTED)
    target_include_directories(CPLEX::CPLEX INTERFACE ${CPLEX_INCLUDE_DIRS})
    target_link_libraries(CPLEX::CPLEX INTERFACE ${CPLEX_LIBRARIES})
    set(THREADS_PREFER_PTHREAD_FLAG ON)
    find_package(Threads REQUIRED)
    target_link_libraries(CPLEX::CPLEX INTERFACE m Threads::Threads dl)
    target_compile_definitions(CPLEX::CPLEX INTERFACE -DIL_STD)
endif ()



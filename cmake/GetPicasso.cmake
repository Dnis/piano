if (USE_PICASSO)
    include(FetchContent)

    FetchContent_Declare(
            PianoPicasso
            GIT_REPOSITORY https://github.com/jasonge27/picasso.git
            GIT_TAG c1bdeb7550af8cccc3361cb4e3d77a095701e317
            SOURCE_DIR ${PROJECT_SOURCE_DIR}/extern/picasso
    )

    FetchContent_GetProperties(PianoPicasso)

    STRING(TOLOWER "PianoPicasso" LOWER_CASE_NAME)
    IF (NOT ${LOWER_CASE_NAME}_POPULATED)
        MESSAGE(STATUS "Downloading picasso")
        FetchContent_Populate(PianoPicasso)
    ENDIF ()

    add_library(picasso SHARED
            ${PROJECT_SOURCE_DIR}/extern/picasso/src/objective/gaussian_naive_update.cpp
            ${PROJECT_SOURCE_DIR}/extern/picasso/src/objective/glm.cpp
            ${PROJECT_SOURCE_DIR}/extern/picasso/src/objective/sqrtmse.cpp
            ${PROJECT_SOURCE_DIR}/extern/picasso/src/solver/actgd.cpp
            ${PROJECT_SOURCE_DIR}/extern/picasso/src/solver/actnewton.cpp
            ${PROJECT_SOURCE_DIR}/extern/picasso/src/solver/solver_params.cpp)
    target_include_directories(picasso SYSTEM PUBLIC
            $<INSTALL_INTERFACE:extern/picasso/include>
            $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/extern/picasso/include>
            $<INSTALL_INTERFACE:extern/picasso/dmlc-core/include>
            $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/extern/picasso/dmlc-core/include>
            PRIVATE
            ${PROJECT_SOURCE_DIR}/extern/picasso/src/objective
            ${PROJECT_SOURCE_DIR}/extern/picasso/src/solver)
    set_property(TARGET picasso PROPERTY CXX_STANDARD 11)
    find_package(OpenMP)
    if (OpenMP_CXX_FOUND OR OPENMP_FOUND)
        target_link_libraries(picasso PUBLIC OpenMP::OpenMP_CXX)
    endif ()
    set_property(TARGET picasso PROPERTY POSITION_INDEPENDENT_CODE ON)
    if (MSVC)
        # Multithreaded compilation
        target_compile_options(picasso PUBLIC /MP)
    else ()
        # Correct error for GCC 5 and cuda
        target_compile_definitions(picasso PUBLIC -D_MWAITXINTRIN_H_INCLUDED -D_FORCE_INLINES)
        # Performance
        target_compile_options(picasso PUBLIC -funroll-loops)
    endif ()
    #[[if (NOT USE_PICASSO_EIGEN)
        find_package(Eigen3 QUIET)
        if (NOT Eigen3_FOUND AND NOT USE_PICASSO_EIGEN)
            message(STATUS "Eigen3 was not found and USE_PICASSO_EIGEN is OFF. Forcing USE_PICASSO_EIGEN to ON.")
            set(USE_PICASSO_EIGEN ON CACHE BOOL "Whether or not to use Eigen provided by PICASSO or Eigen installed on the system" FORCE)
        endif ()
    endif ()
    if (USE_PICASSO_EIGEN)
        message(STATUS "Eigen3 not found. Including Eigen3 from PICASSO.")
        if (APPLE)
            set(CMAKE_MACOSX_RPATH ON)
        endif (APPLE)
        set(BUILD_TESTING OFF CACHE BOOL "" FORCE)
        add_subdirectory(extern/picasso/include/eigen3 EXCLUDE_FROM_ALL)
        if (TARGET eigen)
            add_library(Eigen3::Eigen ALIAS eigen)
        else ()
            message(FATAL_ERROR "Eigen3 was not found.")
        endif ()
    endif ()
    target_link_libraries(picasso PUBLIC Eigen3::Eigen)]]
endif ()
// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 01.04.20.
//

#ifndef PIANO_ALLSUBSETS_H
#define PIANO_ALLSUBSETS_H

#include <Types.h>
#include <RandomAccessIterator.h>
#include <cmath>

namespace piano::stools {
    Subset getSubset(size_t i, size_t maxIndex);

    struct AllSubsets {
        using value_type = Subset;

        explicit AllSubsets(size_t maxIndex) : maxIndex(maxIndex) {}

        size_t maxIndex;

        [[nodiscard]] size_t size() const;

        Subset operator[](size_t i) const;

        [[nodiscard]] RandomAccessIterator<AllSubsets, Subset>
        const_iterator() const;
    };
}


#endif //PIANO_ALLSUBSETS_H

add_library(stools_Conversion SHARED ${PROJECT_SOURCE_DIR}/private/subset_tools/conversion/Conversion.cpp)
add_library(stools::Conversion ALIAS stools_Conversion)

target_include_directories(stools_Conversion
        PUBLIC
        $<INSTALL_INTERFACE:include/piano/subset_tools/conversion>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
        PRIVATE
        ${PROJECT_SOURCE_DIR}/private/subset_tools/conversion
        )

set_property(TARGET stools_Conversion PROPERTY CXX_STANDARD 17)
set_property(TARGET stools_Conversion PROPERTY EXPORT_NAME stools::Conversion)

target_link_libraries(stools_Conversion
        PUBLIC
        Types
        LinearAlgebra)


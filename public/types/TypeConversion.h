// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//
// Created by kreber on 08.09.21.
//

#ifndef PIANO_TYPECONVERSION_H
#define PIANO_TYPECONVERSION_H

#include <cstddef>
#include <string>
#include <limits>
#include <stdexcept>

namespace piano {
    template<typename T>
    struct TypeParseTraits;
}

#define PIANO_REGISTER_PARSE_TYPE(X) template <> struct piano::TypeParseTraits<X> \
    { static const char* name; } ; inline const char* piano::TypeParseTraits<X>::name = #X


PIANO_REGISTER_PARSE_TYPE(int);
PIANO_REGISTER_PARSE_TYPE(long);
PIANO_REGISTER_PARSE_TYPE(unsigned long);
PIANO_REGISTER_PARSE_TYPE(unsigned int);

namespace piano {
    template <class T>
    void assertCastable(size_t i, const std::string& variableName, const std::string& variableExplanation) {
        if (i > static_cast<size_t>(std::numeric_limits<T>::max())) throw std::overflow_error(
                    "The value of " + variableName + " (" + variableExplanation +
                    ") is too large and cannot be casted to " + TypeParseTraits<T>::name + ".");
    }

    int safeCastToInt(size_t i, const std::string& variableName, const std::string& variableExplanation);
    long safeCastToLong(size_t i, const std::string& variableName, const std::string& variableExplanation);
}

#define PIANO_SAFE_CAST_TO_INT(var, varExplanation) safeCastToInt(var, #var, varExplanation)
#define PIANO_SAFE_CAST_TO_LONG(var, varExplanation) safeCastToLong(var, #var, varExplanation)

#endif //PIANO_TYPECONVERSION_H

// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 31.08.20.
//

#ifndef PIANO_EVEXTRACTION_H
#define PIANO_EVEXTRACTION_H

#include <LinearAlgebra.h>
#include <memory>
#include <MultiThreadSkeleton.h>

namespace piano {
    void handOver(const arma::mat &X, const arma::vec &y, const arma::vec &gamma, std::unique_ptr<arma::mat> *XAlloc,
                  std::unique_ptr<arma::vec> *yAlloc, const arma::mat **newXPtr, const arma::vec **newYPtr,
                  double *newRidgeScalarPtr, double percentExtraction, size_t numberOfThreads);
    void handOver(const arma::mat &X, const arma::vec &y, double ridgeScalar, std::unique_ptr<arma::mat> *XAlloc,
                  std::unique_ptr<arma::vec> *yAlloc, const arma::mat **newXPtr, const arma::vec **newYPtr,
                  double *newRidgeScalarPtr, double percentExtraction, size_t numberOfThreads);
}

namespace piano::sls {
class EigenvalueExtraction : public piano::MultiThreadSkeleton {
    public:
        friend void piano::handOver(const arma::mat &X, const arma::vec &y, const arma::vec &gamma,
                                    std::unique_ptr<arma::mat> *XAlloc,
                                    std::unique_ptr<arma::vec> *yAlloc, const arma::mat **newXPtr,
                                    const arma::vec **newYPtr,
                                    double *newRidgeScalarPtr, double percentExtraction, size_t numberOfThreads);
        friend void
        piano::handOver(const arma::mat &X, const arma::vec &y, double ridgeScalar, std::unique_ptr<arma::mat> *XAlloc,
                        std::unique_ptr<arma::vec> *yAlloc, const arma::mat **newXPtr, const arma::vec **newYPtr,
                        double *newRidgeScalarPtr, double percentExtraction, size_t numberOfThreads);

        EigenvalueExtraction(const arma::mat &X, const arma::vec &y, const arma::vec &gamma,
                             double percentExtraction);

        EigenvalueExtraction(const arma::mat &X, const arma::vec &y, double ridgeScalar, double percentExtraction);

        EigenvalueExtraction(const arma::mat &X, const arma::vec &y, double ridgeScalar);

        EigenvalueExtraction(const arma::mat &X, const arma::vec &y, const arma::vec &gamma);

        void setPercentExtraction(double percent);

        void computeExtraction(double percent, arma::mat *XTilde, arma::vec *yTilde, double *r) const;

        [[nodiscard]] const arma::mat &getXTilde() const;

        [[nodiscard]] const arma::vec &getYTilde() const;

        double getR() const;

    private:
        void computeSVDValues() const;
        void computeExtraction() const;

        mutable bool svdsComputed = false;
        mutable bool recomputeExtraction = true;

        std::size_t n;
        std::size_t p;
        const arma::mat *X;
        const arma::vec *y;
        std::unique_ptr<arma::mat> XAlloc;
        std::unique_ptr<arma::vec> yAlloc;
        double ridgeScalar;
        double percent;
        mutable arma::mat XTilde;
        mutable arma::vec yTilde;
        mutable arma::mat SigmatSigma;
        mutable arma::mat SigmatUty;
        mutable arma::mat V;
        mutable double minEigenvalue;
        mutable double r;
    };
}


#endif //PIANO_EVEXTRACTION_H

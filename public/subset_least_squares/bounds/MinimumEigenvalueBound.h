// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/6/20.
//

#ifndef SLS_MINIMUMEIGENVALUEBOUND_H
#define SLS_MINIMUMEIGENVALUEBOUND_H

#include <LinearAlgebra.h>
#include <Maxk.h>
#include <SquaredL2.h>
#include <BoundPrerequisites.h>
#include "DualCauchyBound.h"
#include "TrivialBounds.h"
#include <MultiThreadSkeleton.h>

namespace piano::sls::upper_bound::coefficients {
    template<class Unused = piano::sls::lower_bound::predicted_value::TrivialBound, class UpperPredictedValueBound = piano::sls::upper_bound::predicted_value::DualCauchyBound>
    class MinimumEigenvalueBound : public MultiThreadSkeleton {
    public:
        using LowPredictedValueBound = Unused;
        using UpPredictedValueBound = UpperPredictedValueBound;

        MinimumEigenvalueBound(BoundPrerequisites *prerequisites,
                               UpperPredictedValueBound *upperPredictedValueBound);

        MinimumEigenvalueBound(BoundPrerequisites *prerequisites, Unused *lowerPredictedValueBound,
                               UpperPredictedValueBound *upperPredictedValueBound);

        [[nodiscard]] double bound(size_t i);

        void setNumberOfThreads(std::size_t num) {
            MultiThreadSkeleton::setNumberOfThreads(num);
            upperPredictedValueBound->setNumberOfThreads(num);
        }

        void computeBounds();

    private:
        BoundPrerequisites *prerequisites;
        UpperPredictedValueBound *upperPredictedValueBound;
        std::vector<double> bounds;
        bool boundsComputed = false;
    };

    template<class Unused, class UpperPredictedValueBound>
    MinimumEigenvalueBound<Unused, UpperPredictedValueBound>::MinimumEigenvalueBound(BoundPrerequisites *prerequisites,
                                                                             UpperPredictedValueBound *upperPredictedValueBound)
            :
            prerequisites(prerequisites),
            upperPredictedValueBound(upperPredictedValueBound),
            bounds(prerequisites->getP()) {
    }

    template<class Unused, class UpperPredictedValueBound>
    void MinimumEigenvalueBound<Unused, UpperPredictedValueBound>::computeBounds() {
        auto eigvals = arma::eig_sym(*(prerequisites->getXXplusI()));
        double minEV = eigvals[0];
        for (size_t i = 0; i < prerequisites->getP(); ++i) {
            double term1 = pow(prerequisites->getXXplusI()->at(i, i) - minEV, 2);
            arma::vec XXi = prerequisites->getXXplusI()->col(i);
            double sii = as_scalar(XXi.t() * XXi);
            double term2 = 1.0 / (minEV * (sii - minEV * prerequisites->getXXplusI()->at(i, i)));
            bounds[i] = sqrt(upperPredictedValueBound->bound() * (1.0 / minEV - term1 * term2));
        }
        boundsComputed = true;
    }

    template<class Unused, class UpperPredictedValueBound>
    double MinimumEigenvalueBound<Unused, UpperPredictedValueBound>::bound(size_t i) {
        if (!boundsComputed) computeBounds();
        return bounds[i];
    }

    template<class Unused, class UpperPredictedValueBound>
    MinimumEigenvalueBound<Unused, UpperPredictedValueBound>::MinimumEigenvalueBound(BoundPrerequisites *prerequisites,
                                                                             [[maybe_unused]] Unused *lowerPredictedValueBound,
                                                                             UpperPredictedValueBound *upperPredictedValueBound)
            :
            prerequisites(prerequisites),
            upperPredictedValueBound(upperPredictedValueBound),
            bounds(prerequisites->getP()) {
    }
}


#endif //SLS_MINIMUMEIGENVALUEBOUND_H

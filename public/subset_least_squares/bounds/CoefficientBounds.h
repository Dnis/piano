// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/6/20.
//

#ifndef SLS_COEFFICIENTBOUNDS_H
#define SLS_COEFFICIENTBOUNDS_H

#include <LinearAlgebra.h>
#include <MinimumEigenvalueBound.h>
#include <Conjunction.h>
#include <ConditionBound.h>
#include "TrivialBounds.h"
#include "DualCauchyBound.h"
#include <InverseBound.h>
#include <MultiThreadSkeleton.h>

namespace piano::sls {
    template<class BoundType = upper_bound::coefficients::InverseBound<
            lower_bound::predicted_value::TrivialBound,
            upper_bound::predicted_value::DualCauchyBound>
    >
    class CoefficientBounds : public MultiThreadSkeleton {
    public:
        CoefficientBounds(const arma::mat *X, const arma::vec *y, size_t k) : prerequisites(X, y, 0.0, k) {
            lowerPVBoundPtr = std::make_unique<BoundType::LowPredictedValueBound>(&prerequisites);
            upperPVBoundPtr = std::make_unique<BoundType::UpPredictedValueBound>(&prerequisites);
            boundPtr = std::make_unique<BoundType>(&prerequisites, lowerPVBoundPtr.get(), upperPVBoundPtr.get());
        }

        CoefficientBounds(const arma::mat *X, const arma::vec *y, double ridgeScalar, size_t k) : prerequisites(X, y,
                                                                                                             ridgeScalar,
                                                                                                             k) {
            lowerPVBoundPtr = std::make_unique<typename BoundType::LowPredictedValueBound>(&prerequisites);
            upperPVBoundPtr = std::make_unique<typename BoundType::UpPredictedValueBound>(&prerequisites);
            boundPtr = std::make_unique<BoundType>(&prerequisites, lowerPVBoundPtr.get(), upperPVBoundPtr.get());
        }

        CoefficientBounds(const arma::mat *X, const arma::vec *y, const arma::vec *gamma, size_t k) : prerequisites(X, y,
                                                                                                                 gamma,
                                                                                                                 k) {
            lowerPVBoundPtr = std::make_unique<typename BoundType::LowPredictedValueBound>(&prerequisites);
            upperPVBoundPtr = std::make_unique<typename BoundType::UpPredictedValueBound>(&prerequisites);
            boundPtr = std::make_unique<BoundType>(&prerequisites, lowerPVBoundPtr.get(), upperPVBoundPtr.get());
        }

        [[nodiscard]] double bound(size_t i) const {
            return boundPtr->bound(i);
        }

        void setNumberOfThreads(std::size_t num) {
            MultiThreadSkeleton::setNumberOfThreads(num);
            boundPtr->setNumberOfThreads(num);
            lowerPVBoundPtr->setNumberOfThreads(num);
            upperPVBoundPtr->setNumberOfThreads(num);
        }

    private:
        std::unique_ptr<BoundType> boundPtr;
        std::unique_ptr<typename BoundType::LowPredictedValueBound> lowerPVBoundPtr;
        std::unique_ptr<typename BoundType::UpPredictedValueBound> upperPVBoundPtr;
        BoundPrerequisites prerequisites;
    };
}

#endif //SLS_COEFFICIENTBOUNDS_H

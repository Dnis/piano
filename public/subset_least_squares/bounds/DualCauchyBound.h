// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/18/20.
//

#ifndef SLS_DUALCAUCHYBOUND_H
#define SLS_DUALCAUCHYBOUND_H


#include "BoundPrerequisites.h"
#include <MultiThreadSkeleton.h>

namespace piano::sls::upper_bound::predicted_value {
    class DualCauchyBound : public MultiThreadSkeleton {
    public:
        explicit DualCauchyBound(BoundPrerequisites *prerequisites);
        virtual ~DualCauchyBound() = default;

        [[nodiscard]] virtual double bound();
    protected:
        BoundPrerequisites *prerequisites;

    private:
        double predictionBound;
        bool predictionBoundComputed = false;
        void computePredictionBound();
    };
}

namespace piano::sls::lower_bound::rss {
class DualCauchyBound : upper_bound::predicted_value::DualCauchyBound {
public:
    explicit DualCauchyBound(BoundPrerequisites* prerequisites);

    [[nodiscard]] double bound();
};
}


#endif //SLS_DUALCAUCHYBOUND_H

// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/6/20.
//

#ifndef SLS_CONDITIONBOUND_H
#define SLS_CONDITIONBOUND_H

#include <BoundPrerequisites.h>
#include "TrivialBounds.h"
#include "DualCauchyBound.h"
#include <MultiThreadSkeleton.h>

namespace piano::sls::upper_bound::coefficients {
    template<class Unused = piano::sls::lower_bound::predicted_value::TrivialBound, class UpperPredictedValueBound = piano::sls::upper_bound::predicted_value::DualCauchyBound>
    class ConditionBound : public MultiThreadSkeleton {
    public:
        using LowPredictedValueBound = Unused;
        using UpPredictedValueBound = UpperPredictedValueBound;

        ConditionBound(BoundPrerequisites *prerequisites, UpperPredictedValueBound *upperPredictedValueBound);

        ConditionBound(BoundPrerequisites *prerequisites, Unused *lowerPredictedValueBound,
                       UpperPredictedValueBound *upperPredictedValueBound);

        [[nodiscard]] double bound(size_t i);

        void setNumberOfThreads(std::size_t num) {
            MultiThreadSkeleton::setNumberOfThreads(num);
            upperPredictedValueBound->setNumberOfThreads(num);
        }

        void computeBounds();

    private:
        BoundPrerequisites *prerequisites;
        std::vector<double> bounds;
        UpperPredictedValueBound *upperPredictedValueBound;

        bool boundsComputed = false;
    };

    template<class Unused, class UpperPredictedValueBound>
    ConditionBound<Unused, UpperPredictedValueBound>::ConditionBound(BoundPrerequisites *prerequisites,
                                                             UpperPredictedValueBound *upperPredictedValueBound) :
            prerequisites(prerequisites),
            bounds(prerequisites->getP()),
            upperPredictedValueBound(upperPredictedValueBound) {
    }

    template<class Unused, class UpperPredictedValueBound>
    ConditionBound<Unused, UpperPredictedValueBound>::ConditionBound(BoundPrerequisites *prerequisites,
                                                             [[maybe_unused]] Unused *lowerPredictedValueBound,
                                                             UpperPredictedValueBound *upperPredictedValueBound) :
            prerequisites(prerequisites),
            bounds(prerequisites->getP()),
            upperPredictedValueBound(upperPredictedValueBound) {
    }

    template<class Unused, class UpperPredictedValueBound>
    void ConditionBound<Unused, UpperPredictedValueBound>::computeBounds() {
        auto eigvals = eig_sym(*(prerequisites->getXXplusI()));
        double minEV = eigvals[0];
        double maxEV = eigvals[prerequisites->getP() - 1];
        for (size_t i = 0; i < prerequisites->getP(); ++i) {
            bounds[i] = sqrt(upperPredictedValueBound->bound() * (1.0 / 4.0 * (maxEV / minEV + minEV / maxEV + 2) *
                                                                  (1.0 / prerequisites->getXXplusIInv()->at(i, i))));
        }
    }

    template<class Unused, class UpperPredictedValueBound>
    double ConditionBound<Unused, UpperPredictedValueBound>::bound(size_t i) {
        if (!boundsComputed) computeBounds();
        return bounds[i];
    }
}


#endif //SLS_CONDITIONBOUND_H
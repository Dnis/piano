// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/6/20.
//

#ifndef SLS_CONJUNCTION_H
#define SLS_CONJUNCTION_H

#include <type_traits>
#include <MultiThreadSkeleton.h>

namespace piano::sls::upper_bound::coefficients {
    template<class Bound1, class Bound2>
    class Min : public MultiThreadSkeleton {
        static_assert(std::is_same_v<typename Bound1::LowPredictedValueBound, typename Bound2::LowPredictedValueBound>,
                      "Both LowerPredictedValueBounds must be the same!");
        static_assert(std::is_same_v<typename Bound1::UpPredictedValueBound, typename Bound2::UpPredictedValueBound>,
                      "Both UpperPredictedValueBounds must be the same!");


    public:
        using LowPredictedValueBound = typename Bound1::LowPredictedValueBound;
        using UpPredictedValueBound = typename Bound1::UpPredictedValueBound;

        Min(BoundPrerequisites *prerequisites, LowPredictedValueBound *lowerPredictedValueBound,
                     UpPredictedValueBound *upperPredictedValueBound);

        [[nodiscard]] double bound(size_t i);

        void setNumberOfThreads(std::size_t num) {
            MultiThreadSkeleton::setNumberOfThreads(num);
            bound1.setNumberOfThreads(num);
            bound2.setNumberOfThreads(num);
        }

    private:
        Bound1 bound1;
        Bound2 bound2;
    };

    template<class Bound1, class Bound2>
    double Min<Bound1, Bound2>::bound(size_t i) {
        return std::min(bound1.bound(i), bound2.bound(i));
    }

    template<class Bound1, class Bound2>
    Min<Bound1, Bound2>::Min(BoundPrerequisites *prerequisites, LowPredictedValueBound *lowerPredictedValueBound,
                             UpPredictedValueBound *upperPredictedValueBound) :
                             bound1(prerequisites, lowerPredictedValueBound, upperPredictedValueBound),
                             bound2(prerequisites, lowerPredictedValueBound, upperPredictedValueBound) {

    }
}


#endif //SLS_CONJUNCTION_H

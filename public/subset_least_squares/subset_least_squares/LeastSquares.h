// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/4/20.
//

#ifndef SUBSET_LEAST_SQUARES_LEASTSQUARES_H
#define SUBSET_LEAST_SQUARES_LEASTSQUARES_H

#include <LinearAlgebra.h>
#include <Types.h>
#include <Solution.h>
#include <memory>

namespace piano::sls {
    class LeastSquares {
    public:
        LeastSquares(const arma::mat *X, const arma::vec *y);
        LeastSquares(const arma::mat *X, const arma::vec *y, double ridgeScalar);
        LeastSquares(const arma::mat *X, const arma::vec *y, const arma::vec *gamma);

        arma::vec computeCoefs(Subset&& subset) const;
        [[nodiscard]] arma::vec computeCoefs(const Subset& subset) const;
        [[nodiscard]] arma::vec computeCoefs() const;

        Solution compute(piano::Subset&& subset) const;
        [[nodiscard]] Solution compute(const piano::Subset& subset) const;
        [[nodiscard]] Solution compute() const;

        [[nodiscard]] double eval(const arma::vec &coefs) const;

        void setActivationCosts(const arma::vec& activationCosts);
        void setActivationCosts(double scalarActivationCosts);

        double accumulateActivationCosts(const piano::Subset& subset) const;

    private:
        const arma::mat *X;
        const arma::vec *y;
        std::unique_ptr<arma::vec> gammaImpl;
        const arma::vec *gamma;
        arma::vec activationCosts;

        bool useArmaLeastSquaresSolver = false;

        mutable arma::mat XS;
        mutable arma::mat XX;
        mutable arma::mat G;
        mutable arma::mat GS;

        void computeGramians() const;
        void computeGramians(const Subset& subset) const;
    };
}


#endif //SUBSET_LEAST_SQUARES_LEASTSQUARES_H

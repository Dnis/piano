// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 3/6/20.
//

#ifndef SLS_MAXK_H
#define SLS_MAXK_H

#include <vector>
#include <algorithm>
#include <numeric>
#include <Types.h>

// TODO: besseres Template Programming
namespace piano::sls {
    template <size_t k, class T>
    double maxK(std::vector<T>&& vector) {
        struct less_than_key
        {
            inline bool operator() (const T& struct1, const T& struct2)
            {
                return (struct1 > struct2);
            }
        };

        std::sort(vector.begin(), vector.end(), less_than_key());
        double sum = 0;
        for (size_t i = 0; i < k; ++i) {
            sum += vector[i];
        }
        return sum;
    }

    template <size_t k, class T>
    double maxK(const std::vector<T>& vector) {
        struct less_than_key
        {
            inline bool operator() (const T& struct1, const T& struct2)
            {
                return (struct1 > struct2);
            }
        };

        std::vector<T> v(vector);
        std::sort(v.begin(), v.end(), less_than_key());
        double sum = 0;
        for (size_t i = 0; i < k; ++i) {
            sum += v[i];
        }
        return sum;
    }

    template <class T>
    double maxK(const std::vector<T>& vector, size_t k) {
        struct less_than_key
        {
            inline bool operator() (const T& struct1, const T& struct2)
            {
                return (struct1 > struct2);
            }
        };

        std::vector<T> v(vector);
        std::sort(v.begin(), v.end(), less_than_key());
        double sum = 0;
        for (size_t i = 0; i < k; ++i) {
            sum += v[i];
        }
        return sum;
    }

    template <class T>
    Subset maxKInds(T&& iter, size_t k) {
        Subset idx(iter.size());
        std::iota(idx.begin(), idx.end(), 0);

        std::sort(idx.begin(), idx.end(),
             [&iter](size_t i1, size_t i2) {return iter[i1] > iter[i2];});

        Subset ret(k);
        for (size_t i = 0; i < k; ++i) {
            ret[i] = idx[i];
        }

        return ret;
    }
}


#endif //SLS_MAXK_H

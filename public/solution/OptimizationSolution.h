// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

#ifndef PIANO_OPTIMIZATIONSOLUTION_H
#define PIANO_OPTIMIZATIONSOLUTION_H

#include <PartialSolution.h>
#include <LinearAlgebra.h>

namespace piano {
    struct OptimizationSolution : public PartialSolution {
        arma::vec coefficients;
        size_t n;
        size_t p;

        [[deprecated("Use OptimizationSolutionBuilder instead")]]
        OptimizationSolution(size_t n, size_t p, bool fullCoefficients, const arma::vec &coefficients,
                             double objectiveValue, const Subset& subset, bool provableOptimal,
                             double computationsRuntimeInSeconds,
                             double completeRuntimeInSeconds);

        [[deprecated("Use OptimizationSolutionBuilder instead")]]
        OptimizationSolution(size_t n, size_t p, bool fullCoefficients, const arma::vec &coefficients,
                             double objectiveValue, const Subset& subset, bool provableOptimal,
                             double computationsRuntimeInSeconds, double completeRuntimeInSeconds, double gap);

        [[deprecated("Use OptimizationSolutionBuilder instead")]]
        OptimizationSolution(size_t n, size_t p, bool fullCoefficients, const arma::vec &coefficients,
                             double objectiveValue, const Subset& subset, bool provableOptimal,
                             double computationsRuntimeInSeconds, double completeRuntimeInSeconds, double gap,
                             bool sanityCheck);

        [[deprecated("Use OptimizationSolutionBuilder instead")]]
        OptimizationSolution(size_t n, size_t p, bool fullCoefficients, const arma::vec &coefficients,
                             double objectiveValue, const Subset& subset, bool provableOptimal,
                             double computationsRuntimeInSeconds, double completeRuntimeInSeconds, bool sanityCheck);

        [[deprecated("Use OptimizationSolutionBuilder instead")]]
        OptimizationSolution(size_t n, size_t p, bool fullCoefficients, const arma::vec &coefficients,
                             const PartialSolution &partialSolution);

        OptimizationSolution() = default;

        [[nodiscard]] arma::vec fullCoefficients() const;
    };
}


#endif //PIANO_OPTIMIZATIONSOLUTION_H

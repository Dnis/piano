//
// Created by dennis on 01.10.21.
//

#ifndef PIANO_SOLUTIONBUILDER_H
#define PIANO_SOLUTIONBUILDER_H

#include <Solution.h>
#include <LinearAlgebra.h>
#include <Types.h>
#include <unordered_map>
#include <string>
#include <memory>
#include <PartialSolutionBuilder.h>

#define PIANO_SOLUTION_BUILDER_SETTER(builder) \
PIANO_PARTIAL_SOLUTION_BUILDER_SETTER(builder) \
builder& intercept(double d); \
builder& estimatedPredictionError(double d);


#define PIANO_SOLUTION_BUILDER_SETTER_IMPL(builder) \
PIANO_PARTIAL_SOLUTION_BUILDER_SETTER_IMPL(builder) \
PIANO_SOL_BUILDER_SETTER_IMPL(builder, intercept, double, d) \
PIANO_SOL_BUILDER_SETTER_IMPL_MUSTHAVE(builder, estimatedPredictionError, double, d)

namespace piano {
    class SolutionBuilder {
    public:
        SolutionBuilder(const arma::mat &X, const arma::vec &y, const arma::vec& gamma);

        [[nodiscard]] Solution build() const;

        SolutionBuilder &coefficients(const arma::vec &coefs, bool withZeros);

        PIANO_SOLUTION_BUILDER_SETTER(SolutionBuilder)

        SolutionBuilder &fromPartialSolution(const PartialSolution &partialSolution);

    private:
        std::unique_ptr<Solution> solution;
        const arma::mat &X;
        const arma::vec &y;
        const arma::vec &gamma;

        std::unordered_map<std::string, bool> mustHaves = {
                {"coefficients",                 false},
                {"subset",                       false},
                {"objectiveValue",               false},
                {"estimatedPredictionError",     false},
                {"completeRuntimeInSeconds",     false},
                {"computationsRuntimeInSeconds", false}
        };
    };
}

#endif //PIANO_SOLUTIONBUILDER_H

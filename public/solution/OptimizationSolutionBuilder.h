//
// Created by dennis on 04.10.21.
//

#ifndef PIANO_OPTIMIZATIONSOLUTIONBUILDER_H
#define PIANO_OPTIMIZATIONSOLUTIONBUILDER_H

#include <LinearAlgebra.h>
#include <PartialSolution.h>
#include <PartialSolutionBuilder.h>
#include "OptimizationSolution.h"
#include <memory>

#define PIANO_OPTIMIZATION_SOLUTION_BUILDER_SETTER(builder) \
PIANO_PARTIAL_SOLUTION_BUILDER_SETTER(builder)

#define PIANO_OPTIMIZATION_SOLUTION_BUILDER_SETTER_IMPL(builder) \
PIANO_PARTIAL_SOLUTION_BUILDER_SETTER_IMPL(builder)

namespace piano {
    class OptimizationSolutionBuilder {
    public:
        OptimizationSolutionBuilder(size_t n, size_t p);

        PIANO_OPTIMIZATION_SOLUTION_BUILDER_SETTER(OptimizationSolutionBuilder)

        OptimizationSolutionBuilder &coefficients(const arma::vec &coefs, bool withZeros);

        OptimizationSolutionBuilder &fromPartialSolution(const PartialSolution& partialSolution);

        OptimizationSolution build() const;

    private:
        std::unique_ptr<OptimizationSolution> solution;

        std::unordered_map<std::string, bool> mustHaves = {
                {"subset",                       false},
                {"objectiveValue",               false},
                {"completeRuntimeInSeconds",     false},
                {"computationsRuntimeInSeconds", false},
                {"coefficients",                 false}
        };
    };
}


#endif //PIANO_OPTIMIZATIONSOLUTIONBUILDER_H

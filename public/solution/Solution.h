// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 25.03.20.
//

#ifndef PIANO_SOLUTION_H
#define PIANO_SOLUTION_H

#include <LinearAlgebra.h>
#include <Types.h>
#include "PartialSolution.h"

namespace piano {
    struct Solution : public PartialSolution {
        double rss;
        arma::vec coefficients;
        size_t n;
        size_t p;
        double estimatedPredictionError;
        arma::vec gamma;
        double intercept = 0;
        double nullScore;

        [[deprecated("Use SolutionBuilder instead")]]
        Solution(const arma::mat &X, const arma::vec &y, double ridgeParameter, const Subset &subset, bool fullCoefs,
                 const arma::vec &coefs, double intercept, double objectiveValue, double estimatedPredictionError,
                 bool provableOptimal, double completeRuntimeInSeconds, double computationsRuntimeInSeconds);

        [[deprecated("Use SolutionBuilder instead")]]
        Solution(const arma::mat &X, const arma::vec &y, arma::vec gamma, const Subset &subset, bool fullCoefs,
                 const arma::vec &coefs, double intercept, double objectiveValue, double estimatedPredictionError,
                 bool provableOptimal, double completeRuntimeInSeconds, double computationsRuntimeInSeconds);

        [[deprecated("Use SolutionBuilder instead")]]
        Solution(const arma::mat &X, const arma::vec &y, double ridgeParameter, const Subset &subset, bool fullCoefs,
                 const arma::vec &coefs, double intercept, double objectiveValue, double estimatedPredictionError,
                 bool provableOptimal, double completeRuntimeInSeconds, double computationsRuntimeInSeconds,
                 bool sanityCheck);

        [[deprecated("Use SolutionBuilder instead")]]
        Solution(const arma::mat &X, const arma::vec &y, arma::vec gamma, const Subset &subset, bool fullCoefs,
                 const arma::vec &coefs, double intercept, double objectiveValue, double estimatedPredictionError,
                 bool provableOptimal, double completeRuntimeInSeconds, double computationsRuntimeInSeconds,
                 bool sanityCheck);

        [[deprecated("Use SolutionBuilder instead")]]
        Solution(const arma::mat &X, const arma::vec &y, arma::vec gamma, bool fullCoefs, const arma::vec &coefs,
                 double intercept, double estimatedPredictionError, const PartialSolution &partialSolution);

        [[deprecated("Use SolutionBuilder instead")]]
        Solution(const arma::mat &X, const arma::vec &y, double ridgeParameter, bool fullCoefs, const arma::vec &coefs,
                 double intercept, double estimatedPredictionError, const PartialSolution &partialSolution);

        Solution() = default;

        [[nodiscard]] double mse() const;

        [[nodiscard]] arma::vec fullCoefficients() const;

        [[nodiscard]] double ridgeParameter() const;

        [[nodiscard]] arma::uvec indicatorVector() const;
    };
}


#endif //PIANO_SOLUTION_H

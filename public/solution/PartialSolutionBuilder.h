//
// Created by dennis on 04.10.21.
//

#ifndef PIANO_PARTIALSOLUTIONBUILDER_H
#define PIANO_PARTIALSOLUTIONBUILDER_H

#include <PartialSolution.h>
#include <LinearAlgebra.h>
#include <memory>
#include <unordered_map>

#define PIANO_PARTIAL_SOLUTION_BUILDER_SETTER(builder) \
builder& objectiveValue(double d);\
builder& subset(const Subset& subset);\
builder& provableOptimal(bool b);\
builder& computationsRuntimeInSeconds(double d);\
builder& completeRuntimeInSeconds(double d);\
builder& gap(double d);\
builder& sanity(bool b);\
builder& reachedTimelimit(bool b);\
builder& reachedIterationCount(bool b);

#define PIANO_SOL_BUILDER_SETTER_IMPL(builder, var, type, param) piano::builder& piano::builder::var(type param) { \
            solution->var = param; \
            return *this; \
        }

#define PIANO_SOL_BUILDER_SETTER_IMPL_MUSTHAVE(builder, var, type, param) piano::builder& piano::builder::var(type param) { \
            solution->var = param;                                                                                   \
            mustHaves[#var] = true;\
            return *this; \
        }

#define PIANO_PARTIAL_SOLUTION_BUILDER_SETTER_IMPL(builder) \
PIANO_SOL_BUILDER_SETTER_IMPL_MUSTHAVE(builder, objectiveValue, double, d) \
\
piano::builder& piano::builder::subset(const Subset& s) {   \
            solution->subset = s;                   \
            solution->subsetSize = s.size();\
            mustHaves["subset"] = true;\
            return *this; \
        }                                                   \
                                                            \
PIANO_SOL_BUILDER_SETTER_IMPL(builder, provableOptimal, bool, b) \
PIANO_SOL_BUILDER_SETTER_IMPL_MUSTHAVE(builder, completeRuntimeInSeconds, double, d) \
PIANO_SOL_BUILDER_SETTER_IMPL_MUSTHAVE(builder, computationsRuntimeInSeconds, double, d) \
PIANO_SOL_BUILDER_SETTER_IMPL(builder, gap, double, d) \
PIANO_SOL_BUILDER_SETTER_IMPL(builder, sanity, bool, b) \
PIANO_SOL_BUILDER_SETTER_IMPL(builder, reachedTimelimit, bool, b) \
PIANO_SOL_BUILDER_SETTER_IMPL(builder, reachedIterationCount, bool, b)

namespace piano {
    class PartialSolutionBuilder {
    public:
        PartialSolutionBuilder();

        PIANO_PARTIAL_SOLUTION_BUILDER_SETTER(PartialSolutionBuilder)

        PartialSolution build() const;

    private:
        std::unique_ptr<PartialSolution> solution;

        std::unordered_map<std::string, bool> mustHaves = {
                {"subset",                       false},
                {"objectiveValue",               false},
                {"completeRuntimeInSeconds",     false},
                {"computationsRuntimeInSeconds", false}
        };

    };

    void assertMustHaves(const std::unordered_map<std::string, bool> &mustHaves);
}


#endif //PIANO_PARTIALSOLUTIONBUILDER_H

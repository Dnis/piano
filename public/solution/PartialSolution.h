// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 11.09.20.
//

#ifndef PIANO_PARTIALSOLUTION_H
#define PIANO_PARTIALSOLUTION_H

#include <Types.h>

namespace piano {
    struct PartialSolution {
        double objectiveValue;
        Subset subset;
        size_t subsetSize;
        bool provableOptimal = false;
        double computationsRuntimeInSeconds;
        double completeRuntimeInSeconds;
        double gap = -1;
        bool sanity = true;
        bool reachedTimelimit = false;
        bool reachedIterationCount = false;

        PartialSolution() = default;

        [[deprecated("Use PartialSolutionBuilder instead")]]
        PartialSolution(double objectiveValue, Subset subset, bool provableOptimal, double computationsRuntimeInSeconds,
                        double completeRuntimeInSeconds);

        [[deprecated("Use PartialSolutionBuilder instead")]]
        PartialSolution(double objectiveValue, Subset subset, bool provableOptimal,
                        double computationsRuntimeInSeconds, double completeRuntimeInSeconds, double gap);

        [[deprecated("Use PartialSolutionBuilder instead")]]
        PartialSolution(double objectiveValue, Subset subset, bool provableOptimal,
                        double computationsRuntimeInSeconds, double completeRuntimeInSeconds, double gap,
                        bool sanityCheck);

        [[deprecated("Use PartialSolutionBuilder instead")]]
        PartialSolution(double objectiveValue, Subset subset, bool provableOptimal,
                        double computationsRuntimeInSeconds, double completeRuntimeInSeconds, bool sanityCheck);
    };
}


#endif //PIANO_PARTIALSOLUTION_H

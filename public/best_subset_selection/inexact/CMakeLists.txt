add_subdirectory(padm)
add_subdirectory(no_warmstart)
add_subdirectory(max_min)

message(STATUS "Test Test")
add_library(bss_inexact INTERFACE)
add_library(bss::inexact ALIAS bss_inexact)

set_property(TARGET bss_inexact PROPERTY EXPORT_NAME bss::inexact)

target_link_libraries(bss_inexact
        INTERFACE
        bss::inexact::padm
        bss::inexact::maxmin)
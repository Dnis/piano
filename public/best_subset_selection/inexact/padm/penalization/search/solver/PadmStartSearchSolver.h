// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 25.09.20.
//

#ifndef PIANO_PADMSTARTSEARCHSOLVER_H
#define PIANO_PADMSTARTSEARCHSOLVER_H

// First choice (mlpack)
#ifdef PIANO_USE_PICASSO
#include <PadmStartParameterSearchPICASSOLasso.h>

#ifndef PIANO_DEFAULT_ADM_PARAMETER_SEARCH
#pragma message("Setting default PADM parameter search solver to PICASSO.")
namespace piano::bss::padm::penalization::search {
    using DefaultSearch = piano::bss::padm::penalization::search::picasso::CoordinateDescentSearch;
}
#define PIANO_DEFAULT_ADM_PARAMETER_SEARCH
#endif
#endif

// Second choice (mlpack)
#ifdef PIANO_USE_MLPACK
#include <PadmStartParameterSearchMLPACKLasso.h>

#ifndef PIANO_DEFAULT_ADM_PARAMETER_SEARCH
#pragma message("Setting default PADM parameter search solver to MLPACK.")
namespace piano::bss::padm::penalization::search {
    using DefaultSearch = piano::bss::padm::penalization::search::mlpack::LARSSearch;
}
#define PIANO_DEFAULT_ADM_PARAMETER_SEARCH
#endif
#endif

// Third choice default (internal)
#include <PadmStartParameterSearchInternalLasso.h>
#include <NoPadmStartParameterSearch.h>
#ifndef PIANO_DEFAULT_ADM_PARAMETER_SEARCH
#pragma message("Setting default PADM parameter search solver to internal lasso solver.")
#warning "The internal lasso solver is not recommended right now. Better performance will be achieved with the PICASSO library or the MLPACK library."
namespace internal::bss::padm::penalization::search {
    using DefaultSearch = piano::bss::padm::penalization::search::internal::CoordinateDescentSearch;
}
#endif

#endif //PIANO_PADMSTARTSEARCHSOLVER_H

// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 29.04.20.
//

#ifndef PIANO_PADML1_H
#define PIANO_PADML1_H

#include <LinearAlgebra.h>
#include <Solution.h>
#include <TimeMeasurement.h>
#include <Maxk.h>
#include <SquaredL2.h>
#include <CrossValidation.h>
#include <LeastSquares.h>
#include <TrainingFunctions.h>
#include <Lasso.h>
#include <BssPadmSkeleton.h>

namespace piano::bss::padm::variant {
    template<class LassoSolver = piano::cvbss::Lasso<>>
    class L1Optimizer {
    public:
        L1Optimizer(const arma::mat *X, const arma::vec *y) : X(X), y(y), lassoSolver(*X, *y) {}

        arma::vec optimize(const arma::vec &beta2, double penalization) {
            arma::vec response = -(*X) * beta2 + (*y);
            lassoSolver.setY(response);
            lassoSolver.setLambda(penalization);
            auto sol = lassoSolver.computePartialSolution();
            objectiveValue = sol.objectiveValue;
            return lassoSolver.getBeta() + beta2;
        }

        double objectiveValue = -1;

    private:
        const arma::mat *X;
        const arma::vec *y;
        LassoSolver lassoSolver;

    };

    template<class StartParameterSearch, class PenaltySeries>
    using L1 = PadmSkeleton<StartParameterSearch, PenaltySeries, L1Optimizer<>>;
}

#endif //PIANO_PADML1_H

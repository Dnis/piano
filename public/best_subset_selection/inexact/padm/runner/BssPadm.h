// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

#ifndef PIANO_BSSPADM_H
#define PIANO_BSSPADM_H

#include <PadmL1.h>
#include <DoublingPenalty.h>
#include <BinarySearchPenalty.h>
#include <PredictivePenalty.h>
#include <PadmL2.h>
#include <PadmStartParameterSearch.h>
#include <PadmStartSearchSolver.h>
#include <omp.h>
#include <HandingOver.h>
#include <CrossValidation.h>
#include <BssPadmSkeleton.h>
#include <TypeConversion.h>
#include <SolutionBuilder.h>
#include <MultiThreading.h>

namespace piano::bss {
    /*!
     * Class which implements the PADM from [this article](http://www.optimization-online.org/DB_HTML/2020/11/8124.html).
     * @tparam Variant Can be `padm::variant::L1` or `padm::variant::L2`.
     */
    template<class Variant = padm::variant::L1<padm::StartParameterSearch<>, padm::penalty_series::DoublingPenalty>>
    class Padm : Variant {
        using VariantType = Variant;
        using ThisType = Padm<Variant>;

    public:
        using TrainingFunction = cv::SubsetTraining;

        [[nodiscard]] TrainingFunction getTrainingFunction() const {
            TrainingFunction trainingFunction;
            trainingFunction.gamma = gamma;
            return trainingFunction;
        }

        // using HyperParamTuner = typename VariantType::HyperParamTuner;

        /*[[nodiscard]] HyperParamTuner getHyperParameterTuner() {
            return VariantType::getHyperParameterTuner();
        }*/

        Padm(const arma::mat &X, const arma::vec &y, const arma::vec &gamma) :
                X(&X),
                y(&y),
                gamma(&gamma) {
            piano::handOver(X, y, gamma, &XAlloc, &yAlloc, &(this->newXPtr), &(this->newYPtr));
            VariantType::setXy(this->newXPtr, this->newYPtr);

            setNumberOfThreads(1);
        }

        [[nodiscard]] Solution run(size_t k) {
            // TODO: Move condition into PadmSkeleton.h
            TimeMeasurement measurement;
            measurement.start();
            if (k < X->n_cols) {
                auto res = VariantType::run(k);
                TrainingFunction training = getTrainingFunction();
                double estimatedPredictionError = piano::cv::kFoldCrossValidation(*X, *y, 10, res.subset, cv::mse,
                                                                                  training);
                piano::sls::LeastSquares leastSquares(X, y, gamma);
                arma::vec coefs = leastSquares.computeCoefs(res.subset);

                SolutionBuilder builder(*X, *y, *gamma);
                builder.
                        fromPartialSolution(res).
                        estimatedPredictionError(estimatedPredictionError).
                        coefficients(coefs, false);

                measurement.end();
                builder.completeRuntimeInSeconds(measurement.getDurationInSeconds());

                Solution sol = builder.build();
                return sol;
            } else {
                piano::sls::LeastSquares leastSquares(X, y, gamma);
                Solution sol = leastSquares.compute();
                measurement.end();
                sol.completeRuntimeInSeconds = measurement.getDurationInSeconds();
                return sol;
            }
        }

        void setStartPenalization(double p) {
            VariantType::startPenalization = p;
        }

        void setMaxIterations(size_t n) {
            VariantType::maxIterations = n;
        }

        void setTimelimit(size_t t) {
            VariantType::timelimit = t;
        }

        void setTolerance(double tol) {
            VariantType::tolerance = tol;
        }

        void setRefit(bool b) {
            VariantType::refit = b;
        }

        void mute(bool b) {
            VariantType::setMute(b);
        }

        void doStartParameterSearch(bool b) {
            VariantType::searchStartParameter = b;
        }

        void setStartParameterCorrection(double d) {
            VariantType::setStartPenalizationCorrection(d);
        }

        void setNumberOfThreads(size_t n) {
            // TODO: implement setNumberOfThreads in subroutines
            assertCastable<int>(n, "n", "number of threads");
            omp_set_num_threads(static_cast<int>(n));
            MultiThreading::setNumberOfThreads(n);
        }

        class HyperParamTuner {
            friend class Padm<Variant>;

        public:
            void setGrid(const std::vector<size_t> &ks) {
                grid = ks;
            }

            void setNumberOfThreads(size_t n) {
                numberOfThreads = n;
            }

            void mute(bool b) {
                isMuted = b;
            }

            [[nodiscard]] Solution run() {
                padm->searchStartParameter = true;
                padm->startParameterSearch->mute(true);
                padm->mute(true);

                std::vector<PadmGridElem> padmGrid(grid.size());
                for (size_t i = 0; i < grid.size(); ++i) {
                    padmGrid[i].k = grid[i];
                    padmGrid[i].padm = padm;
                }

                PadmRun padmRun;
                padmRun.isMuted = isMuted;
                PadmGridElem bestGridElem = argGridSearch<std::vector<PadmGridElem> &, PadmRun &, PadmGridElem &>(
                        numberOfThreads, padmGrid, padmRun);

                return bestGridElem.sol;
            }

        private:
            explicit HyperParamTuner(ThisType *padm)
                    : padm(padm) {
                std::vector<size_t> g(padm->X->n_cols);
                std::iota(g.begin(), g.end(), 1);
                setGrid(g);
            }

            std::vector<size_t> grid;
            ThisType *padm;
            size_t numberOfThreads = 1;
            bool isMuted = false;

            struct PadmGridElem {
                Solution sol;
                size_t k;
                ThisType *padm;
            };

            struct PadmRun {
                bool isMuted;

                double operator()(PadmGridElem &grid) {
                    grid.sol = grid.padm->run(grid.k);
                    if (!isMuted) std::cout << "-- k = " << grid.k << " finished." << std::endl;
                    return grid.sol.estimatedPredictionError;
                }
            };
        };

        [[nodiscard]] HyperParamTuner getHyperParameterTuner() {
            return HyperParamTuner(this);
        }

    private:
        std::unique_ptr<arma::mat> XAlloc;
        std::unique_ptr<arma::vec> yAlloc;
        const arma::mat *newXPtr;
        const arma::vec *newYPtr;
        const arma::mat *X;
        const arma::vec *y;
        const arma::vec *gamma;
    };
}

#endif //PIANO_BSSPADM_H

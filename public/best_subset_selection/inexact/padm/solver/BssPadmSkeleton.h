// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 01.05.20.
//

#ifndef PIANO_BSSPADMSKELETON_H
#define PIANO_BSSPADMSKELETON_H

#include <cstdio>
#include <LinearAlgebra.h>
#include <PartialSolution.h>
#include <TimeMeasurement.h>
#include <Maxk.h>
#include <SquaredL2.h>
#include <CrossValidation.h>
#include <LeastSquares.h>
#include <TrainingFunctions.h>
#include <Conversion.h>
#include <algorithm>
#include <GridSearch.h>
#include <PartialSolutionBuilder.h>

namespace piano::bss::padm::variant {
    template<class StartParameterSearch, class PenaltySeries, class FirstDirectionOptimizer>
    class PadmSkeleton {
    protected:
        std::unique_ptr<StartParameterSearch> startParameterSearch;

        void setXy(const arma::mat *X, const arma::vec *y) {
            this->X = X;
            this->y = y;

            startParameterSearch = std::make_unique<StartParameterSearch>(X, y);
        }

        void setMute(bool b) {
            mute = b;
            if (startParameterSearch) startParameterSearch->mute(b);
        }

        void setStartPenalizationCorrection(double d) {
            startParameterSearch->penalizationCorrection = d;
        }

    private:
        const arma::mat *X;
        const arma::vec *y;


    protected:
        double startPenalization = 1e-5;
        size_t maxIterations = 1000;
        size_t timelimit = 60;
        double tolerance = 1e-8;
        bool refit = true;
        bool mute = false;
        bool searchStartParameter = true;

    protected:
        [[nodiscard]] arma::vec
        optimizeDirection1(FirstDirectionOptimizer *firstDirectionOptimizer, const arma::vec &beta2,
                           double &penalization) const {
            return firstDirectionOptimizer->optimize(beta2, penalization);
        }

        [[nodiscard]] arma::vec optimizeDirection2(std::size_t k, const arma::vec &newBeta1) const {
            arma::vec absBeta1 = arma::abs(newBeta1);
            auto inds = piano::sls::maxKInds(absBeta1, k);
            arma::vec newBeta2 = arma::zeros(X->n_cols);
            for (const auto &item: inds) {
                newBeta2[item] = newBeta1[item];
            }
            return newBeta2;
        }

        [[nodiscard]] bool isPartialMinimum(const arma::vec &beta1, const arma::vec &beta2, const arma::vec &newBeta1,
                                            const arma::vec &newBeta2,
                                            double *diff) const {
            double diff1 = arma::norm(newBeta1 - beta1, "inf");
            double diff2 = arma::norm(newBeta2 - beta2, "inf");
            *diff = std::max(diff1, diff2);
            return ((diff1 < tolerance) && (diff2 < tolerance));
        }

        [[nodiscard]] bool
        couplingConditionSatisfied(const arma::vec &beta1, const arma::vec &beta2,
                                   double *error) const {
            *error = arma::norm(beta1 - beta2, "inf");
            return *error < tolerance;
        }

        void printHeader() const {
            if (!mute) {
                std::printf(
                        "| Outer iter. | Inner iter. |  Penalization  |   Beta delta   | Nonzeros | Coupling error |\n");
            }
        }

        void
        printIterationInfo(double &penalization, size_t outerIter, size_t innerIter, double betaDelta, size_t nonzeros,
                           double couplingError) const {
            if (!mute) {
                if (outerIter == 0) {
                    std::printf("| %*zu | %*zu | %14.10f | %14.10f | %*zu |       ?        |\n", 11, outerIter, 11,
                                innerIter,
                                penalization, betaDelta, 8, nonzeros);
                } else {
                    std::printf("| %*zu | %*zu | %14.10f | %14.10f | %*zu | %14.10f |\n", 11, outerIter, 11,
                                innerIter,
                                penalization,
                                betaDelta, 8, nonzeros, couplingError);

                }
            }
        }

        PartialSolution run(std::size_t k) {
            TimeMeasurement measurement;
            PenaltySeries penaltySeries(X, y, startPenalization, k);
            FirstDirectionOptimizer firstDirectionOptimizer(X, y);
            arma::vec beta1;
            arma::vec beta2;
            double penalization;

            beta1 = arma::zeros(X->n_cols);
            beta2 = arma::zeros(X->n_cols);
            std::size_t nonzeros = X->n_cols;
            double couplingError = -1;

            measurement.start();
            penalization = penaltySeries.getPenalization(beta1, beta2, 0, nonzeros);

            printHeader();

            if (searchStartParameter) {
                penaltySeries = PenaltySeries(X, y, startParameterSearch->search(k), k);
                penalization = penaltySeries.getPenalization(beta1, beta2, 0, nonzeros);
            }

            bool reachedTimelimit = false;
            bool reachedIterationCount = false;
            bool sanity = true;
            size_t outerIteration = 0;
            for (size_t i = 0; maxIterations == 0 || i < maxIterations; ++i) {
                if ((timelimit != 0) && (measurement.getMidPointInSeconds() >= timelimit)) {
                    reachedTimelimit = true;
                    break;
                }
                auto newBeta1 = optimizeDirection1(&firstDirectionOptimizer, beta2, penalization);
                nonzeros = piano::stools::numberOfNonZeros(newBeta1, tolerance);
                if (nonzeros < k) {
                    std::cerr << "Warning: PADM penalization too high. First direction solution has " << nonzeros
                              << " nonzeros (k = " << k << "). Solution will not be optimal.\n";
                    sanity = false;
                }
                auto newBeta2 = optimizeDirection2(k, newBeta1);

                double diff;
                if (isPartialMinimum(beta1, beta2, newBeta1, newBeta2, &diff)) {
                    beta1 = newBeta1;
                    beta2 = newBeta2;
                    if (!couplingConditionSatisfied(beta1, beta2, &couplingError)) {
                        ++outerIteration;
                        penalization = penaltySeries.getPenalization(beta1, beta2, outerIteration, nonzeros);
                    } else {
                        printIterationInfo(penalization, outerIteration, i, diff, nonzeros, couplingError);
                        break;
                    }
                } else {
                    beta1 = newBeta1;
                    beta2 = newBeta2;
                }

                printIterationInfo(penalization, outerIteration, i, diff, nonzeros, couplingError);
                if (i == maxIterations - 1) reachedIterationCount = true;
            }
            measurement.end();
            auto subset = piano::stools::fromCoefs(beta2);

            // Sanity check
            if (subset.size() != k) sanity = false;
            if (piano::stools::numberOfNonZeros(beta1, tolerance) != k) sanity = false;
            if (piano::stools::numberOfNonZeros(beta2, tolerance) != k) sanity = false;
            if (!couplingConditionSatisfied(beta1, beta2, &couplingError)) sanity = false;

            PartialSolutionBuilder builder;
            builder.
                    objectiveValue(firstDirectionOptimizer.objectiveValue).
                    subset(subset).
                    provableOptimal(false).
                    computationsRuntimeInSeconds(measurement.getDurationInSeconds()).
                    completeRuntimeInSeconds(-1).
                    sanity(sanity).
                    reachedTimelimit(reachedTimelimit).
                    reachedIterationCount(reachedIterationCount);

            PartialSolution sol = builder.build();

            return sol;
        }
    };
}

#endif //PIANO_BSSPADMSKELETON_H

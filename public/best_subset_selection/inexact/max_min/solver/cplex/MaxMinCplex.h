// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 31.08.20.
//

#ifdef PIANO_USE_CPLEX

#ifndef PIANO_MAXMINCPLEX_H
#define PIANO_MAXMINCPLEX_H

#include <LinearAlgebra.h>
#include <Solution.h>
#include <CplexHelper.h>

namespace piano::bss::max_min::solver {
    class Cplex {
    public:
        Cplex();

        void setData(const arma::mat *X, const arma::vec *y, double ridgeScalar);

        void buildModel(std::size_t k);

        [[nodiscard]] PartialSolution solve(size_t k);

        void setupVariables();

        void setupObjectiveFunction(std::size_t k);

        void setupConstraints();

        void setupCplex();

        void setNumericalScalar(double numericalScalar);


        const arma::mat *X;
        const arma::vec *y;
        double ridgeScalar;

        size_t n, p;

        size_t numberOfThreads = 0;

        size_t timelimit = 360;

        bool mute = false;

    private:
        constexpr static auto delIloEnv = cplex::delIloObject<IloEnv>;
        std::unique_ptr<IloEnv, decltype(delIloEnv)> env;

        constexpr static auto delIloModel = cplex::delIloObject<IloModel>;
        std::unique_ptr<IloModel, decltype(delIloModel)> model;

        std::unique_ptr<IloNumVarArray> alpha;
        std::unique_ptr<IloNumVarArray> u;
        std::unique_ptr<IloNumVarArray> d;
        std::unique_ptr<IloNumVar> t;
        std::unique_ptr<IloNumVar> dBound;

        constexpr static auto delIloCplex = cplex::delIloObject<IloCplex>;
        std::unique_ptr<IloCplex, decltype(delIloCplex)> cplex;

        double numericalScalar;
    };
}


#endif //PIANO_MAXMINCPLEX_H
#endif
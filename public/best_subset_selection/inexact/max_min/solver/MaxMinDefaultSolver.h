//
// Created by Dennis Kreber on 25.12.21.
//

#ifndef PIANO_MAXMINDEFAULTSOLVER_H
#define PIANO_MAXMINDEFAULTSOLVER_H

#include <cplex/MaxMinCplex.h>

namespace piano::bss::max_min::solver {
#ifdef PIANO_MIP_SOLVER_AVAILABLE
    using DefaultSolver = Cplex;
#endif
}

#endif //PIANO_MAXMINDEFAULTSOLVER_H

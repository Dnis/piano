// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 31.08.20.
//

#ifdef PIANO_MIP_SOLVER_AVAILABLE

#ifndef PIANO_MAXMIN_H
#define PIANO_MAXMIN_H

#include <CrossValidation.h>
#include <TimeMeasurement.h>
#include <HandingOver.h>
#include <MaxMinDefaultSolver.h>
#include <SolutionBuilder.h>
#include <BssBase.h>
#include <TrainingFunctions.h>
#include <memory.h>

namespace piano::bss {
    template<class Solver=piano::bss::max_min::solver::DefaultSolver>
    class MaxMin : public Base<cv::SubsetTraining> {
    public:
        MaxMin(const arma::mat &X, const arma::vec &y, const arma::vec &gamma) :
                Base<cv::SubsetTraining>(X, y, gamma, true) {
            solver = std::make_unique<Solver>();
            solver->setData(this->X, this->y, ridgeParameter);
        }

        using TrainingFunction = piano::cv::SubsetTraining;

        [[nodiscard]] TrainingFunction getTraining() const override {
            TrainingFunction trainingFunction;
            trainingFunction.gamma = &originalGamma;
            return trainingFunction;
        }

        PartialSolution computePartialSolution(size_t k) override {
            solver->timelimit = timelimitInSeconds;
            solver->mute = isMuted;
            solver->numberOfThreads = numberOfThreads;
            solver->buildModel(k);
            return solver->solve(k);
        }

    private:
        std::unique_ptr<Solver> solver;
    };
}

#endif //PIANO_MAXMIN_H
#endif
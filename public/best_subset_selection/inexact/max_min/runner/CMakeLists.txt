add_library(bss_inexact_maxmin_Runner INTERFACE)
add_library(bss::inexact::maxmin::Runner ALIAS bss_inexact_maxmin_Runner)

target_include_directories(bss_inexact_maxmin_Runner INTERFACE
        $<INSTALL_INTERFACE:include/piano/best_subset_selection/inexact/max_min/runner>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

set_property(TARGET bss_inexact_maxmin_Runner PROPERTY EXPORT_NAME bss::inexact::maxmin::Runner)

target_link_libraries(bss_inexact_maxmin_Runner
        INTERFACE
        LinearAlgebra
        Solution
        bss::inexact::maxmin::Solver
        TimeMeasurement
        cv::CrossValidation
        DataTransform
        bss_Base)
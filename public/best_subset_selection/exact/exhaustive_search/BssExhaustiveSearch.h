// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

#ifndef PIANO_BSSEXHAUSTIVESEARCH_H
#define PIANO_BSSEXHAUSTIVESEARCH_H

#include <LinearAlgebra.h>
#include <Solution.h>
#include <TrainingFunctions.h>
#include <BssBase.h>

namespace piano::bss {
    class ExhaustiveSearch : public Base<cv::SubsetTraining> {
    public:
        ExhaustiveSearch(const arma::mat *X, const arma::vec *y, const arma::vec *gamma);

        PartialSolution computePartialSolution(size_t k) override;

        using TrainingFunction = cv::SubsetTraining;

        [[nodiscard]] TrainingFunction getTraining() const override;

        void setTimelimit([[maybe_unused]] size_t timelimitInSeconds) override;

        void setIterationLimit([[maybe_unused]] size_t iterationLimit) override;
    };
}


#endif //PIANO_BSSEXHAUSTIVESEARCH_H

// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 02.05.20.
//

#ifdef PIANO_MIP_SOLVER_AVAILABLE

#ifndef PIANO_BSSMIPRUNNER_H
#define PIANO_BSSMIPRUNNER_H

#include <BssMipDefaultSolver.h>
#include <BssPadm.h>
#include <MultiThreading.h>
#include <SolutionBuilder.h>
#include <BssBase.h>
#include <TrainingFunctions.h>
#include <memory>

namespace piano::bss {
    /**
     * Class for solving the best subset selection via a MIP solver.
     * @tparam Warmstart Any class which implements `piano::Solution run(size_t k)`.
     * @tparam Solver
     */
    template<
            class Warmstart = Padm<>,
            class Solver = bss::mip::solver::DefaultSolver
    >
    class Mip : public Base<cv::SubsetTraining> {
    public:
        /**
         * Constructor
         * @param X Design matrix
         * @param y Response vector
         * @param gamma Ridge parameter
         */
        Mip(const arma::mat &X, const arma::vec &y, const arma::vec &gamma) :
                Base<cv::SubsetTraining>(X, y, gamma) {
            solver = std::make_unique<Solver>(&originalX, &originalY, &originalGamma);
        }

        using TrainingFunction = cv::SubsetTraining;

        [[nodiscard]] TrainingFunction getTraining() const override {
            TrainingFunction trainingFunction;
            trainingFunction.gamma = &originalGamma;
            return trainingFunction;
        }

        PartialSolution computePartialSolution(size_t k) override {
            solver->setNumberOfThreads(numberOfThreads);
            solver->timeLimit = timelimitInSeconds;
            solver->isMuted = isMuted;
            solver->iterationLimit = iterationLimit;
            solver->template runAndRegisterWarmstart<Warmstart>(k);
            solver->setNumericalScaling();
            solver->buildModel(k);
            return solver->solve();
        }

        void addWarmstartSolution(const Solution &sol) {
            solver->warmstartSolutions.push_back(sol);
        }

        void addWarmstartSolution(Solution &&sol) {
            solver->warmstartSolutions.push_back(std::move(sol));
        }

        /*!
         * Saves the solver model if a file path is specified.
         * @param fileName Valid file or empty string if no model should be saved.
         * @return Reference to itself.
         */
        void exportModel(const std::string &fileName) {
            if (fileName.empty()) {
                solver->exportModel = false;
            } else {
                solver->exportModel = true;
                solver->exportFile = fileName;
            }
        }

    private:
        std::unique_ptr<Solver> solver;
    };
}

#endif //PIANO_BSSMIPRUNNER_H
#endif
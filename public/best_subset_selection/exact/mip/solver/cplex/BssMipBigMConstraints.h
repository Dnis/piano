// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 02.05.20.
//

#ifdef PIANO_USE_CPLEX

#ifndef PIANO_BSSMIPBIGM_H
#define PIANO_BSSMIPBIGM_H

#include <LinearAlgebra.h>
#include <MultiThreadSkeleton.h>
#include <CoefficientBounds.h>
#include <MultiThreading.h>
#include <exception>
#include <TypeConversion.h>
#include <ilcplex/ilocplex.h>
#include <ilcplex/ilocplexi.h>

namespace piano::bss::mip::indicator_constraints::cplex {
    template<class CoefficientBounds = piano::sls::CoefficientBounds<>, class UpperPredictedValueBound = piano::sls::upper_bound::predicted_value::DualCauchyBound>
    class BigMConstraints : public piano::MultiThreadSkeleton {

    protected:
        BigMConstraints(const arma::mat *X, const arma::vec *y, const arma::vec *gamma) :
                X(X),
                y(y),
                gamma(gamma) {

        }

        void buildConstraints(IloEnv &env, IloModel &model, const IloNumVarArray &beta,
                              const IloBoolVarArray &z, size_t k) const {
            size_t p = X->n_cols;

            CoefficientBounds coefficientBounds(X, y, gamma, p);
            coefficientBounds.setNumberOfThreads(MultiThreading::handOverNumberOfThreads(numberOfThreads));
            /*piano::sls::BoundPrerequisites prerequisites(X, y, gamma, p);
            UpperPredictedValueBound upperPredictedValueBound(&prerequisites);
            upperPredictedValueBound.setNumberOfThreads(numberOfThreads);*/

            assertCastable<long>(p, "p", "number of variables");
            for (long i = 0; i < static_cast<long>(p); i++) {
                double L = coefficientBounds.bound(i);
                model.add(beta[i] <= L * z[i]);
                model.add(beta[i] >= -L * z[i]);
            }

            IloExpr expr(env);
            for (long j = 0; j < static_cast<long>(p); ++j) {
                expr += z[j];
            }

            assertCastable<long>(k, "k", "maximum number of regressors");
            model.add(expr <= static_cast<long>(k));
            expr.end();
        }

    private:
        const arma::mat *X;
        const arma::vec *y;
        const arma::vec *gamma;
    };
}

#endif //PIANO_BSSMIPBIGM_H
#endif
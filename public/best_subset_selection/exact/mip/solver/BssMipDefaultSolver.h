//
// Created by Dennis Kreber on 25.12.21.
//

#ifndef PIANO_BSSMIPDEFAULTSOLVER_H
#define PIANO_BSSMIPDEFAULTSOLVER_H

#include <cplex/BssMipCplex.h>

namespace piano::bss::mip::solver {
#ifdef PIANO_MIP_SOLVER_AVAILABLE
    using DefaultSolver = Cplex<>;
#endif
}
#endif //PIANO_BSSMIPDEFAULTSOLVER_H

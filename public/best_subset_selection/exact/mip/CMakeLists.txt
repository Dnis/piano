add_subdirectory(solver)
add_subdirectory(runner)

add_library(bss_exact_mip INTERFACE)
add_library(bss::exact::mip ALIAS bss_exact_mip)

set_property(TARGET bss_exact_mip PROPERTY EXPORT_NAME bss::exact::mip)

target_link_libraries(bss_exact_mip
        INTERFACE
        bss::exact::mip::Runner
        bss::exact::mip::Solver)
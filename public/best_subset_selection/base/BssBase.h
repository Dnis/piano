// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//
// Created by kreber on 13.10.21.
//

#ifndef PIANO_BSSBASE_H
#define PIANO_BSSBASE_H

#include <RunnerBase.h>
#include <TrainingFunctions.h>
#include <PartialSolution.h>
#include <Solution.h>
#include <TimeMeasurement.h>

namespace piano::bss {
    template<class Training = cv::SubsetTraining>
    class Base : public RunnerBase<Training> {
    public:
        Base(const arma::mat &X, const arma::vec &y, const arma::vec &gamma, bool extractRidgeParameter = false) :
                RunnerBase<Training>(X, y, gamma, extractRidgeParameter) {}

        Solution run(size_t k) {
            TimeMeasurement measurement;
            measurement.start();
            Training training = this->getTraining();
            PartialSolution partialSolution = computePartialSolution(k);
            Solution solution = RunnerBase<Training>::buildSolutionFromPartialSolution(partialSolution, training);
            measurement.end();
            solution.completeRuntimeInSeconds = measurement.getDurationInSeconds();
            return solution;
        }

        OptimizationSolution computeOptimizationSolution(size_t k) {
            PartialSolution partialSolution = computePartialSolution(k);
            return RunnerBase<Training>::buildOptimizationSolutionFromPartialSolution(partialSolution);
        }

        virtual PartialSolution computePartialSolution(size_t k) = 0;

        virtual ~Base() {}
    };
}

#endif //PIANO_BSSBASE_H

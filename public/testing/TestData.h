//
// Created by dennis on 04.10.21.
//

#ifndef PIANO_TESTDATA_H
#define PIANO_TESTDATA_H

#include <SyntheticData.h>

namespace piano::testdata {
    RegressionData small(size_t i = 0);
    RegressionData medium(size_t i = 0);
    RegressionData large(size_t i = 0);
    RegressionData huge(size_t i = 0);
    RegressionData humongous(size_t i = 0);
}


#endif //PIANO_TESTDATA_H

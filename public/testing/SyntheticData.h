//
// Created by dennis on 16.07.21.
//

#ifndef PIANO_SYNTHETICDATA_H
#define PIANO_SYNTHETICDATA_H

#include <LinearAlgebra.h>
#include <cstdlib>
#include <cmath>
#include <Types.h>
#include <EyeCovariance.h>
#include <UniformCoefficients.h>

namespace piano {
    struct RegressionData {
        arma::mat X;
        arma::vec y;
        arma::vec beta;
        Subset subset;
        size_t number_of_nonzeros;
        arma::vec noise;
    };

    template<class Covariance = EyeCovariance, class Coefficients = UniformCoefficients>
    class SyntheticDataGenerator {
    public:
        SyntheticDataGenerator(size_t n, size_t p, size_t k) : n(n), p(p), k(k),
                                                               covariance(p),
                                                               coefficients(p, k) {
        }

        RegressionData draw() {
            RegressionData regressionData;

            typename Covariance::matrix_type covariance_matrix = covariance.generate();
            arma::vec mean = arma::zeros(p);
            regressionData.X = arma::mvnrnd(mean, covariance_matrix, n).t();
            auto drawn_coefficients = coefficients.draw();
            auto beta = drawn_coefficients.beta;
            regressionData.subset = drawn_coefficients.subset;
            regressionData.beta = beta;
            regressionData.number_of_nonzeros = k;

            double error_variance = arma::as_scalar(arma::dot(beta, covariance_matrix * beta)) / snr;
            arma::vec noise = arma::randn(n) * std::pow(error_variance, 0.5);
            regressionData.noise = noise;
            regressionData.y = regressionData.X * beta + noise;

            return regressionData;
        }

        void setSeed(size_t seed) {
            coefficients.setSeed(seed);
            arma::arma_rng::set_seed(seed);
        }

        double snr = 1.0;

    private:
        size_t n, p, k;

    public:
        Covariance covariance;
        Coefficients coefficients;
    };
}

#endif //PIANO_SYNTHETICDATA_H

//
// Created by dennis on 20.10.21.
//

#ifndef PIANO_PIANOASSERT_H
#define PIANO_PIANOASSERT_H

#include <Types.h>

namespace piano {
    bool isSameSubset(Subset expected, Subset actual);
}


#endif //PIANO_PIANOASSERT_H

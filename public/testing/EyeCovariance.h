//
// Created by dennis on 16.07.21.
//

#ifndef PIANO_EYECOVARIANCE_H
#define PIANO_EYECOVARIANCE_H

#include <LinearAlgebra.h>

namespace piano {
    class EyeCovariance {
    public:
        using matrix_type = arma::mat;


        explicit EyeCovariance(size_t p);

        matrix_type generate() const;

    private:
        size_t p;
    };
}


#endif //PIANO_EYECOVARIANCE_H

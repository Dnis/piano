//
// Created by dennis on 16.07.21.
//

#ifndef PIANO_UNIFORMCOEFFICIENTS_H
#define PIANO_UNIFORMCOEFFICIENTS_H

#include <LinearAlgebra.h>
#include <Types.h>

namespace piano {
    struct DrawnCoefficients {
        arma::vec beta;
        Subset subset;
    };

    class UniformCoefficients {
    public:
        UniformCoefficients(size_t p, size_t k);

        [[nodiscard]] DrawnCoefficients draw() const;

        void setSeed(size_t seed);

        void setBounds(double lower, double upper);

        double lower_bound = 1;
        double upper_bound = 10;

    private:
        size_t p, k;
    };
}


#endif //PIANO_UNIFORMCOEFFICIENTS_H

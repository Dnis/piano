//
// Created by dennis on 16.07.21.
//

#ifndef PIANO_RHOCOVARIANCE_H
#define PIANO_RHOCOVARIANCE_H

#include <LinearAlgebra.h>

namespace piano {
    class RhoCovariance {
    public:
        using matrix_type = arma::mat;


        explicit RhoCovariance(size_t p);

        [[nodiscard]] matrix_type generate() const;

        double rho = 0.5;

    private:
        size_t p;
    };
}


#endif //PIANO_RHOCOVARIANCE_H

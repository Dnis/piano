// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 30.03.20.
//

#ifdef PIANO_USE_CPLEX

#ifndef PIANO_CPLEXHELPER_H
#define PIANO_CPLEXHELPER_H

#include <ilcplex/ilocplex.h>
#include <ilcplex/ilocplexi.h>
#include <LinearAlgebra.h>
#include <Types.h>
#include <TypeConversion.h>

namespace piano::cplex {
    template<class T, class CplexArray>
    IloExpr dot(IloEnv &env, T &&vec1, CplexArray &&vec2) {
        using CplexArrayRaw = std::remove_const_t<std::remove_reference_t<CplexArray>>;
        static_assert(std::is_same_v<CplexArrayRaw, IloNumVarArray> || std::is_same_v<CplexArrayRaw, IloBoolVarArray>,
                      "Unsupported type in the second argument (CplexArray&& vec2).");

        IloExpr expr(env);
        size_t c = 0;
        for (size_t i = 0; i < vec1.size(); ++i) {
            expr += vec1[i] * vec2[c++];

        }

        return expr;
    }

    template<class T, class CplexArray>
    IloExpr dot(IloEnv &env, T &&vec1, CplexArray &&vec2, size_t size) {
        using CplexArrayRaw = std::remove_const_t<std::remove_reference_t<CplexArray>>;
        static_assert(std::is_same_v<CplexArrayRaw, IloNumVarArray> || std::is_same_v<CplexArrayRaw, IloBoolVarArray>,
                      "Unsupported type in the second argument (CplexArray&& vec2).");

        IloExpr expr(env);
        size_t c = 0;
        for (size_t i = 0; i < size; ++i) {
            expr += vec1[i] * vec2[c++];

        }

        return expr;
    }

    template<class T>
    void delIloObject(T *obj) {
        if (obj != nullptr) obj->end();
        delete obj;
    }

    template<class CplexArray>
    IloExpr quadraticExpr(IloEnv &env, arma::mat &&mat, CplexArray &&vec) {
        using CplexArrayRaw = std::remove_const_t<std::remove_reference_t<CplexArray>>;
        static_assert(std::is_same_v<CplexArrayRaw, IloNumVarArray> || std::is_same_v<CplexArrayRaw, IloBoolVarArray>,
                      "Unsupported type in the second argument (CplexArray&& vec).");

        IloExpr expr(env);
        for (size_t i = 0; i < mat.n_rows; ++i) {
            for (size_t j = 0; j < mat.n_cols; ++j) {
                expr += mat.at(i, j) * vec[i] * vec[j];
            }
        }
        return expr;
    }

    template<class CplexArray>
    IloExpr quadraticExpr(IloEnv &env, const arma::mat& mat, CplexArray &&vec) {
        using CplexArrayRaw = std::remove_const_t<std::remove_reference_t<CplexArray>>;
        static_assert(std::is_same_v<CplexArrayRaw, IloNumVarArray> || std::is_same_v<CplexArrayRaw, IloBoolVarArray>,
                      "Unsupported type in the second argument (CplexArray&& vec).");

        IloExpr expr(env);
        for (size_t i = 0; i < mat.n_rows; ++i) {
            for (size_t j = 0; j < mat.n_cols; ++j) {
                expr += mat.at(i, j) * vec[i] * vec[j];
            }
        }
        return expr;
    }

    bool extractVar(const IloCplex &cplex, const IloBoolVar &var);

    double extractVar(const IloCplex &cplex, const IloNumVar &var);

    bool extractVar(const IloCplex &cplex, const IloBoolVar &var, size_t solutionIndex);

    double extractVar(const IloCplex &cplex, const IloNumVar &var, size_t solutionIndex);

    template<class VarArray> using OutputArray = std::conditional_t<std::is_same_v<std::remove_reference_t<std::remove_const_t<VarArray>>, IloBoolVarArray>, arma::uvec, arma::vec>;

    template<class VarArray>
    auto extractVars(const IloCplex &cplex, const VarArray &varArray) -> OutputArray<VarArray> {
        OutputArray<VarArray> ret(varArray.getSize());
        for (long i = 0; i < varArray.getSize(); ++i) {
            ret[i] = extractVar(cplex, varArray[i]);
        }
        return ret;
    }

    template<class VarArray>
    auto extractVars(const IloCplex &cplex, const VarArray &varArray, size_t solutionIndex) -> OutputArray<VarArray> {
        OutputArray<VarArray> ret(varArray.getSize());
        assertCastable<long>(solutionIndex, "solutionIndex", "ith solution");
        for (long i = 0; i < varArray.getSize(); ++i) {
            ret[i] = extractVar(cplex, varArray[i], solutionIndex);
        }
        return ret;
    }
}


#endif //PIANO_CPLEXHELPER_H
#endif
// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 07.09.20.
//

#ifndef PIANO_HANDINGOVER_H
#define PIANO_HANDINGOVER_H

#include <LinearAlgebra.h>
#include <memory>

namespace piano {
    void handOver(const arma::mat &X, const arma::vec &y, const arma::vec &gamma, std::unique_ptr<arma::mat> *XAlloc,
                  std::unique_ptr<arma::vec> *yAlloc, const arma::mat **newXPtr, const arma::vec **newYPtr,
                  double *newRidgeScalarPtr);

    void handOver(const arma::mat &X, const arma::vec &y, const arma::vec &gamma, std::unique_ptr<arma::mat> *XAlloc,
                  std::unique_ptr<arma::vec> *yAlloc, const arma::mat **newXPtr, const arma::vec **newYPtr,
                  double *newRidgeScalarPtr, double percentExtraction, size_t numberOfThreads = 1);

    void handOver(const arma::mat &X, const arma::vec &y, double ridgeScalar, std::unique_ptr<arma::mat> *XAlloc,
                  std::unique_ptr<arma::vec> *yAlloc, const arma::mat **newXPtr, const arma::vec **newYPtr,
                  double *newRidgeScalarPtr, double percentExtraction, size_t numberOfThreads = 1);

    void handOver(const arma::mat &X, const arma::vec &y, double ridgeScalar, const arma::mat **newXPtr,
                  const arma::vec **newYPtr, double *newRidgeScalarPtr);

    void handOver(const arma::mat &X, const arma::vec &y, const arma::vec &gamma, std::unique_ptr<arma::mat> *XAlloc,
                  std::unique_ptr<arma::vec> *yAlloc, const arma::mat **newXPtr, const arma::vec **newYPtr);

    double handOverAndNormaliseY(const arma::mat &X, const arma::vec &y, const arma::vec &gamma,
                                 std::unique_ptr<arma::mat> *XAlloc,
                                 std::unique_ptr<arma::vec> *yAlloc, const arma::mat **newXPtr,
                                 const arma::vec **newYPtr);
}


#endif //PIANO_HANDINGOVER_H

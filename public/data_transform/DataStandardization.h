// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 26.03.20.
//

#ifndef PIANO_DATASTANDARDIZATION_H
#define PIANO_DATASTANDARDIZATION_H

#include <LinearAlgebra.h>
#include <Solution.h>
#include <memory>

namespace piano {
    class DataStandardization {
    public:
        DataStandardization(const arma::mat& X, const arma::vec &y, double ridgeParameter, bool normalize, bool intercept);

        [[nodiscard]] double getIntercept(const arma::vec &coefs) const;
        [[nodiscard]] arma::vec recoverCoefficients(const arma::vec &coefs) const;

        [[nodiscard]] Solution recoverSolution(const Solution& sol) const;

        [[nodiscard]] const arma::mat * getX() const;
        [[nodiscard]] const arma::vec* getY() const;
        [[nodiscard]] const arma::vec* getGamma() const;

        [[nodiscard]] size_t getN() const;

        [[nodiscard]] size_t getP() const;

        [[nodiscard]] bool isInterceptApplied() const;

        [[nodiscard]] bool isNormalizeApplied() const;

        [[nodiscard]] double recoverIntercept(const arma::vec &coefs) const;

    private:
        std::unique_ptr<arma::mat> X;
        std::unique_ptr<arma::vec> y;
        std::unique_ptr<arma::vec> gamma;
        std::unique_ptr<arma::vec> normalizeCoefs;
        double normalizeCoefY = 1.0;

        size_t originalP;
        size_t n;
        size_t p;

        bool intercept;
        bool normalize;
    };
}


#endif //PIANO_DATASTANDARDIZATION_H

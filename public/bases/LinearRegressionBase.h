//
// Created by dennis on 30.09.21.
//

#ifndef PIANO_LINEARREGRESSIONBASE_H
#define PIANO_LINEARREGRESSIONBASE_H

#include <LinearAlgebra.h>
#include <Solution.h>
#include <PartialSolution.h>
#include <CrossValidation.h>
#include <LeastSquares.h>
#include <MultiThreadSkeleton.h>
#include <SolutionBuilder.h>
#include <OptimizationSolution.h>
#include <OptimizationSolutionBuilder.h>
#include <list>

namespace piano {
    class LinearRegressionBase : public MultiThreadSkeleton {
    public:
        LinearRegressionBase(const arma::mat &X, const arma::vec &y, const arma::vec &gamma,
                             bool extractRidgeParameter);

        [[nodiscard]] size_t getN() const;

        [[nodiscard]] size_t getP() const;

        virtual void linkIndicators(const std::vector<size_t>& indicatorIndices);
        virtual void clearIndicatorLinks();

    protected:
        const arma::mat &originalX;
        const arma::vec &originalY;
        const arma::vec &originalGamma;
        std::unique_ptr<arma::mat> XAlloc;
        std::unique_ptr<arma::vec> yAlloc;
        const arma::mat *X;
        const arma::vec *y;
        double ridgeParameter;

        size_t n, p;

        std::list<std::vector<size_t>> linkedIndicators{};

        const double percentExtraction = 0.9;

        template<class Training>
        Solution buildSolutionFromPartialSolution(const PartialSolution &partialSolution, Training &&training) const {
            double estimatedPredictionError = piano::cv::kFoldCrossValidation(originalX, originalY, 10,
                                                                              partialSolution.subset,
                                                                              cv::mse,
                                                                              std::forward<Training>(training));
            piano::sls::LeastSquares leastSquares(&originalX, &originalY, &originalGamma);
            arma::vec coefs = leastSquares.computeCoefs(partialSolution.subset);

            SolutionBuilder builder(originalX, originalY, originalGamma);
            builder.fromPartialSolution(partialSolution).
                    coefficients(coefs, false).
                    estimatedPredictionError(estimatedPredictionError);

            return builder.build();
        }

        [[nodiscard]] OptimizationSolution
        buildOptimizationSolutionFromPartialSolution(const PartialSolution &partialSolution) const;

        virtual ~LinearRegressionBase() {}
    };
}


#endif //PIANO_LINEARREGRESSIONBASE_H

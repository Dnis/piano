//
// Created by dennis on 07.11.21.
//

#ifndef PIANO_PRINT_H
#define PIANO_PRINT_H

#include <string>
#include <list>
#include <initializer_list>

namespace piano {
    class PrintTable {
    public:
        void addIntegerColumn(const std::string &label, size_t minSize);
        void addDoubleColumn(const std::string &label,size_t minSize, size_t decimalPlaces);

        void printHeader() const;

        template<class... Args>
        void printRow(Args... args) const {
            std::string out = "| ";
            for (const auto& format: formatStrings) {
                out += format;
                out += " | ";
            }
            out += "\n";
            std::printf(out.c_str(), args...);
        }

    private:
        std::list<std::string> formatStrings{};
        std::list<std::string> names{};
        std::list<size_t> widths{};
    };
}


#endif //PIANO_PRINT_H

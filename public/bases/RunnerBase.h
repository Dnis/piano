//
// Created by dennis on 01.10.21.
//

#ifndef PIANO_RUNNERBASE_H
#define PIANO_RUNNERBASE_H

#include <LinearRegressionBase.h>
#include <LinearAlgebra.h>
#include <numeric>
#include <NotImplementedException.h>

namespace piano {
    template<class Training>
    class RunnerBase : public LinearRegressionBase {
    public:

        RunnerBase(const arma::mat &X, const arma::vec &y, const arma::vec &gamma, bool extractRidgeParameter) :
                LinearRegressionBase(X, y, gamma, extractRidgeParameter) {}

        void mute(bool b) {
            isMuted = b;
        }

        virtual Training getTraining() const = 0;

        virtual void setTimelimit(size_t timelimitInSeconds) {
            RunnerBase::timelimitInSeconds = timelimitInSeconds;
        }

        virtual void setIterationLimit(size_t iterationLimit) {
            RunnerBase::iterationLimit = iterationLimit;
        }

        [[nodiscard]] bool hasTimelimit() const {
            return (timelimitInSeconds != 0);
        }

        [[nodiscard]] bool hasIterationLimit() const {
            return (iterationLimit != 0);
        }

        virtual ~RunnerBase() {}

    protected:
        bool isMuted = false;
        size_t timelimitInSeconds = 0;
        size_t iterationLimit = 0;
    };
}

#define PIANO_TIMELIMIT_NOT_IMPLEMENTED(class) \
throw NotImplementedException("Setting a time limit is not supported for #class")

#define PIANO_ITERATIONLIMIT_NOT_IMPLEMENTED(class) \
throw NotImplementedException("Setting an iteration limit is not supported for #class")

#endif //PIANO_RUNNERBASE_H

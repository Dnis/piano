//
// Created by dennis on 04.10.21.
//

#ifndef PIANO_NOTREADYFORBUILDEXCEPTION_H
#define PIANO_NOTREADYFORBUILDEXCEPTION_H

#include <exception>
#include <unordered_map>
#include <string>

namespace piano {
    class NotReadyForBuildException : public std::exception {
    public:
        explicit NotReadyForBuildException(const std::unordered_map<std::string, bool>& mustHaves);

        const char *what() const noexcept override;

    private:
        std::string message;
    };
}


#endif //PIANO_NOTREADYFORBUILDEXCEPTION_H

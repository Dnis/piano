//
// Created by dennis on 08.06.21.
//

#ifndef PIANO_NOSOLUTIONFOUNDEXCEPTION_H
#define PIANO_NOSOLUTIONFOUNDEXCEPTION_H

#include <exception>
#include <string>

namespace piano {
    class NoSolutionFoundException: public std::exception {
    public:
        explicit NoSolutionFoundException(const std::string&  message);

        const char * what() const noexcept override;

    private:
        std::string message;
    };
}


#endif //PIANO_NOSOLUTIONFOUNDEXCEPTION_H

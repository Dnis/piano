//
// Created by carina on 20.10.21.
//

#ifndef PIANO_DUALLOWERLEVELSOLVER_H
#define PIANO_DUALLOWERLEVELSOLVER_H

#include <LinearAlgebra.h>
#include <Types.h>

namespace piano::cvbss {
   class DualLowerLevelSolver {
   public:
      DualLowerLevelSolver(const std::vector<arma::mat> &trainingMatrices,
                           const std::vector<arma::vec> &trainingResponses,
                           const std::vector<double> &lambdas);

      std::vector<arma::vec> solve(const Subset &subset);

      size_t numberOfThreads = 1;

   private:
      const std::vector<arma::mat>& trainingMatrices;
      const std::vector<arma::vec>& trainingResponses;
      const std::vector<double> &lambdas;

      size_t numberOfPartitions;
   };
}



#endif //PIANO_DUALLOWERLEVELSOLVER_H

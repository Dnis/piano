//
// Created by carina on 20.09.21.
//

#ifdef PIANO_MIP_SOLVER_AVAILABLE

#ifndef PIANO_CVBSSPADM_H
#define PIANO_CVBSSPADM_H

#include <Partition.h>
#include <vector>
#include <TimeMeasurement.h>
#include <Solution.h>
#include <algorithm>
#include <UpperLevelSolution.h>
#include <CrossValidation.h>
#include <TrainingFunctions.h>
#include <CvbssBase.h>
#include <Conversion.h>
#include <PartialSolutionBuilder.h>
#include <UpperLevelSolver.h>
#include <DualLowerLevelSolver.h>
#include <cmath>
#include <Print.h>
#include <iostream>

namespace piano::cvbss {
    template<class UpperLevelSolver = UpperLevelSolver<>, class DualLowerLevelSolver = DualLowerLevelSolver>
    class Padm : public Base<cv::SubsetTraining> {
        using ThisType = Padm<UpperLevelSolver, DualLowerLevelSolver>;

    public:
        using TrainingFunction = cv::SubsetTraining;

        Padm(const arma::mat &X, const arma::vec &y, const arma::vec &gamma) :
                Base<cv::SubsetTraining>(X, y, gamma),
                betas(numberOfPartitions),
                alphas(numberOfPartitions),
                newBetas(numberOfPartitions),
                newAlphas(numberOfPartitions) {
            printTable.addIntegerColumn("Outer iter.", 10);
            printTable.addIntegerColumn("Inner iter.", 10);
            printTable.addDoubleColumn("Penalization", 14, 10);
            printTable.addDoubleColumn("Step delta", 14, 10);
            printTable.addIntegerColumn("Nonzeros", 8);
            printTable.addDoubleColumn("Coupling error", 14, 10);
        }

        [[nodiscard]] cv::SubsetTraining getTraining() const override {
            TrainingFunction trainingFunction;
            trainingFunction.gamma = &originalGamma;
            return trainingFunction;
        }

        PartialSolution computePartialSolution() override {
            initSolvers();
            return runPadm();
        }

        void setMaxInnerIterations(size_t it) {
            maxInnerIterationCount = it;
        }

        void setDualityErrorTolerance(double tol) {
            dualityTolerance = tol;
        }

        void setPartialMinTolerance(double tol) {
            partialMinTolerance = tol;
        }

        void setStartPenaltyParameter(double penaltyParam) {
            startPenaltyParameter = penaltyParam;
        }

        void setTimelimitForUpperLevelSolver(size_t timelimitInSeconds) {
            upperLevelSolverTimelimit = timelimitInSeconds;
        }

        void setIterationLimitForUpperLevelSolver(size_t iterationLimit) {
            upperLevelSolverIterationLimit = iterationLimit;
        }

        void setDensityPenalization(double penalization) {
            densityPenalization = penalization;
        }

    private:
        size_t maxInnerIterationCount = 1000;
        size_t maxOuterIterationCount = 1e6;

        double startPenaltyParameter = 1e-5;
        double dualityTolerance = 1e-4;
        double partialMinTolerance = 1e-6;

        std::unique_ptr<UpperLevelSolver> upperLevelSolver;
        std::unique_ptr<DualLowerLevelSolver> dualLowerLevelSolver;

        size_t upperLevelSolverTimelimit = 0;
        size_t upperLevelSolverIterationLimit = 0;
        double densityPenalization = 1e-4;

        arma::uvec zeta;
        std::vector<arma::vec> betas;
        std::vector<arma::vec> alphas;
        arma::uvec newZeta;
        std::vector<arma::vec> newBetas;
        std::vector<arma::vec> newAlphas;
        Subset subset;

        size_t outerIterationCount;
        size_t innerIterationCount;
        double penaltyParameter;
        double dualityGap;
        double subsequentVariableChange;
        bool sanity;

        PrintTable printTable{};

        void initSolvers() {
            setupPartitions();

            upperLevelSolver = std::make_unique<UpperLevelSolver>(X, y, trainingMatrices, trainingResponses,
                                                                  validationMatrices, validationResponses,
                                                                  lambdas);
            dualLowerLevelSolver = std::make_unique<DualLowerLevelSolver>(trainingMatrices, trainingResponses,
                                                                          lambdas);
            dualLowerLevelSolver->numberOfThreads = this->numberOfThreads;

            upperLevelSolver->setIterationLimit(upperLevelSolverIterationLimit);
            upperLevelSolver->setTimelimit(upperLevelSolverTimelimit);
            upperLevelSolver->setDensityPenalization(densityPenalization);
        }

        PartialSolution runPadm() {
            TimeMeasurement measurement;
            measurement.start();

            resetOptimizationVariables();
            resetIterationMetrics();

            UpperLevelSolution upperLevelSolution;

            printTable.printHeader();

            while (!isDualityGapClosed()) {
                if (isAboveMaxOuterIterationCount()) {
                    break;
                }
                innerIterationCount = 0;

                while (!isPartialMinimum()) {
                    if (isAboveMaxInnerIterationCount() || isAboveTimeLimit(measurement)) {
                        break;
                    }

                    updateOptimizationVariables();

                    // decide which problem we start with, maybe start solving upper level problem with
                    // alphas being all zero-vectors

                    upperLevelSolution = upperLevelSolver->solve(alphas, penaltyParameter);

                    newZeta = upperLevelSolution.zeta;
                    newBetas = upperLevelSolution.betas;
                    subset = piano::stools::toSubset(zeta);

                    newAlphas = dualLowerLevelSolver->solve(stools::toSubset(newZeta));

                    computeSubsequentVariableChange();

                    printTable.printRow(outerIterationCount, innerIterationCount, penaltyParameter,
                                        subsequentVariableChange,
                                        subset.size(),
                                        dualityGap);

                    ++innerIterationCount;
                }

                computeDualityGap();

                penaltyParameter = penaltyParameter * 2;
                ++outerIterationCount;
            }

            measurement.end();

            Subset bestSubset = piano::stools::toSubset(zeta);

            PartialSolutionBuilder builder;
            builder.objectiveValue(upperLevelSolution.objectiveValue).
                    subset(bestSubset).
                    provableOptimal(false).
                    computationsRuntimeInSeconds(measurement.getDurationInSeconds()).
                    completeRuntimeInSeconds(-1).
                    sanity(sanity);

            return builder.build();
        }

        void resetOptimizationVariables() {
            zeta = arma::uvec(p, arma::fill::zeros);
            newZeta = arma::uvec(p, arma::fill::zeros);
            for (size_t i = 0; i < numberOfPartitions; ++i) {
                alphas[i] = arma::zeros(trainingMatrices[i].n_rows);
                newAlphas[i] = arma::zeros(trainingMatrices[i].n_rows);
                betas[i] = arma::zeros(p);
                newBetas[i] = arma::zeros(p);
            }
        }

        void resetIterationMetrics() {
            penaltyParameter = startPenaltyParameter;
            outerIterationCount = 0;

            dualityGap = -1;
            subsequentVariableChange = std::numeric_limits<double>::max();

            sanity = true;
        }

        bool isDualityGapClosed() {
            if (dualityGap < 0) return false;
            return (dualityGap < dualityTolerance);
        }

        void computeDualityGap() {
            if (outerIterationCount == 0) {
                dualityGap = std::numeric_limits<double>::max();
            }

            std::vector<double> dualityGaps(numberOfPartitions);

            for (size_t i = 0; i < numberOfPartitions; ++i) {
                double primalObjectiveFunctionVal;
                double dualObjectiveFunctionVal;
                if (subset.size() > 0) {
                    arma::uvec indices = arma::conv_to<arma::uvec>::from(subset);
                    primalObjectiveFunctionVal = arma::as_scalar(
                            0.5 * newBetas[i](indices).t()
                            * trainingMatrices[i].cols(indices).t()
                            * trainingMatrices[i].cols(indices)
                            * newBetas[i](indices)
                            - trainingResponses[i].t() * trainingMatrices[i].cols(indices)
                              * newBetas[i](indices)
                            + 0.5 * trainingResponses[i].t() * trainingResponses[i]
                            + (1.0 / (2.0 * lambdas[i])) * newBetas[i](indices).t()
                              * newBetas[i](indices));

                    dualObjectiveFunctionVal = arma::as_scalar(trainingResponses[i].t() * newAlphas[i]
                                                               - (lambdas[i] * 0.5) * newAlphas[i].t()
                                                                 * trainingMatrices[i].cols(indices)
                                                                 * trainingMatrices[i].cols(indices).t() *
                                                                 newAlphas[i]
                                                               - 0.5 * newAlphas[i].t() * newAlphas[i]);
                } else {
                    primalObjectiveFunctionVal = arma::as_scalar(
                            0.5 * trainingResponses[i].t() * trainingResponses[i]);

                    dualObjectiveFunctionVal = arma::as_scalar(trainingResponses[i].t() * newAlphas[i]
                                                               - 0.5 * newAlphas[i].t() * newAlphas[i]);
                }

                dualityGaps[i] = std::abs(primalObjectiveFunctionVal - dualObjectiveFunctionVal);
            }

            dualityGap = *max_element(dualityGaps.begin(), dualityGaps.end());
        }

        bool isPartialMinimum() {
            if (innerIterationCount == 0) return false;
            return (subsequentVariableChange < partialMinTolerance);
        };

        void computeSubsequentVariableChange() {
            if (innerIterationCount == 0) {
                subsequentVariableChange = std::numeric_limits<double>::max();
            }

            double diff1 = arma::norm(arma::conv_to<arma::vec>::from(newZeta - zeta), "inf");

            std::vector<double> diffsBeta(numberOfPartitions);
            for (size_t i = 0; i < numberOfPartitions; ++i) {
                diffsBeta[i] = arma::norm(newBetas[i] - betas[i], "inf");
            }

            double diff2 = *max_element(diffsBeta.begin(), diffsBeta.end());

            std::vector<double> diffsAlpha(numberOfPartitions);
            for (size_t i = 0; i < numberOfPartitions; ++i) {
                diffsAlpha[i] = arma::norm(newAlphas[i] - alphas[i], "inf");
            }

            double diff3 = *max_element(diffsAlpha.begin(), diffsAlpha.end());

            subsequentVariableChange = std::max({diff1, diff2, diff3});
        }

        bool isAboveTimeLimit(const TimeMeasurement &measurement) {
            if ((this->timelimitInSeconds != 0) && (measurement.getMidPointInSeconds() >= this->timelimitInSeconds)) {
                sanity = false;
                return true;
            }
            return false;
        }

        bool isAboveMaxInnerIterationCount() {
            sanity = false;
            if (innerIterationCount > maxInnerIterationCount) {
                return true;
            }
            return false;
        }

        bool isAboveMaxOuterIterationCount() {
            if (outerIterationCount > maxOuterIterationCount) {
                sanity = false;
                return true;
            }
            return false;
        }

        void updateOptimizationVariables() {
            zeta = newZeta;
            betas = newBetas;
            alphas = newAlphas;
        }
    };
}


#endif //PIANO_CVBSSPADM_H

#endif
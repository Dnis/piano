//
// Created by carina on 09.10.21.
//

#ifndef PIANO_UPPERLEVELSOLVER_H
#define PIANO_UPPERLEVELSOLVER_H

#ifdef PIANO_MIP_SOLVER_AVAILABLE

#include <UpperLevelSolution.h>
#include <cmath>
#include <vector>
#include <RbssMip.h>
#include <SolutionBuilder.h>
#include <iostream>

namespace piano::cvbss {
    template<class Solver = piano::rbss::Mip<> >
    class UpperLevelSolver {

    public:
        UpperLevelSolver(const arma::mat *X, const arma::vec *y, const std::vector<arma::mat> &trainingMatrices,
                         const std::vector<arma::vec> &trainingResponses,
                         const std::vector<arma::mat> &validationMatrices,
                         const std::vector<arma::vec> &validationResponses,
                         const std::vector<double> &lambdas) :
                X(X),
                y(y),
                trainingMatrices(trainingMatrices),
                validationMatrices(validationMatrices),
                trainingResponses(trainingResponses),
                validationResponses(validationResponses),
                lambdas(lambdas),
                numberOfPartitions(trainingResponses.size()),
                p(trainingMatrices[0].n_cols),
                n(trainingMatrices[0].n_rows + validationMatrices[0].n_rows) {
        }

        UpperLevelSolution solve(const std::vector<arma::vec> &alphas, double penaltyParameter) {

            setupRbssTerms(alphas, penaltyParameter);
            solver = std::make_unique<Solver>(extendedX, extendedY, extendedGamma);
            for (size_t i = 0; i < p; ++i) {
                std::vector<size_t> linkedIndicators(numberOfPartitions);
                for (size_t l = 0; l < numberOfPartitions; ++l) {
                    linkedIndicators[l] = l * p + i;
                }
                solver->linkIndicators(linkedIndicators);
            }
            solver->mute(true);
            if (hasIterationLimit()) solver->setIterationLimit(iterationLimit);
            if (hasTimelimit()) solver->setTimelimit(timelimitInSeconds);
            // solver->setIterationLimit(1000);
            Solution solution = solver->run(activationCosts);

            UpperLevelSolution upperLevelSolution;
            arma::uvec z = stools::toIndicatorVector(solution.subset, solution.p);
            z = z.subvec(0, p - 1);
            upperLevelSolution.zeta = z;
            upperLevelSolution.betas = getBetasFromExtendedBetaVector(solution);
            upperLevelSolution.objectiveValue = solution.objectiveValue;

            return upperLevelSolution;
        }

        void setIterationLimit(size_t limit) {
            this->iterationLimit = limit;
        }

        [[nodiscard]] bool hasIterationLimit() const {
            return (iterationLimit != 0);
        }

        void setTimelimit(size_t timelimitInSeconds) {
            this->timelimitInSeconds = timelimitInSeconds;
        }

        [[nodiscard]] bool hasTimelimit() const {
            return (timelimitInSeconds != 0);
        }

        void setDensityPenalization(double parameter) {
            densityPenalization = parameter;
        }


    private:
        const arma::mat *X;
        const arma::vec *y;

        const std::vector<arma::mat> &trainingMatrices;
        const std::vector<arma::mat> &validationMatrices;

        const std::vector<arma::vec> &trainingResponses;
        const std::vector<arma::vec> &validationResponses;

        const std::vector<double> &lambdas;

        std::unique_ptr<Solver> solver;

        size_t numberOfPartitions;
        size_t p;
        size_t n;

        arma::mat extendedX;
        arma::vec extendedY;
        arma::vec extendedGamma;
        arma::vec activationCosts;

        size_t iterationLimit;
        size_t timelimitInSeconds;
        double densityPenalization = 1e-4;

        void setupRbssTerms(const std::vector<arma::vec> &alphas, double penaltyParameter) {
            initExtendedTerms();

            setupExtendedX(penaltyParameter);
            setupExtendedY(penaltyParameter);

            setupActivationCosts(alphas, penaltyParameter);
        }

        void initExtendedTerms() {
            extendedX = arma::zeros(n * numberOfPartitions, p * numberOfPartitions);
            extendedY = arma::zeros(n * numberOfPartitions);
            extendedGamma = arma::zeros(p * numberOfPartitions);
            activationCosts = arma::zeros(p * numberOfPartitions);
        }

        void setupExtendedX(double penaltyParameter) {
            size_t currentRowIndex = 0;
            setupValidationMatrixTerms(&currentRowIndex);
            setupTrainingMatrixTerms(&currentRowIndex, penaltyParameter);
            setupGammaTerms(penaltyParameter);
        }

        void setupValidationMatrixTerms(size_t *currentRowIndex) {
            size_t blockStartCol = 0;
            for (size_t i = 0; i < numberOfPartitions; i++) {
                extendedX.submat((*currentRowIndex), blockStartCol,
                                 (*currentRowIndex) + validationMatrices[i].n_rows - 1,
                                 blockStartCol + p - 1) = (1 / sqrt(numberOfPartitions)) * validationMatrices[i];
                *currentRowIndex += validationMatrices[i].n_rows;
                blockStartCol += p;
            }
        }

        void setupTrainingMatrixTerms(size_t *currentRowIndex, double penaltyParameter) {
            size_t blockStartCol = 0;
            for (size_t i = 0; i < numberOfPartitions; i++) {
                extendedX.submat(*currentRowIndex, blockStartCol,
                                 (*currentRowIndex) + trainingMatrices[i].n_rows - 1,
                                 blockStartCol + p - 1) = sqrt(penaltyParameter * 0.5) * trainingMatrices[i];
                *currentRowIndex += trainingMatrices[i].n_rows;
                blockStartCol += p;
            }
        }

        void setupGammaTerms(double penaltyParameter) {
            for (size_t i = 0; i < numberOfPartitions; ++i) {
                for (size_t j = 0; j < p; ++j) {
                    extendedGamma[(p * i) + j] = penaltyParameter / (2.0 * lambdas[i]);
                }
            }
        }

        void setupExtendedY(double penaltyParameter) {
            size_t index = 0;
            setupValidationVector(&index);
            setupTrainingVector(&index, penaltyParameter);
        }

        void setupValidationVector(size_t *index) {
            for (size_t i = 0; i < numberOfPartitions; i++) {
                extendedY.subvec(*index, (*index) + validationResponses[i].n_elem - 1) =
                        (1 / sqrt(numberOfPartitions)) * validationResponses[i];
                *index += validationResponses[i].n_elem;
            }
        }

        void setupTrainingVector(size_t *index, double penaltyParameter) {
            for (size_t i = 0; i < numberOfPartitions; i++) {
                extendedY.subvec(*index, (*index) + trainingResponses[i].n_elem - 1) =
                        (sqrt(penaltyParameter * 0.5)) * trainingResponses[i];
                *index += trainingResponses[i].n_elem;
            }
        }

        void setupActivationCosts(const std::vector<arma::vec> &alphas, double penaltyParameter) {
            for (size_t i = 0; i < numberOfPartitions; ++i) {
                for (size_t j = 0; j < p; ++j) {
                    activationCosts[(p * i) + j] = (penaltyParameter * lambdas[i] * 0.5) *
                                        pow(arma::as_scalar(trainingMatrices[i].col(j).t() *
                                        alphas[i]), 2) + densityPenalization;
                }
            }
        }

        std::vector<arma::vec> getBetasFromExtendedBetaVector(const Solution &solution) {
            arma::vec extendedBetaVector = solution.fullCoefficients();
            std::vector<arma::vec> partitionBetas(numberOfPartitions);
            size_t firstIndexOfSubvec = 0;
            for (size_t i = 0; i < numberOfPartitions; i++) {
                partitionBetas[i] = extendedBetaVector.subvec(firstIndexOfSubvec, firstIndexOfSubvec + p - 1);
                firstIndexOfSubvec += p;
            }
            return partitionBetas;
        };
    };
}


#endif //PIANO_UPPERLEVELSOLVER_H

#endif
// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 15.04.20.
//

#ifndef PIANO_LASSO_H
#define PIANO_LASSO_H

#include <Solution.h>
#include <LinearAlgebra.h>
#include <memory>
#include <TrainingFunctions.h>
#include <LassoSolver.h>
#include <omp.h>
#include <HandingOver.h>
#include <CrossValidation.h>
#include <MultiThreadSkeleton.h>
#include <SolutionBuilder.h>

namespace piano::cvbss {
    template<class Solver = lasso::solver::Default>
    class Lasso : public Solver {
    public:
        using ThisType = Lasso<Solver>;

        Lasso(const arma::mat &X, const arma::vec &y, const arma::vec &gamma) :
                X(&X),
                y(&y),
                gamma(&gamma) {
            yScaling = handOverAndNormaliseY(X, y, gamma, &XAlloc, &yAlloc, &newXPtr, &newYPtr);
            // handOver(X, y, gamma, &XAlloc, &yAlloc, &newXPtr, &newYPtr);
            isGammaZero = true;
            for (const auto &item: gamma) {
                if (std::abs(item) > 1e-8) isGammaZero = false;
            }
            Solver::X = newXPtr;
            Solver::y = newYPtr;
            Solver::initialize();
            Solver::setNumberOfThreads(1);
        }

        Lasso(const arma::mat &X, const arma::vec &y) :
                X(&X),
                y(&y) {
            gammaAlloc = std::make_unique<arma::vec>(X.n_cols, arma::fill::zeros);
            gamma = gammaAlloc.get();
            yScaling = handOverAndNormaliseY(X, y, *gamma, &XAlloc, &yAlloc, &newXPtr, &newYPtr);
            // handOver(X, y, *gamma, &XAlloc, &yAlloc, &newXPtr, &newYPtr);
            isGammaZero = true;
            Solver::X = newXPtr;
            Solver::y = newYPtr;
            Solver::initialize();
            Solver::setNumberOfThreads(1);
        }

        [[nodiscard]] Solution run() {
            PartialSolution sol = Solver::run();

            double estimatedPredictionError = piano::cv::kFoldCrossValidation(*(X), *(y), 10, Solver::lambda,
                                                                              piano::cv::mse,
                                                                              getTrainingFunction());

            sol.objectiveValue = sol.objectiveValue / (yScaling * yScaling);

            SolutionBuilder builder(*X, *y, *gamma);
            builder.
                    fromPartialSolution(sol).
                    coefficients(Solver::beta / yScaling, true).
                    estimatedPredictionError(estimatedPredictionError);

            return builder.build();
        }

        PartialSolution computePartialSolution() {
            return Solver::run();
        }

        void computeLambdaPath() {
            Solver::computeLambdaPath();
        }

        [[nodiscard]] std::vector<double> getLambdaPath() {
            std::vector<double> lambdas = Solver::getLambdaPath();
            for (size_t i = 0; i < lambdas.size(); ++i) {
                lambdas[i] /= yScaling;
            }
            return lambdas;
        }

        [[nodiscard]] const std::vector<arma::vec> &getBetaPath() {
            return Solver::getBetaPath();
        }

        [[nodiscard]] const std::vector<::piano::Subset> &getSubsetPath() {
            return Solver::getSubsetPath();
        }

        [[nodiscard]] const std::vector<size_t> &getNonzerosPath() {
            return Solver::getNonzerosPath();
        }

        [[nodiscard]] arma::vec getBeta() const {
            return Solver::beta / yScaling;
        }

        Lasso &setPathLength(size_t n) {
            Solver::setLengthOfPath(n);
        }

        Lasso &setLambda(double l) {
            Solver::setLambda(l * yScaling);
            // Solver::setLambda(l);
            // Solver::lambda = l * yScaling;
            return *this;
        }

        [[deprecated("Set a new X by creating a new solver instance. The function is deprecated because setting a new X would require a solver which however is not implied by the existence of this function.")]]
        Lasso &setX(const arma::mat &X) {
            std::cerr << "Warning: function is deprecated. Set a new X by creating a new solver instance.\n";
            if (!isGammaZero)
                throw std::runtime_error("The data matrixX can only be updated if gamma is the zero vector.");
            XAlloc.reset(new arma::mat(X));
            this->X = XAlloc.get();
            newXPtr = XAlloc.get();
            Solver::X = XAlloc.get();
            Solver::updateDataForX();
            return *this;
        }


        Lasso &setY(const arma::vec &y) {
            if (!isGammaZero)
                throw std::runtime_error("The response y can only be updated if gamma is the zero vector.");
            double yNorm = arma::norm(y, 2);
            yScaling = 1 / yNorm;
            yAlloc.reset(new arma::vec(y * yScaling));
            this->y = yAlloc.get();
            newYPtr = yAlloc.get();
            Solver::y = yAlloc.get();
            Solver::updateDataForY();
            return *this;
        }

        struct TrainingFunction {
            arma::vec operator()(double lambda, const arma::mat &X, const arma::vec &y) {
                ThisType lasso(X, y, arma::zeros(X.n_cols));
                lasso.setLambda(lambda);
                lasso.setNumberOfThreads(1);
                lasso.computePartialSolution();
                return lasso.getBeta();
            }
        };

        [[nodiscard]] TrainingFunction getTrainingFunction() const {
            TrainingFunction trainingFunction;
            return trainingFunction;
        }

        class HyperParamTuner {
            friend class Lasso;

        public:
            void setMinLambda(double d) {
                minLambda = d;
            }

            void setMaxLambda(double d) {
                maxLambda = d;
            }

            void setGridSize(size_t size) {
                gridSize = size;
            }

            void setNumberOfPartitions(size_t size) {
                numberOfPartitions = size;
            }

            void setNumberOfThreads(size_t num) {
                numberOfThreads = num;
            }

            void updateDataForX() {};

            void updateDataForY() {};

            [[nodiscard]] Solution run() {
                auto lassoTraining = lasso->getTrainingFunction();

                if (maxLambda < 0) maxLambda = arma::norm(lasso->newXPtr->t() * (*(lasso->newYPtr)), "inf");
                arma::vec armaLambdas = log10(arma::logspace(minLambda, maxLambda, gridSize));
                std::vector<double> lambdas = arma::conv_to<std::vector<double>>::from(armaLambdas);

                double bestLambda = piano::cv::kFoldCrossValidationGridSearch(numberOfThreads, *(lasso->newXPtr),
                                                                              *(lasso->newYPtr), numberOfPartitions,
                                                                              lambdas, piano::cv::mse, lassoTraining);

                return lasso->setLambda(bestLambda).run();
            }

        private:
            explicit HyperParamTuner(ThisType *lasso) : lasso(lasso) {}

            ThisType *lasso;
            double minLambda = 0;
            double maxLambda = -1;
            size_t gridSize = 100;
            size_t numberOfPartitions = 10;
            size_t numberOfThreads = 1;
        };

        [[nodiscard]] HyperParamTuner getHyperParameterTuner() {
            return HyperParamTuner(this);
        }

    private:
        const arma::mat *X;
        const arma::vec *y;
        const arma::vec *gamma;
        bool isGammaZero;
        const arma::mat *newXPtr;
        const arma::vec *newYPtr;
        std::unique_ptr<arma::mat> XAlloc;
        std::unique_ptr<arma::vec> yAlloc;
        std::unique_ptr<arma::vec> gammaAlloc;

        double yScaling = 1;
    };
}


#endif //PIANO_LASSO_H

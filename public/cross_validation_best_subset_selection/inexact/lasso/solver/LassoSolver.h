// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 27.09.20.
//

#ifndef PIANO_LASSOSOLVER_H
#define PIANO_LASSOSOLVER_H

// First choice default (PICASSO)
#ifdef PIANO_USE_PICASSO
#include <LassoSolverPICASSOCoordinateDescent.h>

#ifndef PIANO_DEFAULT_LASSO_SOLVER
#pragma message("Setting default lasso solver to PICASSO.")
namespace piano::cvbss::lasso::solver {
    using Default = piano::cvbss::lasso::solver::picasso::CoordinateDescent;
}
#define PIANO_DEFAULT_LASSO_SOLVER
#endif
#endif

// Second choice (mlpack)
#ifdef PIANO_USE_MLPACK
#include <LassoSolverMLPACKLARS.h>

#ifndef PIANO_DEFAULT_LASSO_SOLVER
#pragma message("Setting default lasso solver to MLPACK.")
namespace piano::cvbss::lasso::solver {
    using Default = piano::cvbss::lasso::solver::mlpack::LARS;
}
#define PIANO_DEFAULT_LASSO_SOLVER
#endif
#endif

// Third choice
#include <LassoSolverInternalCoordinateDescent.h>
#ifndef PIANO_DEFAULT_LASSO_SOLVER
#pragma message("Setting default lasso solver to internal lasso solver.")
#warning "The internal lasso solver is not recommended right now. Better performance will be achieved with the PICASSO library or the MLPACK library."
namespace piano::cvbss::lasso::solver {
    using Default = piano::cvbss::lasso::solver::internal::CoordinateDescent;
}
#define PIANO_DEFAULT_LASSO_SOLVER
#endif


#endif //PIANO_LASSOSOLVER_H

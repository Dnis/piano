// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 29.05.20.
//

#ifndef PIANO_LASSOSOLVERINTERNALCOORDINATEDESCENT_H
#define PIANO_LASSOSOLVERINTERNALCOORDINATEDESCENT_H

#include <LassoSolverSkeleton.h>
#include <Solution.h>
#include <Types.h>
#include <list>
#include <functional>
#include <unordered_map>
#include <MultiThreadSkeleton.h>
#include <memory>

namespace piano::cvbss::lasso::solver::internal {
    class CoordinateDescent
            : public piano::cvbss::lasso::solver::LassoSolverSkeleton, public piano::MultiThreadSkeleton {
    public:
        void setInitialBeta(const arma::vec &beta);

    protected:
        PartialSolution run();

        void updateDataForY();

        void updateDataForX();

        void initialize();

        void setLambda(double l);

        void setLengthOfPath(size_t lengthOfPath);

        void computeLambdaPath();

        [[nodiscard]] const std::vector<double> &getLambdaPath();

        [[nodiscard]] const std::vector<arma::vec> &getBetaPath();

        [[nodiscard]] const std::vector<::piano::Subset> &getSubsetPath();

        [[nodiscard]] const std::vector<size_t> &getNonzerosPath();

        arma::vec beta;
    private:
        static double softThresholdOperator(double z, double gamma);

        [[nodiscard]] double updateCoordinate(size_t j);

        void updateGradientInformation(size_t i, double newCoordinate);

        void initGradientInformation();

        bool completeCycle();

        double getCovariance(size_t i, size_t j);

        [[nodiscard]] bool isZero(double coord) const;

        std::list<size_t> activeSet;
        size_t numberOfNonzeros;

        size_t p, n;

        double lambdaCorrection = 0.5;

        bool lambdaPathComputed = false;

        std::unique_ptr<std::vector<double>> lambdaPath;
        std::unique_ptr<std::vector<arma::vec>> betaPath;
        std::unique_ptr<std::vector<size_t>> nonzerosPath;
        std::unique_ptr<std::vector<::piano::Subset>> subsetPath;

        struct hash_pair {
            template<class T1, class T2>
            size_t operator()(const std::pair<T1, T2> &p) const {
                auto hash1 = std::hash<T1>{}(p.first);
                auto hash2 = std::hash<T2>{}(p.second);
                return hash1 ^ hash2;
            }
        };


        arma::vec Xty;
        std::unordered_map<std::pair<int, int>, double, hash_pair> covariances;
        arma::vec activeSetCovarianceSums;
        arma::vec XColumnSquaredNorms;
    };
}


#endif //PIANO_LASSOSOLVERINTERNALCOORDINATEDESCENT_H

// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 27.09.20.
//

#ifndef PIANO_LASSOSOLVERPICASSOCOORDINATEDESCENT_H
#define PIANO_LASSOSOLVERPICASSOCOORDINATEDESCENT_H

#include <cassert>
#include <LassoSolverSkeleton.h>
#include <Solution.h>
#include <numeric>
#include <memory>
#include <Types.h>
#include <picasso/objective.hpp>
#include <picasso/solver_params.hpp>
#include <picasso/actgd.hpp>
#include <MultiThreadSkeleton.h>
#include <memory>


namespace piano::cvbss::lasso::solver::picasso {
class CoordinateDescent : public piano::cvbss::lasso::solver::LassoSolverSkeleton, public piano::MultiThreadSkeleton {
    public:
        void setTolerance(double tol);

        void setMaxIterations(size_t iter);

    protected:
        [[nodiscard]] PartialSolution run();

        void initialize();

        void setLambda(double l);

        void computeLambdaPath();

        void updateDataForX();

        void updateDataForY();

        void setLengthOfPath(size_t lengthOfPath);

        [[nodiscard]] const std::vector<double> &getLambdaPath();

        [[nodiscard]] const std::vector<arma::vec> &getBetaPath();

        [[nodiscard]] const std::vector<::piano::Subset> &getSubsetPath();

        [[nodiscard]] const std::vector<size_t> &getNonzerosPath();

        arma::vec beta;

    private:
        size_t p, n;

        // int because picasso only accepts an int
        int maxIterations = std::numeric_limits<int>::max();

        bool lambdaPathComputed = false;

        double lambdaCorrection;

        std::unique_ptr<std::vector<double>> lambdaPath;
        std::unique_ptr<std::vector<arma::vec>> betaPath;
        std::unique_ptr<std::vector<size_t>> nonzerosPath;
        std::unique_ptr<std::vector<::piano::Subset>> subsetPath;

        //std::unique_ptr<::picasso::solver::ActGDSolver> solver;
        //::picasso::GaussianNaiveUpdateObjective *objective;
        std::unique_ptr<::picasso::solver::PicassoSolverParams> param;

        void setupParams();

        // void setupSolver();
    };
}


#endif //PIANO_LASSOSOLVERPICASSOCOORDINATEDESCENT_H

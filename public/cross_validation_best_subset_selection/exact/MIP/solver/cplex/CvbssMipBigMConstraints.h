// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 30.03.20.
//

#ifdef PIANO_USE_CPLEX

#ifndef PIANO_BIGMCONSTRAINTS_H
#define PIANO_BIGMCONSTRAINTS_H

#include <ilcplex/ilocplex.h>
#include <ilcplex/ilocplexi.h>
#include <memory>
#include <CoefficientBounds.h>
#include <BoundPrerequisites.h>
#include <CplexHelper.h>
#include <Partition.h>
#include <TypeConversion.h>

namespace piano::cvbss::mip::indicator_constraints::cplex {
    template<class CoefficientBounds = piano::sls::CoefficientBounds<>, class UpperPredictedValueBound = piano::sls::upper_bound::predicted_value::DualCauchyBound>
    class BigMConstraints {

    protected:
        BigMConstraints(const arma::mat *X, const arma::vec *y, const arma::vec *gamma,
                        size_t numberOfPartitions) :
                X(X),
                y(y),
                gamma(gamma),
                numberOfPartitions(numberOfPartitions) {

        }

        void buildConstraints(IloEnv &env, IloModel &model, const std::vector<IloNumVarArray> &beta,
                              const IloBoolVarArray &z) const {
            size_t p = X->n_cols;
            size_t n = X->n_rows;
            cv::Partition partition(n, numberOfPartitions);

            assertCastable<long>(p, "p", "number of variables");

            for (size_t l = 0; l < numberOfPartitions; l++) {
                arma::mat Xt = partition.getComplementPartitionMatrix(l, *X);
                arma::vec yt = partition.getComplementPartitionVector(l, *y);

                CoefficientBounds coefficientBounds(&Xt, &yt, gamma, p);
                piano::sls::BoundPrerequisites prerequisites(&Xt, &yt, gamma, p);
                UpperPredictedValueBound upperPredictedValueBound(&prerequisites);

                for (long i = 0; i < static_cast<long>(p); i++) {
                    double L = coefficientBounds.bound(i);
                    model.add(beta[l][i] <= L * z[i]);
                    model.add(beta[l][i] >= -L * z[i]);

                    IloExpr expr(env);
                    arma::vec g = Xt.t() * Xt.col(i);
                    expr += piano::cplex::dot(env, g, beta[l]);
                    expr += (*gamma)[i] * beta[l][i];

                    double d = sqrt(prerequisites.getXXplusI()->at(i, i) * upperPredictedValueBound.bound());
                    double M = d - arma::as_scalar(Xt.col(i).t() * yt);
                    double m = d + arma::as_scalar(Xt.col(i).t() * yt);
                    model.add(expr <= M * (1 - z[i]) + as_scalar(Xt.col(i).t() * yt));
                    model.add(expr >= -m * (1 - z[i]) + as_scalar(Xt.col(i).t() * yt));
                    expr.end();
                }
            }
        }

    private:
        const arma::mat *X;
        const arma::vec *y;
        const arma::vec *gamma;
        size_t numberOfPartitions;
    };
}


#endif //PIANO_BIGMCONSTRAINTS_H
#endif
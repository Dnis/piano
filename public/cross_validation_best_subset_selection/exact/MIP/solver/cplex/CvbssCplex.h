// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 26.03.20.
//

#ifdef PIANO_USE_CPLEX

#ifndef PIANO_CVBSSCPLEX_H
#define PIANO_CVBSSCPLEX_H

#include <LinearAlgebra.h>
#include <BoundPrerequisites.h>
#include <ilcplex/ilocplex.h>
#include <ilcplex/ilocplexi.h>
#include <CplexHelper.h>
#include <Partition.h>
#include <SquaredL2.h>
#include <Conversion.h>
#include <LeastSquares.h>
#include <CvbssNoWarmstart.h>
#include <cplex/CvbssMipBigMConstraints.h>
#include <cplex/CvbssMipLogicalConstraints.h>
#include <cplex/CvbssMipLogicalConstraintsWithBounds.h>
#include <cplex/CvbssMipPoolSelector.h>
#include <CrossValidation.h>
#include <ErrorMetrics.h>
#include <list>
#include <type_traits>
#include <TimeMeasurement.h>
#include <PartialSolutionBuilder.h>
#include <PartialSolution.h>
#include <TypeConversion.h>


namespace piano::cvbss::mip::solver {
    template<class IndicatorConstraintsHandler = piano::cvbss::mip::indicator_constraints::cplex::BigMConstraints<>,
            class PoolSelector = pool_selector::cplex::Sparsest>
    class Cplex : IndicatorConstraintsHandler {
    private:
        void setupVariables();

        void setupObjectiveFunction();

        void setupCplex();

        void extractWarmstartVariables(const piano::Solution &sol, std::vector<arma::vec> *betaWarmstart,
                                       arma::uvec *zWarmstart) const;

        constexpr static auto delIloEnv = piano::cplex::delIloObject<IloEnv>;
        std::unique_ptr<IloEnv, decltype(delIloEnv)> env;

        constexpr static auto delIloModel = piano::cplex::delIloObject<IloModel>;
        std::unique_ptr<IloModel, decltype(delIloModel)> model;

        std::unique_ptr<std::vector<IloNumVarArray>> beta;
        std::unique_ptr<IloBoolVarArray> z;

        constexpr static auto delIloCplex = piano::cplex::delIloObject<IloCplex>;
        std::unique_ptr<IloCplex, decltype(delIloCplex)> cplex;

    protected:
        Cplex(const arma::mat *X, const arma::vec *y, const arma::vec *gamma, size_t numberOfPartitions);

        void buildModel();

        template<class Warmstart>
        void runAndRegisterWarmstart();

        void setNumericalScaling();

        [[nodiscard]] PartialSolution solve();

        const arma::mat *X;
        const arma::vec *y;
        const arma::vec *gamma;
        size_t n, p;
        size_t numberOfPartitions;
        size_t timeLimit = 360;
        size_t iterationLimit = 0;
        size_t numberOfThreads = 0;
        bool muteCplex = false;
        std::list<Solution> warmstartSolutions;
        bool exportModel = false;
        std::string exportFile;
        double numericalScalar = 1e5;

        void enableWarmstart(const Solution &warmstartSol);
    };

    template<class IndicatorConstraintsHandler, class PoolSelector>
    Cplex<IndicatorConstraintsHandler, PoolSelector>::Cplex(const arma::mat *X,
                                                            const arma::vec *y,
                                                            const arma::vec *gamma,
                                                            size_t numberOfPartitions)
            :
            IndicatorConstraintsHandler(X, y, gamma, numberOfPartitions),
            env(nullptr, delIloEnv),
            model(nullptr, delIloModel),
            cplex(nullptr, delIloCplex),
            X(X),
            y(y),
            gamma(gamma),
            n(X->n_rows),
            p(X->n_cols),
            numberOfPartitions(numberOfPartitions),
            warmstartSolutions{} {
    }

    template<class IndicatorConstraintsHandler, class PoolSelector>
    void Cplex<IndicatorConstraintsHandler, PoolSelector>::buildModel() {
        env.reset(new IloEnv);
        model.reset(new IloModel(*env));

        setupVariables();
        IndicatorConstraintsHandler::buildConstraints(*env, *model, *beta, *z);
        setupObjectiveFunction();

        setupCplex();

        if (exportModel) cplex->exportModel(exportFile.c_str());
    }

    template<class IndicatorConstraintsHandler, class PoolSelector>
    void Cplex<IndicatorConstraintsHandler, PoolSelector>::setupVariables() {
        beta = std::make_unique<std::vector<IloNumVarArray>>(numberOfPartitions);
        for (size_t l = 0; l < numberOfPartitions; ++l) {
            (*beta)[l] = IloNumVarArray(*env, p, -IloInfinity, IloInfinity);

            for (long i = 0; i < safeCastToLong(p, "p", "number of columns"); i++) {
                std::string beta_name_str = "beta_" + std::to_string(l) + "_" + std::to_string(i);
                (*beta)[l][i].setName(beta_name_str.c_str());
            }
        }

        z = std::make_unique<IloBoolVarArray>(*env, p);
        for (size_t i = 0; i < p; i++) {
            std::string x_name_str = "x_" + std::to_string(i);
            (*z)[i].setName(x_name_str.c_str());
        }


    }

    template<class IndicatorConstraintsHandler, class PoolSelector>
    void
    Cplex<IndicatorConstraintsHandler, PoolSelector>::setupObjectiveFunction() {
        piano::cv::Partition partition(n, numberOfPartitions);
        IloExpr obj(*env);
        for (size_t l = 0; l < numberOfPartitions; ++l) {
            double meanScalar = 1.0 /
                                safeCastToInt(partition.getPartition(l).size(), "partition.getPartition(l).size()",
                                              "partition size");

            arma::mat Xv = partition.getPartitionMatrix(l, *X);
            arma::vec yv = partition.getPartitionVector(l, *y);

            obj += piano::cplex::quadraticExpr(*env, numericalScalar * meanScalar * Xv.t() * Xv, (*beta)[l]);
            obj += piano::cplex::dot(*env, arma::vec(-numericalScalar * meanScalar * 2 * Xv.t() * yv), (*beta)[l]);
            obj += numericalScalar * meanScalar * piano::sls::squaredL2(yv);
        }

        model->add(IloMinimize(*env, obj));
        obj.end();
    }

    template<class IndicatorConstraintsHandler, class PoolSelector>
    void Cplex<IndicatorConstraintsHandler, PoolSelector>::setupCplex() {
        cplex.reset(new IloCplex(*model));
        if (timeLimit != 0) cplex->setParam(IloCplex::Param::TimeLimit, timeLimit);
        if (iterationLimit != 0) cplex->setParam(IloCplex::Param::MIP::Limits::Nodes, iterationLimit - 1);
        if (numberOfThreads != 0) cplex->setParam(IloCplex::Param::Threads, numberOfThreads);
        if (muteCplex) {
            cplex->setOut(env->getNullStream());
            cplex->setParam(IloCplex::Param::MIP::Display, 0);
        } else {
            cplex->setParam(IloCplex::Param::MIP::Display, 3);
        }
        cplex->setParam(IloCplex::Param::MIP::Pool::RelGap, 0.05);
    }

    template<class IndicatorConstraintsHandler, class PoolSelector>
    PartialSolution Cplex<IndicatorConstraintsHandler, PoolSelector>::solve() {
        for (const auto &warmstartSol: warmstartSolutions) {
            enableWarmstart(warmstartSol);
        }


        TimeMeasurement measurement;
        bool solved = false;
        measurement.start();
        try {
            solved = static_cast<bool>(cplex->solve());
        } catch (const IloAlgorithm::Exception &e) {
            std::cout << e.getMessage() << "\n";
        }
        measurement.end();

        if (!solved) {
            IloAlgorithm::Status solStatus = cplex->getStatus();
            if (!muteCplex) env->out() << std::endl << "Solution status: " << solStatus << std::endl;
            throw (-1);
        } else {
            IloAlgorithm::Status solStatus = cplex->getStatus();
            if (!muteCplex) env->out() << std::endl << "Solution status: " << solStatus << std::endl;

            size_t selectedSol = PoolSelector::select(*cplex, *z);

            Subset optSubset = piano::stools::toSubset(piano::cplex::extractVars(*cplex, *z, selectedSol));

            PartialSolutionBuilder builder;
            builder.
                    subset(optSubset).
                    objectiveValue(cplex->getObjValue(
                    safeCastToLong(selectedSol, "selectedSol", "index of solution in CPLEX solution pool"))).
                    provableOptimal(solStatus == IloAlgorithm::Status::Optimal).
                    computationsRuntimeInSeconds(measurement.getDurationInSeconds()).
                    completeRuntimeInSeconds(0).
                    gap(cplex->getMIPRelativeGap());

            return builder.build();
        }
    }

    template<class IndicatorConstraintsHandler, class PoolSelector>
    void Cplex<IndicatorConstraintsHandler, PoolSelector>::Cplex::enableWarmstart(const Solution &warmstartSol) {
        std::vector<arma::vec> betaWarmstart;
        arma::uvec zWarmstart;
        extractWarmstartVariables(warmstartSol, &betaWarmstart, &zWarmstart);

        IloNumVarArray startVar(*env);
        IloNumArray startVal(*env);
        for (size_t j = 0; j < p; ++j) {
            startVar.add((*z)[j]);
            startVal.add(zWarmstart[j]);
        }
        for (size_t l = 0; l < numberOfPartitions; ++l) {
            for (size_t i = 0; i < p; ++i) {
                startVar.add((*beta)[l][i]);
                startVal.add(betaWarmstart[l][i]);
            }
        }

        cplex->addMIPStart(startVar, startVal, IloCplex::MIPStartCheckFeas);
        startVar.end();
        startVal.end();
    }

    template<class IndicatorConstraintsHandler, class PoolSelector>
    void
    Cplex<IndicatorConstraintsHandler, PoolSelector>::extractWarmstartVariables(
            const piano::Solution &sol, std::vector<arma::vec> *betaWarmstart, arma::uvec *zWarmstart) const {
        *zWarmstart = piano::stools::toIndicatorVector(sol.subset, p);

        std::vector<arma::vec> bW(numberOfPartitions);
        piano::cv::Partition partition(n, numberOfPartitions);
        for (size_t l = 0; l < numberOfPartitions; ++l) {
            arma::mat Xt = partition.getComplementPartitionMatrix(l, *X);
            arma::vec yt = partition.getComplementPartitionVector(l, *y);
            piano::sls::LeastSquares leastSquares(&Xt, &yt, gamma);
            bW[l] = leastSquares.compute(sol.subset).fullCoefficients();
        }
        *betaWarmstart = bW;
    }

    template<class IndicatorConstraintsHandler, class PoolSelector>
    template<class Warmstart>
    void Cplex<IndicatorConstraintsHandler, PoolSelector>::runAndRegisterWarmstart() {
        if constexpr (!std::is_same_v<Warmstart, piano::cvbss::NoWarmstart>) {
            Warmstart warmstart(*X, *y, *gamma);
            warmstartSolutions.push_back(warmstart.run());
        }
    }

    template<class IndicatorConstraintsHandler, class PoolSelector>
    void Cplex<IndicatorConstraintsHandler, PoolSelector>::setNumericalScaling() {
        if (!warmstartSolutions.empty()) {
            cv::SubsetTraining trainingFunction{};

            trainingFunction.gamma = gamma;

            double bestError = piano::cv::kFoldCrossValidation(*X, *y, numberOfPartitions,
                                                               warmstartSolutions.front().subset, piano::cv::mse,
                                                               trainingFunction);
            for (const auto &item: warmstartSolutions) {
                double error = piano::cv::kFoldCrossValidation(*X, *y, numberOfPartitions,
                                                               item.subset, piano::cv::mse,
                                                               trainingFunction);
                if (error < bestError) bestError = error;
            }
            numericalScalar = 10.0 / bestError;
        }
    }
}

#endif //PIANO_CVBSSCPLEX_H
#endif
// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 15.04.20.
//

#ifdef PIANO_USE_CPLEX

#ifndef PIANO_LOGICALCONSTRAINTSWITHBOUNDS_H
#define PIANO_LOGICALCONSTRAINTSWITHBOUNDS_H

//
// Created by dennis on 30.03.20.
//

#include <ilcplex/ilocplex.h>
#include <ilcplex/ilocplexi.h>
#include <memory>
#include <vector>
#include <LinearAlgebra.h>
#include <Partition.h>
#include <CplexHelper.h>
#include <CoefficientBounds.h>
#include <exception>
#include <TypeConversion.h>

namespace piano::cvbss::mip::indicator_constraints::cplex {
    template<class CoefficientBounds = piano::sls::CoefficientBounds<>, class UpperPredictedValueBound = piano::sls::upper_bound::predicted_value::DualCauchyBound>
    class LogicalConstraintsWithBounds {
    protected:
        LogicalConstraintsWithBounds(const arma::mat *X, const arma::vec *y, const arma::vec *gamma,
                                     size_t numberOfPartitions) :
                X(X),
                y(y),
                gamma(gamma),
                numberOfPartitions(numberOfPartitions) {

        }

        void buildConstraints(IloEnv &env, IloModel &model, std::vector<IloNumVarArray> &beta,
                              const IloBoolVarArray &z) const {
            size_t p = X->n_cols;
            size_t n = X->n_rows;
            piano::cv::Partition partition(n, numberOfPartitions);

            assertCastable<long>(p, "p", "number of variables");

            for (size_t l = 0; l < numberOfPartitions; l++) {
                arma::mat Xt = partition.getComplementPartitionMatrix(l, *X);
                arma::vec yt = partition.getComplementPartitionVector(l, *y);

                CoefficientBounds coefficientBounds(&Xt, &yt, gamma, p);

                for (long i = 0; i < static_cast<long>(p); i++) {
                    model.add(IloIfThen(env, z[i] == 0, beta[l][i] == 0));

                    double L = coefficientBounds.bound(i);
                    beta[l][i].setBounds(-L, L);

                    IloExpr expr(env);
                    arma::vec g = Xt.t() * Xt.col(i);
                    expr += piano::cplex::dot(env, g, beta[l]);
                    expr += (*gamma)[i] * beta[l][i];
                    model.add(IloIfThen(env, z[i] == 1, expr == as_scalar(Xt.col(i).t() * yt)));
                    expr.end();
                }
            }
        }

    private:
        const arma::mat *X;
        const arma::vec *y;
        const arma::vec *gamma;
        size_t numberOfPartitions;
    };
}

#endif //PIANO_LOGICALCONSTRAINTSWITHBOUNDS_H
#endif
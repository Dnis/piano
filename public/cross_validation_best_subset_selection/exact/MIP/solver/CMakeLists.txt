if (USE_CPLEX)
    find_package(CPLEX)
endif ()

add_library(cvbss_exact_mip_Solver SHARED
        ${PROJECT_SOURCE_DIR}/private/cross_validation_best_subset_selection/exact/MIP/solver/cplex/CvbssMipPoolSelector.cpp)
add_library(cvbss::exact::mip::Solver ALIAS cvbss_exact_mip_Solver)

target_include_directories(cvbss_exact_mip_Solver
        PUBLIC
        $<INSTALL_INTERFACE:include/piano/cross_validation_best_subset_selection/exact/MIP/solver>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
        PRIVATE
        ${PROJECT_SOURCE_DIR}/private/cross_validation_best_subset_selection/exact/MIP/solver
        )

set_property(TARGET cvbss_exact_mip_Solver PROPERTY CXX_STANDARD 17)
set_property(TARGET cvbss_exact_mip_Solver PROPERTY EXPORT_NAME cvbss::exact::mip::Solver)

target_link_libraries(cvbss_exact_mip_Solver
        PUBLIC
        LinearAlgebra
        sls::Bounds
        CPLEXHelper
        cv::Partition
        sls::Helper
        stools::Conversion
        sls::SubsetLeastSquares
        cvbss::inexact::NoWarmstart
        cv::CrossValidation
        TimeMeasurement
        Types)

if (CPLEX_FOUND AND USE_CPLEX)
    target_compile_definitions(cvbss_exact_mip_Solver PUBLIC PIANO_MIP_SOLVER_AVAILABLE)
    target_compile_definitions(cvbss_exact_mip_Solver PUBLIC PIANO_USE_CPLEX)
    target_link_libraries(cvbss_exact_mip_Solver
            PUBLIC
            CPLEX::CPLEX
            )
endif ()



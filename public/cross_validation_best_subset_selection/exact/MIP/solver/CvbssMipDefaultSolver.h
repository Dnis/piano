//
// Created by Dennis Kreber on 25.12.21.
//

#ifndef PIANO_CVBSSMIPDEFAULTSOLVER_H
#define PIANO_CVBSSMIPDEFAULTSOLVER_H

#include <cplex/CvbssCplex.h>

namespace piano::cvbss::mip::solver {
#ifdef PIANO_MIP_SOLVER_AVAILABLE
    using DefaultSolver = Cplex<>;
#endif
}

#endif //PIANO_CVBSSMIPDEFAULTSOLVER_H

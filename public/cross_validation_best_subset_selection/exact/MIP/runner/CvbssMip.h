// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 26.03.20.
//

#ifdef PIANO_MIP_SOLVER_AVAILABLE

#ifndef PIANO_CROSSVALIDATEDBESTSUBSETSELECTION_H
#define PIANO_CROSSVALIDATEDBESTSUBSETSELECTION_H


#include <memory>
#include <LinearAlgebra.h>
#include <Solution.h>
#include <cplex/CvbssCplex.h>
#include <CvbssNoWarmstart.h>
#include <CoefficientBounds.h>
#include <cplex/CvbssMipLogicalConstraints.h>
#include <cplex/CvbssMipLogicalConstraintsWithBounds.h>
#include <cplex/CvbssMipBigMConstraints.h>
#include <cplex/CvbssMipPoolSelector.h>
#include <CoefficientBounds.h>
#include <TimeMeasurement.h>
#include <LeastSquares.h>
#include <TrainingFunctions.h>
#include <Types.h>
#include <CvbssBase.h>
#include <CvbssMipDefaultSolver.h>

namespace piano::cvbss {
    template<class Warmstart = piano::cvbss::NoWarmstart,
            class Solver = mip::solver::DefaultSolver >
    class Mip : Solver, public Base<cv::SubsetTraining> {
        using ThisType = Mip<Warmstart, Solver>;
        using SolverType = Solver;

    public:
        Mip(const arma::mat &X, const arma::vec &y, const arma::vec &gamma, size_t numberOfPartitions = 10) :
                SolverType(&X, &y, &gamma, 10),
                Base<cv::SubsetTraining>(X, y, gamma, numberOfPartitions),
                X(&X),
                y(&y),
                gamma(&gamma) {}

        using TrainingFunction = cv::SubsetTraining;

        [[nodiscard]] TrainingFunction getTraining() const override {
            TrainingFunction trainingFunction;
            trainingFunction.gamma = SolverType::gamma;
            return trainingFunction;
        }

        PartialSolution computePartialSolution() override {
            SolverType::muteCplex = isMuted;
            SolverType::timeLimit = timelimitInSeconds;
            SolverType::iterationLimit = iterationLimit;
            SolverType::template runAndRegisterWarmstart<Warmstart>();
            SolverType::setNumericalScaling();
            SolverType::buildModel();
            return SolverType::solve();
        }

        void setNumberOfThreads(size_t numberOfThreads) override {
            SolverType::numberOfThreads = numberOfThreads;
            Base<cv::SubsetTraining>::setNumberOfThreads(numberOfThreads);
        }

        void addWarmstartSolution(const Solution &sol) {
            SolverType::warmstartSolutions.push_back(sol);
        }

        void addWarmstartSolution(Solution &&sol) {
            SolverType::warmstartSolutions.push_back(std::move(sol));
        }

        void exportModel(const std::string &fileName) {
            if (fileName.empty()) {
                SolverType::exportModel = false;
            } else {
                SolverType::exportModel = true;
                SolverType::exportFile = fileName;
            }
        }

    private:
        const arma::mat *X;
        const arma::vec *y;
        const arma::vec *gamma;
    };
}


#endif //PIANO_CROSSVALIDATEDBESTSUBSETSELECTION_H
#endif
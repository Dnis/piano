add_subdirectory(solver)
add_subdirectory(runner)

add_library(cvbss_exact_mip INTERFACE)
add_library(cvbss::exact::mip ALIAS cvbss_exact_mip)

set_property(TARGET cvbss_exact_mip PROPERTY EXPORT_NAME cvbss::exact::mip)

target_link_libraries(cvbss_exact_mip
        INTERFACE
        cvbss::exact::mip::MIP)
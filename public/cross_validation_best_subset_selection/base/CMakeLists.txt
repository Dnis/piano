add_library(cvbss_Base INTERFACE)
add_library(cvbss::Base ALIAS cvbss_Base)

target_include_directories(cvbss_Base INTERFACE
        $<INSTALL_INTERFACE:include/piano/cross_validation_best_subset_selection/base>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

set_property(TARGET cvbss_Base PROPERTY EXPORT_NAME cvbss::Base)

target_link_libraries(cvbss_Base
        INTERFACE
        Bases
        LinearAlgebra
        TimeMeasurement
        cv::CrossValidation
        sls::EigenvalueExtraction)
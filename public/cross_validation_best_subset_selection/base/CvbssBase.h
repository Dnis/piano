//
// Created by dennis on 30.09.21.
//

#ifndef PIANO_CVBSSBASE_H
#define PIANO_CVBSSBASE_H

#include <LinearRegressionBase.h>
#include <LinearAlgebra.h>
#include <OptimizationSolution.h>
#include <Solution.h>
#include <TimeMeasurement.h>
#include <OptimizationSolutionBuilder.h>
#include <RunnerBase.h>
#include <TrainingFunctions.h>
#include <EigenvalueExtraction.h>

namespace piano::cvbss {
    template<class Training = cv::SubsetTraining>
    class Base : public RunnerBase<Training> {
    public:
        Base(const arma::mat &X, const arma::vec &y, const arma::vec &gamma, size_t numberOfPartitions = 10) :
                RunnerBase<Training>(X, y, gamma, true),
                numberOfPartitions(numberOfPartitions) {}

        Solution run() {
            TimeMeasurement measurement;
            measurement.start();
            Training training = this->getTraining();
            PartialSolution partialSolution = computePartialSolution();
            Solution solution = RunnerBase<Training>::buildSolutionFromPartialSolution(partialSolution, training);
            measurement.end();
            solution.completeRuntimeInSeconds = measurement.getDurationInSeconds();
            return solution;
        }

        OptimizationSolution computeOptimizationSolution() {
            PartialSolution partialSolution = computePartialSolution();
            return RunnerBase<Training>::buildOptimizationSolutionFromPartialSolution(partialSolution);
        }

        virtual PartialSolution computePartialSolution() = 0;

        [[nodiscard]] size_t getNumberOfPartitions() const {
            return numberOfPartitions;
        }

        void setNumberOfPartitions(size_t numberOfPartitions) {
            Base::numberOfPartitions = numberOfPartitions;
            partitionsInitialized = false;
        }

        virtual ~Base() {}

    protected:
        size_t numberOfPartitions = 10;

        std::vector<arma::mat> trainingMatrices;
        std::vector<arma::vec> trainingResponses;

        std::vector<arma::mat> validationMatrices;
        std::vector<arma::vec> validationResponses;

        std::vector<double> ridgeParameters;
        std::vector<double> lambdas;

        void setupPartitions() {
            if (!partitionsInitialized) {
                piano::cv::Partition partition(this->n, numberOfPartitions);

                trainingMatrices = std::vector<arma::mat>(numberOfPartitions);
                validationMatrices = std::vector<arma::mat>(numberOfPartitions);
                trainingResponses = std::vector<arma::vec>(numberOfPartitions);
                validationResponses = std::vector<arma::vec>(numberOfPartitions);
                ridgeParameters = std::vector<double>(numberOfPartitions);
                lambdas = std::vector<double>(numberOfPartitions);

                for (size_t i = 0; i < numberOfPartitions; i++) {
                    arma::mat trainingMatrix = partition.getComplementPartitionMatrix(i, this->originalX);
                    arma::vec trainingVector = partition.getComplementPartitionVector(i, this->originalY);
                    piano::sls::EigenvalueExtraction trainingExtraction(
                            trainingMatrix,
                            trainingVector,
                            this->originalGamma,
                            0.9
                    );
                    trainingExtraction.setNumberOfThreads(this->numberOfThreads);

                    trainingMatrices[i] = trainingExtraction.getXTilde();
                    trainingResponses[i] = trainingExtraction.getYTilde();
                    ridgeParameters[i] = trainingExtraction.getR();
                    lambdas[i] = 1.0 / (2.0 * ridgeParameters[i]);

                    arma::mat Xv = partition.getPartitionMatrix(i, this->originalX);
                    validationMatrices[i] = Xv;

                    arma::vec yv = partition.getPartitionVector(i, this->originalY);
                    validationResponses[i] = yv;
                }

                partitionsInitialized = false;
            }
        }

    private:
        bool partitionsInitialized = false;

    };
}

#endif //PIANO_CVBSSBASE_H

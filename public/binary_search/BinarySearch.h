// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 24.09.20.
//

#ifndef PIANO_BINARYSEARCH_H
#define PIANO_BINARYSEARCH_H

#include <cmath>

namespace piano {
    /*!
     * Method for doing a binary search on an interval, i.e. the value `toSearch` is searched on the interval from
     * `func(lowerBound)` to `func(upperBound)`. The function `func` must be increasing. An `std::runtime_error` is
     * thrown if `func(lowerBound)` is greater than `func(upperBound)`. If the value `toSearch` cannot be found an
     * `std::runtime_error` is thrown. The method decreases the search region iteratively until the size of the interval
     * is smaller or equal to the set tolerance.
     * @tparam Input Type of inputs for the function `func`. That is, the interval `[lowerBound, upperBound]` contains
     * elements of type `Input`.
     * @tparam Output Type of the output of the function `func`.
     * @tparam Func Type of the function. Must be increasing, i.e., if `a >= b`, the condition `func(a) >= func(b)`
     * must hold.
     * @param lowerBound Lower bound of the interval.
     * @param upperBound Upper bound of the interval.
     * @param func Function to evaluate interval points.
     * @param toSearch Value to search for, i.e., the method returns an value `input` such that
     * `func(value)` is approximately equal to `toSearch`.
     * @param returnGE Iteratively the search region is decreased until the tolerance is reached. This parameter
     * determines if the upper bound of the search region is returned or if the lower bound is returned. Default: true.
     * @param tolerance Tolerance to determine when to stop. The binary search stops when
     * `upperBound - lowerBound <= tolerance`. Default: 1e-4.
     * @return
     */
    template<class Input, class Output, class Func>
    Input rationalBinarySearch(const Input &lowerBound, const Input &upperBound, Func &&func, const Output &toSearch,
                               bool returnGE = true, double tolerance = 1e-4) {
        Output lowerVal = func(lowerBound);
        Output upperVal = func(upperBound);

        if ((lowerVal > upperVal)) throw std::runtime_error("func(lowerBound) is greater than func(upperBound).");
        if (toSearch <= lowerVal) {
            if (returnGE) return lowerBound;
            else throw std::runtime_error("No input found satisfying criteria.");
        }
        if (toSearch >= upperVal) {
            if (!returnGE) return upperBound;
            else throw std::runtime_error("No input found satisfying criteria.");
        }

        Input u = upperBound;
        Input l = lowerBound;
        while ((u - l) > tolerance) {
            if (lowerVal == toSearch) return l;
            if (upperVal == toSearch) return u;

            Input midPoint = (u + l) / 2.0;
            Output midValue = func(midPoint);
            if (midValue >= toSearch) {
                u = midPoint;
                upperVal = func(u);
            } else {
                l = midPoint;
                lowerVal = func(l);
            }
        }

        if (returnGE) return u;
        else return l;
    }
}

template<class Input, class Output, class Func, class Grid>
size_t discreteBinarySearch(const Grid &grid, Func &&func, const Output &toSearch,
                            bool returnGE = true, bool ascendingArray = true) {
    int lIndex;
    int uIndex;
    if (ascendingArray) {
        lIndex = 0;
        uIndex = grid.size() - 1;
    } else {
        lIndex = grid.size() - 1;
        uIndex = 0;
    }

    Output lowerVal = func(grid[lIndex]);
    Output upperVal = func(grid[uIndex]);

    if (lowerVal > upperVal) throw std::runtime_error("func(lowerBound) is greater than func(upperBound).");
    if (toSearch <= lowerVal) {
        if (returnGE) return grid[0];
        else throw std::runtime_error("No input found satisfying criteria.");
    }
    if (toSearch >= upperVal) {
        if (!returnGE) return grid[grid.size() - 1];
        else throw std::runtime_error("No input found satisfying criteria.");
    }

    Input u = grid[uIndex];
    Input l = grid[lIndex];

    while (lIndex != uIndex) {
        if (lowerVal == toSearch) return lIndex;
        if (upperVal == toSearch) return uIndex;

        int mIndex;
        if (ascendingArray)
            mIndex = returnGE ? static_cast<int>(std::ceil((uIndex + lIndex) / 2.0)) :
                     static_cast<int>(std::floor((uIndex + lIndex) / 2.0));
        else
            mIndex = !returnGE ? static_cast<int>(std::ceil((uIndex + lIndex) / 2.0)) :
                     static_cast<int>(std::floor((uIndex + lIndex) / 2.0));
        if (mIndex == uIndex) return uIndex;
        if (mIndex == lIndex) return lIndex;
        Input midPoint = grid[mIndex];
        Output midValue = func(midPoint);
        if (midValue >= toSearch) {
            uIndex = mIndex;
            u = midPoint;
            upperVal = func(u);
        } else {
            lIndex = mIndex;
            l = midPoint;
            lowerVal = func(l);
        }
    }

    return u;
}

namespace piano::BinarySearch {
    template<class Input>
    struct Identity {
        const Input &operator()(const Input &input) {
            return input;
        }
    };
}

#endif //PIANO_BINARYSEARCH_H

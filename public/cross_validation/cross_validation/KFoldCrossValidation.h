// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 25.03.20.
//

#ifndef PIANO_KFOLDCROSSVALIDATION_H
#define PIANO_KFOLDCROSSVALIDATION_H

#include <LinearAlgebra.h>
#include <Partition.h>
#include <GridSearch.h>
#include <Types.h>
#include <LeastSquares.h>

namespace piano::cv {
    template<class ValidationFunction, class TrainingFunction, class TrainingInput>
    double kFoldCrossValidation(const arma::mat &X, const arma::vec &y, size_t numberOfPartitions,
                                const TrainingInput &trainingInput, ValidationFunction &&validationFunction,
                                TrainingFunction &&trainingFunction) {
        Partition partition(X.n_rows, numberOfPartitions);

        std::vector<arma::mat> tX(numberOfPartitions);
        std::vector<arma::mat> vX(numberOfPartitions);
        std::vector<arma::vec> ty(numberOfPartitions);
        std::vector<arma::vec> vy(numberOfPartitions);

        for (size_t i = 0; i < numberOfPartitions; ++i) {
            tX[i] = partition.getComplementPartitionMatrix(i, X);
            vX[i] = partition.getPartitionMatrix(i, X);
            ty[i] = partition.getComplementPartitionVector(i, y);
            vy[i] = partition.getPartitionMatrix(i, y);
        }

        double sum = 0;
        for (size_t i = 0; i < numberOfPartitions; ++i) {
            sum += std::forward<ValidationFunction>(validationFunction)(
                           std::forward<TrainingFunction>(trainingFunction)(trainingInput, (tX)[i], (ty)[i]), (vX)[i],
                           (vy)[i]);
        }

        return sum;
    }

    template<class ValidationFunction, class TrainingFunction, class Grid>
    typename std::remove_reference<Grid>::type::value_type
    kFoldCrossValidationGridSearch(size_t numberOfThreads, const arma::mat &X, const arma::vec &y,
                                   size_t numberOfPartitions, const Grid &grid,
                                   ValidationFunction &&validationFunction, TrainingFunction &&trainingFunction,
                                   std::vector<typename std::result_of_t<TrainingFunction &&(
                                           typename std::remove_reference<Grid>::type::value_type, arma::mat,
                                           arma::vec)>> *bestTrainingOutputs) {
        using TrainingInput = typename std::remove_reference<Grid>::type::value_type;
        using TrainingOutput = typename std::result_of_t<TrainingFunction &&(
                typename std::remove_reference<Grid>::type::value_type, arma::mat, arma::vec)>;
        Partition partition(X.n_rows, numberOfPartitions);

        std::vector<arma::mat> tX(numberOfPartitions);
        std::vector<arma::mat> vX(numberOfPartitions);
        std::vector<arma::vec> ty(numberOfPartitions);
        std::vector<arma::vec> vy(numberOfPartitions);

        for (size_t i = 0; i < numberOfPartitions; ++i) {
            tX[i] = partition.getComplementPartitionMatrix(i, X);
            vX[i] = partition.getPartitionMatrix(i, X);
            ty[i] = partition.getComplementPartitionVector(i, y);
            vy[i] = partition.getPartitionVector(i, y);
        }

        struct ExtendedTrainingInput {
            const TrainingInput *trainingInput;
            std::vector<TrainingOutput> trainingOutputs;
        };

        std::vector<ExtendedTrainingInput> extendedGrid(grid.size());
        for (size_t j = 0; j < grid.size(); ++j) {
            extendedGrid[j].trainingInput = &grid[j];
        }

        struct Fn {
            const std::vector<arma::mat> *tX;
            const std::vector<arma::mat> *vX;
            const std::vector<arma::vec> *ty;
            const std::vector<arma::vec> *vy;
            size_t numberOfPartitions;
            ValidationFunction validationFunction;
            TrainingFunction trainingFunction;

            Fn(const std::vector<arma::mat> *tX, const std::vector<arma::mat> *vX, const std::vector<arma::vec> *ty,
               const std::vector<arma::vec> *vy, ValidationFunction &&validationFunction,
               TrainingFunction &&trainingFunction) :
                    tX(tX),
                    vX(vX),
                    ty(ty),
                    vy(vy),
                    validationFunction(std::forward<ValidationFunction>(validationFunction)),
                    trainingFunction(std::forward<TrainingFunction>(trainingFunction)) {
                numberOfPartitions = tX->size();
            };


            double operator()(ExtendedTrainingInput &extendedTrainingInput) {
                extendedTrainingInput.trainingOutputs.resize(numberOfPartitions);
                double sum = 0;
                for (size_t i = 0; i < numberOfPartitions; ++i) {
                    auto trainingOutput = trainingFunction(*(extendedTrainingInput.trainingInput), (*tX)[i], (*ty)[i]);
                    extendedTrainingInput.trainingOutputs[i] = trainingOutput;
                    sum += validationFunction(trainingOutput, (*vX)[i], (*vy)[i]);
                }

                return sum;
            };
        };

        Fn fn(&tX, &vX, &ty, &vy, std::forward<ValidationFunction>(validationFunction),
              std::forward<TrainingFunction>(trainingFunction));

        ExtendedTrainingInput bestExtendedTrainingInput = argGridSearch<std::vector<ExtendedTrainingInput>&, Fn&, ExtendedTrainingInput&>(
                numberOfThreads, extendedGrid, fn);
        *bestTrainingOutputs = bestExtendedTrainingInput.trainingOutputs;
        return *(bestExtendedTrainingInput.trainingInput);
    }

    template<class ValidationFunction, class TrainingFunction, class Grid>
    typename std::remove_reference<Grid>::type::value_type
    kFoldCrossValidationGridSearch(size_t numberOfThreads, const arma::mat &X, const arma::vec &y,
                                   size_t numberOfPartitions, const Grid &grid,
                                   ValidationFunction &&validationFunction, TrainingFunction &&trainingFunction) {
        using TrainingInput = typename std::remove_reference<Grid>::type::value_type;
        Partition partition(X.n_rows, numberOfPartitions);

        std::vector<arma::mat> tX(numberOfPartitions);
        std::vector<arma::mat> vX(numberOfPartitions);
        std::vector<arma::vec> ty(numberOfPartitions);
        std::vector<arma::vec> vy(numberOfPartitions);

        for (size_t i = 0; i < numberOfPartitions; ++i) {
            tX[i] = partition.getComplementPartitionMatrix(i, X);
            vX[i] = partition.getPartitionMatrix(i, X);
            ty[i] = partition.getComplementPartitionVector(i, y);
            vy[i] = partition.getPartitionVector(i, y);
        }

        struct Fn {
            const std::vector<arma::mat> *tX;
            const std::vector<arma::mat> *vX;
            const std::vector<arma::vec> *ty;
            const std::vector<arma::vec> *vy;
            size_t numberOfPartitions;
            ValidationFunction validationFunction;
            TrainingFunction trainingFunction;

            Fn(const std::vector<arma::mat> *tX, const std::vector<arma::mat> *vX, const std::vector<arma::vec> *ty,
               const std::vector<arma::vec> *vy, ValidationFunction &&validationFunction,
               TrainingFunction &&trainingFunction) :
                    tX(tX),
                    vX(vX),
                    ty(ty),
                    vy(vy),
                    validationFunction(std::forward<ValidationFunction>(validationFunction)),
                    trainingFunction(std::forward<TrainingFunction>(trainingFunction)) {
                numberOfPartitions = tX->size();
            };


            double operator()(const TrainingInput& trainingInput) {
                double sum = 0;
                for (size_t i = 0; i < numberOfPartitions; ++i) {
                    auto trainingOutput = trainingFunction(trainingInput, (*tX)[i], (*ty)[i]);
                    sum += validationFunction(trainingOutput, (*vX)[i], (*vy)[i]);
                }

                return sum;
            };
        };

        Fn fn(&tX, &vX, &ty, &vy, std::forward<ValidationFunction>(validationFunction),
              std::forward<TrainingFunction>(trainingFunction));

        return argGridSearch<const Grid&, Fn&, const TrainingInput&>(numberOfThreads, grid, fn);
    }
}

#endif //PIANO_KFOLDCROSSVALIDATION_H
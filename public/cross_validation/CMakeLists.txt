add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/partition)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/cross_validation)

add_library(cv INTERFACE)

target_link_libraries(cv
        INTERFACE
        cv::Partition
        cv::CrossValidation)
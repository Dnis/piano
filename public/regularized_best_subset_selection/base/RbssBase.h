//
// Created by dennis on 15.10.21.
//

#ifndef PIANO_RBSSBASE_H
#define PIANO_RBSSBASE_H

#include <LinearRegressionBase.h>
#include <LinearAlgebra.h>
#include <OptimizationSolution.h>
#include <Solution.h>
#include <TimeMeasurement.h>
#include <OptimizationSolutionBuilder.h>
#include <RunnerBase.h>
#include <TrainingFunctions.h>

namespace piano::rbss {
    template<class Training = cv::SubsetTraining>
    class Base : public RunnerBase<Training> {
    public:
        Base(const arma::mat &X, const arma::vec &y, const arma::vec &gamma, bool extractRidgeParameter = true) :
                RunnerBase<Training>(X, y, gamma, extractRidgeParameter) {}

        Solution run(const arma::vec& activationCosts) {
            TimeMeasurement measurement;
            measurement.start();
            Training training = this->getTraining();
            PartialSolution partialSolution = computePartialSolution(activationCosts);
            Solution solution = RunnerBase<Training>::buildSolutionFromPartialSolution(partialSolution, training);
            measurement.end();
            solution.completeRuntimeInSeconds = measurement.getDurationInSeconds();
            return solution;
        }

        Solution run(double activationCost) {
            TimeMeasurement measurement;
            measurement.start();
            Training training = this->getTraining();
            PartialSolution partialSolution = computePartialSolution(activationCost);
            Solution solution = RunnerBase<Training>::buildSolutionFromPartialSolution(partialSolution, training);
            measurement.end();
            solution.completeRuntimeInSeconds = measurement.getDurationInSeconds();
            return solution;
        }

        OptimizationSolution computeOptimizationSolution(const arma::vec& activationCosts) {
            PartialSolution partialSolution = computePartialSolution(activationCosts);
            return RunnerBase<Training>::buildOptimizationSolutionFromPartialSolution(partialSolution);
        }

        OptimizationSolution computeOptimizationSolution(double activationCost) {
            PartialSolution partialSolution = computePartialSolution(activationCost);
            return RunnerBase<Training>::buildOptimizationSolutionFromPartialSolution(partialSolution);
        }

        virtual PartialSolution computePartialSolution(const arma::vec& activationCosts) = 0;
        virtual PartialSolution computePartialSolution(double activationCost) {
            return computePartialSolution(arma::ones(this->p) * activationCost);
        }

        virtual ~Base() {}
    };
}

#endif //PIANO_RBSSBASE_H

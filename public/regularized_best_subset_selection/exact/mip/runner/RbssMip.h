// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by Dennis on 09.08.21.
//

#ifdef PIANO_MIP_SOLVER_AVAILABLE

#ifndef PIANO_RBSSMIPRUNNER_H
#define PIANO_RBSSMIPRUNNER_H

#include <RbssMipDefaultSolver.h>
#include <RbssNoWarmstart.h>
#include <MultiThreading.h>
#include <SolutionBuilder.h>
#include <RbssBase.h>
#include <memory>

namespace piano::rbss {
    template<
            class Warmstart = NoWarmstart,
            class Solver = piano::rbss::mip::solver::DefaultSolver
    >
    class Mip : public Base<cv::SubsetTraining> {
    public:
        Mip(const arma::mat &X, const arma::vec &y, const arma::vec &gamma) :
                Base<cv::SubsetTraining>(X, y, gamma, false) {
            solver = std::make_unique<Solver>(&originalX, &originalY, &originalGamma);
        }

        using TrainingFunction = cv::SubsetTraining;

        [[nodiscard]] TrainingFunction getTraining() const override {
            TrainingFunction trainingFunction;
            trainingFunction.gamma = &originalGamma;
            return trainingFunction;
        }

        PartialSolution computePartialSolution(const arma::vec &activationCosts) override {
            solver->timeLimit = timelimitInSeconds;
            solver->isMuted = isMuted;
            solver->setNumberOfThreads(numberOfThreads);
            solver->iterationLimit = iterationLimit;
            solver->linkedIndicators = linkedIndicators;
            solver->template runAndRegisterWarmstart<Warmstart>(activationCosts);
            solver->setNumericalScaling();
            solver->buildModel(activationCosts);
            return solver->solve(activationCosts);
        }

        void addWarmstartSolution(const Solution &sol) {
            solver->warmstartSolutions.push_back(sol);
        }

        void addWarmstartSolution(Solution &&sol) {
            solver->warmstartSolutions.push_back(std::move(sol));
        }

        void exportModel(const std::string &fileName) {
            if (fileName.empty()) {
                solver->exportModel = false;
            } else {
                solver->exportModel = true;
                solver->exportFile = fileName;
            }
        }

    private:
        std::unique_ptr<Solver> solver;
    };
}

#endif //PIANO_RBSSMIPRUNNER_H

#endif
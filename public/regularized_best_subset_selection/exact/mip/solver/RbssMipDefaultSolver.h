//
// Created by Dennis Kreber on 25.12.21.
//

#ifndef PIANO_RBSSMIPDEFAULTSOLVER_H
#define PIANO_RBSSMIPDEFAULTSOLVER_H

#include <cplex/RbssMipCplex.h>

namespace piano::rbss::mip::solver {
#ifdef PIANO_MIP_SOLVER_AVAILABLE
    using DefaultSolver = Cplex<>;
#endif
}

#endif //PIANO_RBSSMIPDEFAULTSOLVER_H

//
// Created by carina on 13.09.21.
//

#ifdef PIANO_USE_CPLEX

#ifndef PIANO_RBSSBIGMCONSTRAINTS_H
#define PIANO_RBSSBIGMCONSTRAINTS_H

#include <LinearAlgebra.h>
#include <MultiThreadSkeleton.h>
#include <CoefficientBounds.h>
#include <MultiThreading.h>
#include <exception>
#include <TypeConversion.h>

namespace piano::rbss::mip::indicator_constraints::cplex {
    template<class CoefficientBounds = piano::sls::CoefficientBounds<>, class UpperPredictedValueBound = piano::sls::upper_bound::predicted_value::DualCauchyBound>
    class BigMConstraints : public virtual piano::MultiThreadSkeleton {

    protected:
        BigMConstraints(const arma::mat *X, const arma::vec *y, const arma::vec *gamma) :
                X(X),
                y(y),
                gamma(gamma) {

        }

        void buildConstraints([[maybe_unused]] IloEnv &env, IloModel &model, const IloNumVarArray &beta,
                              const IloBoolVarArray &z) const {
            size_t p = X->n_cols;

            CoefficientBounds coefficientBounds(X, y, gamma, p);
            coefficientBounds.setNumberOfThreads(MultiThreading::handOverNumberOfThreads(numberOfThreads));

            assertCastable<long>(p, "p", "number of variables");
            for (long i = 0; i < static_cast<long>(p); i++) {
                double L = coefficientBounds.bound(i);
                model.add(beta[i] <= L * z[i]);
                model.add(beta[i] >= -L * z[i]);
            }
        }

    private:
        const arma::mat *X;
        const arma::vec *y;
        const arma::vec *gamma;
    };
}

#endif //PIANO_RBSSBIGMCONSTRAINTS_H
#endif

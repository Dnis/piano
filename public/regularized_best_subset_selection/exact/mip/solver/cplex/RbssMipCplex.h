// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by Dennis on 09.08.21.
//

#ifdef PIANO_USE_CPLEX

#ifndef PIANO_RBSSMIPCPLEX_H
#define PIANO_RBSSMIPCPLEX_H

#include <LinearAlgebra.h>
#include <Solution.h>
#include <vector>
#include <CplexHelper.h>
#include <list>
#include <SquaredL2.h>
#include <TimeMeasurement.h>
#include <LeastSquares.h>
#include <Conversion.h>
#include <CrossValidation.h>
#include <RbssNoWarmstart.h>
#include <cplex/RbssLogicalConstraints.h>
#include <cplex/RbssBigMConstraints.h>
#include <MultiThreadSkeleton.h>
#include <PartialSolutionBuilder.h>
#include <iostream>
#include <TypeConversion.h>

namespace piano::rbss::mip::solver {
    template<class IndicatorConstraintsHandler = indicator_constraints::cplex::BigMConstraints<>>
    class Cplex : virtual public MultiThreadSkeleton, IndicatorConstraintsHandler {
    private:
        constexpr static auto delIloEnv = cplex::delIloObject<IloEnv>;
        std::unique_ptr<IloEnv, decltype(delIloEnv)> env;

        constexpr static auto delIloModel = cplex::delIloObject<IloModel>;
        std::unique_ptr<IloModel, decltype(delIloModel)> model;

        std::unique_ptr<IloNumVarArray> beta;
        std::unique_ptr<IloBoolVarArray> z;

        constexpr static auto delIloCplex = cplex::delIloObject<IloCplex>;
        std::unique_ptr<IloCplex, decltype(delIloCplex)> cplex;

    public:
        const arma::mat *X;
        const arma::vec *y;
        const arma::vec *gamma;
        size_t n, p;
        int timeLimit = 360;
        size_t iterationLimit = 0;
        size_t numberOfThreads = 0;
        bool isMuted = false;
        std::list<Solution> warmstartSolutions;
        bool exportModel = false;
        std::string exportFile;
        double numericalScalar = 1e5;

        std::list<std::vector<size_t>> linkedIndicators{};

    public:
        Cplex(const arma::mat *X, const arma::vec *y, const arma::vec *gamma) :
                IndicatorConstraintsHandler(X, y, gamma),
                env(nullptr, delIloEnv),
                model(nullptr, delIloModel),
                cplex(nullptr, delIloCplex),
                X(X),
                y(y),
                gamma(gamma),
                n(X->n_rows),
                p(X->n_cols),
                warmstartSolutions{} {}

        void buildIndicatorLinkConstraints() {
            for (const auto &it: linkedIndicators) {
                long nLinkedZs = safeCastToLong(it.size(), "it.size()", "Number of linked zs");
                for (long i = 0; i < nLinkedZs; ++i) {
                    for (long j = i + 1; j < nLinkedZs; ++j) {
                        model->add((*z)[it[i]] == (*z)[it[j]]);
                    }
                }
            }
        }

        void buildModel(const arma::vec &activationCosts) {
            env.reset(new IloEnv);
            model.reset(new IloModel(*env));

            setupVariables();
            IndicatorConstraintsHandler::setNumberOfThreads(numberOfThreads);
            IndicatorConstraintsHandler::buildConstraints(*env, *model, *beta, *z);
            buildIndicatorLinkConstraints();
            setupObjectiveFunction(activationCosts);

            setupCplex();

            if (exportModel) cplex->exportModel(exportFile.c_str());
        }

        template<class Warmstart>
        void runAndRegisterWarmstart(const arma::vec &activationCosts) {
            if constexpr (!std::is_same_v<Warmstart, NoWarmstart>) {
                Warmstart warmstart(*X, *y, *gamma);
                warmstartSolutions.push_back(warmstart.run(activationCosts));
            } else {
                Solution sol;
                sol.subset = Subset(p);
                std::iota(sol.subset.begin(), sol.subset.end(), 0);
                warmstartSolutions.push_back(sol);
            }
        }

        void enableWarmstart(const Solution &warmstartSol) {
            arma::vec betaWarmstart;
            arma::uvec zWarmstart;
            extractWarmstartVariables(warmstartSol, &betaWarmstart, &zWarmstart);

            IloNumVarArray startVar(*env);
            IloNumArray startVal(*env);
            for (long j = 0; j < safeCastToLong(p, "p", "number of variables"); ++j) {
                startVar.add((*z)[j]);
                startVal.add(safeCastToInt(zWarmstart[j], "zWarmstart[j]", "warmstart"));
            }
            for (long i = 0; i < safeCastToLong(p, "p", "number of variables"); ++i) {
                startVar.add((*beta)[i]);
                startVal.add(betaWarmstart[i]);
            }


            cplex->addMIPStart(startVar, startVal, IloCplex::MIPStartCheckFeas);
            startVar.end();
            startVal.end();
        }

        void setNumericalScaling() {
            if (!warmstartSolutions.empty()) {
                sls::LeastSquares leastSquares(X, y, gamma);
                double bestError = leastSquares.compute(warmstartSolutions.front().subset).rss;
                for (const auto &item: warmstartSolutions) {
                    double error = leastSquares.compute(item.subset).rss;
                    if (error < bestError) bestError = error;
                }
                numericalScalar = 10.0 / bestError;
            }
        }

        [[nodiscard]] PartialSolution solve([[maybe_unused]] const arma::vec &activationCosts) {
            for (const auto &warmstartSol: warmstartSolutions) {
                enableWarmstart(warmstartSol);
            }


            TimeMeasurement measurement;
            bool solved = false;
            measurement.start();
            try {
                solved = static_cast<bool>(cplex->solve());
            } catch (const IloAlgorithm::Exception &e) {
                std::cout << e.getMessage() << "\n";
            }
            measurement.end();

            if (!solved) {
                IloAlgorithm::Status solStatus = cplex->getStatus();
                if (!isMuted) env->out() << std::endl << "Solution status: " << solStatus << std::endl;
                throw (-1);
            } else {
                IloAlgorithm::Status solStatus = cplex->getStatus();
                if (!isMuted) env->out() << std::endl << "Solution status: " << solStatus << std::endl;

                Subset optSubset = stools::toSubset(cplex::extractVars(*cplex, *z));

                PartialSolutionBuilder builder;
                builder.
                        objectiveValue(cplex->getObjValue()).
                        subset(optSubset).
                        provableOptimal(solStatus == IloAlgorithm::Optimal).
                        computationsRuntimeInSeconds(measurement.getDurationInSeconds()).
                        completeRuntimeInSeconds(0).
                        gap(cplex->getMIPRelativeGap()).
                        reachedTimelimit(solStatus != IloAlgorithm::Optimal);

                PartialSolution sol = builder.build();
                return sol;
            }
        }

    private:
        void setupVariables() {
            beta = std::make_unique<IloNumVarArray>(*env, p, -IloInfinity, IloInfinity);
            for (long i = 0; i < safeCastToLong(p, "p", "number of variables"); i++) {
                std::string beta_name_str = "beta_" + std::to_string(i);
                (*beta)[i].setName(beta_name_str.c_str());
            }

            z = std::make_unique<IloBoolVarArray>(*env, p);
            for (long i = 0; i < safeCastToLong(p, "p", "number of variables"); i++) {
                std::string x_name_str = "x_" + std::to_string(i);
                (*z)[i].setName(x_name_str.c_str());
            }
        }

        void setupObjectiveFunction(const arma::vec &activationCosts) {
            IloExpr obj(*env);

            obj += cplex::quadraticExpr(*env, numericalScalar * X->t() * (*X), (*beta));
            obj += cplex::quadraticExpr(*env, numericalScalar * arma::diagmat((*gamma)), (*beta));
            obj += cplex::dot(*env, arma::vec(-numericalScalar * 2 * X->t() * (*y)), (*beta));
            obj += numericalScalar * sls::squaredL2(*y);
            obj += cplex::dot(*env, arma::vec(numericalScalar * activationCosts), (*z));

            model->add(IloMinimize(*env, obj));
            obj.end();
        }

        void setupCplex() {
            cplex.reset(new IloCplex(*model));
            if (timeLimit != 0) cplex->setParam(IloCplex::Param::TimeLimit, timeLimit);
            if (numberOfThreads != 0) cplex->setParam(IloCplex::Param::Threads, numberOfThreads);
            if (iterationLimit != 0) cplex->setParam(IloCplex::Param::MIP::Limits::Nodes, iterationLimit);
            if (isMuted) {
                cplex->setOut(env->getNullStream());
                cplex->setParam(IloCplex::Param::MIP::Display, 0);
            } else {
                cplex->setParam(IloCplex::Param::MIP::Display, 3);
            }
        }

        void extractWarmstartVariables(const Solution &sol, arma::vec *betaWarmstart,
                                       arma::uvec *zWarmstart) const {
            *zWarmstart = stools::toIndicatorVector(sol.subset, p);

            arma::vec bW(p);
            sls::LeastSquares leastSquares(X, y, gamma);
            bW = leastSquares.compute(sol.subset).fullCoefficients();
            *betaWarmstart = bW;
        }
    };
}

#endif //PIANO_RBSSMIPCPLEX_H
#endif
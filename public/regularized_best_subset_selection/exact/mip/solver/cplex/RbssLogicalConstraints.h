// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by Dennis on 09.08.21.
//

#ifdef PIANO_USE_CPLEX

#ifndef PIANO_RBSSLOGICALCONSTRAINTS_H
#define PIANO_RBSSLOGICALCONSTRAINTS_H

#include <LinearAlgebra.h>
#include <MultiThreadSkeleton.h>
#include <ilconcert/iloenv.h>
#include <ilconcert/ilomodel.h>
#include <ilconcert/iloexpression.h>

namespace piano::rbss::mip::indicator_constraints::cplex {
    class LogicalConstraints : public virtual MultiThreadSkeleton {
    protected:
        LogicalConstraints(const arma::mat *X, [[maybe_unused]] const arma::vec *y, [[maybe_unused]] const arma::vec *gamma);

        void buildConstraints(IloEnv &env, IloModel &model, const IloNumVarArray &beta,
                              const IloBoolVarArray &z) const;

    private:
        size_t p;
    };
}

#endif //PIANO_RBSSLOGICALCONSTRAINTS_H
#endif
//
// Created by kreber on 03.08.21.
//

#ifndef PIANO_RBSSEXHAUSTIVESEARCH_H
#define PIANO_RBSSEXHAUSTIVESEARCH_H

#include <LinearAlgebra.h>
#include <Solution.h>
#include <TrainingFunctions.h>
#include <RbssBase.h>

namespace piano::rbss {
    class ExhaustiveSearch : public Base<cv::SubsetTraining> {
    public:
        ExhaustiveSearch(const arma::mat *X, const arma::vec *y, const arma::vec *gamma);

        PartialSolution computePartialSolution(const arma::vec &activationCosts) override;

        PartialSolution computePartialSolution(double activationCost) override;

        using TrainingFunction = cv::SubsetTraining;

        [[nodiscard]] TrainingFunction getTraining() const override;
    };
}


#endif //PIANO_RBSSEXHAUSTIVESEARCH_H

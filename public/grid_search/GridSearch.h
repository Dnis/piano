// Copyright 2021  Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

//
// Created by dennis on 10.03.20.
//

#ifndef GRIDSEARCH_H
#define GRIDSEARCH_H

#include <omp.h>

template<class T>
struct DefaultGridSearchCompare {
    bool operator()(const T &lhs, const T &rhs) const {
        return lhs < rhs;
    }
};

template<class Iter, class Func, class Compare, class FuncInput = typename std::remove_reference<Iter>::type::value_type>
typename std::remove_reference_t<Iter>::value_type
argGridSearch(size_t numberOfThreads, Iter &&iterable, Func &&function, Compare &&comp) {
    using FuncOutput = typename std::result_of_t<Func&&(FuncInput)>;
    std::vector<FuncOutput> results(numberOfThreads);
    std::vector<std::remove_const_t <std::remove_reference_t <FuncInput>>> inputs(numberOfThreads);

    for (size_t j = 0; j < numberOfThreads; ++j) {
        results[j] = function(iterable[j]);
        inputs[j] = iterable[j];
    }

    {
#pragma omp parallel num_threads(numberOfThreads)
        {
#pragma omp for
            for (size_t i = numberOfThreads; i < iterable.size(); ++i) {
                FuncOutput funcOutput = function(iterable[i]);
                int tid = omp_get_thread_num();
                if (comp(funcOutput, results[tid])) {
                    results[tid] = funcOutput;
                    inputs[tid] = iterable[i];
                }
            }
        }
    }


    int min_ind = std::min_element(results.begin(), results.end(), std::forward<Compare>(comp)) - results.begin();
    return inputs[min_ind];
}

template<class Iter, class Func, class FuncInput = typename std::remove_reference<Iter>::type::value_type>
typename std::remove_reference_t<Iter>::value_type
argGridSearch(size_t numberOfThreads, Iter &&iterable, Func &&function) {
    using FuncOutput = typename std::result_of_t<Func&&(FuncInput)>;
    return argGridSearch<Iter, Func, DefaultGridSearchCompare<FuncOutput>, FuncInput>(numberOfThreads,
                                                                                      std::forward<Iter>(iterable),
                                                                                      std::forward<Func>(function),
                                                                                      DefaultGridSearchCompare<FuncOutput>{});
}


template<class Iter, class Func, class Compare, class FuncInput = typename std::remove_reference<Iter>::type::value_type>
double gridSearch(size_t numberOfThreads, Iter &&iterable, Func &&function, Compare &&comp) {
    return function(argGridSearch<Iter, Func, Compare, FuncInput>(numberOfThreads, std::forward<Iter>(iterable),
                                                                  std::forward<Func>(function),
                                                                  std::forward<Compare>(comp)));
}

template<class Iter, class Func, class FuncInput = typename std::remove_reference<Iter>::type::value_type>
double gridSearch(size_t numberOfThreads, Iter &&iterable, Func &&function) {
    return function(argGridSearch<Iter, Func, FuncInput>(numberOfThreads, std::forward<Iter>(iterable),
                                                         std::forward<Func>(function)));
}

#endif //GRIDSEARCH_H

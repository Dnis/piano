.. piano documentation master file, created by
   sphinx-quickstart on Tue Jul 20 12:52:18 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to piano's documentation!
=================================

Introduction
------------

``piano`` is a C++ library implementing and providing algorithms for variable selection/feature selection in linear regression. Let us consider a linear regression problem, that is, we have data :math:`X \in \mathbb{R}^{n \times p}` and a response :math:`y \in \mathbb{R}^n`. We assume that there exists a linear relation between :math:`X` and :math:`y`, i.e., 

.. math::
    y = X\beta^0 + \epsilon

holds, where :math:`\epsilon \in \mathbb{R}^n` is unknown noise and :math:`\beta^0` are the *true* coefficients for the assumed linear relation. Without knowing :math:`\beta^0` and :math:`\epsilon` one can estimate them by solving the least squares problem

.. math:: \min_{\beta \in \mathbb{R}^p} \quad \lVert X \beta - y \rVert^2_2

With that we can predict a response with newly observed data. 
However, this approach quickly fails if :math:`\beta^0` has few non-zero entries. In other words, we have taken statistical variables :math:`X_i` into account which bear no relevance for the prediction. In that case, if we would simply compute a solution to the standard least squares problem we would overfit and hence produce a bad predictive model. ``piano`` provides methods which tackle this issue by either enforcing sparsity or by modifiying the objective function such that the validation error is minimized.

The library is divided into two major approaches:

* :doc:`Best subset selection <getting_started/what_is_best_subset_selection>`
* :doc:`Cross-validation best subset selection <getting_started/what_is_cross_validation_best_subset_selection>`


Getting Started
---------------

.. toctree::
    :maxdepth: 2
    :caption: Getting started:
    
    getting_started/installation
    getting_started/what_is_best_subset_selection
    getting_started/what_is_cross_validation_best_subset_selection  
    getting_started/examples/index

API Reference
-------------

.. toctree::
    :maxdepth: 2
    :caption: API Reference:
    
    api_reference/bss
    api_reference/namespaces
    api_reference/index
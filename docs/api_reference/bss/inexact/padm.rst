PADM
===

Defined in :doc:`BssPadm.h <padm_header>`.

.. doxygenclass:: piano::bss::Padm
    :project: piano
    :members:
    :undoc-members:
    :protected-members:
    :private-members:

Template parameter ``Variant``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. doxygenclass:: piano::bss::padm::variant::ADMSkeleton
    :project: piano


Template parameter ``StartParameterSearch``
"""""""""""""""""""""""""""""""""""""""""""

.. doxygenclass:: piano::bss::padm::StartParameterSearch
    :project: piano
    :members:
    :undoc-members:
    :protected-members:
    :private-members:

Penalty Series
""""""""""""""

.. doxygenclass:: piano::bss::padm::penalty_series::DoublingPenalty
    :project: piano
    :members:
    :undoc-members:
    :protected-members:
    :private-members:

First Direction Optimizer
"""""""""""""""""""""""""

.. doxygenclass:: piano::bss::padm::variant::L1Optimizer
    :project: piano
    :members:
    :undoc-members:
    :protected-members:
    :private-members:

.. doxygenclass:: piano::bss::padm::variant::L2Optimizer
    :project: piano
    :members:
    :undoc-members:
    :protected-members:
    :private-members:


.. toctree::
    :maxdepth: 2
    :caption: BssPadm.h:
    :hidden:

    padm_header
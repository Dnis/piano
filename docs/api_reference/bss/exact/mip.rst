Mip
===

Defined in :doc:`BssMip.h <mip_header>`.

.. doxygenclass:: piano::bss::Mip
    :project: piano
    :members:
    :undoc-members:
    :protected-members:
    :private-members:

Template parameter ``Solver``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. doxygenclass:: piano::bss::mip::solver::Cplex
    :project: piano
    :members:
    :undoc-members:
    :protected-members:
    :private-members:

Template parameter ``IndicatorConstraintsHandler``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. doxygenclass:: piano::bss::mip::indicator_constraints::cplex::LogicalConstraints
    :project: piano
    :members:
    :undoc-members:
    :protected-members:
    :private-members:

.. doxygenclass:: piano::bss::mip::indicator_constraints::cplex::BigMConstraints
    :project: piano
    :members:
    :undoc-members:
    :protected-members:
    :private-members:

.. toctree::
    :maxdepth: 2
    :caption: BssMip.h:
    :hidden:

    mip_header
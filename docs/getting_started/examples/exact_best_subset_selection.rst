Solving a best subset selection via a MIP solver (CPLEX)
========================================================

In this example we will solve a best subset selection problem via CPLEX. That means, when we let CPLEX find the optimal solution, it will be guaranteed to be an optimal solution. Since this can be difficult computation, you can also provide a time limit. However, then the solution is not guaranteed to be optimal.

Let us first include ``piano``:

.. code-block:: c++

    #include <piano.h>

Now we can create some linear regression data:

.. code-block:: c++

    // This generates data with 100 samples and 10 variables, of which 5 are true predictiors
    piano::SyntheticDataGenerator generator(100, 10, 5);

    // Set a seed
    generator.setSeed(4321);

    // Set the signal-to-noise ratio. Higher means less noise, lower means more noise (default = 1.0)
    generator.snr = 5;

    // Set the bounds for the true coefficients. They are uniformly drawn from the given interval.
    // In this case, we draw coefficients between 5 and 25.
    // Note that only 5 coefficients are drawn in this way. The rest are set to zero. 
    // Which subset of coefficients are non-zero is randomly sampled.
    generator.coefficients.lower_bound = 5.0;
    generator.coefficients.upper_bound = 25.0;

    // Draw the data
    piano::RegressionData data = generator.draw();

Before we hand the data over to the solver, we will first standardize the matrix and the response.

.. code-block:: c++

    // We normalize the data. The first two parameters are the matrix X and the response y. The third parameter
    // allows for a ridge regularization. In this case we set it to 0. After that we have to determine if we want
    // to normalize the data (yes) and if we want to have an intercept (no).
    piano::DataStandardization standardization(data.X, data.y, 0, true, false);

Now we can solve a best subset selection problem. 

.. code-block:: c++
    
    // Next, we create the solver using the data from the standardization.
    piano::bss::Mip mip(*(standardization.getX()), *(standardization.getY()), *(standardization.getGamma()));

    // Run the best subset selection solver and let it find a subset of size 5
    piano::Solution sol = mip.run(5);

    // Retransform the solution to the state before the standardization.
    piano::Solution rsol = standardization.recoverSolution(sol);

Next, let us inspect our found solution and how it compares with the true solution. Fortunately, since we generated synthetic data, we know the true predictors. In practise, it is advised to check the result on some test data. 

.. code-block:: c++

    // Compare the computed subset with the true subset
    std::cout << "Subset computed by best subset selection:     ";
    std::cout << piano::toPrettyString(rsol.subset, data.X.n_cols) << "\n";
    std::cout << "True Subset:                                  ";
    std::cout << piano::toPrettyString(data.subset, data.X.n_cols) << "\n\n";

    // Compare the computed coefficients with the true coefficients
    auto estimatedCoefs = rsol.fullCoefficients();
    std::printf("| Estimated | True      |\n");
    for (size_t i = 0; i < data.X.n_cols; ++i) {
        std::printf("| %9.4f | %9.4f |\n", estimatedCoefs[i], data.beta[i]);
    }

The results are as follows:

.. code-block::

    Subset computed by best subset selection:     ******#******####***
    True Subset:                                  ******#******####***
    
    | Estimated | True      |
    |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |
    |    6.2165 |    8.3365 |
    |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |
    |   15.0219 |   14.9279 |
    |   15.3037 |   15.5499 |
    |   14.9352 |   15.5266 |
    |   18.9306 |   20.6778 |
    |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |
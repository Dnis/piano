Solving a best subset selection via heuristics
==============================================

Solving a best subset selection problem exactly is computationally difficult. If the number of variables is a very it quickly becomes intractable to compute a globally optimal solution. Hence, it can make sense to utilize heuristics. Those methods do not guarantee a global optimality but often yield good results nevertheless. 

We first generate synthetic similarly as we did in :doc:`the previous example <exact_best_subset_selection>`.

.. code-block:: c++

    // First we generate some linear regression data with 100 observations, 30 variables, and 10 true predictors
    piano::SyntheticDataGenerator generator(100, 30, 10);
    generator.setSeed(476);
    piano::RegressionData data = generator.draw();

    // We normalize the data. The first two parameters are the matrix X and the response y. The third parameter
    // allows for a ridge regularization. In this case we set it to 0. After that we have to determine if we want
    // to normalize the data (yes) and if we want to have an intercept (no).
    piano::DataStandardization standardization(data.X, data.y, 0, true, false);

We next create 3 solvers: the exact MIP solver, the MaxMin solver and the PADM solver.

.. code-block:: c++

    // Next, we create different best subset selection solvers: MIP, MaxMin, and PADM. MIP finds a global optimal solution
    // while the other two are heuristics
    piano::bss::Mip mip(*(standardization.getX()), *(standardization.getY()), *(standardization.getGamma()));
    piano::bss::MaxMin maxMin(*(standardization.getX()), *(standardization.getY()), *(standardization.getGamma()));
    piano::bss::Padm padm(*(standardization.getX()), *(standardization.getY()), *(standardization.getGamma()));

    // We mute the solvers to reduce visual noise
    mip.muteSolver(true);
    maxMin.mute(true);
    padm.mute(true);

Next, we run all three solvers and unstandardize the solution.

.. code-block:: c++

    // Run the best subset selection solver and let it find a subset of size 5
    piano::Solution exactSol = mip.run(10);
    piano::Solution maxMinSol = maxMin.run(10);
    piano::Solution padmSol = padm.run(10);

    // Retransform the solution to the state before the standardization.
    piano::Solution exactRSol = standardization.recoverSolution(exactSol);
    piano::Solution maxMinRSol = standardization.recoverSolution(maxMinSol);
    piano::Solution padmRSol = standardization.recoverSolution(padmSol);

Finally, we can compare the solutions:

.. code-block:: c++
    
    // Compare the computed subset with the true subset
    std::cout << "True Subset:                   ";
    std::cout << piano::toPrettyString(data.subset, data.X.n_cols) << "\n";
    std::cout << "Subset computed by MIP:        ";
    std::cout << piano::toPrettyString(exactRSol.subset, data.X.n_cols) << "\n";
    std::cout << "Subset computed by MaxMin:     ";
    std::cout << piano::toPrettyString(maxMinRSol.subset, data.X.n_cols) << "\n";
    std::cout << "Subset computed by PADM:       ";
    std::cout << piano::toPrettyString(padmRSol.subset, data.X.n_cols) << "\n";

    // Compare the computed coefficients with the true coefficients
    auto mipEstimatedCoefs = exactRSol.fullCoefficients();
    auto maxMinEstimatedCoefs = maxMinRSol.fullCoefficients();
    auto padmEstimatedCoefs = padmRSol.fullCoefficients();
    std::printf("| MIP       | PADM      | MaxMin    | True      |\n");
    for (size_t i = 0; i < data.X.n_cols; ++i) {
        std::printf("| %9.4f | %9.4f | %9.4f | %9.4f |\n", mipEstimatedCoefs[i], admEstimatedCoefs[i],
                    maxMinEstimatedCoefs[i], data.beta[i]);
    }

and receive

.. code-block::

    True Subset:                   *#****#****#*#*#***##***##***#
    Subset computed by MIP:        ##****#****#***#***#***###***#
    Subset computed by MaxMin:     *#****#****#***#***#**####***#
    Subset computed by PADM:       *#****#****#***#***#**####***#
    | MIP       | PADM      | MaxMin    | True      |
    |   -2.5314 |    0.0000 |    0.0000 |    0.0000 |
    |    7.3802 |    7.5136 |    7.5136 |    8.0256 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |   13.8389 |   13.5184 |   13.5184 |    9.2113 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    9.1695 |    8.7697 |    8.7697 |    9.5633 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |    0.0000 |    2.1324 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    9.3957 |    8.6934 |    8.6934 |    9.2888 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    8.5158 |    8.2873 |    8.2873 |    6.0549 |
    |    0.0000 |    0.0000 |    0.0000 |    3.5705 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    0.0000 |    2.2495 |    2.2495 |    0.0000 |
    |   -2.7627 |   -2.5099 |   -2.5099 |    0.0000 |
    |    8.0796 |    7.7392 |    7.7392 |    6.3261 |
    |    5.3606 |    5.0643 |    5.0643 |    3.9979 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    0.0000 |    0.0000 |    0.0000 |    0.0000 |
    |    5.8612 |    5.8773 |    5.8773 |    5.0393 |


Examples
========

.. toctree::
   :hidden:
   :caption: Examples:
   
   exact_best_subset_selection
   inexact_best_subset_selection

What is best subset selection?
==============================

.. math::
    :nowrap:

    \begin{align*}
    \min_{\beta \in \mathbb{R}^p} \quad & \lVert X \beta - y \rVert^2_2 \\
    \text{s.t.} \quad & \lVert \beta \rVert_0 \leq k
    \end{align*}

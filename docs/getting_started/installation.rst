Installation of piano
=====================

You require some dependencies before installing ``piano``:

Depedencies
-----------

Build tools
^^^^^^^^^^^
* git
* CMake (>= 3.14)
* C++17 compatible compiler

MIP solvers and their respective C++ interfaces
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* IBM CPLEX

.. note::
    ``piano`` does not yet support solvers other than CPLEX. Furthermore, having CPLEX is mandatory at the moment but it is planned to drop the need for a MIP solver since many methods in ``piano`` do not
    require such a solver.

C++ libraries
^^^^^^^^^^^^^
Mandatory
"""""""""
* Armadillo_
* OpenMP
* OpenBLAS

Optional
""""""""
* MLPack_ (>= 3.4.2)

Optional but also included as submodules
""""""""""""""""""""""""""""""""""""""""
* PICASSO_
* googletest_
* googlebenchmark_

.. note::
    The lasso solvers of MLPack_ or PICASSO_ are used. The use of PICASSO_ is highly recommended as it is the fastest algorithm. If neither PICASSO_ nor MLPack_ 
    are available an internal lasso solver will be used. 
    However, the internal solver is vastly inferior to the solvers of PICASSO_ and MLPack_.

.. note::
    You do not have to install any libraries which are included as submodules. If you install the library they should be pulled and included automatically. 

Installation
------------
Execute those line in a terminal in the source folder of this project::

    mkdir build
    cd build
    cmake ..
    make -j
    sudo make install

Running Tests
-------------
In order to run tests you have to set ``PACKAGE_TESTS`` to ``ON`` for 
the cmake configuration. It is recommended to use ``ccmake`` for that task. When 
activating tests you need the following additional dependencies:

* Python 3

and the following Python packages:

* numpy
* sklearn

Then after calling ``make`` execute ``make test`` to run all tests.

Building the documentation
--------------------------
For generating the documentation you have to set ``BUILD_DOC`` to ``ON`` in the cmake configuration. 
Then, the documentation is build when ``make`` is called. You can also build the documentation seperately 
by first calling ``make Doxygen`` and then ``make Sphinx``. 

.. _Armadillo: http://arma.sourceforge.net/
.. _PICASSO: https://jasonge27.github.io/picasso/
.. _MLPack: https://www.mlpack.org/
.. _googletest: https://github.com/google/googletest
.. _googlebenchmark: https://github.com/google/benchmark





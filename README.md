# piano
## What is piano?
Piano is a C++ library, which provides various methods (exact and inexact) 
to compute sparse regression models. It provides two main approaches for finding 
sparse prediction models:
1. Best subset selection
    * [Best Subset Selection via a Modern Optimization
      Lens](https://arxiv.org/pdf/1507.03133.pdf)
    * [An Alternating Method for Cardinality-Constrained
      Optimization: A Computational Study for the Best
      Subset Selection and Sparse Portfolio Problem](http://www.optimization-online.org/DB_FILE/2020/11/8124.pdf)
    * [Cardinality-Constrained Discrete Optimization for Regression](https://ubt.opus.hbz-nrw.de/frontdoor/index/index/docId/1209)
    
2. Cross-validation best subset selection
    * [A mixed-integer optimization approach to an exhaustive
      cross-validated model selection for regression](http://www.optimization-online.org/DB_FILE/2019/05/7188.pdf)
    * [Best subset selection via cross-validation criterion](http://www.optimization-online.org/DB_FILE/2019/01/7028.pdf)
    * [Cardinality-Constrained Discrete Optimization for Regression](https://ubt.opus.hbz-nrw.de/frontdoor/index/index/docId/1209)

## Dependencies
### Recommended dependencies
* IBM CPLEX
* [Armadillo](http://arma.sourceforge.net/)
* OpenMP
* OpenBLAS

### Solvers (required for certain methods, but library also works without MIP solver)
* IBM CPLEX

### C++ libraries
#### Required
* OpenMP
* OpenBLAS

#### Optional
* [Armadillo](http://arma.sourceforge.net/) (if not installed, dependencies for Armadillo must be available)
* [MLPack](https://www.mlpack.org/) >= 3.4.2

### Build tools
* git
* cmake >= 3.14
* C++17 compatible compiler
    
## Installation
Execute those line in a terminal in the source folder of this project:

    mkdir build
    cd build
    cmake ..
    make -j
    sudo make install

## Running Tests
In order to run tests you have to set `PACKAGE_TESTS` to `ON` for 
the cmake configuration. It is recommended to use ccmake for that task. When 
activating tests you need the following additional dependencies:

* Python 3

and the following Python packages:

* numpy
* sklearn

Then after calling `make` execute `make test` to run all tests.

### ToDos
The library is a work in progress project. Hence, there are still a lot of
todos, namely:
* Documentation
* Examples
* Support for more MIP solvers (Gurobi, SCIP, ...)
* Add glmnet as a lasso solver 
    